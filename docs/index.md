# API Documentation

**Authentication:** Bearer (JWT)<br>
**Request Format:** `application/json`<br>
**Response Format:** `application/json`<br>
**JSONP Parameter:** `callback`<br>
**Date Format:** RFC 3339 (e.g. `2017-09-26T18:35:49+01:00`)<br>
**Non-Standard Response Headers:**

- `API-Version`
- `API-Error` *[optional]*
- `API-Debug` *[optional]*

**Boolean Query Parameters:**

- `true` or `on` or `1`
- `false` or `off` or `0`

**User Roles:**

- `ROLE_CLIENT`
- `ROLE_COACH`
- `ROLE_ENTERPRISE_CLIENT` (inherits `ROLE_CLIENT`)
- `ROLE_ENTERPRISE_ADMIN` (inherits `ROLE_ENTERPRISE_CLIENT`)
- `ROLE_LEAD_COACH` (inherits `ROLE_COACH`)
- `ROLE_ADMIN`
- `ROLE_SUPER`

**Credit Types:**

- `PRIVATE`
- `ENTERPRISE`
- `EXECUTIVE`
- `COMPLIMENTARY`

**Expertise Levels:**

- `CAN_COACH`
- `EXPERT`

**Session Types:**

- `DEFAULT` (30 minutes)
- `EXECUTIVE` (60 minutes)

**Session Status:**

- `BOOKED`
- `COMPLETED`
- `CANCELLED`
- `CLIENT_CANCELLED`
- `COACH_CANCELLED`
- `CLIENT_NOT_ATTENDED`
- `COACH_NOT_ATTENDED`

**Notification Events:**

- `DEFAULT`
- `SESSION_STARTED`

**Notification Channels:**

- `EMAIL_PRIMARY`
- `EMAIL_SECONDARY`
- `PUSH`
- `SMS`

---

**URL:** `/api`<br>
**Method:** `GET`<br>
**Authorization:** no<br>
**Response:**<br>

    {
        "app_auth_index": "/api/auth",
        "app_auth_refresh": "/api/auth/refresh",
        "app_password_index": "/api/password",
        "app_password_reset": "/api/password/reset",
        ...
    }
