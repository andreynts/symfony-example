# Auth User

## app_authuser_index
**URL:** `/api/auth/user`<br>
**Method:** `GET`<br>
**Authorization:** yes<br>
**Query Parameters:**

- `preview={boolean}`

**Response:**

    {
        "email": "alex.lokhman.ii@brandwidth.com",
        "first_name": "Alex",
        "last_name": "Lokhman II",
        "secondary_email": null,
        "mobile_number": "07397132233",
        "languages": ["en", "ru"],
        "age_range": "26-30",
        "gender": "Male",
        ...
    }
**Response (`preview=true`):**

    {
        "email": "alex.lokhman.ii@brandwidth.com",
        "roles": ["ROLE_CLIENT"],
        "first_name": "Alex",
        "last_name": "Lokhman II",
        "photo": null
    }
**Notes:** When `preview=false` endpoint will return user-specific data.

## app_authuser_update
**URL:** `/api/auth/user`<br>
**Method:** `PATCH`<br>
**Authorization:** yes<br>
**Request:**

    {
        "last_name": "Lokhman",
        "password": {
            "old": "Old password",
            "new": "New password"
        }
        ...
    }
**Response:**

    {
        "status": "ok"
    }
**Notes:** You should include *only* updated properties in request payload!

## app_authuser_statistics
**URL:** `/api/auth/user/statistics`<br>
**Method:** `GET`<br>
**Authorization:** yes<br>
**User Roles:** `ROLE_CLIENT`<br>
**Response:**

    {
        "sessions": {
            "booked": 1,
            "completed": 0,
            "feedbacks": 0
        },
        ...
    }

## app_authuser_company
**URL:** `/api/auth/user/company`<br>
**Method:** `GET`<br>
**Authorization:** yes<br>
**User Roles:** `ROLE_ENTERPRISE_CLIENT`<br>
**Response:**

    {
        "id": 1,
        "name": "Brandwidth Marketing Ltd",
        "logo": null,
        "website": "https://brandwidth.com/"
    }
