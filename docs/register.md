# Register

## app_register_index
**URL:** `/api/register`<br>
**Method:** `POST`<br>
**Authorization:** no<br>
**Query Parameters:**

- `token=eyJhbGciOiJSUzI1NiJ9.eyJyb2xlcyI6WyJST0xFX1BBU1NXT1J...`

**Request:**

    {
        "first_name": "Alex",
        "last_name": "Lokhman II",
        "photo": "https://thrive-partners.s3-eu-west-1.amazonaws.com/static/107bf4a/12c3042e/fNnShi8f.jpg",
        "email": "Alex.Lokhman.II@brandwidth.com",
        "secondary_email": null,
        "mobile_number": "+79500311699",
        "languages": ["en", "ru"],
        "age_range": "31-35",
        "gender": "Male",
        "country": "GB",
        "nationality": "Russian",
        "password": "Whatever1",
        "company_name": "Brandwidth Marketing Ltd",
        "department": "IT",
        "position": "Senior Software Engineer",
        "region": "Europe",
        "work_duration": "5 years",
        "attitude": "I have to be free to be me in whatever job I am doing.",
        "satisfied_company": 2,
        "recommend_company": 3,
        "future_at_company": 4,
        "future_team": 5,
        "future_company": 6,
        "future_industry": 7
    }
**Response:**

    {
        "status": "ok"
    }
**Notes:** Query parameter `token` will contain `"$company"` property if the user was invited as `ROLE_ENTERPRISE_CLIENT`.

## app_register_coach
**URL:** `/api/register/coach`<br>
**Method:** `POST`<br>
**Authorization:** no<br>
**Query Parameters:**

- `token=eyJhbGciOiJSUzI1NiJ9.eyJyb2xlcyI6WyJST0xFX1BBU1NXT1J...`

**Request:**

    {
        "first_name": "Alex",
        "last_name": "Lokhman II",
        "photo": "https://thrive-partners.s3-eu-west-1.amazonaws.com/static/107bf4a/12c3042e/fNnShi8f.jpg",
        "email": "Alex.Lokhman.II@brandwidth.com",
        "secondary_email": null,
        "mobile_number": "+79500311699",
        "languages": ["en", "ru"],
        "age_range": "31-35",
        "gender": "Male",
        "country": "GB",
        "nationality": "Russian",
        "password": "Whatever1",
        "postcode": "AB12 ABC",
        "address1": "Brandwidth Marketing Ltd",
        "address2": null,
        "city": "Windsor",
        "county": "Berkshire",
        "latitude": 12.345678,
        "longitude": -12.345678,
        "employment_status": "Employed",
        "company_name": "Brandwidth Marketing Ltd",
        "company_website": "https://brandwidth.com/",
        "company_type": null,
        "company_reg_number": null,
        "company_vat_reg_number": null,
        "company_indemnity_provider": null,
        "company_indemnity_cover": "£1 million",
        "other_companies": null,
        "qualifications": [
            { "name": "PhD", "awarded_at": "2017-09-23" }
        ],
        "other_relevant_qualifications": null,
        "other_qualifications": null,
        "tech_expertise": ["Word", "Excel"],
        "other_senior_position": "CTO",
        "corporate_week_time": 35,
        "career_sme_time": 75,
        "career_sme_roles": null,
        "career_corporate_time": 0,
        "career_corporate_roles": null,
        "career_companies": ["PepsiCo"],
        "career_industries": ["Business", "Marketing"],
        "coached_people": ["Senior Manager", "Manager"],
        "coached_companies": ["Business"],
        "coach_hours_in_year": null,
        "industries": [1, 2, 3],
        "expertise": [
            { "id": 1, "level": "CAN_COACH" },
            { "id": 2, "level": "EXPERT" }
        ],
        "about": "About goes here",
        "testimonial": "Testimonials go here"
    }
**Response:**

    {
        "status": "ok"
    }

## app_register_company
**URL:** `/api/register/company`<br>
**Method:** `POST`<br>
**Authorization:** no<br>
**Query Parameters:**

- `token=eyJhbGciOiJSUzI1NiJ9.eyJyb2xlcyI6WyJST0xFX1BBU1NXT1J...`

**Request:**

    {
        "name": "Brandwidth Marketing Ltd",
        "logo": null,
        "website": "https://brandwidth.com/",
        "address1": "7 High Street",
        "address2": null,
        "city": "Windsor",
        "county": "Berkshire",
        "postcode": "SL4 1LD",
        "country": "GB",
        "reg_number": "1234567",
        "vat_reg_number": "1234567",
        "regions": ["Windsor", "Holborn"],
        "departments": ["Production", "Studio"],
        "po_number": null
    }
**Response:**

    {
        "token": "eyJhbGciOiJSUzI1NiJ9.eyJyb2xlcyI6WyJST0xFX1BBU1NXT1J..."
    }
**Notes:** Value of `token` from the response should be passed to `app_register_index`
endpoint to continue registration of `ROLE_ENTERPRISE_ADMIN`.
