# Company

## app_company_index
**URL:** `/api/companies`<br>
**Method:** `GET`<br>
**Authorization:** yes<br>
**User Roles:** `ROLE_ADMIN`<br>
**Query Parameters:**

- `preview={boolean}`
- `is_enabled={boolean}`
- `offset=20`
- `limit=10`
- `sort[name]={ASC,DESC}`
- `sort[credit_enterprise]={ASC,DESC}`
- `sort[credit_executive]={ASC,DESC}`
- `sort[credit_complimentary]={ASC,DESC}`

**Response:**

    {
        "result": [
            {
                "id": 1,
                "name": "Brandwidth Marketing Ltd",
                "contact": {
                    "id": 1,
                    "first_name": "Alex",
                    "last_name": "Lokhman II"
                },
                "credit": {
                    "ENTERPRISE": 142,
                    "EXECUTIVE": 20,
                    "COMPLIMENTARY": 0
                }
            },
            ...
        ],
        "total": 23
    }
**Response (`preview=true`):**

    [
        {
            "id": 1,
            "name": "Brandwidth Marketing Ltd"
        },
        ...
    ]

## app_company_view
**URL:** `/api/companies/{id}`<br>
**Method:** `GET`<br>
**Authorization:** yes<br>
**User Roles:** `ROLE_ENTERPRISE_ADMIN, ROLE_ADMIN`<br>
**Response:**

    {
        "name": "Brandwidth Marketing Ltd",
        "logo": null,
        "website": "https://brandwidth.com/",
        "address1": "7 High Street",
        "address2": null,
        ...
    }

## app_company_update
**URL:** `/api/companies/{id}`<br>
**Method:** `PATCH`<br>
**Authorization:** yes<br>
**User Roles:** `ROLE_ENTERPRISE_ADMIN, ROLE_ADMIN`<br>
**Request:**

    {
        "name": "Brandwidth Marketing Ltd",
        "is_enabled": false
        ...
    }
**Response:**

    {
        "status": "ok"
    }
**Notes:** You should include *only* updated properties in request payload!

## app_company_delete
**URL:** `/api/companies/{id}`<br>
**Method:** `DELETE`<br>
**Authorization:** yes<br>
**User Roles:** `ROLE_ADMIN`<br>
**Response:**

    {
        "status": "ok"
    }

## app_company_credits
**URL:** `/api/companies/{id}/credits`<br>
**Method:** `GET`<br>
**Authorization:** yes<br>
**User Roles:** `ROLE_ADMIN`<br>
**Query Parameters:**

- `preview={boolean}`

**Response:**

    {
        "ENTERPRISE": [
            {
                "id": 32,
                "value": 77,
                "expires_at": "2018-10-16T00:00:00+00:00",
                "is_refund": false,
                "created_at": "2017-10-16T14:46:10+00:00"
            },
            ...
        },
        ...
    }

**Response (`preview=true`):**

    {
        "ENTERPRISE": 2,
        "EXECUTIVE": 7,
        "COMPLIMENTARY": 0
    }
