# Package

## app_package_index
**URL:** `/api/packages`<br>
**Method:** `GET`<br>
**Authorization:** yes<br>
**User Roles:** `ROLE_CLIENT`<br>
**Response:**

    {
        "PRIVATE": [
            {
                "id": 1,
                "price": "125.00",
                "currency": "GBP",
                "credit": 1
            },
            {
                "id": 2,
                "price": "625.00",
                "currency": "GBP",
                "credit": 5
            },
            ...
        ]
    }
**Notes:** Client applications need to use property `currency` in Stripe widget.

## app_package_view
**URL:** `/api/packages/{id}`<br>
**Method:** `GET`<br>
**Authorization:** yes<br>
**User Roles:** `ROLE_CLIENT`<br>
**Response:**

    {
        "price": "625.00",
        "currency": "GBP",
        "credit": 5
    }

## app_package_buy
**URL:** `/api/packages/{id}/buy`<br>
**Method:** `POST`<br>
**Authorization:** yes<br>
**User Roles:** `ROLE_CLIENT`<br>
**Request:**

    {
        "token": "tok_1BEdtBKssny1GsuHFqMRVBl9"
    }
**Response:**

    {
        "status": "ok"
    }
**Notes:** Client applications *should* collect billing information in payment widget.
