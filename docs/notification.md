# Notification

## app_notification_index
**URL:** `/api/notifications`<br>
**Method:** `GET`<br>
**Authorization:** yes<br>
**Response:**

    {
        "DEFAULT": {
            "channels": [
                "EMAIL_PRIMARY",
                "EMAIL_SECONDARY"
            ]
        },
        "SESSION_STARTED": {
            "reminder": 15,
            "channels": [
                "PUSH"
            ]
        },
        ...
    }

## app_notification_update
**URL:** `/api/notifications`<br>
**Method:** `PATCH`<br>
**Authorization:** yes<br>
**Request:**

    [
        {
            "event": "DEFAULT",
            "reminder": null,
            "channels": []
        },
        {
            "event": "SESSION_STARTED",
            "reminder": 15,
            "channels": [
                EMAIL_PRIMARY,
                PUSH
            ]
        },
        ...
    ]
**Response:**

    {
        "status": "ok"
    }
