# Var

## app_var_index
**URL:** `/api/vars`<br>
**Method:** `GET`<br>
**Authorization:** no<br>
**Response:**

    {
        "version": "1.0",
        "time": "2017-09-28T11:36:09.341+01:00"
    }

## app_var_errors
**URL:** `/api/vars/errors`<br>
**Method:** `GET`<br>
**Authorization:** no<br>
**Response:**<br>

    {
        "E_UNDEFINED": 1000,
        "E_BAD_REQUEST": 1001,
        "E_BAD_CREDENTIALS": 1002,
        "E_USER_NOT_FOUND": 1003,
        "E_USER_DISABLED": 1004,
        ...
    }

## app_var_countries
**URL:** `/api/vars/countries`<br>
**Method:** `GET`<br>
**Authorization:** no<br>
**Response:**<br>

    {
        "AF": "Afghanistan",
        "AX": "Åland Islands",
        "AL": "Albania",
        "DZ": "Algeria",
        "AS": "American Samoa",
        ...
    }

## app_var_languages
**URL:** `/api/vars/languages`<br>
**Method:** `GET`<br>
**Authorization:** no<br>
**Response:**<br>

    {
        "ab": "Abkhazian",
        "ace": "Achinese",
        "ach": "Acoli",
        "ada": "Adangme",
        "ady": "Adyghe",
        ...
    }

## app_var_nationality
**URL:** `/api/vars/nationality`<br>
**Method:** `GET`<br>
**Authorization:** no<br>
**Response:**<br>

    [
        "Afghan",
        "Albanian",
        "Algerian",
        "Andorran",
        "Angolan",
        ...
    ]

## app_var_gender
**URL:** `/api/vars/gender`<br>
**Method:** `GET`<br>
**Authorization:** no<br>
**Response:**<br>

    [
        "Male",
        "Female",
        "Trans",
        "Other",
        "Prefer not to say",
        ...
    ]

## app_var_agerange
**URL:** `/api/vars/age-range`<br>
**Method:** `GET`<br>
**Authorization:** no<br>
**Response:**<br>

    [
        "Under 20",
        "21-25",
        "26-30",
        "31-35",
        "36-40",
        ...
    ]

## app_var_industries
**URL:** `/api/vars/industries`<br>
**Method:** `GET`<br>
**Authorization:** no<br>
**Response:**<br>

    [
        {
            "id": 1,
            "name": "Administrative & Support Services"
        },
        {
            "id": 2,
            "name": "Advertising & Marketing"
        },
        {
            "id": 3,
            "name": "Aerospace, Defence & Security"
        },
        {
            "id": 4,
            "name": "Agriculture, forestry & fishing"
        },
        {
            "id": 5,
            "name": "Arts & Culture"
        },
        ...
    ]

## app_var_expertise
**URL:** `/api/vars/expertise`<br>
**Method:** `GET`<br>
**Authorization:** no<br>
**Response:**<br>

    [
        {
            "id": 1,
            "name": "Finance and Funding",
            "group": "SME",
            "is_public": true,
            "expertise": [
                {
                    "id": 1,
                    "name": "Accounting - Accounting and book-keeping"
                },
                {
                    "id": 2,
                    "name": "Accounting - Budgeting and forecasting"
                },
                {
                    "id": 3,
                    "name": "Accounting - Compliance"
                },
                {
                    "id": 4,
                    "name": "Accounting - Financial reporting"
                },
                {
                    "id": 5,
                    "name": "Accounting - Financial systems"
                },
                ...
            ]
        },
        ...
    ]

## app_var_stripe
**URL:** `/api/vars/stripe`<br>
**Method:** `GET`<br>
**Authorization:** no<br>
**Response:**<br>

    {
        "key": "pk_test_zU20MGQUk46LHqfwQ0u6cloW"
    }

## app_var_opentok
**URL:** `/api/vars/opentok`<br>
**Method:** `GET`<br>
**Authorization:** no<br>
**Response:**<br>

    {
        "key": "46049332"
    }

## app_var_clientregions
**URL:** `/api/vars/client/regions`<br>
**Method:** `GET`<br>
**Authorization:** no<br>
**Query Parameters:**

- `company=3`

**Response:**<br>

    [
        "Asia",
        "Middle East",
        "North Africa",
        "Sub-Saharan Africa",
        "Greater Arabia",
        ...
    ]
**Notes:** Query parameter `company` is an identity of the company.

## app_var_clientdepartments
**URL:** `/api/vars/client/departments`<br>
**Method:** `GET`<br>
**Authorization:** no<br>
**Query Parameters:**

- `company=3`

**Response:**<br>

    [
        "Accounts",
        "Communications",
        "Company Secretary",
        "Compliance",
        "Customer Relations",
        ...
    ]
**Notes:** Query parameter `company` is an identity of the company.

## app_var_clientworkduration
**URL:** `/api/vars/client/work-duration`<br>
**Method:** `GET`<br>
**Authorization:** no<br>
**Response:**<br>

    [
        "Less than 3 months",
        "3-6 months",
        "7-12 months",
        "1 year",
        "2 years",
        ...
    ]

## app_var_clientattitude
**URL:** `/api/vars/client/attitude`<br>
**Method:** `GET`<br>
**Authorization:** no<br>
**Response:**<br>

    [
        "I am on my way to the top and I am on the lookout for opportunities to get me there.",
        "My career is an adventure so I am open to new experiences and knowledge - wherever I find them.",
        "I want to learn as much as I can so I can do it for myself one day.",
        "I am happy doing my current job excellently; achieving a fulfilling work / life balance means more to me than promotions.",
        "I have to be free to be me in whatever job I am doing.",
        ...
    ]

## app_var_coachemploymentstatus
**URL:** `/api/vars/coach/employment-status`<br>
**Method:** `GET`<br>
**Authorization:** no<br>
**Response:**<br>

    [
        "Employed",
        "Self-employed",
        "Student",
        "Retired",
        ...
    ]

## app_var_coachcompanytypes
**URL:** `/api/vars/coach/company-types`<br>
**Method:** `GET`<br>
**Authorization:** no<br>
**Response:**<br>

    [
        "Limited Company",
        "Sole Trader",
        ...
    ]

## app_var_coachindemnitycover
**URL:** `/api/vars/coach/indemnity-cover`<br>
**Method:** `GET`<br>
**Authorization:** no<br>
**Response:**<br>

    [
        "£500K",
        "£1 million",
        "£2 million",
        "£3 million",
        "£4 million",
        "£5 million",
        "£5+ million",
        ...
    ]

## app_var_coachtechexpertise
**URL:** `/api/vars/coach/tech-expertise`<br>
**Method:** `GET`<br>
**Authorization:** no<br>
**Response:**<br>

    [
        "Microsoft Office 2010+",
        "Mac",
        "Video Chat (Skype/Zoom/Hangouts/Facetime)",
        "WhatsApp",
        ...
    ]

## app_var_coachseniorpositions
**URL:** `/api/vars/coach/senior-positions`<br>
**Method:** `GET`<br>
**Authorization:** no<br>
**Response:**<br>

    [
        "Executive board",
        "Board / Director",
        "Senior manager",
        "Manager",
        ...
    ]

## app_var_coachcareercompanies
**URL:** `/api/vars/coach/career-companies`<br>
**Method:** `GET`<br>
**Authorization:** no<br>
**Response:**<br>

    [
        "Local businesses near my home town/city",
        "Regional businesses",
        "Businesses with a presence across the UK",
        "International businesses",
        "FTSE100 businesses",
        ...
    ]

## app_var_coachcoachedpeople
**URL:** `/api/vars/coach/coached-people`<br>
**Method:** `GET`<br>
**Authorization:** no<br>
**Response:**<br>

    [
        "Executive board",
        "Board / Director",
        "Senior manager",
        "Manager",
        "Individual Contributor",
        ...
    ]

## app_var_coachcoachedcompanies
**URL:** `/api/vars/coach/coached-companies`<br>
**Method:** `GET`<br>
**Authorization:** no<br>
**Response:**<br>

    [
        "Local businesses near my home town/city",
        "Regional businesses",
        "Businesses with a presence across the UK",
        "International businesses",
        "FTSE100 businesses",
        ...
    ]

## app_var_sessionchangedview
**URL:** `/api/vars/session/changed-view`<br>
**Method:** `GET`<br>
**Authorization:** no<br>
**Response:**<br>

    [
        "Your company",
        "Your line manager",
        "Your colleagues",
        "Your ability to deal with an immediate or pressing problem",
        "Your capacity to influence those you need to",
        ...
    ]

## app_var_sessionputintopractice
**URL:** `/api/vars/session/put-into-practice`<br>
**Method:** `GET`<br>
**Authorization:** no<br>
**Response:**<br>

    [
        "Today",
        "This week",
        "This month",
        "More than a month",
        "Do not know"
    ]

## app_var_sessionreasons
**URL:** `/api/vars/session/reasons`<br>
**Method:** `GET`<br>
**Authorization:** no<br>
**Response:**<br>

    [
        "Thought partnership/sense check",
        "To step up (Personal Development)",
        "To help put other training/development into practice",
        "An issue with the company",
        "An issue with the colleagues",
        ...
    ]

## app_var_sessionclientrelationship
**URL:** `/api/vars/session/client-relationship`<br>
**Method:** `GET`<br>
**Authorization:** no<br>
**Response:**<br>

    [
        "Engaged and thriving",
        "Committed but under-supported",
        "Committed but overwhelmed",
        "Disengaged",
        "Languishing",
        ...
    ]
