# User

## app_user_index
**URL:** `/api/users`<br>
**Method:** `GET`<br>
**Authorization:** yes<br>
**User Roles:** `ROLE_ADMIN`<br>
**Query Parameters:**

- `is_verified={boolean}`
- `is_enabled={boolean}`
- `search=Alex`
- `offset=20`
- `limit=10`
- `sort[name]={ASC,DESC}`

**Response:**

    {
        "result": [
            {
                "id": 2,
                "email": "alex.lokhman.ii@brandwidth.com",
                "roles": ["ROLE_CLIENT"],
                "first_name": "Alex",
                "last_name": "Lokhman II",
                "photo": null,
                "is_verified": true,
                "last_auth_at": "2017-10-27T12:34:57+00:00",
                "created_at": "2017-10-09T10:44:39+00:00"
            },
            ...
        ],
        "total": 23
    }

## app_user_view
**URL:** `/api/users/{id}`<br>
**Method:** `GET`<br>
**Authorization:** yes<br>
**User Roles:** `ROLE_ADMIN`<br>
**Response:**

    {
        "email": "alex.lokhman.iv@brandwidth.com",
        "roles": ["ROLE_CLIENT"],
        "first_name": "Alex",
        "last_name": "Lokhman II",
        "photo": null,
        "is_verified": true,
        "is_enabled": true,
        "last_auth_at": "2017-10-27T12:34:57+00:00",
        "created_at": "2017-10-09T10:44:39+00:00"
`   }

## app_user_update
**URL:** `/api/users/{id}`<br>
**Method:** `PATCH`<br>
**Authorization:** yes<br>
**User Roles:** `ROLE_ADMIN`<br>
**Request:**

    {
        "last_name": "Lokhman",
        "password": "New password",
        "is_verified": false,
        "is_enabled": true,
        ...
    }
**Response:**

    {
        "status": "ok"
    }
**Notes:** You should include *only* updated properties in request payload!

## app_user_delete
**URL:** `/api/users/{id}`<br>
**Method:** `DELETE`<br>
**Authorization:** yes<br>
**User Roles:** `ROLE_ADMIN`<br>
**Response:**

    {
        "status": "ok"
    }
