# Invite

## app_invite_index
**URL:** `/api/invite`<br>
**Method:** `POST`<br>
**Authorization:** yes<br>
**User Roles:** `ROLE_ENTERPRISE_ADMIN, ROLE_ADMIN`<br>
**Request:**

    {
        "email": "alex.lokhman.ii@brandwidth.com",
        "first_name": "Alex",
        "last_name": "Lokhman II",
        "company": 2
    }
**Response:**

    {
        "status": "ok"
    }
**Notes:** Request property `company` is not required for `ROLE_ENTERPRISE_ADMIN`.

## app_invite_coach
**URL:** `/api/invite/coach`<br>
**Method:** `POST`<br>
**Authorization:** yes<br>
**User Roles:** `ROLE_ADMIN`<br>
**Request:**

    {
        "email": "alex.lokhman.ii@brandwidth.com",
        "first_name": "Alex",
        "last_name": "Lokhman II"
    }
**Response:**

    {
        "status": "ok"
    }

## app_invite_company
**URL:** `/api/invite/company`<br>
**Method:** `POST`<br>
**Authorization:** yes<br>
**User Roles:** `ROLE_ADMIN`<br>
**Request:**

    {
        "email": "alex.lokhman.ii@brandwidth.com",
        "first_name": "Alex",
        "last_name": "Lokhman II",
        "company_name": "Brandwidth Marketing Ltd"
    }
**Response:**

    {
        "status": "ok"
    }

## app_invite_import
**URL:** `/api/invite`<br>
**Method:** `PUT`<br>
**Authorization:** yes<br>
**User Roles:** `ROLE_ENTERPRISE_ADMIN, ROLE_ADMIN`<br>
**Request Headers:**

- `Content-Type: text/csv`

**Request:**

    <binary data>
**Response:**

    {
        "invited": 15,
        "failed": [
            "alex.lokhman.ii@brandwidth.com"
        ]
    }
**Notes:** Imported CSV file should be comma-separated and contain the following header:
`Email,"First Name","Last Name"`.

## app_invite_importto
**URL:** `/api/invite/{id}`<br>
**Method:** `PUT`<br>
**Authorization:** yes<br>
**User Roles:** `ROLE_ADMIN`<br>
**Request Headers:**

- `Content-Type: text/csv`

**Request:**

    <binary data>
**Response:**

    {
        "invited": 15,
        "failed": [
            "alex.lokhman.ii@brandwidth.com"
        ]
    }
**Notes:** Imported CSV file should be comma-separated and contain the following header:
`Email,"First Name","Last Name"`.
