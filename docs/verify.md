# Verify

## app_verify_index
**URL:** `/api/verify`<br>
**Method:** `POST`<br>
**Authorization:** no<br>
**Request:**

    {
        "token": "eyJhbGciOiJSUzI1NiJ9.eyJyb2xlcyI6WyJST0xFX1BBU1NXT1J..."
    }
**Response:**

    {
        "status": "ok"
    }

## app_verify_repeat
**URL:** `/api/verify/repeat`<br>
**Method:** `POST`<br>
**Authorization:** no<br>
**Request:**

    {
        "username": "alex.lokhman.ii@brandwidth.com"
    }
**Response:**

    {
        "status": "ok"
    }
