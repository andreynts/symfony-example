# Session

## app_session_index
**URL:** `/api/sessions`<br>
**Method:** `GET`<br>
**Authorization:** yes<br>
**User Roles:** `ROLE_ADMIN`<br>
**Query Parameters:**

- `type=EXECUTIVE`
- `expertise=3`
- `client=2`
- `company=1`
- `coach=5`
- `status=COMPLETED`
- `search=Alex`
- `offset=20`
- `limit=10`
- `sort[type]={ASC,DESC}`
- `sort[started_at]={ASC,DESC}`
- `sort[client_name]={ASC,DESC}`
- `sort[client_company_name]={ASC,DESC}`
- `sort[coach_name]={ASC,DESC}`
- `sort[status]={ASC,DESC}`

**Response:**

    {
        "result": [
            {
                "id": 20,
                "type": "DEFAULT",
                "started_at": "2017-10-10T19:00:00+00:00",
                "ended_at": "2017-10-10T19:30:00+00:00",
                "status": "COACH_NOT_ATTENDED",
                "expertise": {
                  "id": 207,
                  "name": "Sales & Marketing - Strategy"
                },
                "client": {
                  "id": 1,
                  "first_name": "Alex",
                  "last_name": "Lokhman II",
                  "company": {
                    "id": 1,
                    "name": "Brandwidth Marketing Ltd"
                  }
                },
                "coach": {
                  "id": 2,
                  "first_name": "Chris",
                  "last_name": "Rogers"
                }
            },
            ...
        ],
        "total": 23
    }

## app_session_calendar
**URL:** `/api/sessions/calendar{?format}`<br>
**Method:** `GET`<br>
**Authorization:** yes<br>
**User Roles:** `ROLE_CLIENT, ROLE_COACH`<br>
**Query Parameters:**

- `date[start]=2017-10-01T00:00:00+00:00`
- `date[end]=2017-10-31T23:59:59+00:00`
- `sessions[]=23`

**Response:**

    [
        {
            "id": 1,
            "expertise": 23,
            "started_at": "2017-10-08T16:30:07+00:00",
            "ended_at": "2017-10-08T18:30:09+00:00",
            "status": "BOOKED",
            "rescheduled": 2,
            "room_key": "YRhtJluUN16rJtR127eX6b8qX96v3M597i4j",
            "tok_session": "1_MX40NjA0OTMzMn5-MTUxNzMyOTAwNDYwM35uUXlUOUgyVjRqUGVETlFWeG9SK1g1M2J-QX4",
            "tok_token": "T1==cGFydG5lcl9pZD00NjA0OTMzMiZzaWc9NWM0MDYxMGM0ZjU1Mjg2YzcwYzEzMjBiMjdl...",
            "feedback": true,
            "client": {
                "id": 1,
                "first_name": "Alex",
                "last_name": "Lokhman II",
                "company": {
                    "name": "Brandwidth Marketing Ltd",
                    "website": "https://brandwidth.com/"
                }
            }
        },
        ...
    ]
**Notes:** User `ROLE_CLIENT` will receive `coach` payload property instead of `client`.

## app_session_create
**URL:** `/api/sessions`<br>
**Method:** `POST`<br>
**Authorization:** yes<br>
**User Roles:** `ROLE_CLIENT`<br>
**Request:**

    {
        "type": "DEFAULT",
        "availability": 76,
        "started_at": "2017-10-09T21:45:00+00:00",
        "expertise": 2,
        "credit_type": "PRIVATE",
        "details": "Details go here",
        "background": "Background goes here",
        "extra_details": "No",
        "attachments": ["https://thrive-partners.s3-eu-west-1.amazonaws.com/static/107bf4a/12c3042e/fNnShi8f.pdf"]
    }
**Response:**

    {
        "id": 23
    }

## app_session_view
**URL:** `/api/sessions/{id}`<br>
**Method:** `GET`<br>
**Authorization:** yes<br>
**User Roles:** `ROLE_CLIENT, ROLE_COACH, ROLE_ADMIN`<br>
**Query Parameters:**

- `status={boolean}`

**Response:**

    {
        "type": "DEFAULT",
        "expertise": 2,
        "started_at": "2017-10-24T12:15:00+00:00",
        "ended_at": "2017-10-24T12:45:00+00:00",
        "credit_type": "PRIVATE",
        "details": "Details go here",
        "background": "Background goes here",
        "extra_details": "No",
        "attachments": ["https://thrive-partners.s3-eu-west-1.amazonaws.com/static/107bf4a/12c3042e/fNnShi8f.pdf"],
        "status": "BOOKED",
        "rescheduled": 2,
        "created_at": "2017-10-11T10:43:45+00:00",
        "room_key": "3M5joJFu9WnVDud5g6gSaDgZ9bfz9pRh",
        "tok_session": "1_MX40NjA0OTMzMn5-MTUxNzMyOTAwNDYwM35uUXlUOUgyVjRqUGVETlFWeG9SK1g1M2J-QX4",
        "tok_token": "T1==cGFydG5lcl9pZD00NjA0OTMzMiZzaWc9NWM0MDYxMGM0ZjU1Mjg2YzcwYzEzMjBiMjdl...",
        "feedback": null,
        "notes": "Notes go here",
        "client": {
            "id": 1,
            "first_name": "Alex",
            "last_name": "Lokhman II",
            "photo": "https://thrive-partners.s3-eu-west-1.amazonaws.com/static/107bf4a/12c3042e/fNnShi8f.jpg",
            "company": {
                "name": "Brandwidth Marketing Ltd",
                "website": "https://brandwidth.com/",
                "brief": null
            }
        }
    }
**Notes:** User `ROLE_CLIENT` will receive `coach` payload property instead of `client`.
Property `feedback` for `ROLE_COACH` will contain individual feedback objects (or `false`) for client and coach.

## app_session_update
**URL:** `/api/sessions/{id}`<br>
**Method:** `PATCH`<br>
**Authorization:** yes<br>
**User Roles:** `ROLE_CLIENT, ROLE_COACH`<br>
**Request:**

    {
        "attachments": ["https://thrive-partners.s3-eu-west-1.amazonaws.com/static/107bf4a/12c3042e/fNnShi8f.pdf"]
    }
**Response:**

    {
        "status": "ok"
    }

## app_session_delete
**URL:** `/api/sessions/{id}`<br>
**Method:** `DELETE`<br>
**Authorization:** yes<br>
**User Roles:** `ROLE_ADMIN`<br>
**Response:**

    {
        "status": "ok"
    }

## app_session_start
**URL:** `/api/session/{id}/start`<br>
**Method:** `POST`<br>
**Authorization:** yes<br>
**User Roles:** `ROLE_CLIENT, ROLE_COACH`<br>
**Response:**

    {
        "status": "ok"
    }

## app_session_complete
**URL:** `/api/session/{id}/complete`<br>
**Method:** `POST`<br>
**Authorization:** yes<br>
**User Roles:** `ROLE_COACH`<br>
**Response:**

    {
        "status": "ok"
    }

## app_session_abandon
**URL:** `/api/session/{id}/abandon`<br>
**Method:** `POST`<br>
**Authorization:** yes<br>
**User Roles:** `ROLE_CLIENT, ROLE_COACH`<br>
**Response:**

    {
        "status": "ok"
    }

## app_session_cancel
**URL:** `/api/session/{id}/cancel`<br>
**Method:** `POST`<br>
**Authorization:** yes<br>
**User Roles:** `ROLE_CLIENT, ROLE_COACH, ROLE_ADMIN`<br>
**Response:**

    {
        "status": "ok"
    }

## app_session_reschedule
**URL:** `/api/sessions/{id}/reschedule`<br>
**Method:** `POST`<br>
**Authorization:** yes<br>
**User Roles:** `ROLE_CLIENT`<br>
**Request:**

    {
        "availability": 76,
        "started_at": "2017-10-09T21:45:00+00:00"
    }
**Response:**

    {
        "status": "ok"
    }

## app_session_prompt
**URL:** `/api/session/{id}/prompt`<br>
**Method:** `POST`<br>
**Authorization:** yes<br>
**User Roles:** `ROLE_CLIENT, ROLE_COACH`<br>
**Response:**

    {
        "status": "ok"
    }

## app_session_notes
**URL:** `/api/sessions/{id}/notes`<br>
**Method:** `POST`<br>
**Authorization:** yes<br>
**User Roles:** `ROLE_CLIENT, ROLE_COACH`<br>
**Request:**

    {
        "notes": "New notes",
    }
**Response:**

    {
        "status": "ok"
    }

## app_session_feedback
**URL:** `/api/sessions/{id}/feedback`<br>
**Method:** `POST`<br>
**Authorization:** yes<br>
**User Roles:** `ROLE_CLIENT, ROLE_COACH`<br>
**Client Request:**

    {
        "satisfied_session": 3,
        "satisfied_coach": 4,
        "recommend_coaching": 5,
        "put_into_practice": "This week",
        "changed_view": ["Your colleagues", "Your job satisfaction"]
    }
**Coach Request:**

    {
        "reason": "A personal issue",
        "client_relationship": "Languishing",
        "what_right": "Something",
        "key_people": "Some text",
        "key_processes": "Some text",
        "key_communication": "Some text",
        "advice": "Some text",
        "feel_about_company": ["Word 1", "Word 2", "Word 3"],
        "fix_first": "Some text",
        "topics": "Some text",
        "shared_tools": "Some text",
        "next_steps": "Some text"
    }
**Response:**

    {
        "status": "ok"
    }

## app_session_survey
**URL:** `/api/sessions/survey`<br>
**Method:** `POST`<br>
**Authorization:** yes<br>
**User Roles:** `ROLE_CLIENT`<br>
**Request:**

    {
        "has_success": false,
        "did_recommend": true
    }
**Response:**

    {
        "status": "ok"
    }
