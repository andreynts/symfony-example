# Payment

## app_payment_index
**URL:** `/api/payments`<br>
**Method:** `GET`<br>
**Authorization:** yes<br>
**User Roles:** `ROLE_CLIENT`<br>
**Query Parameters:**

- `offset=20`
- `limit=10`
- `sort[created_at]={ASC,DESC}`

**Response:**

    {
        "result": [
            {
                "id": "0000000011",
                "amount": "6250.00",
                "currency": "GBP",
                "invoice": "https://thrive-partners.s3-eu-west-1.amazonaws.com/static/107dab5/2ecd0609/INV0000000011.pdf",
                "items": [
                  {
                    "name": "50 private credits",
                    "amount": "6250.00"
                  }
                ],
                "created_at": "2017-10-19T21:58:17+00:00"

            },
            ...
        ],
        "total": 2
    }
