# Availability

## app_availability_index
**URL:** `/api/availability`<br>
**Method:** `GET`<br>
**Authorization:** yes<br>
**User Roles:** `ROLE_COACH`<br>
**Query Parameters:**

- `date[start]=2017-10-01T00:00:00+00:00`
- `date[end]=2017-10-31T23:59:59+00:00`

**Response:**

    [
        {
            "id": 1,
            "started_at": "2017-09-30T23:45:00+00:00",
            "ended_at": "2017-10-01T01:00:00+00:00"
        },
        {
            "id": 2,
            "started_at": "2017-10-05T12:00:00+00:00",
            "ended_at": "2017-10-05T14:15:00+00:00"
        },
        ...
    ]

## app_availability_create
**URL:** `/api/availability`<br>
**Method:** `POST`<br>
**Authorization:** yes<br>
**User Roles:** `ROLE_COACH`<br>
**Request:**

    {
        "started_at": "2017-09-23T20:15:00+00:00",
        "ended_at": "2017-09-23T20:45:00+00:00"
    }
**Response:**

    {
        "id": 125
    }

## app_availability_view
**URL:** `/api/availability/{id}`<br>
**Method:** `GET`<br>
**Authorization:** yes<br>
**User Roles:** `ROLE_COACH`<br>
**Response:**

    {
        "started_at": "2017-09-30T23:45:00+00:00",
        "ended_at": "2017-10-01T01:00:00+00:00"
    }

## app_availability_update
**URL:** `/api/availability/{id}`<br>
**Method:** `PATCH`<br>
**Authorization:** yes<br>
**User Roles:** `ROLE_COACH`<br>
**Request:**

    {
        "started_at": "2017-09-23T20:15:00+00:00",
        "ended_at": "2017-09-23T20:45:00+00:00"
    }
**Response:**

    {
        "status": "ok"
    }
**Notes:** You should include *only* updated properties in request payload!

## app_availability_delete
**URL:** `/api/availability/{id}`<br>
**Method:** `DELETE`<br>
**Authorization:** yes<br>
**User Roles:** `ROLE_COACH`<br>
**Response:**

    {
        "status": "ok"
    }
