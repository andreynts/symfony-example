# Password

## app_password_index
**URL:** `/api/password`<br>
**Method:** `POST`<br>
**Authorization:** no<br>
**Request:**

    {
        "username": "alex.lokhman.ii@brandwidth.com"
    }
**Response:**

    {
        "status": "ok"
    }

## app_password_reset
**URL:** `/api/password/reset`<br>
**Method:** `POST`<br>
**Authorization:** no<br>
**Request:**

    {
        "token": "eyJhbGciOiJSUzI1NiJ9.eyJyb2xlcyI6WyJST0xFX1BBU1NXT1J...",
        "password": "Whatever1"
    }
**Response:**

    {
        "status": "ok"
    }
