# Upload

## app_upload_index
**URL:** `/api/upload`<br>
**Method:** `PUT`<br>
**Authorization:** yes<br>
**Request Headers:**

- `Content-Type: application/zip`
- `Name: my-uploaded-file.zip` *[optional]*

**Request:**

    <binary data>
**Response:**

    {
        "url": "https://thrive-partners.s3-eu-west-1.amazonaws.com/static/107bf4a/12c3042e/my-uploaded-file.zip"
    }

## app_upload_userphoto
**URL:** `/api/upload/user-photo`<br>
**Method:** `PUT`<br>
**Authorization:** optional<br>
**Request Headers:**

- `Content-Type: image/png`
- `Name: my-uploaded-photo.png` *[optional]*

**Request:**

    <binary data>
**Response:**

    {
        "url": "https://thrive-partners.s3-eu-west-1.amazonaws.com/static/107bf4a/12c3042e/fNnShi8f.jpg"
    }
**Notes:** Photo will be cropped and resized to the required resolution. System will
automatically generate additional `@2x` and `@3x` images with corresponding suffix.

## app_upload_companylogo
**URL:** `/api/upload/company-logo`<br>
**Method:** `PUT`<br>
**Authorization:** optional<br>
**Request Headers:**

- `Content-Type: image/png`
- `Name: my-uploaded-photo.png` *[optional]*

**Request:**

    <binary data>
**Response:**

    {
        "url": "https://thrive-partners.s3-eu-west-1.amazonaws.com/static/107bf4a/12c3042e/fNnShi8f.jpg"
    }
**Notes:** See notes for `app_upload_userphoto` endpoint.
