<?php

namespace App\Repository;

use Doctrine\ORM\EntityRepository;
use App\Entity\ClientCredit;

class CompanyCreditAllocationRepository extends EntityRepository
{
    public function getCreditByClient($client, array $types)
    {
        $credits = $this->createQueryBuilder('c')
            ->select('ct.type', 'SUM(c.value)')
            ->innerJoin('App:CreditType', 'ct', 'WITH', 'c.creditType = ct.id')
            ->where('c.client = :client AND ct.type IN (:types)')
            ->andWhere('c.value > 0 AND c.expiresAt >= CURRENT_DATE()')
            ->setParameter('client', $client)
            ->setParameter('types', $types)
            ->groupBy('ct.type')
            ->getQuery()
            ->getResult('HYDRATE_KEY_PAIR');

        return array_replace(array_fill_keys($types, 0), $credits);
    }

    public function getCreditByCompany($company, array $types)
    {
        $credits = $this->createQueryBuilder('c')
            ->select('ct.type', 'SUM(c.value)')
            ->innerJoin('App:CreditType', 'ct', 'WITH', 'c.creditType = ct.id')
            ->where('c.company = :company AND ct.type IN (:types)')
            ->andWhere('c.value > 0 AND c.expiresAt >= CURRENT_DATE()')
            ->setParameter('client', $company)
            ->setParameter('types', $types)
            ->groupBy('ct.type')
            ->getQuery()
            ->getResult('HYDRATE_KEY_PAIR');

        return array_replace(array_fill_keys($types, 0), $credits);
    }

    public function getTotalAvailableCreditByClientId($id)
    {
        $credits = $this->createQueryBuilder('cca')
            ->select('c.id', 'SUM(cca.value) as available')
            ->leftJoin('App:Client', 'c', 'WITH', 'cca.client = c')
            ->where('c.id = :id')
            ->andWhere('cca.value > 0')
            ->setParameter('id', $id)
            ->groupBy('c.id')
            ->getQuery()
            ->getOneOrNullResult();

        $available = 0;
        if ($credits !== null) {
            $available = $credits['available'];
        }
        return $available;
    }
}