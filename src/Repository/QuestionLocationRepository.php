<?php

namespace App\Repository;

use Doctrine\ORM\EntityRepository;

class QuestionLocationRepository extends EntityRepository
{
    public function checkExistByValue($label)
    {
        return $this->createQueryBuilder('cl')
                ->select('COUNT(cl)')
                ->where('cl.value = :value')
                ->setParameter('value', $label)
                ->getQuery()
                ->getSingleScalarResult() > 0;
    }

    public function findLocationByValue(string $value)
    {
        return $this->createQueryBuilder('ql')
            ->select('ql')
            ->where('ql.value = :value')
            ->setParameter('value', $value)
            ->getQuery()
            ->getOneOrNullResult();
    }
}