<?php


namespace App\Repository;

use App\Entity\StyleType;
use Doctrine\ORM\EntityRepository;

class StyleRepository extends EntityRepository
{
    public function findPartialByName(string $name)
    {
        return $this->createQueryBuilder('s')
            ->select('PARTIAL s.{id,name,value,styleType}')
            ->where('s.name = :name')
            ->setParameter('name', $name)
            ->getQuery()
            ->getOneOrNullResult();
    }
}