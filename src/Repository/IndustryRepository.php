<?php

namespace App\Repository;

use Doctrine\ORM\EntityRepository;

class IndustryRepository extends EntityRepository
{
    public function getItems()
    {
        return $this->createQueryBuilder('i')
            ->orderBy('i.name')
            ->getQuery()
            ->getResult();
    }

    public function findPartialByIdList(array $idList)
    {
        return $this->createQueryBuilder('i')
            ->select('PARTIAL i.{id}')
            ->where('i IN (:id)')
            ->setParameter('id', $idList)
            ->getQuery()
            ->getResult();
    }
}
