<?php

namespace App\Repository;

use Doctrine\ORM\EntityRepository;

class CreditTypeRepository extends EntityRepository
{
    public function checkExistType(string $type)
    {
        $qb = $this->createQueryBuilder('ct')
            ->select('COUNT(ct)')
            ->where('ct.type = :type')
            ->setParameter('type', $type);

        return $qb->getQuery()->getSingleScalarResult() > 0;
    }

    public function getCreditTypeByType(string $type)
    {
        return $this->getEntityManager()
            ->createQuery('SELECT ct FROM App:CreditType ct WHERE ct.type = :type')
            ->setParameter('type', $type)
            ->getOneOrNullResult();
    }

    public function getAllTypes($visable = null): array
    {
        $types = $this->createQueryBuilder('ct')
            ->select('ct.id, ct.type, ct.isVisible, ct.title');

        if (!is_null($visable)) {
            $types->where('ct.isVisible = :visible')
                ->setParameter('visible', $visable);
        }

        return $types->getQuery()
            ->getResult();
    }

    public function getAssociateTypes(): array
    {
        $results = [];
        $types = $this->getEntityManager()
                ->createQuery('SELECT ct FROM App:CreditType ct')
                ->getResult();

        if (!empty($types)) {
            foreach ($types as $type) {
                $results[$type->getType()] = $type;
            }
        }
        return $results;
    }

    public function getAssociateTypeId(): array
    {
        $results = [];
        $types = $this->getEntityManager()
            ->createQuery('SELECT ct FROM App:CreditType ct')
            ->getResult();

        if (!empty($types)) {
            foreach ($types as $type) {
                $results[$type->getId()] = $type;
            }
        }
        return $results;
    }

    /**
     * Get all available types
     * @param string $return - type|id
     * @param null $visable
     * @return array
     */
    public function getAllAvailableTypes($return = 'type', $visable = null): array
    {
        $select = ($return == 'type') ? 'ct.type' : 'ct.id';
        $types = $this->createQueryBuilder('ct')
            ->select($select);

        if (!is_null($visable)) {
            $types->where('ct.isVisible = :visible')
                ->setParameter('visible', $visable);
        }

        return $types->getQuery()
            ->getResult('HYDRATE_COLUMN');
    }

    /**
     * Get all types for company
     * @param string $return - type|id
     * @param null $visable
     * @param array $exclude
     * @return array
     */
    public function getAllCompanyTypes(string $return = 'type', $visable = null, array $exclude = []): array
    {
        $select = ($return == 'type') ? 'ct.type' : 'ct.id';
        $types = $this->createQueryBuilder('ct')
            ->select($select);

        if (!is_null($visable)) {
            $types->where('ct.isVisible = :visible')
                ->setParameter('visible', $visable);
        }

        if (!empty($exclude)) {
            $types->andWhere('ct.type NOT IN (:exclude)')
                ->setParameter('exclude', $exclude);
        }

        return $types->getQuery()
            ->getResult('HYDRATE_COLUMN');
    }

    /**
     * Get all types for client
     * @param string $return - type|id
     * @param null $visable
     * @return array
     */
    public function getAllClientTypes($return = 'type', $visable = null): array
    {
        $select = ($return == 'type') ? 'ct.type' : 'ct.id';
        $types = $this->createQueryBuilder('ct')
            ->select($select);

        if (!is_null($visable)) {
            $types->where('ct.isVisible = :visible')
                ->setParameter('visible', $visable);
        }

        return $types->getQuery()
            ->getResult('HYDRATE_COLUMN');
    }
}