<?php

namespace App\Repository;

use App\Entity\CompanyCredit;
use Doctrine\ORM\EntityRepository;

class CompanyCreditRepository extends EntityRepository
{
    public function getPreviewCreditByCompany($company, array $types)
    {
        $credits = $this->createQueryBuilder('c')
            ->select('ct.type', 'SUM(c.value)')
            ->innerJoin('App:CreditType', 'ct', 'WITH', 'c.creditType = ct.id')
            ->where('c.company = :company AND ct.type IN (:types)')
            ->andWhere('c.value > 0 AND c.expiresAt >= CURRENT_DATE()')
            ->setParameter('company', $company)
            ->setParameter('types', $types)
            ->groupBy('ct.type')
            ->getQuery()
            ->getResult('HYDRATE_KEY_PAIR');

        return array_replace(array_fill_keys($types, 0), $credits);
    }

    public function getCreditByCompany($company, array $types)
    {
        $qb = $this->createQueryBuilder('c')
            ->select(
                'c.id',
                'ct.type',
                'c.value',
                'c.expiresAt expires_at',
                'c.isRefund is_refund',
                'c.createdAt created_at')
            ->innerJoin('App:CreditType', 'ct', 'WITH', 'c.creditType = ct.id')
            ->where('c.company = :company AND ct.type IN (:types)')
            ->andWhere('c.value > 0 AND c.expiresAt >= CURRENT_DATE()')
            ->setParameter('company', $company)
            ->setParameter('types', $types)
            ->orderBy('ct.type')
            ->addOrderBy('c.expiresAt');

        $credits = [];
        foreach ($qb->getQuery()->getArrayResult() as $credit) {
            $type = $credit['type'];
            unset($credit['type']);
            $credits[$type][] = $credit;
        }

        return array_replace(array_fill_keys($types, []), $credits);
    }

    public function getCredits($company)
    {
        return $this->createQueryBuilder('c')
            ->select(
                'c.id',
                'ct.type',
                'ct.id credit_type',
                'c.value',
                'c.expiresAt expires_at',
                'c.isRefund is_refund',
                'c.createdAt created_at')
            ->innerJoin('App:CreditType', 'ct', 'WITH', 'c.creditType = ct.id')
            ->where('c.company = :company')
            ->andWhere('c.value > 0 AND c.expiresAt >= CURRENT_DATE()')
            ->setParameter('company', $company)
            ->orderBy('ct.type')
            ->addOrderBy('c.expiresAt')
            ->getQuery()
            ->getArrayResult();
    }

    public function getAllAvailableCredits($company)
    {
        $credits = $this->createQueryBuilder('c')
            ->select('ct.type', 'SUM(c.value)')
            ->innerJoin('App:CreditType', 'ct', 'WITH', 'c.creditType = ct.id')
            ->where('c.company = :company')
            ->andWhere('c.value > 0 AND c.expiresAt >= CURRENT_DATE()')
            ->setParameter('company', $company)
            ->groupBy('ct.type')
            ->getQuery()
            ->getResult('HYDRATE_KEY_PAIR');

        return array_replace(array_fill_keys([], 0), $credits);
    }
}
