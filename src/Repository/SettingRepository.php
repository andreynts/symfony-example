<?php

namespace App\Repository;

use Doctrine\ORM\EntityRepository;

class SettingRepository extends EntityRepository
{
    public function getSettingsByLocation($settingLocation)
    {
        return $this->createQueryBuilder('s')
            ->select('s')
            ->where('s.settingLocation = :location')
            ->setParameter('location', $settingLocation)
            ->getQuery()
            ->getResult();
    }

    public function findPartialByName(string $name)
    {
        return $this->createQueryBuilder('s')
            ->select('PARTIAL s.{id,value}')
            ->where('s.name = :name')
            ->setParameter('name', $name)
            ->getQuery()
            ->getOneOrNullResult();
    }
}