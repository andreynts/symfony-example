<?php


namespace App\Repository;


use Doctrine\ORM\EntityRepository;

class SettingLocationRepository extends EntityRepository
{
    public function findLocationByValue(string $value)
    {
        return $this->createQueryBuilder('sl')
            ->select('sl')
            ->where('sl.value = :value')
            ->setParameter('value', $value)
            ->getQuery()
            ->getOneOrNullResult();
    }
}