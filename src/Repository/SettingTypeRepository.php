<?php

namespace App\Repository;

use Doctrine\ORM\EntityRepository;

class SettingTypeRepository extends EntityRepository
{
    public function findPartialByTypeName(string $type_name)
    {
        return $this->createQueryBuilder('st')
            ->select('PARTIAL st.{id,type_name}')
            ->where('st.type_name = :type_name')
            ->setParameter('type_name', $type_name)
            ->getQuery()
            ->getOneOrNullResult();
    }
}