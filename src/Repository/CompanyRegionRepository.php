<?php

namespace App\Repository;

use Doctrine\ORM\EntityRepository;

class CompanyRegionRepository extends EntityRepository
{
    public function getNamesByCompany($company): array
    {
        return $this->createQueryBuilder('r')
            ->select('r.name')
            ->where('r.company = :company')
            ->setParameter('company', $company)
            ->orderBy('r.name')
            ->getQuery()
            ->getResult('HYDRATE_COLUMN');
    }
}
