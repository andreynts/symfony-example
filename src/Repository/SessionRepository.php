<?php

namespace App\Repository;

use Doctrine\ORM\EntityRepository;

class SessionRepository extends EntityRepository
{
    public function countByStatus(string $status, $client = null, $coach = null): int
    {
        $qb = $this->createQueryBuilder('s')
            ->select('COUNT(s)')
            ->where('s.status = :status')
            ->setParameter('status', $status);

        if (null !== $client) {
            $qb->andWhere('s.client = :client')
                ->setParameter('client', $client);
        }

        if (null !== $coach) {
            $qb->andWhere('s.coach = :coach')
                ->setParameter('coach', $coach);
        }

        return $qb->getQuery()->getSingleScalarResult();
    }

    public function checkCoachUserWithClient($user, $client): bool
    {
        return $this->createQueryBuilder('s')
            ->select('COUNT(s)')
            ->join('s.coach', 'c', 'WITH', 'c.user = :user')
            ->where('s.client = :client')
            ->setParameter('user', $user)
            ->setParameter('client', $client)
            ->getQuery()
            ->getSingleScalarResult() > 0;
    }

    public function checkSessionClashed(\DateTime $startedAt, \DateTime $endedAt, $client): bool
    {
        $qb = $this->createQueryBuilder('s');
        $qb->select('COUNT(s)')
            ->where('s.client = :client')
            ->andWhere($qb->expr()->orX(
                's.startedAt >= :started_at AND s.startedAt < :ended_at',
                's.endedAt > :started_at AND s.endedAt <= :ended_at',
                's.startedAt <= :started_at AND s.endedAt >= :ended_at'
            ))
            ->setParameter('client', $client)
            ->setParameter('started_at', $startedAt)
            ->setParameter('ended_at', $endedAt);

        return $qb->getQuery()->getSingleScalarResult() > 0;
    }

    public function findById(int $id)
    {
        return $this->createQueryBuilder('ql')
            ->select('s')
            ->where('s.id = :id')
            ->setParameter('id', $id)
            ->getQuery()
            ->getOneOrNullResult();
    }
}
