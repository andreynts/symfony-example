<?php

namespace App\Repository;

use Doctrine\ORM\EntityRepository;

class CompanyRepository extends EntityRepository
{
    public function checkNameExists(string $name, $company = null): bool
    {
        $qb = $this->createQueryBuilder('c')
            ->select('COUNT(c)')
            ->where('c.name = :name')
            ->setParameter('name', $name);

        if (null !== $company) {
            $qb->andWhere('c <> :company')
                ->setParameter('company', $company);
        }

        return $qb->getQuery()->getSingleScalarResult() > 0;
    }

    public function getCompanyById(int $id)
    {
        return $this->createQueryBuilder('c')
            ->where('c.id = :id')
            ->setParameter('id', $id)
            ->getQuery()
            ->getOneOrNullResult();
    }
}
