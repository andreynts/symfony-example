<?php

namespace App\Repository;

use App\Entity\Session;
use Doctrine\ORM\EntityRepository;

class SessionSurveyRepository extends EntityRepository
{
    public function getSurveyBySession(Session $session)
    {
        return $this->createQueryBuilder('a')
            ->select('a')
            ->where('a.session = :session')
            ->setParameter('session', $session)
            ->getQuery()
            ->setMaxResults(1)
            ->getOneOrNullResult();
    }
}
