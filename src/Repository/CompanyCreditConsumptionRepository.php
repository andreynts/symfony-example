<?php

namespace App\Repository;

use App\Entity\Company;
use Doctrine\ORM\EntityRepository;
use App\Entity\Session;

class CompanyCreditConsumptionRepository extends EntityRepository
{
    public function getCreditConsumptionBySessionId(Session $session)
    {
        return $this->createQueryBuilder('c')
            ->where('c.session = :session')
            ->setParameter('session', $session)
            ->getQuery()
            ->getOneOrNullResult();

    }

    public function getAllCompanyCreditConsumption(Company $company)
    {
        return $this->createQueryBuilder('c')
            ->where('c.company = :company')
            ->setParameter('company', $company)
            ->getQuery()
            ->getResult();
    }
}