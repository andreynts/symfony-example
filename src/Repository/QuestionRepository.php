<?php

namespace App\Repository;

use Doctrine\ORM\EntityRepository;

class QuestionRepository extends EntityRepository
{
    public function getFormByQuestionLocation($questionLocation)
    {
        return $this->createQueryBuilder('q')
            ->select('q')
            ->where('q.questionLocation = :location')
            ->setParameter('location', $questionLocation)
            ->orderBy('q.position')
            ->getQuery()
            ->getResult();
    }

    public function getQuestionByFieldName(string $name)
    {
        return $this->createQueryBuilder('q')
            ->select('q')
            ->where('JSON_GET_TEXT(q.values, \'name\') = :value')
            ->setParameter('value', $name)
            ->getQuery()
            ->getOneOrNullResult();
    }
}