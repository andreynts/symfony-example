<?php


namespace App\Repository;

use Doctrine\ORM\EntityRepository;

class EmailTemplateParameterRepository extends EntityRepository
{
    public function findPartialByParamName(string $param_name)
    {
        return $this->createQueryBuilder('etp')
            ->select('PARTIAL etp.{id,name,value,emailTemplateType}')
            ->where('etp.name = :param_name')
            ->setParameter('param_name', $param_name)
            ->getQuery()
            ->getOneOrNullResult();
    }
}