<?php

namespace App\Repository;

use Doctrine\ORM\EntityRepository;

class CoachRepository extends EntityRepository
{
    public function findPartialById(int $id)
    {
        return $this->createQueryBuilder('c')
            ->select('PARTIAL c.{id}')
            ->where('c.id = :id')
            ->setParameter('id', $id)
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function findPartialByUser($user)
    {
        return $this->createQueryBuilder('c')
            ->select('PARTIAL c.{id}')
            ->where('c.user = :user')
            ->setParameter('user', $user)
            ->getQuery()
            ->getOneOrNullResult();
    }
}
