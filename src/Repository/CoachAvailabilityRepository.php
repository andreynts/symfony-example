<?php

namespace App\Repository;

use Doctrine\ORM\EntityRepository;

class CoachAvailabilityRepository extends EntityRepository
{
    public function getOneAvailabile($availability, \DateTime $startedAt, \DateTime $endedAt, $coach = null)
    {
        $qb = $this->createQueryBuilder('a')
            ->leftJoin('App:Session', 's', 'WITH', 's.availability = a')
            ->where('a = :availability AND s.id IS NULL')
            ->andWhere('a.startedAt <= :started_at AND a.endedAt >= :ended_at')
            ->setParameter('availability', $availability)
            ->setParameter('started_at', $startedAt)
            ->setParameter('ended_at', $endedAt);

        if (null !== $coach) {
            $qb->andWhere('a.coach = :coach')
                ->setParameter('coach', $coach);
        }

        return $qb->getQuery()->getOneOrNullResult();
    }
}
