<?php

namespace App\Repository;

use Doctrine\ORM\EntityRepository;

class CompanyDepartmentRepository extends EntityRepository
{
    public function getNamesByCompany($company): array
    {
        return $this->createQueryBuilder('d')
            ->select('d.name')
            ->where('d.company = :company')
            ->setParameter('company', $company)
            ->orderBy('d.name')
            ->getQuery()
            ->getResult('HYDRATE_COLUMN');
    }
}
