<?php

namespace App\Repository;

use Doctrine\ORM\EntityRepository;

class SessionCoachFeedbackRepository extends EntityRepository
{
    public function checkFeedbackExists($session): bool
    {
        return $this->createQueryBuilder('sf')
            ->select('COUNT(sf)')
            ->where('sf.session = :session')
            ->setParameter('session', $session)
            ->getQuery()
            ->getSingleScalarResult() > 0;
    }
}
