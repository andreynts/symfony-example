<?php

namespace App\Repository;

use App\Entity\ClientCredit;
use Doctrine\ORM\EntityRepository;

class ClientCreditRepository extends EntityRepository
{
    public function getPreviewCreditByClient($client, array $types)
    {
        $credits = $this->createQueryBuilder('c')
            ->select('ct.type', 'SUM(c.value)')
            ->innerJoin('App:CreditType', 'ct', 'WITH', 'c.creditType = ct.id')
            ->where('c.client = :client AND ct.type IN (:types)')
            ->andWhere('c.value > 0 AND c.expiresAt >= CURRENT_DATE()')
            ->setParameter('client', $client)
            ->setParameter('types', $types)
            ->groupBy('ct.type')
            ->getQuery()
            ->getResult('HYDRATE_KEY_PAIR');

        return array_replace(array_fill_keys($types, 0), $credits);
    }

    public function getCreditByClient($client, array $types)
    {
        $qb = $this->createQueryBuilder('c')
            ->select(
                'c.id',
                'ct.type',
                'c.value',
                'c.expiresAt expires_at',
                'c.isRefund is_refund',
                'c.createdAt created_at')
            ->innerJoin('App:CreditType', 'ct', 'WITH', 'c.creditType = ct.id')
            ->where('c.client = :client AND ct.type IN (:types)')
            ->andWhere('c.value > 0 AND c.expiresAt >= CURRENT_DATE()')
            ->setParameter('client', $client)
            ->setParameter('types', $types)
            ->orderBy('ct.type')
            ->addOrderBy('c.expiresAt');

        $credits = [];
        foreach ($qb->getQuery()->getArrayResult() as $credit) {
            $type = $credit['type'];
            unset($credit['type']);
            $credits[$type][] = $credit;
        }

        return array_replace(array_fill_keys($types, []), $credits);
    }

    public function getAvailableCredits($client)
    {
        return $this->createQueryBuilder('c')
            ->select(
                'c.id',
                'ct.type credit_type',
                'c.value',
                'c.expiresAt expires_at',
                'c.isRefund is_refund',
                'c.createdAt created_at')
            ->innerJoin('App:CreditType', 'ct', 'WITH', 'c.creditType = ct.id')
            ->where('c.client = :client')
            ->andWhere('c.value > 0 AND c.expiresAt >= CURRENT_DATE()')
            ->setParameter('client', $client)
            ->orderBy('ct.type')
            ->addOrderBy('c.expiresAt')
            ->getQuery()
            ->getArrayResult();
    }
}
