<?php

namespace App\Repository;

use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Security\User\UserLoaderInterface;

class UserRepository extends EntityRepository implements UserLoaderInterface
{
    public function loadUserByUsername($username)
    {
        return $this->createQueryBuilder('u')
            ->where('u.username = :username')
            ->setParameter('username', $username)
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function checkUsernameExists(string $username, $user = null): bool
    {
        $qb = $this->createQueryBuilder('u')
            ->select('COUNT(u)')
            ->where('LOWER(u.username) = LOWER(:username)')
            ->setParameter('username', $username);

        if (null !== $user) {
            $qb->andWhere('u <> :user')
                ->setParameter('user', $user);
        }

        return $qb->getQuery()->getSingleScalarResult() > 0;
    }
}
