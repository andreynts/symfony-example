<?php


namespace App\Repository;


use Doctrine\ORM\EntityRepository;

class EmailTemplateTypesRepository extends EntityRepository
{
    public function findPartialByTemplateName(string $email_template_name)
    {
        return $this->createQueryBuilder('ett')
            ->select('PARTIAL ett.{id,email_template_name}')
            ->where('ett.email_template_name = :type_name')
            ->setParameter('type_name', $email_template_name)
            ->getQuery()
            ->getOneOrNullResult();
    }
}