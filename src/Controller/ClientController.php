<?php

namespace App\Controller;

use App\Entity\Client;
use App\Entity\ClientCredit;
use App\Entity\CompanyCredit;
use App\Enum\CreditTypeEnum;
use App\Enum\ErrorEnum;
use App\Enum\SessionStatusEnum;
use App\Utility\RequestValidator;
use App\Validator\Constraints as AppAssert;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

class ClientController extends AbstractController
{
    use Traits\UserTrait;
    use Traits\ClientTrait;

    /**
     * @Security("is_granted('ROLE_ENTERPRISE_ADMIN') or is_granted('ROLE_ADMIN')")
     * @Route("/clients")
     * @Method("GET")
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $creditTypeRepository = $em->getRepository('App:CreditType');
        $companyCreditAllocationRepository = $this->getDoctrine()->getManager()->getRepository('App:CompanyCreditAllocation');
        $creditTypes = $creditTypeRepository->getAssociateTypeId();
        $types = array_keys($creditTypes);

        $sortFields = ['name', 'department', 'position', 'region'];
        foreach ($creditTypes as $k => $type) {
            $sortFields[] = 'credit_' . strtolower($k);
        }

        $collection = [
            'is_company_contact' => [
                new Assert\NotBlank(),
                new AppAssert\Boolean(),
            ],
            'is_company_admin' => [
                new Assert\NotBlank(),
                new AppAssert\Boolean(),
            ],
            'is_verified' => [
                new Assert\NotBlank(),
                new AppAssert\Boolean(),
            ],
            'search' => [
                new Assert\NotBlank(),
                new Assert\Length(['min' => 3]),
            ],
            'offset' => [
                new Assert\NotBlank(),
                new Assert\Range(['min' => 0]),
            ],
            'limit' => [
                new Assert\NotBlank(),
                new Assert\Range(['min' => 1, 'max' => 500]),
            ],
            'sort' => new AppAssert\Chain([
                new Assert\Type('array'),
                new Assert\Collection([
                    'allowMissingFields' => true,
                    'fields' => array_fill_keys($sortFields, [
                        new Assert\NotBlank(),
                        new Assert\Choice([
                            'choices' => ['ASC', 'DESC'],
                            'strict' => true,
                        ]),
                    ]),
                ]),
            ]),
        ];

        if ($this->isGranted('ROLE_ADMIN')) {
            $collection['is_enabled'] = [
                new Assert\NotBlank(),
                new AppAssert\Boolean(),
            ];
            $collection['company'] = [
                new Assert\NotBlank(),
                new Assert\GreaterThan(0),
            ];
        }

        $validator = $this->get(RequestValidator::class);
        $validator->validate($request->query, new Assert\Collection([
            'allowMissingFields' => true,
            'fields' => $collection,
        ]));

        $isEnabled = $request->query->getBoolean('is_enabled', true);

        $qb = $em->createQueryBuilder()
            ->select(
                'c.id',
                'u.username email',
                'u.firstName first_name',
                'u.lastName last_name',
                "CONCAT(u.firstName, ' ', u.lastName) HIDDEN name",
                'u.photo',
                'c.mobileNumber mobile_number',
                'c.department',
                'c.position',
                'c.region',
                'c.referenceNumber',
                "CASE WHEN 'ROLE_ENTERPRISE_ADMIN' = ANY_OF(STRING_TO_ARRAY(u.roles)) THEN TRUE ELSE FALSE END AS company_is_admin",
                'c.isCompanyContact company_is_contact',
                'u.isVerified is_verified',
                'u.lastAuthAt last_auth_at',
                'u.createdAt created_at')
            ->from('App:Client', 'c')
            ->join('c.user', 'u')
            ->leftJoin('c.credits', 'cc', 'WITH', 'cc.expiresAt >= CURRENT_DATE()')
            ->where('u.isEnabled = :is_enabled')
            ->setParameter('is_enabled', $isEnabled)
            ->groupBy('c.id', 'u.id');

        $company = $request->query->get('company');
        if ($this->isGranted('ROLE_ENTERPRISE_ADMIN')) {
            $company = $this->getClientUser()->getCompany();
        } else {
            $qb->addSelect('IDENTITY(c.company) company_id', 'c.companyName company_name');
        }
        if (null !== $company) {
            $qb->andWhere('c.company = :company')
                ->setParameter('company', $company);
        }

        $dql = "0";
        foreach ($types as $type) {
            $dql = $dql . '+SUM(CASE WHEN cc.creditType = ' . $type . ' THEN cc.value ELSE 0 END)';
        }
        $dql = $dql . 'as client_credits_available';
        $qb->addSelect($dql);


        if ($request->query->has('is_company_admin')) {
            $qb->andWhere("'ROLE_ENTERPRISE_ADMIN' = ANY_OF(STRING_TO_ARRAY(u.roles))");
        }
        if ($request->query->has('is_company_contact')) {
            $qb->andWhere('c.isCompanyContact = :is_company_contact')
                ->setParameter('is_company_contact', $request->query->getBoolean('is_company_contact'));
        }

        if ($request->query->has('is_verified')) {
            $qb->andWhere('u.isVerified = :is_verified')
                ->setParameter('is_verified', $request->query->getBoolean('is_verified'));
        }

        if ($search = $request->query->get('search')) {
            $qb->andWhere('LOWER(u.firstName) LIKE LOWER(:search)')
                ->setParameter('search', $search . '%');
        }

        $qbId = clone $qb;
        $qbId->select('c.id');

        $total = $em->createQueryBuilder()
            ->select('COUNT(c0)')
            ->from('App:Client', 'c0')
            ->where('c0 IN (' . $qbId->getDql() . ')')
            ->setParameters($qbId->getParameters())
            ->getQuery()
            ->getSingleScalarResult();

        $sort = $request->query->get('sort', ['name' => 'ASC']);
        foreach ($sort as $field => $order) {
            $qb->addOrderBy($field, $order);
        }

        $offset = $request->query->getInt('offset');
        $limit = $request->query->getInt('limit', 20);
        $qb->setFirstResult($offset)->setMaxResults($limit);

        $result = $qb->getQuery()->getArrayResult();
        foreach ($result as &$client) {
            if (isset($client['company_name'])) {
                if (null !== $client['company_id']) {
                    $client['company']['id'] = $client['company_id'];
                }
                $client['company']['name'] = $client['company_name'];
                unset($client['company_id'], $client['company_name']);
            }

            $client['company']['is_admin'] = $client['company_is_admin'];
            $client['company']['is_contact'] = $client['company_is_contact'];
            unset($client['company_is_admin'], $client['company_is_contact']);

            if (isset($client['id'])) {
                $client['client_credits_consumption'] = $this->getClientConsumptionById($client['id']);
                $client['client_credits_allocation'] = $companyCreditAllocationRepository->getTotalAvailableCreditByClientId($client['id']);
                $qbSessions = $em->createQueryBuilder()
                    ->select('COUNT(s.id) as sessions_completed')
                    ->from('App:Session', 's')
                    ->leftJoin('App:Client', 'c', 'WITH', 's.client = c')
                    ->where('s.status = :status')
                    ->andWhere('c.id = :id')
                    ->setParameter('status', SessionStatusEnum::COMPLETED)
                    ->setParameter('id', $client['id'])
                    ->getQuery()
                    ->getOneOrNullResult();

                $sessionsCompleted = 0;
                if ($qbSessions !== null) {
                    $sessionsCompleted = $qbSessions['sessions_completed'];
                }

                $client['client_sessions_completed'] = $sessionsCompleted;
            }


        }

        return [
            'result' => $result,
            'total' => $total
        ];
    }

    /**
     * @Security("is_granted('view', client) or is_granted('ROLE_ADMIN')")
     * @Route("/clients/{id}", requirements={"id": "\d+"})
     * @Method("GET")
     */
    public function viewAction(Client $client)
    {
        return $this->viewClient($client);
    }

    /**
     * @Security("is_granted('ROLE_ADMIN')")
     * @Route("/clients/{id}", requirements={"id": "\d+"})
     * @Method("PATCH")
     */
    public function updateAction(Request $request, Client $client)
    {
        $this->saveClient($request, $client);

        return self::STATUS_OK;
    }

    /**
     * @Security("is_granted('ROLE_ADMIN')")
     * @Route("/clients/{id}", requirements={"id": "\d+"})
     * @Method("DELETE")
     */
    public function deleteAction(Client $client)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($client->getUser());
        $em->flush();

        return self::STATUS_OK;
    }

    /**
     * @Security("is_granted('company', client) or is_granted('ROLE_ADMIN')")
     * @Route("/clients/{id}/company", requirements={"id": "\d+"})
     * @Method("PATCH")
     */
    public function companyAction(Request $request, Client $client)
    {
        $validator = $this->get(RequestValidator::class);
        $validator->validate($request->request, [
            new Assert\NotBlank(),
            new Assert\Collection([
                'allowMissingFields' => true,
                'fields' => [
                    'is_admin' => new AppAssert\Chain([
                        new Assert\Type('bool'),
                        new Assert\NotNull(),
                    ]),
                    'is_contact' => new AppAssert\Chain([
                        new Assert\Type('bool'),
                        new Assert\NotNull(),
                    ]),
                    'reference_number' => new AppAssert\Chain([
                        new Assert\Type('string'),
                        new Assert\Length(['max' => 16])
                    ]),
                ],
            ]),
        ]);

        $user = $client->getUser();
        if ($request->request->has('is_admin')) {
            if ($request->request->get('is_admin')) {
                $user->setRoles(['ROLE_ENTERPRISE_ADMIN']);
            } else {
                $user->setRoles(['ROLE_ENTERPRISE_CLIENT']);
            }
        }
        if ($request->request->has('is_contact')) {
            $client->setCompanyContact($request->request->get('is_contact'));
        }

        if ($request->request->has('reference_number')) {
            $client->setReferenceNumber($request->request->get('reference_number'));
        }

        $em = $this->getDoctrine()->getManager();
        $em->persist($client);
        $em->persist($user);
        $em->flush();

        return self::STATUS_OK;
    }

    /**
     * @Security("is_granted('ROLE_ADMIN')")
     * @Route("/clients/{id}/promote", requirements={"id": "\d+"})
     * @Method("POST")
     */
    public function promoteAction(Request $request, Client $client)
    {
        $company = $client->getCompany();

        $validator = $this->get(RequestValidator::class);
        $validator->validate($request->request, new Assert\Collection([
            'company' => new AppAssert\Chain([
                new Assert\Type('int'),
                new Assert\NotNull(),
                new Assert\GreaterThan(0),
                new Assert\Callback(function ($value, ExecutionContextInterface $context) use (&$company) {
                    $repository = $this->getDoctrine()->getRepository('App:Company');
                    if (null === $company = $repository->find($value)) {
                        $context->buildViolation('Invalid company')->addViolation();
                    }
                }),
            ]),
        ]));

        $em = $this->getDoctrine()->getManager();

        $client->setCompanyName($company->getName());
        $client->setCompany($company);
        $em->persist($client);

        $user = $client->getUser();
        $user->setRoles(['ROLE_ENTERPRISE_CLIENT']);
        $em->persist($user);

        $em->flush();

        return self::STATUS_OK;
    }

    /**
     * @Security("is_granted('demote', client) or is_granted('ROLE_ADMIN')")
     * @Route("/clients/{id}/demote", requirements={"id": "\d+"})
     * @Method("POST")
     */
    public function demoteAction(Client $client)
    {
        $em = $this->getDoctrine()->getManager();

        $company = $client->getCompany();
        $client->setCompany(null);
        $em->persist($client);

        $user = $client->getUser();
        $user->setRoles(['ROLE_CLIENT']);
        $em->persist($user);

        $qb = $em->createQueryBuilder()
            ->select('cc')
            ->from('App:ClientCredit', 'cc')
            ->where('cc.client = :client AND cc.type IN (:types)')
            ->andWhere('cc.value > 0 AND cc.expiresAt >= CURRENT_DATE()')
            ->setParameter('client', $client)
            ->setParameter('types', [
                CreditTypeEnum::ENTERPRISE,
                CreditTypeEnum::EXECUTIVE,
            ]);

        foreach ($qb->getQuery()->getResult() as $credit) {
            $refundCredit = new CompanyCredit();
            $refundCredit->setCompany($company);
            $refundCredit->setType($credit->getType());
            $refundCredit->setValue($credit->getValue());
            $refundCredit->setExpiresAt($credit->getExpiresAt());
            $refundCredit->setRefund(true);
            $em->persist($refundCredit);
            $em->remove($credit);
        }

        $em->flush();

        return self::STATUS_OK;
    }

    /**
     * @Security("is_granted('sessions', client) or is_granted('ROLE_ADMIN')")
     * @Route("/clients/{id}/sessions", requirements={"id": "\d+"})
     * @Method("GET")
     */
    public function sessionsAction(Request $request, Client $client)
    {
        $validator = $this->get(RequestValidator::class);
        $validator->validate($request->query, new Assert\Collection([
            'allowMissingFields' => true,
            'fields' => ['date' => new AppAssert\Chain([
                new Assert\Type('array'),
                new Assert\Collection([
                    'start' => [
                        new Assert\NotBlank(),
                        new Assert\DateTime(['format' => \DateTime::RFC3339]),
                    ],
                    'end' => [
                        new Assert\NotBlank(),
                        new Assert\DateTime(['format' => \DateTime::RFC3339]),
                    ],
                ]),
            ])],
        ]));

        $date = $request->query->get('date', [
            'start' => 'first day of this month 00:00:00',
            'end' => 'last day of this month 23:59:59',
        ]);
        $startedAt = new \DateTime($date['start']);
        $endedAt = new \DateTime($date['end']);
        if ($startedAt >= $endedAt) {
            throw new BadRequestHttpException('E_INTERVAL_INVALID', null, ErrorEnum::E_INTERVAL_INVALID);
        }

        $em = $this->getDoctrine()->getManager();
        $qb = $em->createQueryBuilder();
        $qb->select(
            'PARTIAL s.{id,startedAt,endedAt,status,rescheduled} session',
            'PARTIAL e.{id}',
            'PARTIAL c.{id}',
            'PARTIAL cu.{id,firstName,lastName}')
            ->from('App:Session', 's')
            ->join('s.expertise', 'e')
            ->join('s.coach', 'c')
            ->join('c.user', 'cu')
            ->where('s.client = :client')
            ->andWhere('s.startedAt >= :started_at OR s.endedAt > :started_at')
            ->andWhere('s.endedAt <= :ended_at OR s.startedAt < :ended_at')
            ->setParameter('client', $client)
            ->setParameter('started_at', $startedAt)
            ->setParameter('ended_at', $endedAt)
            ->orderBy('s.startedAt', 'DESC');

        $payload = [];
        foreach ($qb->getQuery()->getResult() as $result) {
            $session = $result['session'];
            $coach = $session->getCoach();
            $user = $coach->getUser();

            $payload[] = [
                'id' => $session->getId(),
                'expertise' => $session->getExpertise()->getId(),
                'started_at' => $session->getStartedAt(),
                'ended_at' => $session->getEndedAt(),
                'status' => $session->getStatus(),
                'rescheduled' => $session->getRescheduled(),
                'coach' => [
                    'id' => $coach->getId(),
                    'first_name' => $user->getFirstName(),
                    'last_name' => $user->getLastName(),
                ],
            ];
        }

        return $payload;
    }

    /**
     * @Security("is_granted('credits', client) or is_granted('ROLE_ADMIN')")
     * @Route("/clients/{id}/credits", requirements={"id": "\d+"})
     * @Method("GET")
     */
    public function creditsAction(Request $request, Client $client)
    {
        /*$types = [CreditTypeEnum::COMPLIMENTARY];
        if ($this->isGranted('ROLE_ENTERPRISE_CLIENT', null, $client->getUser())) {
            array_unshift($types, CreditTypeEnum::ENTERPRISE, CreditTypeEnum::EXECUTIVE);
        }
        if (!$this->isGranted('ROLE_ENTERPRISE_ADMIN')) {
            array_unshift($types, CreditTypeEnum::PRIVATE_);
        }*/

        $creditTypeRepository = $this->getDoctrine()->getRepository('App:CreditType');
        $clientCreditTypes = $creditTypeRepository->getAllClientTypes();

        $repository = $this->getDoctrine()->getRepository('App:ClientCredit');
        if ($request->query->getBoolean('preview')) {
            return $repository->getPreviewCreditByClient($client, $clientCreditTypes);
        }

        return $repository->getCreditByClient($client, $clientCreditTypes);
    }

    /**
     * @Security("is_granted('credits', client) or is_granted('ROLE_ADMIN')")
     * @Route("/clients/{id}/assigned-credits", requirements={"id": "\d+"})
     * @Method("GET")
     */
    public function assignedCreditsAction(Request $request, Client $client)
    {
        $creditTypeRepository = $this->getDoctrine()->getRepository('App:CreditType');
        $clientCreditTypes = $creditTypeRepository->getAllClientTypes();

        $result = [
            'total' => 0,
            'result' => []
        ];

        $em = $this->getDoctrine()->getManager();
        $result['result'] = $em->createQueryBuilder()
            ->select('cca.id', 'ct.type', 'cca.value', 'cca.createdAt')
            ->from('App:CompanyCreditAllocation', 'cca')
            ->innerJoin('App:CreditType', 'ct', 'WITH', 'cca.creditType = ct.id')
            ->where('cca.client = :client AND ct.type IN (:types)')
            ->andWhere('cca.value > 0')
            ->setParameter('client', $client)
            ->setParameter('types', $clientCreditTypes)
            ->getQuery()
            ->getArrayResult();

        foreach ($result['result'] as $credit) {
            $result['total'] = $result['total'] + $credit['value'];
        }

        return $result;
    }

    /**
     * @Security("is_granted('ROLE_ADMIN')")
     * @Route("/clients/status")
     * @Method("POST")
     */
    public function statusAction(Request $request)
    {
        $validator = $this->get(RequestValidator::class);
        $validator->validate($request->request, new Assert\Collection([
            'clients' => new AppAssert\Chain([
                new Assert\Type('array'),
                new Assert\NotNull(),
                new Assert\Count(['min' => 1, 'max' => 1000]),
                new Assert\All(new AppAssert\Chain([
                    new Assert\Type('int'),
                    new Assert\NotNull(),
                    new Assert\GreaterThan(0),
                ])),
                new Assert\Callback(function ($value, ExecutionContextInterface $context) {
                    $em = $this->getDoctrine()->getManager();
                    $qb = $em->createQueryBuilder()
                        ->select('COUNT(c)')
                        ->from('App:Client', 'c')
                        ->where('c IN (:clients)')
                        ->setParameter('clients', $value);

                    if ($qb->getQuery()->getSingleScalarResult() !== count($value)) {
                        $context->buildViolation('Invalid clients')->addViolation();
                    }
                }),
            ]),
            'is_enabled' => new AppAssert\Chain([
                new Assert\Type('bool'),
                new Assert\NotNull(),
            ]),
        ]));

        $em = $this->getDoctrine()->getManager();
        $em->createQueryBuilder()
            ->update('App:User', 'u')
            ->set('u.isEnabled', ':is_enabled')
            ->where('u IN (SELECT IDENTITY(c.user) FROM App:Client c WHERE c IN (:clients))')
            ->setParameter('is_enabled', $request->request->get('is_enabled'))
            ->setParameter('clients', $request->request->get('clients'))
            ->getQuery()
            ->execute();

        return self::STATUS_OK;
    }

    public function getClientConsumptionById($id)
    {
        $clientRepository = $this->getDoctrine()->getManager()->getRepository('App:Client');
        $client = $clientRepository->getClientById($id);

        $consumption = 0;
        if ($client) {
            $creditConsumption = $client->getConsumptionCredit(true);
            if (!empty($creditConsumption)) {
                $consumption = (is_array($creditConsumption)) ? array_sum($creditConsumption) : 0;
            }
        }
        return $consumption;
    }
}
