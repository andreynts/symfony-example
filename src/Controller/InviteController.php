<?php

namespace App\Controller;

use App\Entity\Company;
use App\Entity\Invite;
use App\Enum\ErrorEnum;
use App\Utility\Mailer;
use App\Utility\RequestValidator;
use App\Validator\Constraints as AppAssert;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

class InviteController extends AbstractController
{
    const IMPORT_HEADER = ['Email', 'First Name', 'Last Name'];

    /**
     * @Security("is_granted('ROLE_ENTERPRISE_ADMIN') or is_granted('ROLE_ADMIN')")
     * @Route("/invite")
     * @Method("POST")
     */
    public function indexAction(Request $request)
    {
        $collection = $this->getInviteValidatorContraints();
        if ($this->isGranted('ROLE_ADMIN')) {
            $collection['company'] = new AppAssert\Chain([
                new Assert\Type('int'),
                new Assert\GreaterThan(0),
            ]);
        }

        $validator = $this->get(RequestValidator::class);
        $validator->validate($request->request, new Assert\Collection($collection));

        $email = strtolower($request->request->get('email'));
        $firstName = $request->request->get('first_name');
        $lastName = $request->request->get('last_name');
        $company = $request->request->get('company');

        if ($this->isGranted('ROLE_ADMIN') && null !== $company) {
            $repository = $this->getDoctrine()->getRepository('App:Company');
            if (null === $company = $repository->find($company)) {
                throw new BadRequestHttpException('Invalid company', null, ErrorEnum::E_BAD_REQUEST);
            }
        } elseif ($this->isGranted('ROLE_ENTERPRISE_ADMIN')) {
            $company = $this->getClientUser()->getCompany();
        }

        $this->inviteClient($email, $firstName, $lastName, $company);

        return self::STATUS_OK;
    }

    /**
     * @Security("is_granted('ROLE_ADMIN')")
     * @Route("/invite/coach")
     * @Method("POST")
     */
    public function coachAction(Request $request)
    {
        $validator = $this->get(RequestValidator::class);
        $constraints = $this->getInviteValidatorContraints();
        $validator->validate($request->request, new Assert\Collection($constraints));

        $email = strtolower($request->request->get('email'));
        $firstName = $request->request->get('first_name');

        $token = $this->get('lexik_jwt_authentication.encoder')->encode([
            'exp' => (new \DateTime('+1 year'))->getTimestamp(),
            '$invite:coach' => true,
            '$username' => $email,
            '$first_name' => $firstName,
            '$last_name' => $request->request->get('last_name'),
        ]);

        $mailer = $this->get(Mailer::class);
        $mailer->sendTo($email, 'invite/coach.html.twig', [
            'first_name' => $firstName,
            'token' => $token,
        ]);

        return self::STATUS_OK;
    }

    /**
     * @Security("is_granted('ROLE_ADMIN')")
     * @Route("/invite/company")
     * @Method("POST")
     */
    public function companyAction(Request $request)
    {
        $validator = $this->get(RequestValidator::class);
        $constraints = $this->getInviteValidatorContraints();
        $validator->validate($request->request, new Assert\Collection($constraints + [
            'company_name' => new AppAssert\Chain([
                new Assert\Type('string'),
                new Assert\NotBlank(),
                new Assert\Length(['max' => 64]),
                new Assert\Callback(function ($value, ExecutionContextInterface $context) {
                    if ($this->getDoctrine()->getRepository('App:Company')->checkNameExists($value)) {
                        $context->buildViolation('E_COMPANY_EXISTS')
                            ->setCode(ErrorEnum::E_COMPANY_EXISTS)
                            ->addViolation();
                    }
                }),
            ]),
        ]));

        $email = strtolower($request->request->get('email'));
        $firstName = $request->request->get('first_name');

        $token = $this->get('lexik_jwt_authentication.encoder')->encode([
            'exp' => (new \DateTime('+1 year'))->getTimestamp(),
            '$invite:company' => true,
            '$username' => $email,
            '$first_name' => $firstName,
            '$last_name' => $request->request->get('last_name'),
            '$company' => [
                'name' => $request->request->get('company_name'),
            ],
        ]);

        $mailer = $this->get(Mailer::class);
        $mailer->sendTo($email, 'invite/company.html.twig', [
            'first_name' => $firstName,
            'token' => $token,
        ]);

        return self::STATUS_OK;
    }

    /**
     * @Security("is_granted('ROLE_ENTERPRISE_ADMIN') or is_granted('ROLE_ADMIN')")
     * @Route("/invite")
     * @Method("PUT")
     */
    public function importAction(Request $request)
    {
        if ($this->isGranted('ROLE_ENTERPRISE_ADMIN')) {
            $company = $this->getClientUser()->getCompany();
        }

        return $this->importToAction($request, $company ?? null);
    }

    /**
     * @Security("is_granted('ROLE_ADMIN')")
     * @Route("/invite/{id}", requirements={"id": "\d+"})
     * @Method("PUT")
     */
    public function importToAction(Request $request, Company $company = null)
    {
        if ('csv' !== $request->getContentType()) {
            throw new BadRequestHttpException('CSV format is required', null, ErrorEnum::E_BAD_FORMAT);
        }

        $handle = $request->getContent(true);
        if (self::IMPORT_HEADER !== fgetcsv($handle)) {
            throw new BadRequestHttpException('Invalid CSV', null, ErrorEnum::E_BAD_FORMAT);
        }

        set_time_limit(0);

        $validator = $this->get('validator');
        $constraints = $this->getInviteValidatorContraints();
        $collection = new Assert\Collection(array_values($constraints));

        $invited = [];
        while (false !== $raw = fgetcsv($handle)) {
            $row = array_map('trim', $raw);

            if (isset($invited[$row[0]])) {
                continue;
            }

            $errors = $validator->validate($row, $collection);
            if (0 !== count($errors)) {
                $failed[] = mb_substr($row[0], 0, 255);
                continue;
            }

            $this->inviteClient($row[0], $row[1], $row[2], $company);
            $invited[$row[0]] = true;
        }

        $payload['invited'] = count($invited);
        if (isset($failed)) {
            $payload['failed'] = $failed;
        }

        return $payload;
    }

    private function inviteClient(string $email, string $firstName, string $lastName, Company $company = null)
    {
        try {
            if ($company !== null) {
                $em = $this->getDoctrine()->getManager();
                if (!$em->isOpen()) {
                    $em = $em->create(
                        $em->getConnection(),
                        $em->getConfiguration()
                    );
                }

                $invite = new Invite();
                $invite->setEmail($email);
                $invite->setCompany($company);
                $invite->setFirstName($firstName);
                $invite->setLastName($lastName);

                $em->persist($invite);
                $em->flush();
            }
        } catch (\Exception $e) {
            var_dump($e->getMessage()); exit;
        }

        $payload = [
            'exp' => (new \DateTime('+1 year'))->getTimestamp(),
            '$invite:client' => true,
            '$username' => $email,
            '$first_name' => $firstName,
            '$last_name' => $lastName,
            '$inviteId' => $invite->getId(),
        ];

        if (null !== $company) {
            $payload['$company'] = [
                'id' => $company->getId(),
                'name' => $company->getName(),
            ];
        }

        $token = $this->get('lexik_jwt_authentication.encoder')->encode($payload);

        $mailer = $this->get(Mailer::class);
        $mailer->sendTo($email, 'invite/client.html.twig', [
            'first_name' => $payload['$first_name'],
            'token' => $token,
        ]);
    }

    private function getInviteValidatorContraints(): array
    {
        return [
            'email' => new AppAssert\Chain([
                new Assert\Type('string'),
                new Assert\NotBlank(),
                new Assert\Email(['checkMX' => true]),
                new Assert\Callback(function ($value, ExecutionContextInterface $context) {
                    if ($this->getDoctrine()->getRepository('App:User')->checkUsernameExists($value)) {
                        $context->buildViolation('E_USER_EXISTS')
                            ->setCode(ErrorEnum::E_USER_EXISTS)
                            ->addViolation();
                    }
                }),
            ]),
            'first_name' => new AppAssert\Chain([
                new Assert\Type('string'),
                new Assert\NotBlank(),
                new Assert\Length(['max' => 32]),
            ]),
            'last_name' => new AppAssert\Chain([
                new Assert\Type('string'),
                new Assert\NotBlank(),
                new Assert\Length(['max' => 32]),
            ]),
        ];
    }
}
