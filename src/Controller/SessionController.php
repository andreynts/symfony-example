<?php

namespace App\Controller;

use App\Entity\Answers;
use App\Entity\ClientCredit;
use App\Entity\ClientCreditConsumption;
use App\Entity\CoachAvailability;
use App\Entity\CompanyCredit;
use App\Entity\CompanyCreditConsumption;
use App\Entity\Recording;
use App\Entity\Session;
use App\Entity\SessionAttachment;
use App\Entity\SessionClientFeedback;
use App\Entity\SessionClientFeedbackChange;
use App\Entity\SessionCoachFeedback;
use App\Entity\SessionSurvey;
use App\Enum\AnswerTypeEnum;
use App\Enum\ErrorEnum;
use App\Enum\ExpertiseLevelEnum;
use App\Enum\SessionStatusEnum;
use App\Enum\SessionTypeEnum;
use App\Event\NotificationEvent;
use App\Exception\PaymentRequiredHttpException;
use App\Utility\DataReader;
use App\Utility\EventRequestGenerator;
use App\Utility\Mailer;
use App\Utility\OpenTokClient;
use App\Utility\RandomGenerator;
use App\Utility\RequestValidator;
use App\Validator\Constraints as AppAssert;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\ConflictHttpException;
use Symfony\Component\Validator\Constraints as Assert;
use App\Utility\EventRequestGenerator as ERGenerator;

class SessionController extends AbstractController
{
    /**
     * @Security("is_granted('ROLE_ADMIN')")
     * @Route("/sessions")
     * @Method("GET")
     */
    public function indexAction(Request $request)
    {
        $sortFields = [
            'type',
            'started_at',
            'client_name',
            'client_company_name',
            'coach_name',
            'status',
        ];

        $validator = $this->get(RequestValidator::class);
        $validator->validate($request->query, new Assert\Collection([
            'allowMissingFields' => true,
            'fields' => [
                'type' => [
                    new Assert\NotBlank(),
                    new Assert\Choice([
                        'choices' => SessionTypeEnum::values(),
                        'strict' => true,
                    ]),
                ],
                'expertise' => [
                    new Assert\NotBlank(),
                    new Assert\GreaterThan(0),
                ],
                'client' => [
                    new Assert\NotBlank(),
                    new Assert\GreaterThan(0),
                ],
                'company' => [
                    new Assert\NotBlank(),
                    new Assert\GreaterThan(0),
                ],
                'coach' => [
                    new Assert\NotBlank(),
                    new Assert\GreaterThan(0),
                ],
                'status' => [
                    new Assert\NotBlank(),
                    new Assert\Choice([
                        'choices' => SessionStatusEnum::values(),
                        'strict' => true,
                    ]),
                ],
                'search' => [
                    new Assert\NotBlank(),
                    new Assert\Length(['min' => 3]),
                ],
                'offset' => [
                    new Assert\NotBlank(),
                    new Assert\Range(['min' => 0]),
                ],
                'limit' => [
                    new Assert\NotBlank(),
                    new Assert\Range(['min' => 1, 'max' => 500]),
                ],
                'sort' => new AppAssert\Chain([
                    new Assert\Type('array'),
                    new Assert\Collection([
                        'allowMissingFields' => true,
                        'fields' => array_fill_keys($sortFields, [
                            new Assert\NotBlank(),
                            new Assert\Choice([
                                'choices' => ['ASC', 'DESC'],
                                'strict' => true,
                            ]),
                        ]),
                    ]),
                ]),
            ],
        ]));

        $em = $this->getDoctrine()->getManager();
        $qb = $em->createQueryBuilder()
            ->select(
                's.id',
                's.type',
                'e.id expertise_id',
                'e.name expertise_name',
                's.startedAt started_at',
                's.endedAt ended_at',
                'cl.id client_id',
                'clu.firstName client_first_name',
                'clu.lastName client_last_name',
                "CONCAT(clu.firstName, ' ', clu.lastName) HIDDEN client_name",
                'cl.companyName client_company_name',
                'clc.id client_company_id',
                'co.id coach_id',
                'cou.firstName coach_first_name',
                'cou.lastName coach_last_name',
                "CONCAT(cou.firstName, ' ', cou.lastName) HIDDEN coach_name",
                's.status')
            ->from('App:Session', 's')
            ->join('s.client', 'cl')
            ->join('cl.user', 'clu')
            ->leftJoin('cl.company', 'clc')
            ->join('s.coach', 'co')
            ->join('co.user', 'cou')
            ->join('s.expertise', 'e');

        if ($type = $request->query->get('type')) {
            $qb->andWhere('s.type = :type')
                ->setParameter('type', $type);
        }
        if ($expertise = $request->query->get('expertise')) {
            $qb->andWhere('s.expertise = :expertise')
                ->setParameter('expertise', $expertise);
        }
        if ($client = $request->query->get('client')) {
            $qb->andWhere('s.client = :client')
                ->setParameter('client', $client);
        }
        if ($company = $request->query->get('company')) {
            $qb->andWhere('cl.company = :company')
                ->setParameter('company', $company);
        }
        if ($coach = $request->query->get('coach')) {
            $qb->andWhere('s.coach = :coach')
                ->setParameter('coach', $coach);
        }
        if ($status = $request->query->get('status')) {
            $qb->andWhere('s.status = :status')
                ->setParameter('status', $status);
        }

        if ($search = $request->query->get('search')) {
            $qb->andWhere($qb->expr()->orX(
                'LOWER(clu.firstName) LIKE LOWER(:search)',
                'LOWER(cou.firstName) LIKE LOWER(:search)',
                'LOWER(e.name) LIKE LOWER(:search)'))
                ->setParameter('search', $search . '%');
        }

        $qbId = clone $qb;
        $qbId->select('s.id');

        $total = $em->createQueryBuilder()
            ->select('COUNT(s0)')
            ->from('App:Session', 's0')
            ->where('s0 IN (' . $qbId->getDql() . ')')
            ->setParameters($qbId->getParameters())
            ->getQuery()
            ->getSingleScalarResult();

        $sort = $request->query->get('sort', ['started_at' => 'ASC']);
        foreach ($sort as $field => $order) {
            $qb->addOrderBy($field, $order);
        }

        $offset = $request->query->getInt('offset');
        $limit = $request->query->getInt('limit', 20);
        $qb->setFirstResult($offset)->setMaxResults($limit);

        $result = $qb->getQuery()->getArrayResult();
        foreach ($result as &$session) {
            $session['expertise']['id'] = $session['expertise_id'];
            $session['expertise']['name'] = $session['expertise_name'];
            unset($session['expertise_id'], $session['expertise_name']);

            $session['client']['id'] = $session['client_id'];
            $session['client']['first_name'] = $session['client_first_name'];
            $session['client']['last_name'] = $session['client_last_name'];
            unset($session['client_id'], $session['client_first_name'], $session['client_last_name']);

            if (null !== $session['client_company_id']) {
                $session['client']['company']['id'] = $session['client_company_id'];
            }
            $session['client']['company']['name'] = $session['client_company_name'];
            unset($session['client_company_id'], $session['client_company_name']);

            $session['coach']['id'] = $session['coach_id'];
            $session['coach']['first_name'] = $session['coach_first_name'];
            $session['coach']['last_name'] = $session['coach_last_name'];
            unset($session['coach_id'], $session['coach_first_name'], $session['coach_last_name']);
        }

        return [
            'result' => $result,
            'total' => $total,
        ];
    }

    /**
     * @Security("is_granted('ROLE_CLIENT') or is_granted('ROLE_COACH')")
     * @Route("/sessions/calendar.{_format}")
     * @Method("GET")
     */
    public function calendarAction(Request $request)
    {
        $validator = $this->get(RequestValidator::class);
        $validator->validate($request->query, new Assert\Collection([
            'allowMissingFields' => true,
            'fields' => [
                'date' => new AppAssert\Chain([
                    new Assert\Type('array'),
                    new Assert\Collection([
                        'start' => [
                            new Assert\NotBlank(),
                            new Assert\DateTime(['format' => \DateTime::RFC3339]),
                        ],
                        'end' => [
                            new Assert\NotBlank(),
                            new Assert\DateTime(['format' => \DateTime::RFC3339]),
                        ],
                    ]),
                ]),
                'sessions' => new AppAssert\Chain([
                    new Assert\Type('array'),
                    new Assert\Count(['min' => 1]),
                    new Assert\All(new AppAssert\Chain([
                        new Assert\NotNull(),
                        new Assert\GreaterThan(0),
                    ])),
                ]),
            ],
        ]));

        $em = $this->getDoctrine()->getManager();
        $qb = $em->createQueryBuilder();
        $qb->select(
            'PARTIAL s.{id,startedAt,endedAt,status,rescheduled,roomKey,tokSession,tokClientToken,tokCoachToken} session',
            'PARTIAL e.{id,name}')
            ->from('App:Session', 's')
            ->join('s.expertise', 'e')
            ->orderBy('s.startedAt', 'DESC');

        if ($date = $request->query->get('date')) {
            $startedAt = new \DateTime($date['start']);
            $endedAt = new \DateTime($date['end']);
            if ($startedAt >= $endedAt) {
                throw new BadRequestHttpException('E_INTERVAL_INVALID', null, ErrorEnum::E_INTERVAL_INVALID);
            }
            $qb->andWhere('s.startedAt >= :started_at OR s.endedAt > :started_at')
                ->andWhere('s.endedAt <= :ended_at OR s.startedAt < :ended_at')
                ->setParameter('started_at', $startedAt)
                ->setParameter('ended_at', $endedAt);
        }

        if ($sessions = $request->query->get('sessions')) {
            $qb->andWhere('s IN (:sessions)')
                ->setParameter('sessions', $sessions);
        }

        if ($this->isGranted('ROLE_CLIENT')) {
            $qb->addSelect(
                'PARTIAL c.{id}',
                'PARTIAL cu.{id,firstName,lastName}')
                ->join('s.coach', 'c')
                ->join('c.user', 'cu')
                ->andWhere('s.client = :client')
                ->setParameter('client', $this->getClientUser());
        } elseif ($this->isGranted('ROLE_COACH')) {
            $qb->addSelect(
                'PARTIAL c.{id,companyName}',
                'PARTIAL cu.{id,firstName,lastName}',
                'PARTIAL cc.{id,name,website}')
                ->join('s.client', 'c')
                ->join('c.user', 'cu')
                ->leftJoin('c.company', 'cc')
                ->andWhere('s.coach = :coach')
                ->setParameter('coach', $this->getCoachUser());
        }

        if ('csv' === $request->getRequestFormat()) {
            return array_map(function ($row) {
                $session = $row['session'];
                $user = $this->getUser();

                $result['Expertise'] = $session->getExpertise()->getName();
                $result['Started At'] = $user->withTimezone($session->getStartedAt());
                $result['Ended At'] = $user->withTimezone($session->getEndedAt());
                $result['Status'] = $session->getStatus();

                if ($this->isGranted('ROLE_CLIENT')) {
                    $user = $session->getCoach()->getUser();
                    $result['Coach Name'] = $user->getFirstName() . ' ' . $user->getLastName();
                } elseif ($this->isGranted('ROLE_COACH')) {
                    $user = $session->getClient()->getUser();
                    $result['Client Name'] = $user->getFirstName() . ' ' . $user->getLastName();
                    $result['Client Company'] = $session->getClient()->getCompanyName();
                }

                return $result;
            }, $qb->getQuery()->getResult());
        }

        return array_map(function ($row) {
            $session = $row['session'];
            $result['id'] = $session->getId();
            $result['expertise'] = $session->getExpertise()->getId();
            $result['started_at'] = $session->getStartedAt();
            $result['ended_at'] = $session->getEndedAt();
            $result['status'] = $session->getStatus();
            $result['rescheduled'] = $session->getRescheduled();

            if (SessionStatusEnum::BOOKED === $session->getStatus()) {
                $result['room_key'] = $session->getRoomKey();
                $result['tok_session'] = $session->getTokSession();
                if ($this->isGranted('ROLE_CLIENT')) {
                    $result['tok_token'] = $session->getTokClientToken();
                } elseif ($this->isGranted('ROLE_COACH')) {
                    $result['tok_token'] = $session->getTokCoachToken();
                }
            }
            if (SessionStatusEnum::COMPLETED === $session->getStatus()) {
                $feedbackForm = $this->isGranted('ROLE_COACH') ? 'SESSION_FEEDBACK_COACH_FORM' : 'SESSION_FEEDBACK_CLIENT_FORM';

                $questLocRep = $this->getDoctrine()->getManager()->getRepository('App:QuestionLocation');
                $questionLocation = $questLocRep->findLocationByValue($feedbackForm);

                if ($questionLocation) {
                    $queryBuilder = $this->getDoctrine()->getManager()->createQueryBuilder();

                    $queryBuilder->select('a.id')
                        ->from('App:Answers', 'a')
                        ->innerJoin('App:Question', 'q', 'WITH', 'a.question = q AND q.questionLocation = :location')
                        ->where('a.session = :session')
                        ->setParameter(':session', $session)
                        ->setParameter(':location', $questionLocation);
                    $feedbacks = $queryBuilder->getQuery()->getResult();

                    $result['feedback'] = (count($feedbacks) > 0);
                }
            }

            if ($this->isGranted('ROLE_CLIENT')) {
                $coach = $session->getCoach();
                $user = $coach->getUser();
                $result['coach'] = [
                    'id' => $coach->getId(),
                    'first_name' => $user->getFirstName(),
                    'last_name' => $user->getLastName(),
                ];
            } elseif ($this->isGranted('ROLE_COACH')) {
                $client = $session->getClient();
                $user = $client->getUser();
                $result['client'] = [
                    'id' => $client->getId(),
                    'first_name' => $user->getFirstName(),
                    'last_name' => $user->getLastName(),
                    'company' => ['name' => $client->getCompanyName()],
                ];
                if (null !== $company = $client->getCompany()) {
                    $result['client']['company']['name'] = $company->getName();
                    $result['client']['company']['website'] = $company->getWebsite();
                }
            }

            return $result;
        }, $qb->getQuery()->getResult());
    }

    /**
     * @Security("is_granted('ROLE_CLIENT')")
     * @Route("/sessions")
     * @Method("POST")
     */
    public function createAction(Request $request)
    {
        $sessionTypeList = [SessionTypeEnum::DEFAULT_];
        if ($this->isGranted('ROLE_ENTERPRISE_CLIENT')) {
            $sessionTypeList[] = SessionTypeEnum::EXECUTIVE;
        }

        $validator = $this->get(RequestValidator::class);
        $validator->validate($request->request, new Assert\Collection([
            'type' => new AppAssert\Chain([
                new Assert\Type('string'),
                new Assert\NotBlank(),
                new Assert\Choice([
                    'choices' => $sessionTypeList,
                    'strict' => true,
                ]),
            ]),
            'availability' => new AppAssert\Chain([
                new Assert\Type('int'),
                new Assert\NotNull(),
                new Assert\GreaterThan(0),
            ]),
            'started_at' => new AppAssert\Chain([
                new Assert\Type('string'),
                new Assert\NotBlank(),
                new Assert\DateTime(['format' => \DateTime::RFC3339]),
            ]),
            'expertise' => new AppAssert\Chain([
                new Assert\Type('int'),
                new Assert\NotNull(),
                new Assert\GreaterThan(0),
            ]),
            'credit_type' => new AppAssert\Chain([
                new Assert\Type('array'),
                new Assert\NotBlank(),
            ]),
            'details' => new AppAssert\Chain([
                new Assert\Type('string'),
                new Assert\NotBlank(),
                new Assert\Length(['max' => 5000]),
            ]),
            'background' => new AppAssert\Chain([
                new Assert\Type('string'),
                new Assert\NotBlank(),
                new Assert\Length(['max' => 5000]),
            ]),
            'extra_details' => new AppAssert\Chain([
                new Assert\Type('string'),
                new Assert\NotBlank(),
                new Assert\Length(['max' => 5000]),
            ]),
            'attachments' => new AppAssert\Chain([
                new Assert\Type('array'),
                new Assert\NotNull(),
                new AppAssert\Unique(),
                new Assert\All(new AppAssert\Chain([
                    new Assert\NotBlank(),
                    new Assert\Type('string'),
                    new Assert\Length(['max' => 255]),
                    new AppAssert\Url([
                        'checkDNS' => true,
                        'allowedHostNames' => $this->getParameter('static_hosts'),
                        'allowedContentTypes' => SessionAttachment::CONTENT_TYPES,
                    ]),
                ])),
            ]),
            'feedback' => new AppAssert\Chain([
                new Assert\Type('array'),
                new Assert\NotNull(),
            ]),
        ]));

        $client = $this->getClientUser(false);

        $em = $this->getDoctrine()->getManager();
        $noFeedbacks = $em->createQueryBuilder()
            ->select('COUNT(s)')
            ->from('App:Session', 's')
            ->leftJoin('App:Answers', 'sf', 'WITH', 'sf.session = s')
            ->where('s.client = :client')
            ->andWhere('s.status = :status')
            ->andWhere('sf.id IS NULL')
            ->setParameter('client', $client)
            ->setParameter('status', SessionStatusEnum::COMPLETED)
            ->getQuery()
            ->getSingleScalarResult();

        if ($noFeedbacks > 0) {
            throw new AccessDeniedHttpException('E_FEEDBACK_REQUIRED', null, ErrorEnum::E_FEEDBACK_REQUIRED);
        }

        $startedAt = new \DateTime($request->request->get('started_at'));
        if ($startedAt < new \DateTime('+' . Session::TIME_OFFSET . ' minutes')) {
            throw new BadRequestHttpException('E_INTERVAL_PAST', null, ErrorEnum::E_INTERVAL_PAST);
        }
        if (is_float($startedAt->getTimestamp() / (Session::INTERVAL * 60))) {
            throw new BadRequestHttpException('E_INTERVAL_INVALID', null, ErrorEnum::E_INTERVAL_INVALID);
        }

        $type = $request->request->get('type');
        $duration = Session::DURATION[$type];

        $endedAt = clone $startedAt;
        $endedAt->modify('+' . $duration . ' minutes');

        if ($em->getRepository('App:Session')->checkSessionClashed($startedAt, $endedAt, $client)) {
            throw new BadRequestHttpException('E_INTERVAL_CLASHED', null, ErrorEnum::E_INTERVAL_CLASHED);
        }

        $availability = $em->getRepository('App:CoachAvailability')
            ->getOneAvailabile($request->request->get('availability'), $startedAt, $endedAt);
        if (null === $availability) {
            throw new BadRequestHttpException('Invalid availability', null, ErrorEnum::E_BAD_REQUEST);
        }

        $coach = $availability->getCoach();
        if (!in_array($type, $coach->getSessionTypes())) {
            throw new BadRequestHttpException('Invalid coach', null, ErrorEnum::E_BAD_REQUEST);
        }

        $params = [
            'coach' => $coach,
            'expertise' => $request->request->get('expertise'),
        ];
        if (SessionTypeEnum::EXECUTIVE === $type) {
            $params['level'] = ExpertiseLevelEnum::EXPERT;
        }
        $expertise = $em->getRepository('App:CoachExpertise')->findOneBy($params);
        if (null === $expertise) {
            throw new BadRequestHttpException('Invalid expertise', null, ErrorEnum::E_BAD_REQUEST);
        }

        $creditCost = 0;
        $creditInfo = $request->request->get('credit_type');
        if (is_array($creditInfo) && isset($creditInfo['type'])) {
            $creditType = $creditInfo['type'];
            $creditCost = $creditInfo['value'];
        } else {
            $creditType = (is_string($creditInfo)) ? $creditInfo : null;
        }

        $totalCompanyCredits = 0;
        if ($creditType == 'company_credit') {
            $companyRepository = $em->getRepository('App:CompanyCredit');
            $companyCredits = $companyRepository->getAllAvailableCredits($client->getCompany());
            $totalCompanyCredits = (!empty($companyCredits)) ? array_sum($companyCredits) : 0;
        }

        //check the number of credits available to the company
        if (!$totalCompanyCredits || $totalCompanyCredits < $creditCost) {
            throw new PaymentRequiredHttpException('E_CREDIT_REQUIRED', null, ErrorEnum::E_CREDIT_REQUIRED);
        }

        $clientCredits = $em->createQueryBuilder()
            ->select('ct.type', 'SUM(c.value)')
            ->from('App:ClientCredit', 'c')
            ->innerJoin('App:CreditType', 'ct', 'WITH', 'c.creditType = ct.id')
            ->where('c.client = :client')
            ->andWhere('c.value > 0 AND c.expiresAt >= CURRENT_DATE()')
            ->setParameter('client', $client)
            ->groupBy('ct.type')
            ->getQuery()
            ->getResult('HYDRATE_KEY_PAIR');

        $totalClientCredits = (!empty($clientCredits)) ? array_sum($clientCredits) : 0;

        //check the number of credits available to the client
        if (!$totalClientCredits || $totalClientCredits < $creditCost) {
            throw new PaymentRequiredHttpException('E_CREDIT_REQUIRED', null, ErrorEnum::E_CREDIT_REQUIRED);
        }

        $em->beginTransaction();

        try {
            $availStartedAt = $availability->getStartedAt();
            $startedAtDiff = ($startedAt->getTimestamp() - $availStartedAt->getTimestamp()) / 60;
            if ($startedAtDiff - Session::INTERVAL >= Session::MIN_DURATION) {
                $availEndedAt = clone $startedAt;
                $availEndedAt->modify('-' . Session::INTERVAL . ' minutes');

                $availBefore = new CoachAvailability();
                $availBefore->setCoach($coach);
                $availBefore->setStartedAt($availStartedAt);
                $availBefore->setEndedAt($availEndedAt);
                $em->persist($availBefore);
            }

            $availEndedAt = $availability->getEndedAt();
            $endedAtDiff = ($availEndedAt->getTimestamp() - $endedAt->getTimestamp()) / 60;
            if ($endedAtDiff - Session::INTERVAL >= Session::MIN_DURATION) {
                $availStartedAt = clone $endedAt;
                $availStartedAt->modify('+' . Session::INTERVAL . ' minutes');

                $availAfter = new CoachAvailability();
                $availAfter->setCoach($coach);
                $availAfter->setStartedAt($availStartedAt);
                $availAfter->setEndedAt($availEndedAt);
                $em->persist($availAfter);
            }

            $session = new Session();
            $session->setClient($client);
            $session->setCoach($coach);
            $session->setAvailability($availability);
            $session->setExpertise($expertise->getExpertise());
            $session->setType($request->request->get('type'));
            $session->setStartedAt($startedAt);
            $session->setEndedAt($endedAt);
            $session->setCreditType(null);
            $session->setDetails($request->request->get('details'));
            $session->setBackground($request->request->get('background'));
            $session->setExtraDetails($request->request->get('extra_details'));
            $session->setRoomKey(RandomGenerator::randomString(32));
            $session->setStatus(SessionStatusEnum::BOOKED);

            $openTok = $this->get(OpenTokClient::class);
            $session->setTokSession($openTok->createSession()->getSessionId());

            foreach ($request->request->get('attachments') as $url) {
                $attachment = new SessionAttachment();
                $attachment->setSession($session);
                $attachment->setPath($url);
                $session->getAttachments()->add($attachment);
            }

            $availability->setStartedAt($startedAt);
            $availability->setEndedAt($endedAt);

            $em->persist($availability);
            $em->persist($session);


            $em->flush();

            if ($request->request->has('feedback')) {
                $feedbacks = $request->request->get('feedback');
                foreach ($feedbacks as $k => $data) {
                    $question = $em->getRepository('App:Question')->getQuestionByFieldName($k);
                    if (!$question) {
                        continue;
                    }

                    $answer = new Answers();
                    $answer
                        ->setSession($session)
                        ->setClient($client)
                        ->setCoach($coach)
                        ->setQuestion($question)
                        ->setResponse([$k => $data]);

                    $em->persist($answer);
                }
                $em->flush();
            }

            if ($creditCost) {
                //Client credit consumption
                $creditTypeName = $this->setClientCreditSpend($session, $creditCost);

                if (!$creditTypeName) {
                    throw new PaymentRequiredHttpException('E_CREDIT_REQUIRED', null, ErrorEnum::E_CREDIT_REQUIRED);
                }

                $session->setCreditType($creditTypeName);
                $em->persist($session);
                $em->flush();

                //Company credit consumption
                $this->setCompanyCreditSpend($session);
            }
        } catch (\Exception $e) {
            $em->rollback();
            var_dump($e->getMessage());
            return;
        }

        $em->commit();

        $dispatcher = $this->get('event_dispatcher');
        $mailer = $this->get(Mailer::class);

        $clientUser = $client->getUser();
        $coachUser = $coach->getUser();

        $eventStartedAt = $clientUser->withTimezone($session->getStartedAt());
        $timezone = $clientUser->getTimezoneFromDate($eventStartedAt);

        $params = [
            ERGenerator::ORGANIZER => getenv('MAILER_FROM'),
            ERGenerator::TO => [
                $clientUser->getEmail(),
            ],
            ERGenerator::TOPIC => "MyThrive Session",
            ERGenerator::SUMMARY => "MyThrive Session",
            ERGenerator::DESCRIPTION => $session->getExpertise()->getName() . " with " . $coachUser->getFirstName() . " " . $coachUser->getLastName(),
            ERGenerator::LOCATION => "Online",
            ERGenerator::DTSTART => $eventStartedAt,
            ERGenerator::DTEND => $clientUser->withTimezone($session->getEndedAt()),
            'uid' => $session->getId(),
            'first_name' => $clientUser->getFirstName(),
            'expertise_name' => $session->getExpertise()->getName(),
            'coach_first_name' => $coachUser->getFirstName(),
            'coach_last_name' => $coachUser->getLastName(),
            'started_at' => $eventStartedAt,
            'ended_at' => $clientUser->withTimezone($session->getEndedAt()),
            'ics_url' => $mailer->getSessionIcsUrl($session),
            'location' => ERGenerator::DEFAULT_SESSION_LOCATION,
            'current_timezone' => $timezone,
        ];

        $ERGenerator = $this->get(EventRequestGenerator::class);
        $clientMessage = $ERGenerator->createMessage($params, 'session/create/client.html.twig');
        $ERGenerator->send($clientMessage, $params);

        $eventStartedAt = $coachUser->withTimezone($session->getStartedAt());
        $timezone = $coachUser->getTimezoneFromDate($eventStartedAt);

        $params = [
            ERGenerator::ORGANIZER => getenv('MAILER_FROM'),
            ERGenerator::TO => [
                $coachUser->getEmail(),
            ],
            ERGenerator::TOPIC => "MyThrive Session",
            ERGenerator::SUMMARY => "MyThrive Session",
            ERGenerator::DESCRIPTION => $session->getExpertise()->getName() . " with " . $clientUser->getFirstName() . " " . $clientUser->getLastName(),
            ERGenerator::LOCATION => "Online",
            ERGenerator::DTSTART => $eventStartedAt,
            ERGenerator::DTEND => $coachUser->withTimezone($session->getEndedAt()),
            'uid' => $session->getId(),
            'first_name' => $coachUser->getFirstName(),
            'expertise_name' => $session->getExpertise()->getName(),
            'client_first_name' => $clientUser->getFirstName(),
            'client_last_name' => $clientUser->getLastName(),
            'client_position' => $client->getPosition(),
            'client_company_name' => $client->getCompanyName(),
            'started_at' => $eventStartedAt,
            'ended_at' => $coachUser->withTimezone($session->getEndedAt()),
            'ics_url' => '',
            'location' => ERGenerator::DEFAULT_SESSION_LOCATION,
            'current_timezone' => $timezone,

        ];

        $ERGenerator = $this->get(EventRequestGenerator::class);
        $coachMessage = $ERGenerator->createMessage($params, 'session/create/coach.html.twig');
        $ERGenerator->send($coachMessage, $params);

        return ['id' => $session->getId()];
    }

    /**
     * @Security("is_granted('view', session) or is_granted('ROLE_COACH') or is_granted('ROLE_ADMIN')")
     * @Route("/sessions/{id}", requirements={"id": "\d+"})
     * @Method("GET")
     */
    public function viewAction(Request $request, Session $session)
    {
        if ($request->query->getBoolean('status')) {
            return ['status' => $session->getStatus()];
        }

        $payload = [
            'type' => $session->getType(),
            'expertise' => $session->getExpertise()->getId(),
            'started_at' => $session->getStartedAt(),
            'ended_at' => $session->getEndedAt(),
            'credit_type' => $session->getCreditType(),
            'attachments' => array_map(function ($attachment) {
                return $attachment->getPath();
            }, $session->getAttachments()->getValues()),
            'status' => $session->getStatus(),
            'rescheduled' => $session->getRescheduled(),
            'created_at' => $session->getCreatedAt(),
            'ics_url' => $this->get(Mailer::class)->getSessionIcsUrl($session),
        ];

        $answerRepository = $this->getDoctrine()->getManager()->getRepository('App:Answers');
        $locationRepository = $this->getDoctrine()->getManager()->getRepository('App:QuestionLocation');
        $location = $locationRepository->findLocationByValue('SESSION_CREATE_CLIENT_FORM');

        $payload['sessionQuestions'] = null;
        $skipQuestionList = ['make_changes', 'recommended'];
        if ($location) {
            $answers = $answerRepository->getAnswerBySessionAndLocation($session, $location);
            foreach ($answers as $answer) {
                $question = $answer->getQuestion();

                if (!$question) {
                    continue;
                }

                $values = $question->getValues();
                $data = $answer->getResponse();
                if (isset($values['name']) && isset($data[$values['name']]) && !in_array($values['name'], $skipQuestionList)) {
                    $payload['sessionQuestions'][$values['name']] = [
                        'question' => $question->getLabel(),
                        'answer' => $data[$values['name']],
                    ];
                }
            }
        }

        if (SessionStatusEnum::BOOKED === $session->getStatus()) {
            $payload['room_key'] = $session->getRoomKey();
            $payload['tok_session'] = $session->getTokSession();
            if ($this->isGranted('ROLE_CLIENT')) {
                $payload['tok_token'] = $session->getTokClientToken();
            } elseif ($this->isGranted('ROLE_COACH')) {
                $payload['tok_token'] = $session->getTokCoachToken();
            }
        }
        if (SessionStatusEnum::COMPLETED === $session->getStatus()) {
            $em = $this->getDoctrine()->getManager();

            $questionLocationRepo = $em->getRepository('App:QuestionLocation');
            $questionRepo = $em->getRepository('App:Question');
            $answerRepo = $em->getRepository('App:Answers');
            $clientLocation = $questionLocationRepo->findLocationByValue('SESSION_FEEDBACK_CLIENT_FORM');
            $coachLocation = $questionLocationRepo->findLocationByValue('SESSION_FEEDBACK_COACH_FORM');

            $clientFeedback = null;
            if ($clientLocation) {
                $clientQuestion = $questionRepo->getFormByQuestionLocation($clientLocation);
                $clientAnswers = $answerRepo->getAnswerBySession($session);

                $clientFeedback = $this->prepareFeedbackData($clientQuestion, $clientAnswers);
            }

            if ($this->isGranted('ROLE_CLIENT')) {
                $payload['feedback'] = $clientFeedback;
            } else { // ROLE_COACH, ROLE_ADMIN
                $coachFeedback = null;
                if ($coachLocation) {
                    $coachQuestion = $questionRepo->getFormByQuestionLocation($coachLocation);
                    $coachAnswers = $answerRepo->getAnswerBySession($session);

                    $coachFeedback = $this->prepareFeedbackData($coachQuestion, $coachAnswers);
                }

                $payload['feedback'] = [
                    'client' => $clientFeedback,
                    'coach' => $coachFeedback,
                ];
            }
        }
        if ($this->isGranted('ROLE_CLIENT')) {
            $payload['notes'] = $session->getClientNotes();
            $client = $session->getClient();
            if (null !== $client->getCompany()) {
                $company = $client->getCompany();
                $payload['company']['name'] = $company->getName();
                $payload['company']['website'] = $company->getWebsite();
                $payload['company']['brief'] = $company->getBrief();
                $payload['company']['tok_record'] = $company->getTokRecord();
            }

        } elseif ($this->isGranted('ROLE_COACH')) {
            if ($session->getCoach()->getId() === $this->getCoachUser()->getId()) {
                $payload['notes'] = $session->getCoachNotes();
            } else {  // hide sensitive data from a different coach
                unset($payload['attachments'], $payload['room_key'],
                    $payload['tok_session'], $payload['tok_token']);
            }
        }

        $coach = $session->getCoach();
        $user = $coach->getUser();
        $payload['coach'] = [
            'id' => $coach->getId(),
            'first_name' => $user->getFirstName(),
            'last_name' => $user->getLastName(),
            'photo' => $user->getPhoto(),
            'mobile' => $coach->getMobileNumber(),
        ];


        if (!$this->isGranted('ROLE_CLIENT')) {
            $client = $session->getClient();
            $user = $client->getUser();
            $payload['client'] = [
                'id' => $client->getId(),
                'first_name' => $user->getFirstName(),
                'last_name' => $user->getLastName(),
                'photo' => $user->getPhoto(),
                'company' => ['name' => $client->getCompanyName()],
                'mobile' => $client->getMobileNumber(),
            ];
            if (null !== $client->getCompany()) {
                $company = $client->getCompany();
                $payload['client']['company']['name'] = $company->getName();
                $payload['client']['company']['website'] = $company->getWebsite();
                $payload['client']['company']['brief'] = $company->getBrief();
            }
        }

        return $payload;
    }

    /**
     * @Security("is_granted('update', session) or is_granted('ROLE_ADMIN')")
     * @Route("/sessions/{id}", requirements={"id": "\d+"})
     * @Method("PATCH")
     */
    public function updateAction(Request $request, Session $session)
    {
        $validator = $this->get(RequestValidator::class);
        $validator->validate($request->request, [
            new Assert\NotBlank(),
            new Assert\Collection([
                'allowMissingFields' => true,
                'fields' => [
                    'attachments' => new AppAssert\Chain([
                        new Assert\Type('array'),
                        new Assert\NotNull(),
                        new AppAssert\Unique(),
                        new Assert\All(new AppAssert\Chain([
                            new Assert\NotBlank(),
                            new Assert\Type('string'),
                            new Assert\Length(['max' => 255]),
                            new AppAssert\Url([
                                'checkDNS' => true,
                                'allowedHostNames' => $this->getParameter('static_hosts'),
                                'allowedContentTypes' => SessionAttachment::CONTENT_TYPES,
                            ]),
                        ])),
                    ]),
                ],
            ]),
        ]);

        if ($request->request->has('attachments')) {
            $session->getAttachments()->clear();
            foreach ($request->request->get('attachments') as $url) {
                $attachment = new SessionAttachment();
                $attachment->setSession($session);
                $attachment->setPath($url);
                $session->getAttachments()->add($attachment);
            }
        }

        $em = $this->getDoctrine()->getManager();
        $em->persist($session);
        $em->flush();

        return self::STATUS_OK;
    }

    /**
     * @Security("is_granted('ROLE_ADMIN')")
     * @Route("/sessions/{id}", requirements={"id": "\d+"})
     * @Method("DELETE")
     */
    public function deleteAction(Session $session)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($session);
        $em->flush();

        return self::STATUS_OK;
    }

    /**
     * @Security("is_granted('start', session)")
     * @Route("/sessions/{id}/start", requirements={"id": "\d+"})
     * @Method("POST")
     */
    public function startAction(Session $session)
    {
        if ($this->isGranted('ROLE_ENTERPRISE_CLIENT') || $this->isGranted('ROLE_CLIENT')) {
            $openTok = $this->get(OpenTokClient::class);
            $archive = $openTok->startArchive($session->getTokSession(), [
                'name' => 'Session #' . $session->getId(),
            ]);

            if (null !== $archive) {
                $recording = new Recording();
                $recording->setSession($session);
                $recording->setArchiveId($archive->id);

                $em = $this->getDoctrine()->getManager();
                $em->persist($recording);
                $em->flush();
            }
        }

        return self::STATUS_OK;
    }

    /**
     * Security("is_granted('complete', session)")
     * @Route("/sessions/{id}/complete", requirements={"id": "\d+"})
     * @Method("POST")
     */
    public function completeAction(Session $session)
    {
        $session->setStatus(SessionStatusEnum::COMPLETED);

        $em = $this->getDoctrine()->getManager();

        $em->persist($session);
        $em->flush();

        $client = $session->getClient();
        $coach = $session->getCoach();

        $dispatcher = $this->get('event_dispatcher');
        $mailer = $this->get(Mailer::class);

        $clientMessage = $mailer->create('session/complete/client.html.twig', [
            'first_name' => $client->getUser()->getFirstName(),
            'expertise_name' => $session->getExpertise()->getName(),
        ]);
        $clientNotification = new NotificationEvent($client->getUser());
        $clientNotification->setMessage($clientMessage);
        $clientNotification->setSecondaryEmail($client->getSecondaryEmail());
        $clientNotification->setMobileNumber($client->getMobileNumber());
        $dispatcher->dispatch(NotificationEvent::NAME, $clientNotification);

        $coachMessage = $mailer->create('session/complete/coach.html.twig', [
            'first_name' => $coach->getUser()->getFirstName(),
        ]);
        $coachNotification = new NotificationEvent($coach->getUser());
        $coachNotification->setMessage($coachMessage);
        $coachNotification->setSecondaryEmail($coach->getSecondaryEmail());
        $coachNotification->setMobileNumber($coach->getMobileNumber());
        $dispatcher->dispatch(NotificationEvent::NAME, $coachNotification);

        return self::STATUS_OK;
    }

    /**
     * @Security("is_granted('prompt', session)")
     * @Route("/sessions/{id}/prompt", requirements={"id": "\d+"})
     * @Method("POST")
     */
    public function promptAction(Session $session)
    {
        $dispatcher = $this->get('event_dispatcher');
        $mailer = $this->get(Mailer::class);

        if ($this->isGranted('ROLE_CLIENT')) {
            $coach = $session->getCoach();
            $coachUser = $coach->getUser();
            $message = $mailer->create('session/prompt/coach.html.twig', [
                'first_name' => $coachUser->getFirstName(),
            ]);
            $notification = new NotificationEvent($coachUser);
            $notification->setMessage($message);
            $notification->setSecondaryEmail($coach->getSecondaryEmail());
            $notification->setMobileNumber($coach->getMobileNumber());
            $dispatcher->dispatch(NotificationEvent::NAME, $notification);
        } elseif ($this->isGranted('ROLE_COACH')) {
            $client = $session->getClient();
            $clientUser = $client->getUser();
            $message = $mailer->create('session/prompt/client.html.twig', [
                'first_name' => $clientUser->getFirstName(),
            ]);
            $notification = new NotificationEvent($clientUser);
            $notification->setMessage($message);
            $notification->setSecondaryEmail($client->getSecondaryEmail());
            $notification->setMobileNumber($client->getMobileNumber());
            $dispatcher->dispatch(NotificationEvent::NAME, $notification);
        }

        return self::STATUS_OK;
    }

    /**
     * @Security("is_granted('cancel', session) or is_granted('ROLE_ADMIN')")
     * @Route("/sessions/{id}/cancel", requirements={"id": "\d+"})
     * @Method("POST")
     */
    public function cancelAction(Request $request, Session $session)
    {
        $refund = false;
        if ($this->isGranted('ROLE_ADMIN')) {
            $validator = $this->get(RequestValidator::class);
            $validator->validate($request->request, new Assert\Collection([
                'fields' => ['refund' => new AppAssert\Chain([
                    new Assert\Type('bool'),
                    new Assert\NotNull(),
                ])],
            ]));
            $refund = $request->request->get('refund');
        }

        $client = $session->getClient();
        $coach = $session->getCoach();

        $session->setAvailability(null);
        $session->setStatus(SessionStatusEnum::CANCELLED);

        if ($this->isGranted('ROLE_CLIENT')) {
            $session->setStatus(SessionStatusEnum::CLIENT_CANCELLED);
            if ($session->getStartedAt() > new \DateTime('+48 hours')) {
                $refund = true;
            }
        } elseif ($this->isGranted('ROLE_COACH')) {
            $session->setStatus(SessionStatusEnum::COACH_CANCELLED);
            $refund = true;
        }

        $em = $this->getDoctrine()->getManager();

        if ($refund) {
            $clientRepository = $this->getDoctrine()->getRepository('App:ClientCreditConsumption');
            $companyRepository = $this->getDoctrine()->getRepository('App:CompanyCreditConsumption');
            $clientCreditConsumption = $clientRepository->getCreditConsumptionBySessionId($session);
            $companyCreditConsumption = $companyRepository->getCreditConsumptionBySessionId($session);

            $creditCost = 1;
            if ($clientCreditConsumption !== null) {
                $creditCost = $clientCreditConsumption->getCreditCost();
                $em->remove($clientCreditConsumption);
            }

            $clientCredit = new ClientCredit();
            $clientCredit
                ->setClient($client)
                ->setType($session->getCreditType())
                ->setValue($creditCost)
                ->setRefund(true)
                ->setExpiresAt(new \DateTime(ClientCredit::REFUND_EXPIRY));
            $em->persist($clientCredit);

            $companyCreditCost = 1;
            if ($companyCreditConsumption !== null) {
                $companyCreditCost = $companyCreditConsumption->getCreditCost();
                $em->remove($companyCreditConsumption);
            }

            $companyCredit = new CompanyCredit();
            $companyCredit
                ->setCompany($client->getCompany())
                ->setType($session->getCreditType())
                ->setValue($companyCreditCost)
                ->setRefund(true)
                ->setExpiresAt(new \DateTime(CompanyCredit::REFUND_EXPIRY));
            $em->persist($companyCredit);
        }

        $em->persist($session);
        $em->flush();

        $dispatcher = $this->get('event_dispatcher');
        $mailer = $this->get(Mailer::class);

        $mailerClientView = 'session/cancel/client.html.twig';
        $mailerCoachView = 'session/cancel/coach.html.twig';
        if ($this->isGranted('ROLE_CLIENT')) {
            $mailerClientView = 'session/cancel/client/client.html.twig';
            $mailerCoachView = 'session/cancel/client/coach.html.twig';
        } elseif ($this->isGranted('ROLE_COACH')) {
            $mailerClientView = 'session/cancel/coach/client.html.twig';
            $mailerCoachView = 'session/cancel/coach/coach.html.twig';
        }

        $clientUser = $client->getUser();
        $coachUser = $coach->getUser();

        $calendarParams = [
            ERGenerator::ORGANIZER => getenv('MAILER_FROM'),
            ERGenerator::TO => [
                $clientUser->getEmail(),
            ],
            ERGenerator::TOPIC => "MyThrive Session",
            ERGenerator::SUMMARY => "MyThrive Session",
            ERGenerator::DESCRIPTION => $session->getExpertise()->getName() . " with " . $coachUser->getFirstName() . " " . $coachUser->getLastName(),
            ERGenerator::LOCATION => "Online",
            ERGenerator::DTSTART => $clientUser->withTimezone($session->getStartedAt()),
            ERGenerator::DTEND => $clientUser->withTimezone($session->getEndedAt()),

            'event_status' => 'CANCELLED',
            ERGenerator::METHOD => 'CANCEL',

            'uid' => $session->getId(),
            'first_name' => $clientUser->getFirstName(),
            'expertise_name' => $session->getExpertise()->getName(),
            'coach_first_name' => $coachUser->getFirstName(),
            'coach_last_name' => $coachUser->getLastName(),
            'started_at' => $clientUser->withTimezone($session->getStartedAt()),
        ];

        $ERGenerator = $this->get(EventRequestGenerator::class);
        $clientMessage = $ERGenerator->createMessage($calendarParams, $mailerClientView);
        $ERGenerator->send($clientMessage, $calendarParams);

        $calendarParams = [
            ERGenerator::ORGANIZER => getenv('MAILER_FROM'),
            ERGenerator::TO => [
                $coachUser->getEmail(),
            ],
            ERGenerator::TOPIC => "MyThrive Session",
            ERGenerator::SUMMARY => "MyThrive Session",
            ERGenerator::DESCRIPTION => $session->getExpertise()->getName() . " with " . $clientUser->getFirstName() . " " . $clientUser->getLastName(),
            ERGenerator::LOCATION => "Online",
            ERGenerator::DTSTART => $coachUser->withTimezone($session->getStartedAt()),
            ERGenerator::DTEND => $coachUser->withTimezone($session->getEndedAt()),

            'event_status' => 'CANCELLED',
            ERGenerator::METHOD => 'CANCEL',

            'uid' => $session->getId(),
            'first_name' => $coachUser->getFirstName(),
            'expertise_name' => $session->getExpertise()->getName(),
            'client_first_name' => $clientUser->getFirstName(),
            'client_last_name' => $clientUser->getLastName(),
            'client_position' => $client->getPosition(),
            'client_company_name' => $client->getCompanyName(),
            'started_at' => $coachUser->withTimezone($session->getStartedAt()),
        ];

        $ERGenerator = $this->get(EventRequestGenerator::class);
        $coachMessage = $ERGenerator->createMessage($calendarParams, $mailerCoachView);
        $ERGenerator->send($coachMessage, $calendarParams);

        if ($this->isGranted('ROLE_COACH')) {
            $mailer->sendToAdmin('session/cancel/coach/admin.html.twig', [
                'expertise_name' => $session->getExpertise()->getName(),
                'client_first_name' => $clientUser->getFirstName(),
                'client_last_name' => $clientUser->getLastName(),
                'client_position' => $client->getPosition(),
                'client_company_name' => $client->getCompanyName(),
                'coach_first_name' => $coachUser->getFirstName(),
                'coach_last_name' => $coachUser->getLastName(),
                'started_at' => $session->getStartedAt(),
            ]);

            if ($company = $client->getCompany()) {
                $mailer->sendToCompanyAdmin($company, 'session/cancel/coach/enterprise-admin.html.twig', [
                    'expertise_name' => $session->getExpertise()->getName(),
                    'client_first_name' => $clientUser->getFirstName(),
                    'client_last_name' => $clientUser->getLastName(),
                    'coach_first_name' => $coachUser->getFirstName(),
                    'coach_last_name' => $coachUser->getLastName(),
                    'started_at' => $session->getStartedAt(),
                ]);
            }
        }

        return self::STATUS_OK;
    }

    /**
     * @Security("is_granted('reschedule', session)")
     * @Route("/sessions/{id}/reschedule", requirements={"id": "\d+"})
     * @Method("POST")
     */
    public function rescheduleAction(Request $request, Session $session)
    {
        $validator = $this->get(RequestValidator::class);
        $validator->validate($request->request, new Assert\Collection([
            'availability' => new AppAssert\Chain([
                new Assert\Type('int'),
                new Assert\NotNull(),
                new Assert\GreaterThan(0),
            ]),
            'started_at' => new AppAssert\Chain([
                new Assert\Type('string'),
                new Assert\NotBlank(),
                new Assert\DateTime(['format' => \DateTime::RFC3339]),
            ]),
        ]));

        $startedAt = new \DateTime($request->request->get('started_at'));
        if ($startedAt < new \DateTime('+' . Session::TIME_OFFSET . ' minutes')) {
            throw new BadRequestHttpException('E_INTERVAL_PAST', null, ErrorEnum::E_INTERVAL_PAST);
        }
        if (is_float($startedAt->getTimestamp() / (Session::INTERVAL * 60))) {
            throw new BadRequestHttpException('E_INTERVAL_INVALID', null, ErrorEnum::E_INTERVAL_INVALID);
        }

        $client = $session->getClient();
        $coach = $session->getCoach();
        $duration = Session::DURATION[$session->getType()];

        $endedAt = clone $startedAt;
        $endedAt->modify('+' . $duration . ' minutes');

        $em = $this->getDoctrine()->getManager();
        if ($em->getRepository('App:Session')->checkSessionClashed($startedAt, $endedAt, $client)) {
            throw new BadRequestHttpException('E_INTERVAL_CLASHED', null, ErrorEnum::E_INTERVAL_CLASHED);
        }

        $availability = $em->getRepository('App:CoachAvailability')
            ->getOneAvailabile($request->request->get('availability'), $startedAt, $endedAt, $coach);
        if (null === $availability) {
            throw new BadRequestHttpException('Invalid availability', null, ErrorEnum::E_BAD_REQUEST);
        }

        $availStartedAt = $availability->getStartedAt();
        $startedAtDiff = ($startedAt->getTimestamp() - $availStartedAt->getTimestamp()) / 60;
        if ($startedAtDiff - Session::INTERVAL >= Session::MIN_DURATION) {
            $availEndedAt = clone $startedAt;
            $availEndedAt->modify('-' . Session::INTERVAL . ' minutes');

            $availBefore = new CoachAvailability();
            $availBefore->setCoach($coach);
            $availBefore->setStartedAt($availStartedAt);
            $availBefore->setEndedAt($availEndedAt);
            $em->persist($availBefore);
        }

        $availEndedAt = $availability->getEndedAt();
        $endedAtDiff = ($availEndedAt->getTimestamp() - $endedAt->getTimestamp()) / 60;
        if ($endedAtDiff - Session::INTERVAL >= Session::MIN_DURATION) {
            $availStartedAt = clone $endedAt;
            $availStartedAt->modify('+' . Session::INTERVAL . ' minutes');

            $availAfter = new CoachAvailability();
            $availAfter->setCoach($coach);
            $availAfter->setStartedAt($availStartedAt);
            $availAfter->setEndedAt($availEndedAt);
            $em->persist($availAfter);
        }

        $session->setRescheduled($session->getRescheduled() + 1);
        $session->setAvailability($availability);
        $session->setStartedAt($startedAt);
        $session->setEndedAt($endedAt);

        $availability->setStartedAt($startedAt);
        $availability->setEndedAt($endedAt);

        $em->persist($availability);
        $em->persist($session);
        $em->flush();

        $dispatcher = $this->get('event_dispatcher');
        $mailer = $this->get(Mailer::class);

        $clientUser = $client->getUser();
        $coachUser = $coach->getUser();

        $clientMessage = $mailer->create('session/reschedule/client.html.twig', [
            'first_name' => $clientUser->getFirstName(),
            'expertise_name' => $session->getExpertise()->getName(),
            'coach_first_name' => $coachUser->getFirstName(),
            'coach_last_name' => $coachUser->getLastName(),
            'started_at' => $clientUser->withTimezone($session->getStartedAt()),
            'ended_at' => $clientUser->withTimezone($session->getEndedAt()),
            'ics_url' => $mailer->getSessionIcsUrl($session),
        ]);
        $clientNotification = new NotificationEvent($clientUser);
        $clientNotification->setMessage($clientMessage);
        $clientNotification->setSecondaryEmail($client->getSecondaryEmail());
        $clientNotification->setMobileNumber($client->getMobileNumber());
        $dispatcher->dispatch(NotificationEvent::NAME, $clientNotification);

        $coachMessage = $mailer->create('session/reschedule/coach.html.twig', [
            'first_name' => $coachUser->getFirstName(),
            'expertise_name' => $session->getExpertise()->getName(),
            'client_first_name' => $clientUser->getFirstName(),
            'client_last_name' => $clientUser->getLastName(),
            'client_position' => $client->getPosition(),
            'client_company_name' => $client->getCompanyName(),
            'started_at' => $coachUser->withTimezone($session->getStartedAt()),
            'ended_at' => $coachUser->withTimezone($session->getEndedAt()),
            'ics_url' => $mailer->getSessionIcsUrl($session),
        ]);
        $coachNotification = new NotificationEvent($coachUser);
        $coachNotification->setMessage($coachMessage);
        $coachNotification->setSecondaryEmail($coach->getSecondaryEmail());
        $coachNotification->setMobileNumber($coach->getMobileNumber());
        $dispatcher->dispatch(NotificationEvent::NAME, $coachNotification);

        return self::STATUS_OK;
    }

    /**
     * @Security("is_granted('abandon', session)")
     * @Route("/sessions/{id}/abandon", requirements={"id": "\d+"})
     * @Method("POST")
     */
    public function abandonAction(Session $session)
    {
        if ($this->isGranted('ROLE_CLIENT')) {
            $session->setStatus(SessionStatusEnum::COACH_NOT_ATTENDED);
        } elseif ($this->isGranted('ROLE_COACH')) {
            $session->setStatus(SessionStatusEnum::CLIENT_NOT_ATTENDED);
        }

        $em = $this->getDoctrine()->getManager();
        $em->persist($session);
        $em->flush();

        $client = $session->getClient();
        $coach = $session->getCoach();

        $clientUser = $client->getUser();
        $coachUser = $coach->getUser();

        $dispatcher = $this->get('event_dispatcher');
        $mailer = $this->get(Mailer::class);

        if ($this->isGranted('ROLE_CLIENT')) {
            $clientMessage = $mailer->create('session/abandon/client/client.html.twig', [
                'first_name' => $clientUser->getFirstName(),
                'expertise_name' => $session->getExpertise()->getName(),
                'started_at' => $clientUser->withTimezone($session->getStartedAt()),
            ]);
            $clientNotification = new NotificationEvent($clientUser);
            $clientNotification->setMessage($clientMessage);
            $clientNotification->setSecondaryEmail($client->getSecondaryEmail());
            $clientNotification->setMobileNumber($client->getMobileNumber());
            $dispatcher->dispatch(NotificationEvent::NAME, $clientNotification);

            $coachMessage = $mailer->create('session/abandon/client/coach.html.twig', [
                'first_name' => $coachUser->getFirstName(),
                'expertise_name' => $session->getExpertise()->getName(),
                'client_first_name' => $clientUser->getFirstName(),
                'client_last_name' => $clientUser->getLastName(),
                'client_position' => $client->getPosition(),
                'client_company_name' => $client->getCompanyName(),
                'started_at' => $coachUser->withTimezone($session->getStartedAt()),
            ]);
            $coachNotification = new NotificationEvent($coachUser);
            $coachNotification->setMessage($coachMessage);
            $coachNotification->setSecondaryEmail($coach->getSecondaryEmail());
            $coachNotification->setMobileNumber($coach->getMobileNumber());
            $dispatcher->dispatch(NotificationEvent::NAME, $coachNotification);

            $mailer->sendToAdmin('session/abandon/client/admin.html.twig', [
                'expertise_name' => $session->getExpertise()->getName(),
                'client_first_name' => $clientUser->getFirstName(),
                'client_last_name' => $clientUser->getLastName(),
                'client_position' => $client->getPosition(),
                'client_company_name' => $client->getCompanyName(),
                'coach_first_name' => $coachUser->getFirstName(),
                'coach_last_name' => $coachUser->getLastName(),
                'started_at' => $session->getStartedAt(),
            ]);
        } elseif ($this->isGranted('ROLE_COACH')) {
            $clientMessage = $mailer->create('session/abandon/coach/client.html.twig', [
                'first_name' => $clientUser->getFirstName(),
                'expertise_name' => $session->getExpertise()->getName(),
                'coach_first_name' => $coachUser->getFirstName(),
                'coach_last_name' => $coachUser->getLastName(),
                'started_at' => $clientUser->withTimezone($session->getStartedAt()),
            ]);
            $clientNotification = new NotificationEvent($clientUser);
            $clientNotification->setMessage($clientMessage);
            $clientNotification->setSecondaryEmail($client->getSecondaryEmail());
            $clientNotification->setMobileNumber($client->getMobileNumber());
            $dispatcher->dispatch(NotificationEvent::NAME, $clientNotification);

            if ($company = $client->getCompany()) {
                $mailer->sendToCompanyAdmin($company, 'session/abandon/coach/enterprise-admin.html.twig', [
                    'expertise_name' => $session->getExpertise()->getName(),
                    'client_first_name' => $clientUser->getFirstName(),
                    'client_last_name' => $clientUser->getLastName(),
                    'client_position' => $client->getPosition(),
                    'client_company_name' => $client->getCompanyName(),
                    'started_at' => $session->getStartedAt(),
                ]);
            }
        }

        return self::STATUS_OK;
    }

    /**
     * @Security("is_granted('notes', session)")
     * @Route("/sessions/{id}/notes", requirements={"id": "\d+"})
     * @Method("POST")
     */
    public function notesAction(Request $request, Session $session)
    {
        $validator = $this->get(RequestValidator::class);
        $validator->validate($request->request, new Assert\Collection([
            'notes' => new AppAssert\Chain([
                new Assert\Type('string'),
                new Assert\NotIdenticalTo(''),
                new Assert\Length(['max' => 5000]),
            ]),
        ]));

        if ($this->isGranted('ROLE_CLIENT')) {
            $session->setClientNotes($request->request->get('notes'));
        } elseif ($this->isGranted('ROLE_COACH')) {
            $session->setCoachNotes($request->request->get('notes'));
        }

        $em = $this->getDoctrine()->getManager();
        $em->persist($session);
        $em->flush();

        return self::STATUS_OK;
    }

    /**
     * @Security("is_granted('feedback', session)")
     * @Route("/sessions/{id}/feedback", requirements={"id": "\d+"})
     * @Method("POST")
     */
    public function feedbackAction(Request $request, Session $session)
    {
        if ($this->isGranted('ROLE_CLIENT')) {
            $reader = $this->get(DataReader::class);
            $putIntoPracticeList = $reader->read('session/put-into-practice.json');
            $changedViewList = $reader->read('session/changed-view.json');

            $validator = $this->get(RequestValidator::class);
            $validator->validate($request->request, new Assert\Collection([
                'satisfied_session' => new AppAssert\Chain([
                    new Assert\Type('int'),
                    new Assert\NotNull(),
                    new Assert\Range(['min' => 1, 'max' => 10]),
                ]),
                'satisfied_coach' => new AppAssert\Chain([
                    new Assert\Type('int'),
                    new Assert\NotNull(),
                    new Assert\Range(['min' => 1, 'max' => 10]),
                ]),
                'recommend_coaching' => new AppAssert\Chain([
                    new Assert\Type('int'),
                    new Assert\NotNull(),
                    new Assert\Range(['min' => 1, 'max' => 10]),
                ]),
                'put_into_practice' => new AppAssert\Chain([
                    new Assert\Type('string'),
                    new Assert\NotBlank(),
                    new Assert\Choice([
                        'choices' => $putIntoPracticeList,
                        'strict' => true,
                    ]),
                ]),
                'changed_view' => new AppAssert\Chain([
                    new Assert\Type('array'),
                    new Assert\NotNull(),
                    new Assert\Count(['min' => 1]),
                    new AppAssert\Unique(),
                    new Assert\All(new AppAssert\Chain([
                        new Assert\Type('string'),
                        new Assert\NotBlank(),
                        new Assert\Choice([
                            'choices' => $changedViewList,
                            'strict' => true,
                        ]),
                    ])),
                ]),
            ]));

            $em = $this->getDoctrine()->getManager();
            if ($em->getRepository('App:SessionClientFeedback')->checkFeedbackExists($session)) {
                throw new ConflictHttpException('E_FEEDBACK_EXISTS', null, ErrorEnum::E_FEEDBACK_EXISTS);
            }

            $satisfiedCoach = $request->request->get('satisfied_coach');

            $feedback = new SessionClientFeedback();
            $feedback->setSession($session);
            $feedback->setSatisfiedSession($request->request->get('satisfied_session'));
            $feedback->setSatisfiedCoach($satisfiedCoach);
            $feedback->setRecommendCoaching($request->request->get('recommend_coaching'));
            $feedback->setPutIntoPractice($request->request->get('put_into_practice'));
            foreach ($request->request->get('changed_view') as $name) {
                $changedView = new SessionClientFeedbackChange();
                $changedView->setSessionClientFeedback($feedback);
                $changedView->setName($name);
                $feedback->getChangedView()->add($changedView);
            }

            $em->persist($feedback);
            $em->flush();

            if ($satisfiedCoach < 9) {
                $client = $session->getClient();
                $coach = $session->getCoach();

                $mailer = $this->get(Mailer::class);
                $mailer->sendToAdmin('session/feedback/admin.html.twig', [
                    'expertise_name' => $session->getExpertise()->getName(),
                    'client_first_name' => $client->getUser()->getFirstName(),
                    'client_last_name' => $client->getUser()->getLastName(),
                    'client_position' => $client->getPosition(),
                    'client_company_name' => $client->getCompanyName(),
                    'coach_first_name' => $coach->getUser()->getFirstName(),
                    'coach_last_name' => $coach->getUser()->getLastName(),
                    'started_at' => $session->getStartedAt(),
                ]);
            }
        } elseif ($this->isGranted('ROLE_COACH')) {
            $validator = $this->get(RequestValidator::class);
            $validator->validate($request->request, new Assert\Collection([
                'reason' => new AppAssert\Chain([
                    new Assert\Type('string'),
                    new Assert\NotBlank(),
                    new Assert\Length(['max' => 250]),
                ]),
                'client_relationship' => new AppAssert\Chain([
                    new Assert\Type('string'),
                    new Assert\NotBlank(),
                    new Assert\Length(['max' => 250]),
                ]),
                'what_right' => new AppAssert\Chain([
                    new Assert\Type('string'),
                    new Assert\NotIdenticalTo(''),
                    new Assert\Length(['max' => 500]),
                ]),
                'key_people' => new AppAssert\Chain([
                    new Assert\Type('string'),
                    new Assert\NotIdenticalTo(''),
                    new Assert\Length(['max' => 500]),
                ]),
                'key_processes' => new AppAssert\Chain([
                    new Assert\Type('string'),
                    new Assert\NotIdenticalTo(''),
                    new Assert\Length(['max' => 500]),
                ]),
                'key_communication' => new AppAssert\Chain([
                    new Assert\Type('string'),
                    new Assert\NotIdenticalTo(''),
                    new Assert\Length(['max' => 500]),
                ]),
                'advice' => new AppAssert\Chain([
                    new Assert\Type('string'),
                    new Assert\NotIdenticalTo(''),
                    new Assert\Length(['max' => 500]),
                ]),
                'feel_about_company' => new AppAssert\Chain([
                    new Assert\Type('array'),
                    new Assert\NotNull(),
                    new Assert\Count(['min' => 3, 'max' => 3]),
                    new Assert\All(new AppAssert\Chain([
                        new Assert\Type('string'),
                        new Assert\NotBlank(),
                        new Assert\Length(['max' => 32]),
                    ])),
                ]),
                'fix_first' => new AppAssert\Chain([
                    new Assert\Type('string'),
                    new Assert\NotBlank(),
                    new Assert\Length(['max' => 500]),
                ]),
                'topics' => new AppAssert\Chain([
                    new Assert\Type('string'),
                    new Assert\NotBlank(),
                    new Assert\Length(['max' => 500]),
                ]),
                'shared_tools' => new AppAssert\Chain([
                    new Assert\Type('string'),
                    new Assert\NotBlank(),
                    new Assert\Length(['max' => 500]),
                ]),
                'next_steps' => new AppAssert\Chain([
                    new Assert\Type('string'),
                    new Assert\NotBlank(),
                    new Assert\Length(['max' => 500]),
                ]),
            ]));

            $em = $this->getDoctrine()->getManager();
            if ($em->getRepository('App:SessionCoachFeedback')->checkFeedbackExists($session)) {
                throw new ConflictHttpException('E_FEEDBACK_EXISTS', null, ErrorEnum::E_FEEDBACK_EXISTS);
            }

            $feedback = new SessionCoachFeedback();
            $feedback->setSession($session);
            $feedback->setReason($request->request->get('reason'));
            $feedback->setClientRelationship($request->request->get('client_relationship'));
            $feedback->setWhatRight($request->request->get('what_right'));
            $feedback->setKeyPeople($request->request->get('key_people'));
            $feedback->setKeyProcesses($request->request->get('key_processes'));
            $feedback->setKeyCommunication($request->request->get('key_communication'));
            $feedback->setAdvice($request->request->get('advice'));
            $feedback->setFeelAboutCompany($request->request->get('feel_about_company'));
            $feedback->setFixFirst($request->request->get('fix_first'));
            $feedback->setTopics($request->request->get('topics'));
            $feedback->setSharedTools($request->request->get('shared_tools'));
            $feedback->setNextSteps($request->request->get('next_steps'));

            $em->persist($feedback);
            $em->flush();
        }

        return self::STATUS_OK;
    }

    /**
     * @Security("is_granted('ROLE_CLIENT')")
     * @Route("/sessions/survey")
     * @Method("POST")
     */
    public function surveyAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $session = $em->createQueryBuilder()
            ->select('PARTIAL s.{id}')
            ->from('App:Session', 's')
            ->where('s.client = :client')
            ->andWhere('s.status = :status')
            ->setParameter('client', $this->getClientUser())
            ->setParameter('status', SessionStatusEnum::COMPLETED)
            ->orderBy('s.endedAt', 'DESC')
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();

        if (null === $session) {
            throw new AccessDeniedHttpException('E_COMPLETED_SESSION_REQUIRED',
                null, ErrorEnum::E_COMPLETED_SESSION_REQUIRED);
        }

        $validator = $this->get(RequestValidator::class);
        $validator->validate($request->request, new Assert\Collection([
            'has_success' => new AppAssert\Chain([
                new Assert\Type('bool'),
                new Assert\NotNull(),
            ]),
            'did_recommend' => new AppAssert\Chain([
                new Assert\Type('bool'),
                new Assert\NotNull(),
            ]),
        ]));

        $survey = new SessionSurvey();
        $survey->setSession($session);
        $survey->setHasSuccess($request->request->get('has_success'));
        $survey->setDidRecommend($request->request->get('did_recommend'));

        $em->persist($survey);
        $em->flush();

        return self::STATUS_OK;
    }

    /**
     * @Security("is_granted('ROLE_ADMIN')")
     * @Route("/sessions/updatestatus")
     * @Method("POST")
     */
    public function updatestatusAction(Request $request)
    {
        $validator = $this->get(RequestValidator::class);
        $validator->validate($request->request, new Assert\Collection([
            'id' => new AppAssert\Chain([
                new Assert\Type('string'),
                new Assert\NotNull(),
            ]),
            'status' => new AppAssert\Chain([
                new Assert\Type('string'),
                new Assert\NotBlank(),
                new Assert\Length(['max' => 250]),
            ]),
        ]));


        $em = $this->getDoctrine()->getManager();
        $count = $em->createQueryBuilder()
            ->update('App:Session', 's')
            ->set('s.status', ':new_status')
            ->where('s.id = :id')
            ->setParameter('id', (int)$request->request->get('id'))
            ->setParameter('new_status', $request->request->get('status'))
            ->getQuery()
            ->execute();

        return $count ? self::STATUS_OK : ['status' => 'failed'];
    }

    /**
     * @Security("is_granted('ROLE_CLIENT')")
     * @Route("/sessions/availableLength")
     * @Method("GET")
     */
    public function sessionLengthAction(Request $request)
    {
        $availableSessionLength = [];
        $client = $this->getClientUser();
        $clientRepository = $this->getDoctrine()->getRepository('App:ClientCredit');
        $companyRepository = $this->getDoctrine()->getRepository('App:CompanyCredit');
        $creditTypeRepository = $this->getDoctrine()->getRepository('App:CreditType');
        $clientCreditTypes = $creditTypeRepository->getAllClientTypes();
        $clientCredits = $clientRepository->getPreviewCreditByClient($client, $clientCreditTypes);
        $companyCredits = $companyRepository->getPreviewCreditByCompany($client->getCompany(), $clientCreditTypes); //need change to $companyCreditType

        $company = $client->getCompany();
        $sessionRestricted = $noCredits = null;
        if ($company) {
            $sessionLength = $company->getAvailableSessionLength();
            asort($sessionLength);
            if (!empty($clientCredits) && !empty($sessionLength)) {
                $clientCreditAmount = array_sum($clientCredits);
                $companyCreditAmount = array_sum($companyCredits);

                if ($companyCreditAmount < 1) {
                    $noCredits = 'Your organisation has no credits please contact your administrator';
                } else {
                    foreach ($sessionLength as $sessionK => $sessionV) {
                        if ($clientCreditAmount >= $sessionV) {
                            $time = intval($sessionV / 2);
                            $time .= (($sessionV % 2) == 0) ? ':00' : ':30';
                            if ($sessionK === '05') {
                                $label = $sessionV . ' credit (length 30 min)';
                            } else {
                                $label = $sessionV . ' credits (length ' . $time . ' )';
                            }
                            if (isset($companyCreditAmount) && $sessionV <= $companyCreditAmount) {
                                $availableSessionLength[] = [
                                    'label' => $label,
                                    'type' => 'company_credit',
                                    'value' => $sessionV,
                                ];
                            }
                        }

                        if ($sessionV > $companyCreditAmount) {
                            $sessionRestricted = 'Your length of session has been restricted due to the credits your organisation has available';
                        }
                    }
                }
            }
        } else {
            //If a Client doesn’t belong to an Enterprise it will default to 30 minutes
            if (!empty($clientCredits)) {
                foreach ($clientCredits as $ck => $cv) {
                    $availableSessionLength[] = [
                        'label' => ucfirst(strtolower($ck)) . ' 1 credit (length 30 min)',
                        'type' => $ck,
                        'value' => 1,
                    ];
                }
            }
        }
        return [
            'data' => $availableSessionLength,
            'session_restricted' => $sessionRestricted,
            'no_credits' => $noCredits,
        ];
    }

    private function setClientCreditSpend($session, $creditCost)
    {
        $em = $this->getDoctrine()->getManager();

        $em->beginTransaction();

        try {
            $creditType = '';
            //Client consumption
            if ($session) {
                $client = $session->getClient();

                $repository = $em->getRepository('App:ClientCredit');
                $credits = $repository->getAvailableCredits($client);

                if (array_sum(array_column($credits, 'value')) < $creditCost) {
                    throw new PaymentRequiredHttpException('E_CREDIT_REQUIRED', null, ErrorEnum::E_CREDIT_REQUIRED);
                }

                $index = 0;
                $updated = [];
                $pending = $creditCost;

                while ($pending) {
                    $credit = $credits[$index];
                    if ($credit['value'] >= $pending) {
                        $credits[$index]['value'] -= $pending;
                        $pending = 0;
                    } else {
                        $pending -= $credit['value'];
                        $credits[$index]['value'] = 0;
                    }
                    if (0 === $updated[$credit['id']] = $credits[$index]['value']) {
                        ++$index; // move to next company credit record
                    }

                    if (isset($credit['credit_type'])) {
                        $creditType = $credit['credit_type'];
                    }
                }

                if ($deleted = array_keys($updated, 0, true)) {
                    $updated = array_diff_key($updated, array_flip($deleted));
                    $em->createQueryBuilder()
                        ->delete('App:ClientCredit', 'c')
                        ->where('c IN (:deleted)')
                        ->setParameter('deleted', $deleted)
                        ->getQuery()
                        ->execute();
                }

                foreach ($updated as $id => $credit) {
                    $em->createQuery('UPDATE App:ClientCredit c SET c.value = ?0 WHERE c.id = ?1')
                        ->setParameter(0, $credit)
                        ->setParameter(1, $id)
                        ->execute();
                }

                $clientCreditConsumption = new ClientCreditConsumption();
                $clientCreditConsumption
                    ->setSession($session)
                    ->setClient($client)
                    ->setCreditCost($creditCost);
                $em->persist($clientCreditConsumption);

                $em->flush();
            }
        } catch (\Exception $exception) {
            $em->rollback();
            var_dump($exception->getMessage());
            return false;
        }
        $em->commit();
        return $creditType;
    }

    private function setCompanyCreditSpend($session)
    {
        $em = $this->getDoctrine()->getManager();

        $em->beginTransaction();

        try {
            //Company consumption
            $repository = $this->getDoctrine()->getRepository('App:ClientCreditConsumption');
            $clientCreditConsumption = $repository->getCreditConsumptionBySessionId($session);

            if ($clientCreditConsumption !== null) {
                $client = $session->getClient();
                $company = $client ? $client->getCompany() : null;

                $repository = $em->getRepository('App:CompanyCredit');
                $credits = $repository->getCredits($company);
                if (array_sum(array_column($credits, 'value')) < $clientCreditConsumption->getCreditCost()) {
                    throw new PaymentRequiredHttpException('E_CREDIT_REQUIRED', null, ErrorEnum::E_CREDIT_REQUIRED);
                }

                $index = 0;
                $updated = [];
                $pending = $clientCreditConsumption->getCreditCost();

                while ($pending) {
                    $credit = $credits[$index];
                    if ($credit['value'] >= $pending) {
                        $credits[$index]['value'] -= $pending;
                        $pending = 0;
                    } else {
                        $pending -= $credit['value'];
                        $credits[$index]['value'] = 0;
                    }
                    if (0 === $updated[$credit['id']] = $credits[$index]['value']) {
                        ++$index; // move to next company credit record
                    }
                }

                if ($deleted = array_keys($updated, 0, true)) {
                    $updated = array_diff_key($updated, array_flip($deleted));
                    $em->createQueryBuilder()
                        ->delete('App:CompanyCredit', 'c')
                        ->where('c IN (:deleted)')
                        ->setParameter('deleted', $deleted)
                        ->getQuery()
                        ->execute();
                }

                foreach ($updated as $id => $credit) {
                    $em->createQuery('UPDATE App:CompanyCredit c SET c.value = ?0 WHERE c.id = ?1')
                        ->setParameter(0, $credit)
                        ->setParameter(1, $id)
                        ->execute();
                }

                $companyCreditConsumption = new CompanyCreditConsumption();
                $companyCreditConsumption
                    ->setSession($session)
                    ->setClient($client)
                    ->setCompany($client->getCompany())
                    ->setCreditCost($clientCreditConsumption->getCreditCost());
                $em->persist($companyCreditConsumption);

                $em->flush();
            }
        } catch (\Exception $exception) {
            $em->rollback();
            var_dump($exception->getMessage());
            return false;
        }
        $em->commit();
        return true;
    }

    private function prepareFeedbackData(array $questions, array $answers)
    {
        $result = null;
        foreach ($questions as $question) {
            $values = $question->getValues();
            foreach ($answers as $answer) {
                $response = $answer->getResponse();
                if (isset($values['name']) && isset($response[$values['name']])) {
                    $answer = $response[$values['name']];
                    if ($question->getAnswerType() == AnswerTypeEnum::TYPE_RADIO_BUTTON_RATING) {
                        $countRange = (isset($values['range'])) ? count($values['range']) : '';
                        $answer .= ($countRange) ? '/' . $countRange : '';
                    }

                    $result[$values['name']] = [
                        'question' => $question->getLabel(),
                        'answer' => $answer,
                    ];
                }
            }
        }
        return $result;
    }
}
