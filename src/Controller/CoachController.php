<?php

namespace App\Controller;

use App\Entity\Coach;
use App\Entity\Session;
use App\Enum\ErrorEnum;
use App\Enum\SessionTypeEnum;
use App\Utility\RequestValidator;
use App\Validator\Constraints as AppAssert;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\QueryBuilder;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\ConflictHttpException;
use Symfony\Component\Validator\Constraints as Assert;

class CoachController extends AbstractController
{
    use Traits\UserTrait;
    use Traits\CoachTrait;

    /**
     * @Security("is_granted('ROLE_ADMIN')")
     * @Route("/coaches")
     * @Method("GET")
     */
    public function indexAction(Request $request)
    {
        $sortFields = ['name', 'rating'];

        $validator = $this->get(RequestValidator::class);
        $validator->validate($request->query, new Assert\Collection([
            'allowMissingFields' => true,
            'fields' => [
                'session_type' => [
                    new Assert\NotBlank(),
                    new Assert\Choice([
                        'choices' => SessionTypeEnum::values(),
                        'strict' => true,
                    ]),
                ],
                'is_verified' => [
                    new Assert\NotBlank(),
                    new AppAssert\Boolean(),
                ],
                'is_enabled' => [
                    new Assert\NotBlank(),
                    new AppAssert\Boolean(),
                ],
                'search' => [
                    new Assert\NotBlank(),
                    new Assert\Length(['min' => 3]),
                ],
                'offset' => [
                    new Assert\NotBlank(),
                    new Assert\Range(['min' => 0]),
                ],
                'limit' => [
                    new Assert\NotBlank(),
                    new Assert\Range(['min' => 1, 'max' => 500]),
                ],
                'sort' => new AppAssert\Chain([
                    new Assert\Type('array'),
                    new Assert\Collection([
                        'allowMissingFields' => true,
                        'fields' => array_fill_keys($sortFields, [
                            new Assert\NotBlank(),
                            new Assert\Choice([
                                'choices' => ['ASC', 'DESC'],
                                'strict' => true,
                            ]),
                        ]),
                    ]),
                ]),
            ],
        ]));

        $isEnabled = $request->query->getBoolean('is_enabled', true);

        $em = $this->getDoctrine()->getManager();
        $qb = $em->createQueryBuilder()
            ->select(
                'c.id',
                'u.username email',
                'u.firstName first_name',
                'u.lastName last_name',
                "CONCAT(u.firstName, ' ', u.lastName) HIDDEN name",
                'u.photo',
                'c.mobileNumber mobile_number',
                'c.sessionTypes session_types',
                'AVG(sf.satisfiedCoach) rating',
                'u.isVerified is_verified',
                'u.lastAuthAt last_auth_at',
                'u.createdAt created_at')
            ->from('App:Coach', 'c')
            ->join('c.user', 'u')
            ->leftJoin('c.sessions', 's')
            ->leftJoin('App:SessionClientFeedback', 'sf', 'WITH', 'sf.session = s')
            ->where('u.isEnabled = :is_enabled')
            ->setParameter('is_enabled', $isEnabled)
            ->groupBy('c.id', 'u.id');

        if ($sessionType = $request->query->get('session_type')) {
            $qb->andWhere(':session_type = ANY_OF(STRING_TO_ARRAY(c.sessionTypes))')
                ->setParameter('session_type', $sessionType);
        }

        if ($request->query->has('is_verified')) {
            $qb->andWhere('u.isVerified = :is_verified')
                ->setParameter('is_verified', $request->query->getBoolean('is_verified'));
        }

        if ($search = $request->query->get('search')) {
            $qb->andWhere('LOWER(u.firstName) LIKE LOWER(:search)')
                ->setParameter('search', $search.'%');
        }

        $qbId = clone $qb;
        $qbId->select('c.id');

        $total = $em->createQueryBuilder()
            ->select('COUNT(c0)')
            ->from('App:Coach', 'c0')
            ->where('c0 IN ('.$qbId->getDql().')')
            ->setParameters($qbId->getParameters())
            ->getQuery()
            ->getSingleScalarResult();

        $sort = $request->query->get('sort', ['name' => 'ASC']);
        foreach ($sort as $field => $order) {
            $qb->addOrderBy($field, $order);
        }

        $offset = $request->query->getInt('offset');
        $limit = $request->query->getInt('limit', 20);
        $qb->setFirstResult($offset)->setMaxResults($limit);

        $result = $qb->getQuery()->getArrayResult();

        return [
            'result' => $result,
            'total' => $total,
        ];
    }

    /**
     * @Security("is_granted('ROLE_CLIENT') or is_granted('ROLE_COACH')")
     * @Route("/coaches/search")
     * @Method("GET")
     */
    public function searchAction(Request $request)
    {
        $collection = [
            'date' => new AppAssert\Chain([
                new Assert\Type('array'),
                new Assert\Collection([
                    'start' => [
                        new Assert\NotBlank(),
                        new Assert\DateTime(['format' => \DateTime::RFC3339]),
                    ],
                    'end' => [
                        new Assert\NotBlank(),
                        new Assert\DateTime(['format' => \DateTime::RFC3339]),
                    ],
                ]),
            ]),
            'language' => new AppAssert\Chain([
                new Assert\Type('array'),
                new Assert\All([
                    new Assert\NotBlank(),
                    new Assert\Language(),
                ]),
            ]),
            'expertise' => new AppAssert\Chain([
                new Assert\Type('array'),
                new Assert\All([
                    new Assert\NotBlank(),
                    new Assert\GreaterThan(0),
                ]),
            ]),
            'industry' => new AppAssert\Chain([
                new Assert\Type('array'),
                new Assert\All([
                    new Assert\NotBlank(),
                    new Assert\GreaterThan(0),
                ]),
            ]),
            'offset' => [
                new Assert\NotBlank(),
                new Assert\Range(['min' => 0]),
            ],
            'limit' => [
                new Assert\NotBlank(),
                new Assert\Range(['min' => 1, 'max' => 500]),
            ],
            'sort' => new AppAssert\Chain([
                new Assert\Type('array'),
                new Assert\Collection([
                    'allowMissingFields' => true,
                    'fields' => ['first_name' => [
                        new Assert\NotBlank(),
                        new Assert\Choice([
                            'choices' => ['ASC', 'DESC'],
                            'strict' => true,
                        ]),
                    ]],
                ]),
            ]),
            'rand' => [
                new Assert\NotBlank(),
                new Assert\Range(['min' => 0, 'max' => 0xFFFFFFFF]),
            ],
        ];

        if ($this->isGranted('ROLE_ENTERPRISE_CLIENT') || $this->isGranted('ROLE_COACH')) {
            $collection['session_type'] = [
                new Assert\NotBlank(),
                new Assert\Choice([
                    'choices' => SessionTypeEnum::values(),
                    'strict' => true,
                ]),
            ];
            /* @deprecated */
            $collection['is_executive'] = [
                new Assert\NotBlank(),
                new AppAssert\Boolean(),
            ];
        }
        if ($this->isGranted('ROLE_CLIENT')) {
            $collection['is_favourite'] = [
                new Assert\NotBlank(),
                new AppAssert\Boolean(),
            ];
        }

        $validator = $this->get(RequestValidator::class);
        $validator->validate($request->query, new Assert\Collection([
            'allowMissingFields' => true,
            'fields' => $collection,
        ]));

        $em = $this->getDoctrine()->getManager();
        $qb = $em->createQueryBuilder()
            ->select(
                'c.id',
                'u.firstName first_name',
                'u.lastName last_name',
                'u.photo',
                'c.sessionTypes session_types')
            ->from('App:Coach', 'c')
            ->join('c.user', 'u')
            ->where('u.isEnabled = TRUE AND u.isVerified = TRUE')
            ->groupBy('c.id', 'u.id');

        if ($date = $request->query->get('date')) {
            $startedAt = new \DateTime($date['start']);
            $endedAt = new \DateTime($date['end']);
            if ($startedAt >= $endedAt) {
                throw new BadRequestHttpException('E_INTERVAL_INVALID', null, ErrorEnum::E_INTERVAL_INVALID);
            }

            $qb->leftJoin('c.availability', 'ca')
                ->leftJoin('App:Session', 'cas', 'WITH', 'cas.availability = ca')
                ->andWhere('cas.id IS NULL')
                ->andWhere('ca.startedAt >= :started_at OR ca.endedAt > :started_at')
                ->andWhere('ca.endedAt <= :ended_at OR ca.startedAt < :ended_at')
                ->setParameter('started_at', $startedAt)
                ->setParameter('ended_at', $endedAt);
        }

        if ($this->isGranted('ROLE_ENTERPRISE_CLIENT') || $this->isGranted('ROLE_COACH')) {
            /* @deprecated */
            if ($request->query->getBoolean('is_executive')) {
                $request->query->set('session_type', SessionTypeEnum::EXECUTIVE);
            }
            if ($sessionType = $request->query->get('session_type')) {
                $qb->andWhere(':session_type = ANY_OF(STRING_TO_ARRAY(c.sessionTypes))')
                    ->setParameter('session_type', $sessionType);
            }
        }

        if ($this->isGranted('ROLE_CLIENT')) {
            $qb->addSelect('CASE WHEN COUNT(cc) > 0 THEN TRUE ELSE FALSE END AS is_favourite')
                ->leftJoin('App:Client', 'cc', 'WITH', 'c MEMBER OF cc.favouriteCoaches AND cc.id = :id')
                ->setParameter('id', $this->getClientUser()->getId());

            if ($request->query->has('is_favourite')) {
                $isFavourite = $request->query->getBoolean('is_favourite');
                $qb->andHaving($isFavourite ? 'COUNT(cc) > 0' : 'COUNT(cc) = 0');
            }
        }

        if ($language = $request->query->get('language')) {
            foreach ($language as $i => $language) {
                $qb->andWhere(':language'.$i.' = ANY_OF(STRING_TO_ARRAY(c.languages))')
                    ->setParameter('language'.$i, $language);
            }
        }

        if ($expertise = $request->query->get('expertise')) {
            $qb->andWhere('('. // hello, relational division!
                    'SELECT COUNT(ce) FROM App:CoachExpertise ce '.
                    'WHERE c = ce.coach AND ce.expertise IN (:expertise)'.
                ') = :expertise_size')
                ->setParameter('expertise', $expertise)
                ->setParameter('expertise_size', count($expertise));
        }

        if ($industry = $request->query->get('industry')) {
            $qb->andWhere('('. // relational division, again!
                    'SELECT COUNT(i) FROM App:Industry i '.
                    'WHERE i MEMBER OF c.industries AND i IN (:industry)'.
                ') = :industry_size')
                ->setParameter('industry', $industry)
                ->setParameter('industry_size', count($industry));
        }

        $qbId = clone $qb;
        $qbId->select('c.id');

        $total = $em->createQueryBuilder()
            ->select('COUNT(c0)')
            ->from('App:Coach', 'c0')
            ->where('c0 IN ('.$qbId->getDql().')')
            ->setParameters($qbId->getParameters())
            ->getQuery()
            ->getSingleScalarResult();

        if ($this->isGranted('ROLE_ENTERPRISE_CLIENT') || $this->isGranted('ROLE_COACH')) {
            $qbSessionType = clone $qb;
            $filters['session_type'] = $qbSessionType
                ->select('UNNEST(STRING_TO_ARRAY(c.sessionTypes))')
                ->distinct()
                ->getQuery()
                ->getResult('HYDRATE_COLUMN');

            /* @deprecated */
            $filters['is_executive'] = [false];
            if (in_array(SessionTypeEnum::EXECUTIVE, $filters['session_type'])) {
                $filters['is_executive'] = [false, true];
            }
        }

        if ($this->isGranted('ROLE_CLIENT')) {
            $qbFavourite = clone $qb;
            $filters['is_favourite'] = $qbFavourite
                ->select('CASE WHEN COUNT(cc) > 0 THEN TRUE ELSE FALSE END AS _')
                ->distinct()
                ->getQuery()
                ->getResult('HYDRATE_COLUMN');
        }

        $qbLanguage = clone $qb;
        $filters['language'] = $qbLanguage
            ->select('UNNEST(STRING_TO_ARRAY(c.languages))')
            ->distinct()
            ->getQuery()
            ->getResult('HYDRATE_COLUMN');

        $filters['expertise'] = $em->createQueryBuilder()
            ->select('e0.id')
            ->distinct()
            ->from('App:CoachExpertise', 'ce0')
            ->innerJoin('ce0.expertise', 'e0')
            ->where('ce0.coach IN ('.$qbId->getDql().')')
            ->setParameters($qbId->getParameters())
            ->getQuery()->getResult('HYDRATE_COLUMN');

        $filters['industry'] = $em->createQueryBuilder()
            ->select('i0.id')
            ->distinct()
            ->from('App:Industry', 'i0')
            ->leftJoin('App:Coach', 'c0', 'WITH', 'i0 MEMBER OF c0.industries')
            ->where('c0 IN ('.$qbId->getDql().')')
            ->setParameters($qbId->getParameters())
            ->getQuery()
            ->getResult('HYDRATE_COLUMN');

        $rand = $request->query->getInt('rand', random_int(0, 0xFFFFFFFF));

        $sort = $request->query->get('sort', []);
        if (!$sort && $this->isGranted('ROLE_CLIENT')) {
            $seed = $rand / 0xFFFFFFFF;
            $conn = $em->getConnection();
            $conn->executeQuery('SELECT setseed(?)', [$seed]);

            $qb->addSelect('RANDOM() AS HIDDEN rand')
                ->orderBy('is_favourite', 'DESC')
                ->addOrderBy('rand');
        } elseif (!$sort) {
            $sort['first_name'] = 'ASC';
        }

        foreach ($sort as $field => $order) {
            $qb->addOrderBy($field, $order);
        }

        $offset = $request->query->getInt('offset');
        $limit = $request->query->getInt('limit', 20);
        $qb->setFirstResult($offset)->setMaxResults($limit);


      if ($this->isGranted('ROLE_ENTERPRISE_CLIENT')) {
        $em = $this->getDoctrine()->getManager();
        $coachCompanies = $em->getRepository('App:CoachCompany')->findBy(['company' => $this->getClientUser()->getCompany()]);

        if (!empty($coachCompanies)) {
          $coaches = [];
          foreach ($coachCompanies as $company) {
            $coaches[] = $company->getCoach();
          }
          $qb->andWhere('c.id IN (:coaches)')
              ->setParameter('coaches', $coaches);
          /**
           * @var QueryBuilder $qbId
           */
          /**
           * @var EntityManagerInterface $em
           */

          $total = $em->createQueryBuilder()
            ->select('COUNT(c0)')
            ->from('App:Coach', 'c0')
            ->where('c0 IN ('.$qbId->getDql().')')
            ->andWhere('c0.id IN (:coaches)')
            ->setParameters($qbId->getParameters())
            ->setParameter('coaches', $coaches)
            ->getQuery()->getSingleScalarResult();
        }
      }

        $result = $qb->getQuery()->getArrayResult();

        /* @deprecated */
        if ($this->isGranted('ROLE_ENTERPRISE_CLIENT') || $this->isGranted('ROLE_COACH')) {
            foreach ($result as &$coach) {
                $coach['is_executive'] = in_array(SessionTypeEnum::EXECUTIVE, $coach['session_types']);
            }
        }

        return [
            'result' => $result,
            'total' => $total,
            'filters' => $filters,
            'rand' => $rand,
        ];
    }

    /**
     * @Route("/coaches/{id}", requirements={"id": "\d+"})
     * @Method("GET")
     */
    public function viewAction(Coach $coach)
    {
        return $this->viewCoach($coach);
    }

    /**
     * @Security("is_granted('ROLE_ADMIN')")
     * @Route("/coaches/{id}", requirements={"id": "\d+"})
     * @Method("PATCH")
     */
    public function updateAction(Request $request, Coach $coach)
    {
        $this->saveCoach($request, $coach);

        return self::STATUS_OK;
    }

    /**
     * @Security("is_granted('ROLE_ADMIN')")
     * @Route("/coaches/{id}", requirements={"id": "\d+"})
     * @Method("DELETE")
     */
    public function deleteAction(Coach $coach)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($coach->getUser());
        $em->flush();

        return self::STATUS_OK;
    }

    /**
     * @Security("is_granted('ROLE_CLIENT')")
     * @Route("/coaches/{id}/availability", requirements={"id": "\d+"})
     * @Method("GET")
     */
    public function availabilityAction(Request $request, Coach $coach)
    {
        $sessionTypes = [SessionTypeEnum::DEFAULT_];
        if ($this->isGranted('ROLE_ENTERPRISE_CLIENT')) {
            $sessionTypes[] = SessionTypeEnum::EXECUTIVE;
        }

        $validator = $this->get(RequestValidator::class);
        $validator->validate($request->query, new Assert\Collection([
            'allowMissingFields' => true,
            'fields' => [
                'date' => new AppAssert\Chain([
                    new Assert\Type('array'),
                    new Assert\Collection([
                        'start' => [
                            new Assert\NotBlank(),
                            new Assert\DateTime(['format' => \DateTime::RFC3339]),
                        ],
                        'end' => [
                            new Assert\NotBlank(),
                            new Assert\DateTime(['format' => \DateTime::RFC3339]),
                        ],
                    ]),
                ]),
                'session_type' => [
                    new Assert\NotBlank(),
                    new Assert\Choice([
                        'choices' => $sessionTypes,
                        'strict' => true,
                    ]),
                ],
            ],
        ]));

        $date = $request->query->get('date', [
            'start' => 'first day of this month 00:00:00',
            'end' => 'last day of this month 23:59:59',
        ]);
        $startedAt = new \DateTime($date['start']);
        $endedAt = new \DateTime($date['end']);
        if ($startedAt >= $endedAt) {
            throw new BadRequestHttpException('E_INTERVAL_INVALID', null, ErrorEnum::E_INTERVAL_INVALID);
        }

        $offset = new \DateTime('+'.Session::TIME_OFFSET.' minutes');

        $em = $this->getDoctrine()->getManager();
        $qb = $em->createQueryBuilder();
        $qb->select('a')
            ->from('App:CoachAvailability', 'a')
            ->leftJoin('App:Session', 's', 'WITH', 's.availability = a')
            ->where('a.coach = :coach AND s.id IS NULL')
            ->andWhere($qb->expr()->orX(
                'a.startedAt > :offset',
                ':offset BETWEEN a.startedAt AND a.endedAt'
            ))
            ->andWhere('a.startedAt >= :started_at OR a.endedAt > :started_at')
            ->andWhere('a.endedAt <= :ended_at OR a.startedAt < :ended_at')
            ->setParameter('coach', $coach)
            ->setParameter('offset', $offset)
            ->setParameter('started_at', $startedAt)
            ->setParameter('ended_at', $endedAt)
            ->orderBy('a.startedAt');

        $sessionType = $request->query->get('session_type', SessionTypeEnum::DEFAULT_);
        $interval = new \DateInterval('PT'.Session::INTERVAL.'M');
        $duration = Session::DURATION[$sessionType];
        $margin = $duration - Session::INTERVAL;

        $payload = [];
        foreach ($qb->getQuery()->getResult() as $availability) {
            $startedAtPeriod = $availability->getStartedAt();
            $endedAtPeriod = $availability->getEndedAt()->modify('-'.$margin.' minutes');
            $period = new \DatePeriod($startedAtPeriod, $interval, $endedAtPeriod);
            foreach ($period as $dateTime) {
                if ($dateTime < $startedAt || $dateTime > $endedAt || $dateTime < $offset) {
                    continue;
                }

                $id = $availability->getId();
                $payload[$id]['id'] = $id;
                $payload[$id]['availability'][] = $dateTime;
            }
        }

        return array_values($payload);
    }

    /**
     * @Security("is_granted('ROLE_CLIENT')")
     * @Route("/coaches/{id}/favourite", requirements={"id": "\d+"})
     * @Method("POST")
     */
    public function favouriteAction(Request $request, Coach $coach)
    {
        $validator = $this->get(RequestValidator::class);
        $validator->validate($request->request, new Assert\Collection([
            'is_favourite' => new AppAssert\Chain([
                new Assert\Type('bool'),
                new Assert\NotNull(),
            ]),
        ]));

        $client = $this->getClientUser();
        $favouriteCoaches = $client->getFavouriteCoaches();
        if ($request->request->get('is_favourite')) {
            if ($favouriteCoaches->contains($coach)) {
                throw new ConflictHttpException('E_COACH_IS_FAVOURITE', null, ErrorEnum::E_COACH_IS_FAVOURITE);
            }
            $favouriteCoaches->add($coach);
        } else {
            if (!$favouriteCoaches->contains($coach)) {
                throw new ConflictHttpException('E_COACH_NOT_FAVOURITE', null, ErrorEnum::E_COACH_NOT_FAVOURITE);
            }
            $favouriteCoaches->removeElement($coach);
        }
        $em = $this->getDoctrine()->getManager();
        $em->persist($client);
        $em->flush();

        return self::STATUS_OK;
    }
}
