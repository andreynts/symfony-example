<?php

namespace App\Controller;

use App\Enum\ErrorEnum;
use App\Utility\DataReader;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Cache;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Intl\Intl;

/**
 * @Cache(etag="_version", public=true)
 * @Method("GET")
 */
class VarController extends AbstractController
{
    /**
     * @Cache
     * @Route("/vars")
     */
    public function indexAction()
    {
        return [
            'version' => $this->getParameter('version'),
            'time' => new \DateTime(),
        ];
    }

    /**
     * @Route("/vars/errors")
     */
    public function errorsAction()
    {
        return ErrorEnum::items();
    }

    /**
     * @Route("/vars/styles")
     */
    public function stylesAction()
    {
        $em = $this->getDoctrine()->getManager();
        $qb = $em->createQueryBuilder()
            ->select(
                'c.id',
                'c.name',
                'c.value',
                'u.type_name')
            ->from('App:Style', 'c')
            ->join('c.styleType', 'u');
        $result = $qb->getQuery()->getArrayResult();
        return $result;
    }

    /**
     * @Route("/vars/countries")
     */
    public function countriesAction()
    {
        $countries = Intl::getRegionBundle()->getCountryNames();
        if (is_array($countries)) {
            $countries['NS'] = 'Rather Not Say';
        }
        return $countries;
    }

    /**
     * @Route("/vars/languages")
     */
    public function languagesAction(DataReader $reader)
    {
        return $reader->read('languages.json');
    }

    /**
     * @Route("/vars/nationality")
     */
    public function nationalityAction(DataReader $reader)
    {
        return $reader->read('nationality.json');
    }

    /**
     * @Route("/vars/gender")
     */
    public function genderAction(DataReader $reader)
    {
        return $reader->read('gender.json');
    }

    /**
     * @Route("/vars/age-range")
     */
    public function ageRangeAction(DataReader $reader)
    {
        return $reader->read('age-range.json');
    }

    /**
     * @Route("/vars/industries")
     */
    public function industriesAction()
    {
        $em = $this->getDoctrine()->getManager();

        return $em->getRepository('App:Industry')->getItems();
    }

    /**
     * @Route("/vars/expertise")
     */
    public function expertiseAction()
    {
        $em = $this->getDoctrine()->getManager();

        return $em->getRepository('App:ExpertiseCategory')->getItemsWithExpertise();
    }

    /**
     * @Route("/vars/stripe")
     */
    public function stripeAction()
    {
        return ['key' => getenv('STRIPE_PUBLIC_KEY')];
    }

    /**
     * @Route("/vars/opentok")
     */
    public function opentokAction()
    {
        return ['key' => getenv('OPENTOK_API_KEY')];
    }

    /**
     * @Route("/vars/client/regions")
     */
    public function clientRegionsAction(Request $request, DataReader $reader)
    {
        if (0 !== $companyId = $request->query->getInt('company')) {
            $repository = $this->getDoctrine()->getRepository('App:CompanyRegion');
            $request->attributes->remove('_cache');

            return $repository->getNamesByCompany($companyId);
        }

        return $reader->read('client/regions.json');
    }

    /**
     * @Route("/vars/client/departments")
     */
    public function clientDepartmentsAction(Request $request, DataReader $reader)
    {
        if (0 !== $companyId = $request->query->getInt('company')) {
            $repository = $this->getDoctrine()->getRepository('App:CompanyDepartment');
            $request->attributes->remove('_cache');

            return $repository->getNamesByCompany($companyId);
        }

        return $reader->read('client/departments.json');
    }

    /**
     * @Route("/vars/client/work-duration")
     */
    public function clientWorkDurationAction(DataReader $reader)
    {
        return $reader->read('client/work-duration.json');
    }

    /**
     * @Route("/vars/client/attitude")
     */
    public function clientAttitudeAction(DataReader $reader)
    {
        return $reader->read('client/attitude.json');
    }

    /**
     * @Route("/vars/coach/employment-status")
     */
    public function coachEmploymentStatusAction(DataReader $reader)
    {
        return $reader->read('coach/employment-status.json');
    }

    /**
     * @Route("/vars/coach/company-types")
     */
    public function coachCompanyTypesAction(DataReader $reader)
    {
        return $reader->read('coach/company-types.json');
    }

    /**
     * @Route("/vars/coach/indemnity-cover")
     */
    public function coachIndemnityCoverAction(DataReader $reader)
    {
        return $reader->read('coach/indemnity-cover.json');
    }

    /**
     * @Route("/vars/coach/tech-expertise")
     */
    public function coachTechExpertiseAction(DataReader $reader)
    {
        return $reader->read('coach/tech-expertise.json');
    }

    /**
     * @Route("/vars/coach/senior-positions")
     */
    public function coachSeniorPositionsAction(DataReader $reader)
    {
        return $reader->read('coach/senior-positions.json');
    }

    /**
     * @Route("/vars/coach/career-companies")
     */
    public function coachCareerCompaniesAction(DataReader $reader)
    {
        return $reader->read('coach/career-companies.json');
    }

    /**
     * @Route("/vars/coach/coached-people")
     */
    public function coachCoachedPeopleAction(DataReader $reader)
    {
        return $reader->read('coach/coached-people.json');
    }

    /**
     * @Route("/vars/coach/coached-companies")
     */
    public function coachCoachedCompaniesAction(DataReader $reader)
    {
        return $reader->read('coach/coached-companies.json');
    }

    /**
     * @Route("/vars/session/changed-view")
     */
    public function sessionChangedViewAction(DataReader $reader)
    {
        return $reader->read('session/changed-view.json');
    }

    /**
     * @Route("/vars/session/put-into-practice")
     * @Route("/vars/session/put-into-practice")
     */
    public function sessionPutIntoPracticeAction(DataReader $reader)
    {
        return $reader->read('session/put-into-practice.json');
    }

    /**
     * @Route("/vars/session/reasons")
     */
    public function sessionReasonsAction(DataReader $reader)
    {
        return $reader->read('session/reasons.json');
    }

    /**
     * @Route("/vars/session/client-relationship")
     */
    public function sessionClientRelationshipAction(DataReader $reader)
    {
        return $reader->read('session/client-relationship.json');
    }
}
