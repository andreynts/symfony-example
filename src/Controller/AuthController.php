<?php

namespace App\Controller;

use App\Enum\ErrorEnum;
use App\Exception\UnauthorizedHttpException;
use App\Utility\RequestValidator;
use App\Validator\Constraints as AppAssert;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;

class AuthController extends AbstractController
{
    /**
     * @Route("/auth")
     * @Method("POST")
     */
    public function indexAction(Request $request)
    {
        $validator = $this->get(RequestValidator::class);
        $validator->validate($request->request, new Assert\Collection([
            'username' => new AppAssert\Chain([
                new Assert\Type('string'),
                new Assert\NotBlank(),
            ]),
            'password' => new AppAssert\Chain([
                new Assert\Type('string'),
                new Assert\NotBlank(),
            ]),
            'timezone' => new AppAssert\Chain([
                new Assert\Type('string'),
                new Assert\NotIdenticalTo(''),
                new Assert\Choice([
                    'choices' => \DateTimeZone::listIdentifiers(\DateTimeZone::ALL_WITH_BC),
                    'strict' => true,
                ]),
            ]),
        ]));


        $userRepository = $this->getDoctrine()->getRepository('App:User');
        $user = $userRepository->loadUserByUsername($request->request->get('username'));
        if (null === $user) {
            throw new UnauthorizedHttpException('E_USER_NOT_FOUND', null, ErrorEnum::E_USER_NOT_FOUND);
        }
        if (!$user->isEnabled()) {
            throw new UnauthorizedHttpException('E_USER_DISABLED', null, ErrorEnum::E_USER_DISABLED);
        }
        if (!$user->isVerified()) {
            throw new UnauthorizedHttpException('E_USER_NOT_VERIFIED', null, ErrorEnum::E_USER_NOT_VERIFIED);
        }
        if (!$user->isAccountNonLocked()) {
            throw new UnauthorizedHttpException('E_USER_LOCKED', null, ErrorEnum::E_USER_LOCKED);
        }

        $em = $this->getDoctrine()->getManager();

        $encoder = $this->get('security.encoder_factory')->getEncoder($user);
        if (!$encoder->isPasswordValid($user->getPassword(), $request->request->get('password'), $user->getSalt())) {
            $user->setAuthFails($user->getAuthFails() + 1);
            $em->persist($user);
            $em->flush();

            throw new UnauthorizedHttpException('E_BAD_CREDENTIALS', null, ErrorEnum::E_BAD_CREDENTIALS);
        }

        $jwtManager = $this->get('lexik_jwt_authentication.jwt_manager');
        $token = $jwtManager->create($user);

        $user->setAuthFails(0);
        $user->setLastAuthAt(new \DateTime());
        if ($timezone = $request->request->get('timezone')) {
            $user->setTimezone($timezone);
        }

        $em->persist($user);
        $em->flush();

        return ['token' => $token];
    }

    /**
     * @Route("/auth/refresh")
     * @Method("POST")
     */
    public function refreshAction()
    {
        $jwtManager = $this->get('lexik_jwt_authentication.jwt_manager');
        $token = $jwtManager->create($this->getUser());

        return ['token' => $token];
    }
}
