<?php

namespace App\Controller\Traits;

use App\Entity\Client;
use App\Utility\DataReader;
use App\Utility\RequestValidator;
use App\Validator\Constraints as AppAssert;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;
use App\Entity\ClientCredit;
use App\Enum\CreditTypeEnum;

trait ClientTrait
{
    private $ratherNotSay = 'Rather Not Say';

    private function viewClient(Client $client): array
    {
        $user = $client->getUser();
        $payload['email'] = $user->getEmail();
        $payload['first_name'] = $user->getFirstName();
        $payload['last_name'] = $user->getLastName();
        $payload['photo'] = $user->getPhoto();

        $payload['company'] = ['name' => $client->getCompanyName()];
        $payload['department'] = $client->getDepartment();
        $payload['position'] = $client->getPosition();

        $isPersonal = $this->isGranted('ROLE_CLIENT') &&
            $this->getClientUser()->getId() === $client->getId();

        if ($isPersonal || $this->isGranted('ROLE_ADMIN')) {
            $payload['secondary_email'] = $client->getSecondaryEmail();
            $payload['mobile_number'] = $client->getMobileNumber();
            $payload['languages'] = $client->getLanguages();
            $payload['age_range'] = $client->getAgeRange();
            $payload['gender'] = $client->getGender();
            $payload['country'] = $client->getCountry();
            $payload['nationality'] = $client->getNationality();
            $payload['region'] = $client->getRegion();
            $payload['work_duration'] = $client->getWorkDuration();
            $payload['attitude'] = $client->getAttitude();
            $payload['satisfied_company'] = $client->getSatisfiedCompany();
            $payload['recommend_company'] = $client->getRecommendCompany();
            $payload['future_at_company'] = $client->getFutureAtCompany();
            $payload['future_team'] = $client->getFutureTeam();
            $payload['future_company'] = $client->getFutureCompany();
            $payload['future_industry'] = $client->getFutureIndustry();
            $payload['reference_number'] = $client->getReferenceNumber();
        }
        if (null !== $company = $client->getCompany()) {
            $payload['company']['name'] = $company->getName();
            if ($this->isGranted('ROLE_ENTERPRISE_ADMIN') || $this->isGranted('ROLE_ADMIN')) {
                $payload['company']['id'] = $company->getId();
                $payload['company']['is_admin'] = $this->isGranted('ROLE_ENTERPRISE_ADMIN', null, $user);
                $payload['company']['is_contact'] = $client->isCompanyContact();
            }
        }
        if (!$isPersonal) {
            $payload['is_verified'] = $user->isVerified();
            $payload['is_enabled'] = $user->isEnabled();
        }

        if ($this->isGranted('ROLE_ENTERPRISE_ADMIN')) {
            $payload['reference_number'] = $client->getReferenceNumber();
        }

        return $payload;
    }

    private function saveClient(Request $request, Client $client)
    {
        $this->_validateClient($request, $client);

        if ($request->request->has('secondary_email')) {
            $client->setSecondaryEmail($request->request->get('secondary_email'));
        }
        if ($request->request->has('mobile_number')) {
            $client->setMobileNumber($request->request->get('mobile_number'));
        }
        if ($request->request->has('languages')) {
            $client->setLanguages($request->request->get('languages'));
        }
        if ($request->request->has('age_range') && $request->request->get('age_range') != null) {
            $client->setAgeRange($request->request->get('age_range'));
        }
        if ($request->request->has('gender') && $request->request->get('gender') != null) {
            $client->setGender($request->request->get('gender'));
        }
        if ($request->request->has('country') && $request->request->get('country') != null) {
            $client->setCountry($request->request->get('country'));
        }
        if ($request->request->has('nationality') && $request->request->get('nationality') != null) {
            $client->setNationality($request->request->get('nationality'));
        }
        if ($request->request->has('company_name')) {
            $client->setCompanyName($request->request->get('company_name'));
        }
        if ($request->request->has('department') && $request->request->get('department') != null) {
            $client->setDepartment($request->request->get('department'));
        }
        if ($request->request->has('position') && $request->request->get('position') != null) {
            $client->setPosition($request->request->get('position'));
        }
        if ($request->request->has('region') && $request->request->get('region') != null) {
            $client->setRegion($request->request->get('region'));
        }
        if ($request->request->has('work_duration') && $request->request->get('work_duration') != null) {
            $client->setWorkDuration($request->request->get('work_duration'));
        }
        if ($request->request->has('attitude') && $request->request->get('attitude') != null) {
            $client->setAttitude($request->request->get('attitude'));
        }
        if ($request->request->has('satisfied_company')) {
            $client->setSatisfiedCompany($request->request->get('satisfied_company'));
        }
        if ($request->request->has('recommend_company')) {
            $client->setRecommendCompany($request->request->get('recommend_company'));
        }
        if ($request->request->has('future_at_company') && $request->request->get('future_at_company') != null) {
            $client->setFutureAtCompany($request->request->get('future_at_company'));
        }
        if ($request->request->has('future_team') && $request->request->get('future_team') != null) {
            $client->setFutureTeam($request->request->get('future_team'));
        }
        if ($request->request->has('future_company') && $request->request->get('future_company') != null) {
            $client->setFutureCompany($request->request->get('future_company'));
        }
        if ($request->request->has('future_industry') && $request->request->get('future_industry') != null) {
            $client->setFutureIndustry($request->request->get('future_industry'));
        }

        $em = $this->getDoctrine()->getManager();
        $em->persist($client);

	   /* $credit = new ClientCredit();
	    $credit->setClient($client);
	    $credit->setType(CreditTypeEnum::COMPLIMENTARY);
	    $credit->setValue(1);
	    $credit->setRefund(true);
	    $credit->setExpiresAt(new \DateTime(ClientCredit::REFUND_EXPIRY));
	    $em->persist($credit);*/

        $this->saveUser($request, $client->getUser());
    }

    private function _validateClient(Request $request, Client $client)
    {
        $onUpdate = null !== $client->getId();

        $reader = $this->get(DataReader::class);
        $ageRangeList = $reader->read('age-range.json');
        $genderList = $reader->read('gender.json');
        $nationalityList = $reader->read('nationality.json');
        $workDurationList = $reader->read('client/work-duration.json');
        $attitudeList = $reader->read('client/attitude.json');

        if (null !== $company = $client->getCompany()) {
            $regionRepository = $this->getDoctrine()->getRepository('App:CompanyRegion');
            $regionList = $regionRepository->getNamesByCompany($company);
            $departmentRepository = $this->getDoctrine()->getRepository('App:CompanyDepartment');
            $departmentList = $departmentRepository->getNamesByCompany($company);
        } else {
            $regionList = $reader->read('client/regions.json');
            $departmentList = $reader->read('client/departments.json');
        }

        if (is_array($departmentList)) {
            array_unshift($departmentList, $this->ratherNotSay);
        }
        if (is_array($regionList)) {
            array_unshift($regionList, $this->ratherNotSay);
        }

        $collection = $this->_getUserConstraint($client->getUser()) + [
            // STEP 1
            'mobile_number' => new AppAssert\Chain([
                new Assert\Type('string'),
                new Assert\NotBlank(),
                new Assert\Length(['max' => 32]),
            ]),

            // STEP 2
            'company_name' => new AppAssert\Chain([
                new Assert\Type('string'),
                new Assert\NotBlank(),
                new Assert\Length(['max' => 64]),
            ]),

            // STEP 3
            'satisfied_company' => new AppAssert\Chain([
                new Assert\Type('int'),
                new Assert\NotNull(),
                new Assert\Range(['min' => 1, 'max' => 10]),
            ]),
            'recommend_company' => new AppAssert\Chain([
                new Assert\Type('int'),
                new Assert\NotNull(),
                new Assert\Range(['min' => 1, 'max' => 10]),
            ]),
        ];

        if ($request->request->has('secondary_email')) {
            $collection['secondary_email'] =  new AppAssert\Chain([
                new Assert\Type('string'),
                new Assert\NotIdenticalTo(''),
                new Assert\Length(['max' => 128]),
                new Assert\Email(['checkMX' => true]),
            ]);
        }

        if ($request->request->has('languages')) {
            $collection['languages'] = new AppAssert\Chain([
                new Assert\Type('array'),
                new Assert\NotNull(),
                new AppAssert\Unique(),
                new Assert\All(new AppAssert\Chain([
                    new Assert\Type('string'),
                    new Assert\NotBlank(),
                    new Assert\Language(),
                ])),
            ]);
        }

        if ($request->request->has('age_range')) {
            $collection['age_range'] = new AppAssert\Chain([
                new Assert\Type('string'),
                new Assert\NotIdenticalTo(''),
                new Assert\Choice([
                    'choices' => $ageRangeList,
                    'strict' => true,
                ]),
            ]);
        }

        if ($request->request->has('gender')) {
            $collection['gender'] = new AppAssert\Chain([
                new Assert\Type('string'),
                new Assert\Choice([
                    'choices' => $genderList,
                    'strict' => true,
                ]),
            ]);
        }

        if ($request->request->has('country')) {
            $collection['country'] = new AppAssert\Chain([
                new Assert\Type('string'),
                new Assert\NotBlank(),
            ]);
        }

        if ($request->request->has('nationality')) {
            $collection['nationality'] = new AppAssert\Chain([
                new Assert\Type('string'),
                new Assert\Choice([
                    'choices' => $nationalityList,
                    'strict' => true,
                ]),
            ]);
        }

        if ($request->request->has('department')) {
            $collection['department'] = new AppAssert\Chain([
                new Assert\Type('string'),
                new Assert\Choice([
                    'choices' => $departmentList,
                    'strict' => true,
                ]),
            ]);
        }

        if ($request->request->has('position')) {
            $collection['position'] = new AppAssert\Chain([
                new Assert\Type('string'),
                new Assert\Length(['max' => 72]),
            ]);
        }

        if ($request->request->has('region')) {
            $collection['region'] = new AppAssert\Chain([
                new Assert\Type('string'),
                new Assert\Choice([
                    'choices' => $regionList,
                    'strict' => true,
                ]),
            ]);
        }

        if ($request->request->has('work_duration')) {
            $collection['work_duration'] = new AppAssert\Chain([
                new Assert\Type('string'),
                new Assert\Choice([
                    'choices' => $workDurationList,
                    'strict' => true,
                ]),
            ]);
        }

        if ($request->request->has('attitude')) {
            $collection['attitude'] = new AppAssert\Chain([
                new Assert\Type('string'),
                new Assert\Choice([
                    'choices' => $attitudeList,
                    'strict' => true,
                ]),
            ]);
        }

        if ($request->request->has('future_at_company')) {
            $collection['future_at_company'] = new AppAssert\Chain([
                new Assert\Type('int'),
                new Assert\Range(['min' => 1, 'max' => 10]),
            ]);
        }

        if ($request->request->has('future_team')) {
            $collection['future_team'] = new AppAssert\Chain([
                new Assert\Type('int'),
                new Assert\Range(['min' => 1, 'max' => 10]),
            ]);
        }

        if ($request->request->has('future_company')) {
            $collection['future_company'] = new AppAssert\Chain([
                new Assert\Type('int'),
                new Assert\Range(['min' => 1, 'max' => 10]),
            ]);
        }

        if ($request->request->has('future_industry')) {
            $collection['future_industry'] = new AppAssert\Chain([
                new Assert\Type('int'),
                new Assert\Range(['min' => 1, 'max' => 10]),
            ]);
        }

        if ($request->request->has('reference_number')) {
            $collection['reference_number'] = new AppAssert\Chain([
                new Assert\Type('string'),
                new Assert\Length(['max' => 16])
            ]);
        }

        $validator = $this->get(RequestValidator::class);
        $validator->validate($request->request, [
            new Assert\NotBlank(),
            new Assert\Collection([
                'allowMissingFields' => $onUpdate,
                'fields' => $collection,
            ]),
        ]);
    }
}
