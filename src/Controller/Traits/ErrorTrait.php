<?php

namespace App\Controller\Traits;

use Symfony\Component\HttpFoundation\Request;
use App\Utility\RequestValidator;
use App\Validator\Constraints as AppAssert;
use Symfony\Component\Validator\Constraints as Assert;
use App\Entity\ErrorLog;

trait ErrorTrait
{
    private function saveError(Request $request, ErrorLog $errorLog)
    {
        $this->_validateError($request, $errorLog);

        if ($request->request->has('sessionId')) {
            $errorLog->setSessionId($request->request->get('sessionId'));
        }

        if ($request->request->has('action')) {
            $errorLog->setAction($request->request->get('action'));
        }

        if ($request->request->has('errorCode')) {
            $errorLog->setErrorCode($request->request->get('errorCode'));
        }

        if ($request->request->has('errorMessage')) {
            $errorLog->setErrorMessage($request->request->get('errorMessage'));
        }

        $em = $this->getDoctrine()->getManager();
        $em->persist($errorLog);
    }

    public function _validateError(Request $request, ErrorLog $errorLog)
    {
        $onUpdate = null !== $errorLog->getId();

        $collection = [
            'sessionId' => new AppAssert\Chain([
                new Assert\Type('string'),
                new Assert\NotBlank(),
                new Assert\Length(['max' => 100]),
            ]),
            'action' => new AppAssert\Chain([
                new Assert\Type('string'),
                new Assert\NotBlank(),
                new Assert\Length(['max' => 100]),
            ]),
            'errorCode' => new AppAssert\Chain([
                new Assert\Type('string'),
                new Assert\NotBlank(),
                new Assert\Length(['max' => 50]),
            ]),
            'errorMessage' => new AppAssert\Chain([
                new Assert\Type('string'),
                new Assert\NotBlank(),
                new Assert\Length(['max' => 256]),
            ])
        ];

        $validator = $this->get(RequestValidator::class);
        $validator->validate($request->request, [
            new Assert\NotBlank(),
            new Assert\Collection([
                'allowMissingFields' => $onUpdate,
                'fields' => $collection,
            ]),
        ]);
    }
}