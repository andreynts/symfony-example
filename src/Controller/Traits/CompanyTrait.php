<?php

namespace App\Controller\Traits;

use App\Entity\CoachCompany;
use App\Entity\Company;
use App\Entity\CompanyDepartment;
use App\Entity\CompanyRegion;
use App\Enum\ErrorEnum;
use App\Utility\RequestValidator;
use App\Validator\Constraints as AppAssert;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

trait CompanyTrait
{
    private function viewCompany(Company $company): array
    {
        $payload['name'] = $company->getName();
        $payload['logo'] = $company->getLogo();
        $payload['website'] = $company->getWebsite();
        $payload['address1'] = $company->getAddress1();
        $payload['address2'] = $company->getAddress2();
        $payload['city'] = $company->getCity();
        $payload['county'] = $company->getCounty();
        $payload['postcode'] = $company->getPostcode();
        $payload['country'] = $company->getCountry();
        $payload['reg_number'] = $company->getRegNumber();
        $payload['vat_reg_number'] = $company->getVatRegNumber();
        $payload['regions'] = $company->getRegions(true);
        $payload['departments'] = $company->getDepartments(true);
        $payload['po_number'] = $company->getPoNumber();
        $payload['brief'] = $company->getBrief();
        $payload['is_enabled'] = $company->isEnabled();
        $payload['tok_record'] = $company->getTokRecord();
        $payload['enable_coaches'] = $company->getCoaches(true);
        $payload['session_length'] = $company->getSessionLength();

        return $payload;
    }

    private function saveCompany(Request $request, Company $company)
    {
        $this->_validateCompany($request, $company);

        if ($request->request->has('name')) {
            $company->setName($request->request->get('name'));
        }
        if ($request->request->has('logo')) {
            $company->setLogo($request->request->get('logo'));
        }
        if ($request->request->has('website')) {
            $company->setWebsite($request->request->get('website'));
        }
        if ($request->request->has('address1')) {
            $company->setAddress1($request->request->get('address1'));
        }
        if ($request->request->has('address2')) {
            $company->setAddress2($request->request->get('address2'));
        }
        if ($request->request->has('city')) {
            $company->setCity($request->request->get('city'));
        }
        if ($request->request->has('county')) {
            $company->setCounty($request->request->get('county'));
        }
        if ($request->request->has('postcode')) {
            $company->setPostcode($request->request->get('postcode'));
        }
        if ($request->request->has('country')) {
            $company->setCountry($request->request->get('country'));
        }
        if ($request->request->has('reg_number')) {
            $company->setRegNumber($request->request->get('reg_number'));
        }
        if ($request->request->has('vat_reg_number')) {
            $company->setVatRegNumber($request->request->get('vat_reg_number'));
        }
        if ($request->request->has('regions')) {
            $company->getRegions()->clear();
            foreach ($request->request->get('regions') as $name) {
                $companyRegion = new CompanyRegion();
                $companyRegion->setCompany($company);
                $companyRegion->setName($name);
                $company->getRegions()->add($companyRegion);
            }
        }
        if ($request->request->has('departments')) {
            $company->getDepartments()->clear();
            foreach ($request->request->get('departments') as $name) {
                $companyDepartment = new CompanyDepartment();
                $companyDepartment->setCompany($company);
                $companyDepartment->setName($name);
                $company->getDepartments()->add($companyDepartment);
            }
        }
        if ($request->request->has('po_number')) {
            $company->setPoNumber($request->request->get('po_number'));
        }
        if ($request->request->has('brief')) {
            $company->setBrief($request->request->get('brief'));
        }
        if ($request->request->has('is_enabled')) {
            $company->setEnabled($request->request->get('is_enabled'));
        }
        if ($request->request->has('tok_record')) {
		      $company->setTokRecord($request->request->get('tok_record'));
	    }

        if ($request->request->has('enable_coaches')) {
            $company->getCoaches()->clear();
            foreach ($request->request->get('enable_coaches') as $coach) {
                $coachEntity = $this->getDoctrine()
                    ->getRepository('App:Coach')
                    ->find($coach);
                $coachCompany = new CoachCompany();
                $coachCompany->setCompany($company);
                $coachCompany->setCoach($coachEntity);
                $coachCompany->setStatus('enable');
                $company->getCoaches()->add($coachCompany);
            }
        }

        if ($request->request->has('session_length')) {
            $company->setSessionLength($request->request->get('session_length'));
        }

        $em = $this->getDoctrine()->getManager();
        $em->persist($company);
        $em->flush();
    }

    private function _validateCompany(Request $request, Company $company)
    {
        $onUpdate = null !== $company->getId();

        $collection = [
            'name' => new AppAssert\Chain([
                new Assert\Type('string'),
                new Assert\NotBlank(),
                new Assert\Length(['max' => 64]),
                new Assert\Callback(function ($value, ExecutionContextInterface $context) use ($company) {
                    if (null === $company->getId()) {
                        $company = null;
                    }
                    if ($this->getDoctrine()->getRepository('App:Company')->checkNameExists($value, $company)) {
                        $context->buildViolation('E_COMPANY_EXISTS')
                            ->setCode(ErrorEnum::E_COMPANY_EXISTS)
                            ->addViolation();
                    }
                }),
            ]),
            'logo' => new AppAssert\Chain([
                new Assert\Type('string'),
                new Assert\NotIdenticalTo(''),
                new Assert\Length(['max' => 255]),
                new AppAssert\Url([
                    'checkDNS' => false,
                    'allowedHostNames' => $this->getParameter('static_hosts'),
                    'allowedContentTypes' => ['image/jpeg'],
                ]),
            ]),
            'website' => new AppAssert\Chain([
                new Assert\Type('string'),
                new Assert\NotIdenticalTo(''),
                new Assert\Length(['max' => 255]),
                new Assert\Url(['checkDNS' => false]),
            ]),
            'address1' => new AppAssert\Chain([
                new Assert\Type('string'),
                new Assert\NotBlank(),
                new Assert\Length(['max' => 48]),
            ]),
            'address2' => new AppAssert\Chain([
                new Assert\Type('string'),
                new Assert\NotIdenticalTo(''),
                new Assert\Length(['max' => 48]),
            ]),
            'city' => new AppAssert\Chain([
                new Assert\Type('string'),
                new Assert\NotBlank(),
                new Assert\Length(['max' => 32]),
            ]),
            'county' => new AppAssert\Chain([
                new Assert\Type('string'),
                new Assert\NotIdenticalTo(''),
                new Assert\Length(['max' => 32]),
            ]),
            'postcode' => new AppAssert\Chain([
                new Assert\Type('string'),
                new Assert\NotBlank(),
                new Assert\Length(['max' => 16]),
            ]),
            'country' => new AppAssert\Chain([
                new Assert\Type('string'),
                new Assert\NotBlank(),
                new Assert\Country(),
            ]),
            'reg_number' => new AppAssert\Chain([
                new Assert\Type('string'),
                new Assert\NotIdenticalTo(''),
                new Assert\Length(['max' => 64]),
            ]),
            'vat_reg_number' => new AppAssert\Chain([
                new Assert\Type('string'),
                new Assert\NotIdenticalTo(''),
                new Assert\Length(['max' => 64]),
            ]),
            'regions' => new AppAssert\Chain([
                new Assert\Type('array'),
                new Assert\NotNull(),
                new Assert\Count(['min' => 1]),
                new AppAssert\Unique(),
                new Assert\All(new AppAssert\Chain([
                    new Assert\Type('string'),
                    new Assert\NotBlank(),
                    new Assert\Length(['max' => 72]),
                ])),
            ]),
            'departments' => new AppAssert\Chain([
                new Assert\Type('array'),
                new Assert\NotNull(),
                new Assert\Count(['min' => 1]),
                new AppAssert\Unique(),
                new Assert\All(new AppAssert\Chain([
                    new Assert\Type('string'),
                    new Assert\NotBlank(),
                    new Assert\Length(['max' => 72]),
                ])),
            ]),
            'po_number' => new AppAssert\Chain([
                new Assert\Type('string'),
                new Assert\NotIdenticalTo(''),
                new Assert\Length(['max' => 32]),
            ]),
            'tok_record' => new AppAssert\Chain([
	            new Assert\Type('bool'),
	            new Assert\NotNull(),
            ]),
            'session_length' => new AppAssert\Chain([
                new Assert\Type('array'),
                new Assert\NotNull(),
                new Assert\All(new AppAssert\Chain([
                    new Assert\Type('boolean'),
                ])),
            ]),
        ];

        if ($onUpdate) {
            $collection['brief'] = new AppAssert\Chain([
                new Assert\Type('string'),
                new Assert\NotIdenticalTo(''),
                new Assert\Length(['max' => 2500]),
            ]);
        }

        if ($this->isGranted('ROLE_ADMIN')) {
            $collection['is_enabled'] = new AppAssert\Chain([
                new Assert\Type('bool'),
                new Assert\NotNull(),
            ]);
        }
        //
        if ($request->request->has('enable_coaches')) {
            $collection['enable_coaches'] = new AppAssert\Chain([
                new Assert\Type('array'),
                new Assert\Count(['min' => 0]),
                new Assert\All(new AppAssert\Chain([
                    new Assert\Type('integer'),
                    new Assert\NotBlank(),
                ])),
            ]);
        }

        $validator = $this->get(RequestValidator::class);
        $validator->validate($request->request, [
            new Assert\NotBlank(),
            new Assert\Collection([
                'allowMissingFields' => $onUpdate,
                'fields' => $collection,
            ]),
        ]);
    }
}
