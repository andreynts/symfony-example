<?php

namespace App\Controller\Traits;

use App\Entity\User;
use App\Enum\ErrorEnum;
use App\Utility\Mailer;
use App\Utility\RequestValidator;
use App\Validator\Constraints as AppAssert;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

trait UserTrait
{
    private function viewUser(User $user)
    {
        $payload['email'] = $user->getEmail();
        $payload['roles'] = $user->getRoles();
        $payload['first_name'] = $user->getFirstName();
        $payload['last_name'] = $user->getLastName();
        $payload['photo'] = $user->getPhoto();
        $payload['is_verified'] = $user->isVerified();
        $payload['is_enabled'] = $user->isEnabled();
        $payload['last_auth_at'] = $user->getLastAuthAt();
        $payload['created_at'] = $user->getCreatedAt();
        $payload['email_updates'] = $user->getEmailUpdates();

        return $payload;
    }

    private function saveUser(Request $request, User $user)
    {
        $onUpdate = null !== $user->getId();

        $username = $user->getUsername();
        if ($request->request->has('email')) {
            $user->setUsername($request->request->get('email'));
        }

        if ($request->request->has('password')) {
            $password = $request->request->get('password');
            if (is_array($password)) {
                $password = $password['new'];
            }
            $encoder = $this->get('security.encoder_factory')->getEncoder($user);
            $user->setPassword($encoder->encodePassword($password, $user->getSalt()));
        }
        if ($request->request->has('first_name')) {
            $user->setFirstName($request->request->get('first_name'));
        }
        if ($request->request->has('last_name')) {
            $user->setLastName($request->request->get('last_name'));
        }
        if ($request->request->has('photo')) {
            $user->setPhoto($request->request->get('photo'));
        }
        if ($request->request->has('is_verified')) {
            $user->setVerified($request->request->get('is_verified'));
        }

        $isEnabled = $user->isEnabled();
        if ($request->request->has('is_enabled')) {
            $user->setEnabled($request->request->get('is_enabled'));
        }

	    if ($request->request->has('email_updates')) {
		    $user->setEmailUpdates($request->request->get('email_updates'));
	    }

        $em = $this->getDoctrine()->getManager();
        $em->persist($user);
        $em->flush();

        if ($onUpdate) {
            // executes only when the user is updated

            if ($username !== $user->getUsername() && !$this->isGranted('ROLE_ADMIN')) {
                $user->setVerified(false);
                $this->sendUserVerification($user, 'verify/user.html.twig', [
                    'first_name' => $user->getFirstName(),
                ]);
            }

            if (!$user->isEnabled() && $user->isEnabled() !== $isEnabled) {
                $mailer = $this->get(Mailer::class);
                $mailer->sendTo($user->getEmail(), 'user/disable.html.twig', [
                    'first_name' => $user->getFirstName(),
                ]);
            }
        }
    }

    private function validateUser(Request $request, User $user)
    {
        $onUpdate = null !== $user->getId();

        $collection = $this->_getUserConstraint($user);
        $validator = $this->get(RequestValidator::class);
        $validator->validate($request->request, [
            new Assert\NotBlank(),
            new Assert\Collection([
                'allowMissingFields' => $onUpdate,
                'fields' => $collection,
            ]),
        ]);
    }

    private function _getUserConstraint(User $user): array
    {
        $collection = [
            'first_name' => new AppAssert\Chain([
                new Assert\Type('string'),
                new Assert\NotBlank(),
                new Assert\Length(['max' => 32]),
            ]),
            'last_name' => new AppAssert\Chain([
                new Assert\Type('string'),
                new Assert\NotBlank(),
                new Assert\Length(['max' => 32]),
            ]),
            'photo' => new AppAssert\Chain([
                new Assert\Type('string'),
                new Assert\NotIdenticalTo(''),
                new Assert\Length(['max' => 255]),
                new AppAssert\Url([
                    'checkDNS' => false,
                    'allowedHostNames' => $this->getParameter('static_hosts'),
                    'allowedContentTypes' => ['image/jpeg'],
                ]),
            ]),
            'email' => new AppAssert\Chain([
                new Assert\Type('string'),
                new Assert\NotBlank(),
                new Assert\Length(['max' => 128]),
                new Assert\Email(['checkMX' => true]),
                new Assert\Callback(function ($value, ExecutionContextInterface $context) use ($user) {
                    if (null === $user->getId()) {
                        $user = null;
                    }
                    if ($this->getDoctrine()->getRepository('App:User')->checkUsernameExists($value, $user)) {
                        $context->buildViolation('E_USER_EXISTS')
                            ->setCode(ErrorEnum::E_USER_EXISTS)
                            ->addViolation();
                    }
                }),
            ]),
            'password' => new AppAssert\Chain([
                new Assert\Type('string'),
                new Assert\NotBlank(),
                new Assert\Length(['min' => 3, 'max' => 72]),
                new Assert\Regex('/^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.{8,})/'),
            ]),
            'email_updates' => new AppAssert\Chain([
	            new Assert\Type('bool'),
            ]),
        ];

        $isMyself = $this->getUser() && $this->getUser()->getId() === $user->getId();
        if (null !== $user->getId() && (!$this->isGranted('ROLE_ADMIN') || $isMyself)) {
            $collection['password'] = new AppAssert\Chain([
                new Assert\Type('array'),
                new Assert\NotNull(),
                new Assert\Collection([
                    'old' => $collection['password'],
                    'new' => clone $collection['password'],
                ]),
                new Assert\Callback(function ($value, ExecutionContextInterface $context) use ($user) {
                    $encoder = $this->get('security.encoder_factory')->getEncoder($user);
                    if (!$encoder->isPasswordValid($user->getPassword(), $value['old'], $user->getSalt())) {
                        $context->buildViolation('Wrong password')->setCode(ErrorEnum::E_BAD_CREDENTIALS)->addViolation();
                    }
                }),
            ]);
        }

        if ($this->isGranted('ROLE_ADMIN')) {
            $collection['is_verified'] = new AppAssert\Chain([
                new Assert\Type('bool'),
                new Assert\NotNull(),
            ]);
            $collection['is_enabled'] = new AppAssert\Chain([
                new Assert\Type('bool'),
                new Assert\NotNull(),
            ]);
        }

        return $collection;
    }

    private function sendUserVerification(User $user, string $view, array $parameters = []): int
    {
        $parameters['token'] = $this->get('lexik_jwt_authentication.encoder')->encode([
            'exp' => (new \DateTime('+1 year'))->getTimestamp(),
            '$verify' => true,
            '$username' => $user->getUsername(),
        ]);

        return $this->get(Mailer::class)->sendTo($user->getEmail(), $view, $parameters);
    }
}
