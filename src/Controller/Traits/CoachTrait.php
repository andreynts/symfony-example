<?php

namespace App\Controller\Traits;

use App\Entity\Coach;
use App\Entity\CoachCareerCompany;
use App\Entity\CoachCareerIndustry;
use App\Entity\CoachCoachedCompany;
use App\Entity\CoachCoachedPeople;
use App\Entity\CoachExpertise;
use App\Entity\CoachQualification;
use App\Entity\CoachTechExpertise;
use App\Enum\ErrorEnum;
use App\Enum\ExpertiseLevelEnum;
use App\Enum\SessionTypeEnum;
use App\Utility\DataReader;
use App\Utility\Mailer;
use App\Utility\RequestValidator;
use App\Validator\Constraints as AppAssert;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Validator\Constraints as Assert;

trait CoachTrait
{
    private function viewCoach(Coach $coach): array
    {
        $user = $coach->getUser();
        $payload['first_name'] = $user->getFirstName();
        $payload['last_name'] = $user->getLastName();
        $payload['photo'] = $user->getPhoto();
        $payload['about'] = $coach->getAbout();
        $payload['testimonial'] = $coach->getTestimonial();
        $payload['languages'] = $coach->getLanguages();
        $payload['industries'] = $coach->getIndustries(true);
        $payload['expertise'] = $coach->getExpertise(true);
        $payload['session_types'] = $coach->getSessionTypes();
        $payload['country'] = $coach->getCountry();

        /* @deprecated */
        if (in_array(SessionTypeEnum::EXECUTIVE, $coach->getSessionTypes())) {
            $payload['is_executive'] = true;
        }

        if ($this->isGranted('ROLE_COACH') || $this->isGranted('ROLE_ADMIN')) {
            $payload['email'] = $user->getEmail();
            $payload['mobile_number'] = $coach->getMobileNumber();
        }

        $isPersonal = $this->isGranted('ROLE_COACH') &&
            $this->getCoachUser()->getId() === $coach->getId();

        if ($isPersonal || $this->isGranted('ROLE_ADMIN')) {
            $payload['secondary_email'] = $coach->getSecondaryEmail();
            $payload['age_range'] = $coach->getAgeRange();
            $payload['gender'] = $coach->getGender();
            $payload['nationality'] = $coach->getNationality();
            $payload['address1'] = $coach->getAddress1();
            $payload['address2'] = $coach->getAddress2();
            $payload['city'] = $coach->getCity();
            $payload['county'] = $coach->getCounty();
            $payload['postcode'] = $coach->getPostcode();
            $payload['latitude'] = $coach->getLatitude();
            $payload['longitude'] = $coach->getLongitude();
            $payload['employment_status'] = $coach->getEmploymentStatus();
            $payload['company_name'] = $coach->getCompanyName();
            $payload['company_website'] = $coach->getCompanyWebsite();
            $payload['company_type'] = $coach->getCompanyType();
            $payload['company_reg_number'] = $coach->getCompanyRegNumber();
            $payload['company_vat_reg_number'] = $coach->getCompanyVatRegNumber();
            $payload['company_indemnity_provider'] = $coach->getCompanyIndemnityProvider();
            $payload['company_indemnity_cover'] = $coach->getCompanyIndemnityCover();
            $payload['other_companies'] = $coach->getOtherCompanies();
            $payload['qualifications'] = $coach->getQualifications(true);
            $payload['other_relevant_qualifications'] = $coach->getOtherRelevantQualifications();
            $payload['other_qualifications'] = $coach->getOtherQualifications();
            $payload['tech_expertise'] = $coach->getTechExpertise(true);
            $payload['other_senior_position'] = $coach->getOtherSeniorPosition();
            $payload['corporate_week_time'] = $coach->getCorporateWeekTime();
            $payload['career_sme_time'] = $coach->getCareerSmeTime();
            $payload['career_sme_roles'] = $coach->getCareerSmeRoles();
            $payload['career_corporate_time'] = $coach->getCareerCorporateTime();
            $payload['career_corporate_roles'] = $coach->getCareerCorporateRoles();
            $payload['career_companies'] = $coach->getCareerCompanies(true);
            $payload['career_industries'] = $coach->getCareerIndustries(true);
            $payload['coached_people'] = $coach->getCoachedPeople(true);
            $payload['coached_companies'] = $coach->getCoachedCompanies(true);
            $payload['coach_hours_in_year'] = $coach->getCoachHoursInYear();
        }
        if ($this->isGranted('ROLE_CLIENT')) {
            $favouriteCoaches = $this->getClientUser()->getFavouriteCoaches();
            $payload['is_favourite'] = $favouriteCoaches->contains($coach);
        }
        if (!$isPersonal) {
            $payload['is_verified'] = $user->isVerified();
            $payload['is_enabled'] = $user->isEnabled();
        }

        return $payload;
    }

    private function saveCoach(Request $request, Coach $coach)
    {
        $onUpdate = null !== $coach->getId();

        $payload = $this->_validateCoach($request, $coach);

        if ($request->request->has('secondary_email')) {
            $coach->setSecondaryEmail($request->request->get('secondary_email'));
        }
        if ($request->request->has('mobile_number')) {
            $coach->setMobileNumber($request->request->get('mobile_number'));
        }
        if ($request->request->has('languages')) {
            $coach->setLanguages($request->request->get('languages'));
        }
        if ($request->request->has('age_range')) {
            $coach->setAgeRange($request->request->get('age_range'));
        }
        if ($request->request->has('gender')) {
            $coach->setGender($request->request->get('gender'));
        }
        if ($request->request->has('country')) {
            $coach->setCountry($request->request->get('country'));
        }
        if ($request->request->has('nationality')) {
            $coach->setNationality($request->request->get('nationality'));
        }
        if ($request->request->has('address1')) {
            $coach->setAddress1($request->request->get('address1'));
        }
        if ($request->request->has('address2')) {
            $coach->setAddress2($request->request->get('address2'));
        }
        if ($request->request->has('city')) {
            $coach->setCity($request->request->get('city'));
        }
        if ($request->request->has('county')) {
            $coach->setCounty($request->request->get('county'));
        }
        if ($request->request->has('postcode')) {
            $coach->setPostcode($request->request->get('postcode'));
        }
        if ($request->request->has('latitude')) {
            $coach->setLatutude($request->request->get('latitude'));
        }
        if ($request->request->has('longitude')) {
            $coach->setLongitude($request->request->get('longitude'));
        }
        if ($request->request->has('employment_status')) {
            $coach->setEmploymentStatus($request->request->get('employment_status'));
        }
        if ($request->request->has('company_name')) {
            $coach->setCompanyName($request->request->get('company_name'));
        }
        if ($request->request->has('company_website')) {
            $coach->setCompanyWebsite($request->request->get('company_website'));
        }
        if ($request->request->has('company_type')) {
            $coach->setCompanyType($request->request->get('company_type'));
        }
        if ($request->request->has('company_reg_number')) {
            $coach->setCompanyRegNumber($request->request->get('company_reg_number'));
        }
        if ($request->request->has('company_vat_reg_number')) {
            $coach->setCompanyVatRegNumber($request->request->get('company_vat_reg_number'));
        }
        if ($request->request->has('company_indemnity_provider')) {
            $coach->setCompanyIndemnityProvider($request->request->get('company_indemnity_provider'));
        }
        if ($request->request->has('company_indemnity_cover')) {
            $coach->setCompanyIndemnityCover($request->request->get('company_indemnity_cover'));
        }
        if ($request->request->has('other_companies')) {
            $coach->setOtherCompanies($request->request->get('other_companies'));
        }
        if ($request->request->has('qualifications')) {
            $coach->getQualifications()->clear();
            foreach ($request->request->get('qualifications') as $qualification) {
                $coachQualification = new CoachQualification();
                $coachQualification->setCoach($coach);
                $coachQualification->setName($qualification['name']);
                $coachQualification->setAwardedAt(new \DateTime($qualification['awarded_at']));
                $coach->getQualifications()->add($coachQualification);
            }
        }
        if ($request->request->has('other_relevant_qualifications')) {
            $coach->setOtherRelevantQualifications($request->request->get('other_relevant_qualifications'));
        }
        if ($request->request->has('other_qualifications')) {
            $coach->setOtherQualifications($request->request->get('other_qualifications'));
        }
        if ($request->request->has('tech_expertise')) {
            $coach->getTechExpertise()->clear();
            foreach ($request->request->get('tech_expertise') as $name) {
                $coachTechExpertise = new CoachTechExpertise();
                $coachTechExpertise->setCoach($coach);
                $coachTechExpertise->setName($name);
                $coach->getTechExpertise()->add($coachTechExpertise);
            }
        }
        if ($request->request->has('other_senior_position')) {
            $coach->setOtherSeniorPosition($request->request->get('other_senior_position'));
        }
        if ($request->request->has('corporate_week_time')) {
            $coach->setCorporateWeekTime($request->request->get('corporate_week_time'));
        }
        if ($request->request->has('career_sme_time')) {
            $coach->setCareerSmeTime($request->request->get('career_sme_time'));
        }
        if ($request->request->has('career_sme_roles')) {
            $coach->setCareerSmeRoles($request->request->get('career_sme_roles'));
        }
        if ($request->request->has('career_corporate_time')) {
            $coach->setCareerCorporateTime($request->request->get('career_corporate_time'));
        }
        if ($request->request->has('career_corporate_roles')) {
            $coach->setCareerCorporateRoles($request->request->get('career_corporate_roles'));
        }
        if ($request->request->has('career_companies')) {
            $coach->getCareerCompanies()->clear();
            foreach ($request->request->get('career_companies') as $name) {
                $coachCareerCompany = new CoachCareerCompany();
                $coachCareerCompany->setCoach($coach);
                $coachCareerCompany->setName($name);
                $coach->getCareerCompanies()->add($coachCareerCompany);
            }
        }
        if ($request->request->has('career_industries')) {
            $coach->getCareerIndustries()->clear();
            foreach ($request->request->get('career_industries') as $name) {
                $coachCareerIndustry = new CoachCareerIndustry();
                $coachCareerIndustry->setCoach($coach);
                $coachCareerIndustry->setName($name);
                $coach->getCareerIndustries()->add($coachCareerIndustry);
            }
        }
        if ($request->request->has('coached_people')) {
            $coach->getCoachedPeople()->clear();
            foreach ($request->request->get('coached_people') as $name) {
                $coachCoachedPeople = new CoachCoachedPeople();
                $coachCoachedPeople->setCoach($coach);
                $coachCoachedPeople->setName($name);
                $coach->getCoachedPeople()->add($coachCoachedPeople);
            }
        }
        if ($request->request->has('coached_companies')) {
            $coach->getCoachedCompanies()->clear();
            foreach ($request->request->get('coached_companies') as $name) {
                $coachCoachedCompany = new CoachCoachedCompany();
                $coachCoachedCompany->setCoach($coach);
                $coachCoachedCompany->setName($name);
                $coach->getCoachedCompanies()->add($coachCoachedCompany);
            }
        }
        if ($request->request->has('coach_hours_in_year')) {
            $coach->setCoachHoursInYear($request->request->get('coach_hours_in_year'));
        }
        if ($request->request->has('industries')) {
            $coach->getIndustries()->clear();
            foreach ($payload['industries'] as $industry) {
                $coach->getIndustries()->add($industry);
            }
        }
        if ($request->request->has('expertise')) {
            $coach->getExpertise()->clear();
            foreach ($payload['expertise'] as $expertise) {
                $coachExpertise = new CoachExpertise();
                $coachExpertise->setCoach($coach);
                $coachExpertise->setExpertise($expertise['expertise']);
                $coachExpertise->setLevel($expertise['level']);
                $coach->getExpertise()->add($coachExpertise);
            }
        }
        if ($request->request->has('about')) {
            $coach->setAbout($request->request->get('about'));
        }
        if ($request->request->has('testimonial')) {
            $coach->setTestimonial($request->request->get('testimonial'));
        }
        if ($request->request->has('session_types')) {
            $coach->setSessionTypes($request->request->get('session_types'));
        }

        $em = $this->getDoctrine()->getManager();
        $em->persist($coach);

        $user = $coach->getUser();
        $isVerified = $user->isVerified();
        $this->saveUser($request, $user);

        if ($onUpdate) {
            // executes only when the coach is updated
            if ($user->isVerified() && $user->isVerified() !== $isVerified) {
                $mailer = $this->get(Mailer::class);
                $mailer->sendTo($user->getEmail(), 'verify/coach.html.twig', [
                    'first_name' => $user->getFirstName(),
                ]);
            }
        }
    }

    private function _validateCoach(Request $request, Coach $coach): array
    {
        $onUpdate = null !== $coach->getId();

        $reader = $this->get(DataReader::class);
        $ageRangeList = $reader->read('age-range.json');
        $genderList = $reader->read('gender.json');
        $nationalityList = $reader->read('nationality.json');
        $indemnityCoverList = $reader->read('coach/indemnity-cover.json');

        $collection = $this->_getUserConstraint($coach->getUser()) + [
            // STEP 1
            'secondary_email' => new AppAssert\Chain([
                new Assert\Type('string'),
                new Assert\NotIdenticalTo(''),
                new Assert\Length(['max' => 128]),
                new Assert\Email(['checkMX' => true]),
            ]),
            'mobile_number' => new AppAssert\Chain([
                new Assert\Type('string'),
                new Assert\NotBlank(),
                new Assert\Length(['max' => 32]),
            ]),
            'age_range' => new AppAssert\Chain([
                new Assert\Type('string'),
                new Assert\NotBlank(),
                new Assert\Choice([
                    'choices' => $ageRangeList,
                    'strict' => true,
                ]),
            ]),
            'gender' => new AppAssert\Chain([
                new Assert\Type('string'),
                new Assert\NotBlank(),
                new Assert\Choice([
                    'choices' => $genderList,
                    'strict' => true,
                ]),
            ]),
            'country' => new AppAssert\Chain([
                new Assert\Type('string'),
                new Assert\NotBlank(),
                new Assert\Country(),
            ]),
            'nationality' => new AppAssert\Chain([
                new Assert\Type('string'),
                new Assert\NotBlank(),
                new Assert\Choice([
                    'choices' => $nationalityList,
                    'strict' => true,
                ]),
            ]),
            'address1' => new AppAssert\Chain([
                new Assert\Type('string'),
                new Assert\NotBlank(),
                new Assert\Length(['max' => 48]),
            ]),
            'address2' => new AppAssert\Chain([
                new Assert\Type('string'),
                new Assert\NotIdenticalTo(''),
                new Assert\Length(['max' => 48]),
            ]),
            'city' => new AppAssert\Chain([
                new Assert\Type('string'),
                new Assert\NotBlank(),
                new Assert\Length(['max' => 32]),
            ]),
            'county' => new AppAssert\Chain([
                new Assert\Type('string'),
                new Assert\NotIdenticalTo(''),
                new Assert\Length(['max' => 32]),
            ]),
            'postcode' => new AppAssert\Chain([
                new Assert\Type('string'),
                new Assert\NotBlank(),
                new Assert\Length(['max' => 16]),
            ]),
            'latitude' => new AppAssert\Chain([
                new Assert\Type('float'),
                new Assert\Range(['min' => -85, 'max' => 85]),
            ]),
            'longitude' => new AppAssert\Chain([
                new Assert\Type('float'),
                new Assert\Range(['min' => -180, 'max' => 180]),
            ]),

            // STEP 2
            'employment_status' => new AppAssert\Chain([
                new Assert\Type('string'),
                new Assert\NotBlank(),
                new Assert\Length(['max' => 32]),
            ]),
            'company_name' => new AppAssert\Chain([
                new Assert\Type('string'),
                new Assert\NotIdenticalTo(''),
                new Assert\Length(['max' => 64]),
            ]),
            'company_website' => new AppAssert\Chain([
                new Assert\Type('string'),
                new Assert\NotIdenticalTo(''),
                new Assert\Length(['max' => 255]),
                new Assert\Url(['checkDNS' => true]),
            ]),
            'company_type' => new AppAssert\Chain([
                new Assert\Type('string'),
                new Assert\NotIdenticalTo(''),
                new Assert\Length(['max' => 32]),
            ]),
            'company_reg_number' => new AppAssert\Chain([
                new Assert\Type('string'),
                new Assert\NotIdenticalTo(''),
                new Assert\Length(['max' => 64]),
            ]),
            'company_vat_reg_number' => new AppAssert\Chain([
                new Assert\Type('string'),
                new Assert\NotIdenticalTo(''),
                new Assert\Length(['max' => 64]),
            ]),
            'company_indemnity_provider' => new AppAssert\Chain([
                new Assert\Type('string'),
                new Assert\NotIdenticalTo(''),
                new Assert\Length(['max' => 64]),
            ]),
            'company_indemnity_cover' => new AppAssert\Chain([
                new Assert\Type('string'),
                new Assert\NotIdenticalTo(''),
                new Assert\Choice([
                    'choices' => $indemnityCoverList,
                    'strict' => true,
                ]),
            ]),
            'other_companies' => new AppAssert\Chain([
                new Assert\Type('string'),
                new Assert\NotIdenticalTo(''),
                new Assert\Length(['max' => 512]),
            ]),

            // STEP 3
            'qualifications' => new AppAssert\Chain([
                new Assert\Type('array'),
                new Assert\NotNull(),
                new Assert\Count(['max' => 3]),
                new AppAssert\Unique(['name', 'awarded_at']),
                new Assert\All(new AppAssert\Chain([
                    new Assert\Type('array'),
                    new Assert\NotNull(),
                    new Assert\Collection([
                        'name' => new AppAssert\Chain([
                            new Assert\Type('string'),
                            new Assert\NotBlank(),
                            new Assert\Length(['max' => 128]),
                        ]),
                        'awarded_at' => new AppAssert\Chain([
                            new Assert\Type('string'),
                            new Assert\NotBlank(),
                            new Assert\Date(),
                        ]),
                    ]),
                ])),
            ]),
            'other_relevant_qualifications' => new AppAssert\Chain([
                new Assert\Type('string'),
                new Assert\NotIdenticalTo(''),
                new Assert\Length(['max' => 128]),
            ]),
            'other_qualifications' => new AppAssert\Chain([
                new Assert\Type('string'),
                new Assert\NotIdenticalTo(''),
                new Assert\Length(['max' => 128]),
            ]),
            'languages' => new AppAssert\Chain([
                new Assert\Type('array'),
                new Assert\NotNull(),
                new Assert\Count(['min' => 1]),
                new AppAssert\Unique(),
                new Assert\All(new AppAssert\Chain([
                    new Assert\Type('string'),
                    new Assert\NotBlank(),
                    new Assert\Language(),
                ])),
            ]),
            'tech_expertise' => new AppAssert\Chain([
                new Assert\Type('array'),
                new Assert\NotNull(),
                new AppAssert\Unique(),
                new Assert\All(new AppAssert\Chain([
                    new Assert\Type('string'),
                    new Assert\NotBlank(),
                    new Assert\Length(['max' => 64]),
                ])),
            ]),

            // STEP 4
            'other_senior_position' => new AppAssert\Chain([
                new Assert\Type('string'),
                new Assert\NotBlank(),
                new Assert\Length(['max' => 64]),
            ]),
            'corporate_week_time' => new AppAssert\Chain([
                new Assert\Type('int'),
                new Assert\NotNull(),
                new Assert\Range(['min' => 0, 'max' => 100]),
            ]),
            'career_sme_time' => new AppAssert\Chain([
                new Assert\Type('int'),
                new Assert\NotNull(),
                new Assert\Range(['min' => 0, 'max' => 100]),
            ]),
            'career_sme_roles' => new AppAssert\Chain([
                new Assert\Type('string'),
                new Assert\NotIdenticalTo(''),
                new Assert\Length(['max' => 128]),
            ]),
            'career_corporate_time' => new AppAssert\Chain([
                new Assert\Type('int'),
                new Assert\NotNull(),
                new Assert\Range(['min' => 0, 'max' => 100]),
            ]),
            'career_corporate_roles' => new AppAssert\Chain([
                new Assert\Type('string'),
                new Assert\NotIdenticalTo(''),
                new Assert\Length(['max' => 128]),
            ]),
            'career_companies' => new AppAssert\Chain([
                new Assert\Type('array'),
                new Assert\NotNull(),
                new Assert\Count(['min' => 1]),
                new AppAssert\Unique(),
                new Assert\All(new AppAssert\Chain([
                    new Assert\Type('string'),
                    new Assert\NotBlank(),
                    new Assert\Length(['max' => 64]),
                ])),
            ]),
            'career_industries' => new AppAssert\Chain([
                new Assert\Type('array'),
                new Assert\NotNull(),
                new Assert\Count(['min' => 1]),
                new AppAssert\Unique(),
                new Assert\All(new AppAssert\Chain([
                    new Assert\Type('string'),
                    new Assert\NotBlank(),
                    new Assert\Length(['max' => 64]),
                ])),
            ]),

            // STEP 5
            'coached_people' => new AppAssert\Chain([
                new Assert\Type('array'),
                new Assert\NotNull(),
                new Assert\Count(['min' => 1]),
                new AppAssert\Unique(),
                new Assert\All(new AppAssert\Chain([
                    new Assert\Type('string'),
                    new Assert\NotBlank(),
                    new Assert\Length(['max' => 64]),
                ])),
            ]),
            'coached_companies' => new AppAssert\Chain([
                new Assert\Type('array'),
                new Assert\NotNull(),
                new Assert\Count(['min' => 1]),
                new AppAssert\Unique(),
                new Assert\All(new AppAssert\Chain([
                    new Assert\Type('string'),
                    new Assert\NotBlank(),
                    new Assert\Length(['max' => 64]),
                ])),
            ]),
            'coach_hours_in_year' => new AppAssert\Chain([
                new Assert\Type('string'),
                new Assert\NotBlank(),
                new Assert\Length(['max' => 128]),
            ]),
            'industries' => new AppAssert\Chain([
                new Assert\Type('array'),
                new Assert\NotNull(),
                new Assert\Count(['min' => 1]),
                new AppAssert\Unique(),
                new Assert\All(new AppAssert\Chain([
                    new Assert\Type('int'),
                    new Assert\NotNull(),
                    new Assert\GreaterThan(0),
                ])),
            ]),
            'expertise' => new AppAssert\Chain([
                new Assert\Type('array'),
                new Assert\NotNull(),
                new Assert\Count(['min' => 1]),
                new AppAssert\Unique('id'),
                new Assert\All(new AppAssert\Chain([
                    new Assert\Type('array'),
                    new Assert\NotNull(),
                    new Assert\Collection([
                        'id' => new AppAssert\Chain([
                            new Assert\Type('int'),
                            new Assert\NotNull(),
                            new Assert\GreaterThan(0),
                        ]),
                        'level' => new AppAssert\Chain([
                            new Assert\Type('string'),
                            new Assert\NotBlank(),
                            new Assert\Choice([
                                'choices' => ExpertiseLevelEnum::values(),
                                'strict' => true,
                            ]),
                        ]),
                    ]),
                ])),
            ]),

            // STEP 6
            'about' => new AppAssert\Chain([
                new Assert\Type('string'),
                new Assert\NotBlank(),
                new Assert\Length(['max' => 1000]),
            ]),
            'testimonial' => new AppAssert\Chain([
                new Assert\Type('string'),
                new Assert\NotBlank(),
                new Assert\Length(['max' => 350]),
            ]),
        ];

        if ($this->isGranted('ROLE_ADMIN')) {
            $collection['session_types'] = new AppAssert\Chain([
                new Assert\Type('array'),
                new Assert\NotNull(),
                new Assert\Count(['min' => 1]),
                new AppAssert\Unique(),
                new AppAssert\Exists(SessionTypeEnum::DEFAULT_),
                new Assert\All(new AppAssert\Chain([
                    new Assert\Type('string'),
                    new Assert\NotBlank(),
                    new Assert\Choice([
                        'choices' => SessionTypeEnum::values(),
                        'strict' => true,
                    ]),
                ])),
            ]);
        }

        $validator = $this->get(RequestValidator::class);
        $validator->validate($request->request, [
            new Assert\NotBlank(),
            new Assert\Collection([
                'allowMissingFields' => $onUpdate,
                'fields' => $collection,
            ]),
        ]);

        $em = $this->getDoctrine()->getManager();

        $payload['industries'] = [];
        if ($request->request->has('industries')) {
            $industryIdList = $request->request->get('industries');
            $industries = $em->getRepository('App:Industry')->findById($industryIdList);
            if (count($industryIdList) !== count($industries)) {
                throw new BadRequestHttpException('Invalid industries', null, ErrorEnum::E_BAD_REQUEST);
            }
            $payload['industries'] = $industries;
        }

        $payload['expertise'] = [];
        if ($request->request->has('expertise')) {
            $expertiseList = $request->request->get('expertise');
            $expertiseLevelList = array_column($expertiseList, 'level', 'id');
            $expertise = $em->getRepository('App:Expertise')->findById(array_keys($expertiseLevelList));
            if (count($expertiseLevelList) !== count($expertise)) {
                throw new BadRequestHttpException('Invalid expertise', null, ErrorEnum::E_BAD_REQUEST);
            }
            $payload['expertise'] = array_map(function ($expertise) use ($expertiseLevelList) {
                $level = $expertiseLevelList[$expertise->getId()];

                return compact('expertise', 'level');
            }, $expertise);
        }

        return $payload;
    }
}
