<?php

namespace App\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

class SettingController extends AbstractController
{
    /**
     * @Route("/settings/apps")
     * @Method("GET")
     */
    public function indexAction(Request $request)
    {
        $locationName = $request->query->get('location');
        $result = [];

        if ($locationName) {
            $em = $this->getDoctrine()->getManager();
            $settingsLocRepo = $em->getRepository('App:SettingLocation');
            $location = $settingsLocRepo->findLocationByValue($locationName);

            if ($location) {
                $questionRepository = $em->getRepository('App:Setting');

                $result = $questionRepository->getSettingsByLocation($location);
            }
        }

        return [
            "result" => $result,
            "location" => $locationName
        ];
    }
}