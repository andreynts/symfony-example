<?php

namespace App\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

class QuestionController extends AbstractController
{
    /**
     * @Security("is_granted('ROLE_CLIENT') or is_granted('ROLE_COACH')")
     * @Route("/questions")
     * @Method("GET")
     */
    public function indexAction(Request $request)
    {
        //TODO
    }

    /**
     * @Security("is_granted('ROLE_CLIENT') or is_granted('ROLE_COACH')")
     * @Route("/questions/get-questions")
     * @Method("GET")
     */
    public function getQuestionsAction(Request $request)
    {
        $locationName = $request->query->get('location');

        if ($locationName) {
            $em = $this->getDoctrine()->getManager();
            $questionLocRepo = $em->getRepository('App:QuestionLocation');
            $location = $questionLocRepo->findLocationByValue($locationName);

            if ($location) {
                $questionRepository = $em->getRepository('App:Question');

                return $questionRepository->getFormByQuestionLocation($location);
            }
        }

        return [];
    }

    /**
     * @Security("is_granted('ROLE_CLIENT') or is_granted('ROLE_COACH')")
     * @Route("/questions/create")
     * @Method("POST")
     */
    public function createAction(Request $request)
    {
        //TODO createAction
    }
}