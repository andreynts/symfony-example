<?php

namespace App\Controller;

use App\Entity\CoachAvailability;
use App\Entity\Session;
use App\Enum\ErrorEnum;
use App\Utility\RequestValidator;
use App\Validator\Constraints as AppAssert;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @Security("is_granted('ROLE_COACH')")
 */
class AvailabilityController extends AbstractController
{
    /**
     * @Route("/availability")
     * @Method("GET")
     */
    public function indexAction(Request $request)
    {
        $validator = $this->get(RequestValidator::class);
        $validator->validate($request->query, new Assert\Collection([
            'allowMissingFields' => true,
            'fields' => ['date' => new AppAssert\Chain([
                new Assert\Type('array'),
                new Assert\Collection([
                    'start' => [
                        new Assert\NotBlank(),
                        new Assert\DateTime(['format' => \DateTime::RFC3339]),
                    ],
                    'end' => [
                        new Assert\NotBlank(),
                        new Assert\DateTime(['format' => \DateTime::RFC3339]),
                    ],
                ]),
            ])],
        ]));

        $date = $request->query->get('date', [
            'start' => 'first day of this month 00:00:00',
            'end' => 'last day of this month 23:59:59',
        ]);
        $startedAt = new \DateTime($date['start']);
        $endedAt = new \DateTime($date['end']);
        if ($startedAt >= $endedAt) {
            throw new BadRequestHttpException('E_INTERVAL_INVALID', null, ErrorEnum::E_INTERVAL_INVALID);
        }

        $em = $this->getDoctrine()->getManager();
        $qb = $em->createQueryBuilder();
        $qb->select('a.id', 'a.startedAt started_at', 'a.endedAt ended_at')
            ->from('App:CoachAvailability', 'a')
            ->leftJoin('App:Session', 's', 'WITH', 's.availability = a')
            ->where('a.coach = :coach AND s.id IS NULL')
            ->andWhere('a.startedAt >= :started_at OR a.endedAt > :started_at')
            ->andWhere('a.endedAt <= :ended_at OR a.startedAt < :ended_at')
            ->setParameter('coach', $this->getCoachUser())
            ->setParameter('started_at', $startedAt)
            ->setParameter('ended_at', $endedAt)
            ->orderBy('a.startedAt');

        return $qb->getQuery()->getArrayResult();
    }

    /**
     * @Route("/availability")
     * @Method("POST")
     */
    public function createAction(Request $request)
    {
        $this->validateRequest($request);

        $startedAt = new \DateTime($request->request->get('started_at'));
        $endedAt = new \DateTime($request->request->get('ended_at'));

        $this->validateInterval($startedAt, $endedAt);

        $availability = new CoachAvailability();
        $availability->setCoach($this->getCoachUser());
        $availability->setStartedAt($startedAt);
        $availability->setEndedAt($endedAt);

        $em = $this->getDoctrine()->getManager();
        $em->persist($availability);
        $em->flush();

        return ['id' => $availability->getId()];
    }

    /**
     * @Security("is_granted('view', availability)")
     * @Route("/availability/{id}", requirements={"id": "\d+"})
     * @Method("GET")
     */
    public function viewAction(CoachAvailability $availability)
    {
        return $availability;
    }

    /**
     * @Security("is_granted('update', availability)")
     * @Route("/availability/{id}", requirements={"id": "\d+"})
     * @Method("PATCH")
     */
    public function updateAction(Request $request, CoachAvailability $availability)
    {
        $this->validateRequest($request, true);

        $startedAt = $availability->getStartedAt();
        if ($startedAtRequest = $request->request->get('started_at')) {
            $startedAt = new \DateTime($startedAtRequest);
        }

        $endedAt = $availability->getEndedAt();
        if ($endedAtRequest = $request->request->get('ended_at')) {
            $endedAt = new \DateTime($endedAtRequest);
        }

        if ($startedAt == $availability->getStartedAt() && $endedAt == $availability->getEndedAt()) {
            return self::STATUS_OK;
        }

        $this->validateInterval($startedAt, $endedAt, $availability);

        $availability->setStartedAt($startedAt);
        $availability->setEndedAt($endedAt);

        $em = $this->getDoctrine()->getManager();
        $em->persist($availability);
        $em->flush();

        return self::STATUS_OK;
    }

    /**
     * @Security("is_granted('delete', availability)")
     * @Route("/availability/{id}", requirements={"id": "\d+"})
     * @Method("DELETE")
     */
    public function deleteAction(CoachAvailability $availability)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($availability);
        $em->flush();

        return self::STATUS_OK;
    }

    private function validateRequest(Request $request, bool $allowMissingFields = false)
    {
        $validator = $this->get(RequestValidator::class);
        $validator->validate($request->request, [
            new Assert\NotBlank(),
            new Assert\Collection([
                'allowMissingFields' => $allowMissingFields,
                'fields' => [
                    'started_at' => new AppAssert\Chain([
                        new Assert\Type('string'),
                        new Assert\NotBlank(),
                        new Assert\DateTime(['format' => \DateTime::RFC3339]),
                    ]),
                    'ended_at' => new AppAssert\Chain([
                        new Assert\Type('string'),
                        new Assert\NotBlank(),
                        new Assert\DateTime(['format' => \DateTime::RFC3339]),
                    ]),
                ],
            ]),
        ]);
    }

    private function validateInterval(\DateTime $startedAt, \DateTime $endedAt, CoachAvailability $exclude = null)
    {
        if ($startedAt <= new \DateTime()) {
            throw new BadRequestHttpException('E_INTERVAL_PAST', null, ErrorEnum::E_INTERVAL_PAST);
        }

        $startedAtTs = $startedAt->getTimestamp();
        $endedAtTs = $endedAt->getTimestamp();
        if (Session::MIN_DURATION > ($endedAtTs - $startedAtTs) / 60) {
            throw new BadRequestHttpException('Too small interval', null, ErrorEnum::E_INTERVAL_INVALID);
        }

        if (is_float($startedAtTs / (Session::INTERVAL * 60)) || is_float($endedAtTs / (Session::INTERVAL * 60))) {
            throw new BadRequestHttpException('Uneven interval', null, ErrorEnum::E_INTERVAL_INVALID);
        }

        $startedAtSub = clone $startedAt;
        $startedAtSub->modify('-'.Session::INTERVAL.' minutes');

        $endedAtAdd = clone $endedAt;
        $endedAtAdd->modify('+'.Session::INTERVAL.' minutes');

        $em = $this->getDoctrine()->getManager();
        $qb = $em->createQueryBuilder();
        $qb->select('COUNT(a)')
            ->from('App:CoachAvailability', 'a')
            ->where('a.coach = :coach')
            ->andWhere($qb->expr()->orX(
                'a.startedAt >= :started_at AND a.startedAt < :ended_at',
                'a.endedAt > :started_at AND a.endedAt <= :ended_at',
                'a.startedAt <= :started_at AND a.endedAt >= :ended_at'
            ))
            ->setParameter('coach', $this->getCoachUser())
            ->setParameter('started_at', $startedAtSub)
            ->setParameter('ended_at', $endedAtAdd);

        if (null !== $exclude) {
            $qb->andWhere('a <> :exclude')
                ->setParameter('exclude', $exclude);
        }

        if ($qb->getQuery()->getSingleScalarResult() > 0) {
            throw new BadRequestHttpException('E_INTERVAL_CLASHED', null, ErrorEnum::E_INTERVAL_CLASHED);
        }
    }
}
