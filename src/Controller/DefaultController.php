<?php

namespace App\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Cache;
use Symfony\Component\Routing\RouterInterface;

class DefaultController extends AbstractController
{
    /**
     * @Cache(public=true, maxage=86400)
     */
    public function indexAction(RouterInterface $router)
    {
        foreach ($router->getRouteCollection() as $name => $route) {
            if (0 === strpos($name, 'app_')) {
                $routes[$name] = $route->getPath();
            }
        }
        ksort($routes);

        return $routes;
    }
}
