<?php

namespace App\Controller;

use App\Entity\Client;
use App\Entity\Coach;
use App\Entity\Company;
use App\Entity\User;
use App\Utility\Mailer;
use App\Utility\RequestValidator;
use App\Validator\Constraints\Token as TokenConstraint;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;

class RegisterController extends AbstractController
{
    use Traits\UserTrait;
    use Traits\ClientTrait;
    use Traits\CoachTrait;
    use Traits\CompanyTrait;

    /**
     * @Route("/register")
     * @Method("POST")
     */
    public function indexAction(Request $request)
    {
        $user = new User();
        $user->setRoles(['ROLE_CLIENT']);

        $client = new Client();
        $client->setUser($user);

        $tokenConstraint = new TokenConstraint([
            '$invite:client',
            '$username',
            '$first_name',
            '$last_name',
        ]);

        $validator = $this->get(RequestValidator::class);
        $validator->validate($request->query, new Assert\Collection([
            'allowMissingFields' => true,
            'fields' => ['token' => $tokenConstraint],
        ]));

        $isEnterpriseAdmin = false;
        if ($payload = $tokenConstraint->payload) {
            $request->request->set('email', $payload['$username']);
            if (isset($payload['$company'])) {
                $repository = $this->getDoctrine()->getRepository('App:Company');
                $company = $repository->find($payload['$company']['id']);
                $request->request->set('company_name', $company->getName());
                $user->setRoles(['ROLE_ENTERPRISE_CLIENT']);
                $client->setCompany($company);

                if (isset($payload['$invite:company'])) {
                    $user->setRoles(['ROLE_ENTERPRISE_ADMIN']);
                    $client->setCompanyContact(true);
                    $isEnterpriseAdmin = true;
                }

                if (isset($payload['$invite:client']) && isset($payload['$inviteId'])) {
                    $inviteRepository = $this->getDoctrine()->getRepository('App:Invite');
                    $invite = $inviteRepository->find($payload['$inviteId']);

                    if ($invite) {
                        $invite->setRegistered(true);
                        $em = $this->getDoctrine()->getManager();
                        $em->persist($invite);
                    }
                }
            }
        }

        $this->saveClient($request, $client);
        $this->sendUserVerification($user, 'register/client.html.twig', [
            'first_name' => $user->getFirstName(),
            'is_enterprise_admin' => $isEnterpriseAdmin,
        ]);

        return self::STATUS_OK;
    }

    /**
     * @Route("/register/coach")
     * @Method("POST")
     */
    public function coachAction(Request $request)
    {
        $user = new User();
        $user->setRoles(['ROLE_COACH']);

        $coach = new Coach();
        $coach->setUser($user);

        $tokenConstraint = new TokenConstraint([
            '$invite:coach',
            '$username',
            '$first_name',
            '$last_name',
        ]);

        $validator = $this->get(RequestValidator::class);
        $validator->validate($request->query->get('token'), $tokenConstraint);

        $payload = $tokenConstraint->payload;
        $request->request->set('email', $payload['$username']);

        $this->saveCoach($request, $coach);

        $mailer = $this->get(Mailer::class);
        $mailer->sendTo($user->getEmail(), 'register/coach.html.twig', [
            'first_name' => $user->getFirstName(),
        ]);
        $mailer->sendToAdmin('register/admin.html.twig', [
            'coach_first_name' => $user->getFirstName(),
            'coach_last_name' => $user->getLastName(),
        ]);

        return self::STATUS_OK;
    }

    /**
     * @Route("/register/company")
     * @Method("POST")
     */
    public function companyAction(Request $request)
    {
        $company = new Company();

        $tokenConstraint = new TokenConstraint([
            '$invite:company',
            '$username',
            '$first_name',
            '$last_name',
            '$company',
        ]);

        $validator = $this->get(RequestValidator::class);
        $validator->validate($request->query->get('token'), $tokenConstraint);

        $payload = $tokenConstraint->payload;
        $request->request->set('name', $payload['$company']['name']);

        $this->saveCompany($request, $company);

        $payload['$invite:client'] = true;
        $payload['$company']['id'] = $company->getId();
        $token = $this->get('lexik_jwt_authentication.encoder')->encode($payload);

        return ['token' => $token];
    }
}
