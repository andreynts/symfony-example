<?php

namespace App\Controller;

use App\Enum\ErrorEnum;
use App\Utility\IcsGenerator;
use App\Utility\RequestValidator;
use App\Validator\Constraints\Token as TokenConstraint;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class MailController extends AbstractController
{
    public function indexAction(Request $request, $token)
    {
        $templatesType = ['GENERAL_TEMPLATE'];
        $em = $this->container->get('doctrine.orm.entity_manager');

        if (null === $token) {
            if (!$this->get('kernel')->isDebug()) {
                throw new AccessDeniedHttpException('E_ACCESS_DENIED', null, ErrorEnum::E_ACCESS_DENIED);
            }

            $view = $request->query->get('_view');
            $request->query->remove('_view');
            $params = $request->query->all();
        } else {
            $tokenConstraint = new TokenConstraint(['$view', '$params']);
            $validator = $this->get(RequestValidator::class);
            $validator->validate($token, $tokenConstraint);
            $view = $tokenConstraint->payload['$view'];
            $params = $tokenConstraint->payload['$params'];
        }

        if (0 === strpos($view, '@ics')) {
            $icsGenerator = new IcsGenerator();
            $params = array_change_key_case($params, CASE_UPPER);
            foreach (IcsGenerator::ICS_PROPERTIES as $property) {
                if (isset($params[$property])) {
                    $icsGenerator[$property] = $params[$property];
                }
            }

            $filename = ltrim(strrchr($view, '/'), '/');
            $response = new Response((string) $icsGenerator);
            $dispositionHeader = $response->headers->makeDisposition(
                ResponseHeaderBag::DISPOSITION_ATTACHMENT, $filename, 'calendar.ics');
            $response->headers->set('Content-Disposition', $dispositionHeader);
            $response->headers->set('Content-Type', 'text/calendar');

            return $response;
        }

        foreach ($templatesType as $templateType) {
            if ($templateType !== null) {
                $em = $this->container->get('doctrine.orm.entity_manager');
                $templateParameteres = $em->createQueryBuilder()
                    ->select(
                        'p.id',
                        'p.name',
                        'p.value',
                        't.email_template_name'
                    )
                    ->from('App:EmailTemplateParameter', 'p')
                    ->join('p.emailTemplateType', 't')
                    ->where('t.email_template_name = :param_name')
                    ->setParameter('param_name', $templateType)
                    ->getQuery()
                    ->getResult();
                foreach ($templateParameteres as $param) {
                    $params[$param['name']] = $param['value'];
                }
            }
        }
        $emailsStyles = $em->getRepository('App:Style')
            ->getEmailStyles();

        foreach ($emailsStyles as $style) {
            $params[$style['name']] = $style['value'];
        }


        try {
            return $this->render('mail/'.str_replace('..', '', $view), $params);
        } catch (\Twig_Error $ex) {
            throw new BadRequestHttpException($ex->getMessage(), $ex, ErrorEnum::E_BAD_REQUEST);
        }
    }
}
