<?php

namespace App\Controller;

use App\Utility\FileUploader;
use App\Utility\RequestValidator;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;

class UploadController extends AbstractController
{
    /**
     * @Route("/upload")
     * @Method("PUT")
     */
    public function indexAction(Request $request)
    {
        $uploader = $this->get(FileUploader::class);
        $file = $uploader->createFileFromRequest($request);

        $validator = $this->get(RequestValidator::class);
        $validator->validate($file, new Assert\File([
            'maxSize' => '8M',
            'mimeTypes' => [
                'application/msword',                                                           // doc
                'application/vnd.openxmlformats-officedocument.wordprocessingml.document',      // docx
                'application/vnd.ms-excel',                                                     // xls
                'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',            // xlsx
                'application/vnd.ms-powerpoint',                                                // ppt
                'application/vnd.openxmlformats-officedocument.presentationml.presentation',    // pptx
                'application/pdf',                                                              // pdf
                'image/jpeg',                                                                   // jpg
                'image/png',                                                                    // png
            ],
        ]));

        return ['url' => $uploader->storeFile($file)];
    }

    /**
     * @Route("/upload/user-photo")
     * @Method("PUT")
     */
    public function userPhotoAction(Request $request)
    {
        $uploader = $this->get(FileUploader::class);
        $file = $uploader->createFileFromRequest($request, 'jpg', true);

        $validator = $this->get(RequestValidator::class);
        $validator->validate($file, new Assert\Image([
            'maxSize' => '8M',
            'minWidth' => 300,
            'minHeight' => 300,
        ]));

        return ['url' => $uploader->storeImage($file, 100, 100)];
    }

    /**
     * @Route("/upload/company-logo")
     * @Method("PUT")
     */
    public function companyLogoAction(Request $request)
    {
        return $this->userPhotoAction($request);
    }
}
