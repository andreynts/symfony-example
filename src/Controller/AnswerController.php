<?php

namespace App\Controller;

use App\Entity\Answers;
use App\Enum\ErrorEnum;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class AnswerController extends AbstractController
{
    /**
     * @Security("is_granted('ROLE_CLIENT') or is_granted('ROLE_COACH')")
     * @Route("/answers")
     * @Method("GET")
     */
    public function indexAction(Request $request)
    {
        //
    }

    /**
     * @Security("is_granted('ROLE_CLIENT') or is_granted('ROLE_COACH')")
     * @Route("/answer/create")
     * @Method("POST")
     */
    public function createSessionFeedbackAction(Request $request)
    {
        $id = $request->request->get('sessionId');
        $answerData = $request->request->get('data');

        $em = $this->getDoctrine()->getManager();
        $session = $em->getRepository('App:Session')->find($id);

        if (!$session) {
            throw new BadRequestHttpException('Invalid session', null, ErrorEnum::E_BAD_REQUEST);
        }
        $coach = $session->getCoach();
        $client = $session->getClient();

        $em->beginTransaction();
        try {
            foreach ($answerData as $data) {
                $question = $em->getRepository('App:Question')->find($data['id']);
                if (!$question || !isset($data['name']) || !isset($data['value'])) { continue; }

                $answer = new Answers();
                $answer
                    ->setSession($session)
                    ->setClient($client)
                    ->setCoach($coach)
                    ->setQuestion($question)
                    ->setResponse([$data['name'] => $data['value']]);

                $em->persist($answer);
            }
            $em->flush();
        } catch (\Exception $exception) {
            $em->rollback();
            return;
        }
        $em->commit();

        return self::STATUS_OK;
    }
}