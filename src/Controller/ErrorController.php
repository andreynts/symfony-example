<?php

namespace App\Controller;

use App\Entity\ErrorLog;
use App\Utility\OpenTokClient;
use Symfony\Component\HttpFoundation\Request;
use App\Utility\RequestValidator;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;


class ErrorController extends AbstractController
{
    use Traits\ErrorTrait;

    /**
     * @Route("/errors")
     * @Method("GET")
     */
    public function indexAction(Request $request)
    {
        $tokenConstraint = new TokenConstraint(['$verify', '$username']);

        $validator = $this->get(RequestValidator::class);
        $validator->validate($request->request, new Assert\Collection([
            'token' => $tokenConstraint,
        ]));
    }

    /**
     * @Route("/errors/save")
     * @Method("POST")
     */
    public function saveAction(Request $request)
    {
        $request->request->set('sessionId', (string)$request->request->get('sessionId'));
        $request->request->set('errorCode', (string)$request->request->get('errorCode'));

        $this->saveError($request, new ErrorLog());

        return self::STATUS_OK;
    }
}