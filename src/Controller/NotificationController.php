<?php

namespace App\Controller;

use App\Entity\Notification;
use App\Enum\NotificationChannelEnum;
use App\Enum\NotificationEventEnum;
use App\Utility\RequestValidator;
use App\Validator\Constraints as AppAssert;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

class NotificationController extends AbstractController
{
    /**
     * @Route("/notifications")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $qb = $em->createQueryBuilder()
            ->select('n.event', 'n.reminder', 'n.channels')
            ->from('App:Notification', 'n', 'n.event')
            ->where('n.user = :user')
            ->setParameter('user', $this->getUser());

        $payload = $qb->getQuery()->getArrayResult();
        foreach ($payload as &$notification) {
            unset($notification['event']);
            if (null === $notification['reminder']) {
                unset($notification['reminder']);
            }
        }

        return $payload;
    }

    /**
     * @Route("/notifications")
     * @Method("PATCH")
     */
    public function updateAction(Request $request)
    {
        $validator = $this->get(RequestValidator::class);
        $validator->validate($request->request, [
            new Assert\Count(['min' => 1]),
            new Assert\All(new AppAssert\Chain([
                new Assert\Type('array'),
                new Assert\NotBlank(),
                new Assert\Collection([
                    'event' => new AppAssert\Chain([
                        new Assert\Type('string'),
                        new Assert\NotBlank(),
                        new Assert\Choice([
                            'choices' => NotificationEventEnum::values(),
                            'strict' => true,
                        ]),
                    ]),
                    'reminder' => new AppAssert\Chain([
                        new Assert\Type('int'),
                        new Assert\Range(['min' => 1, 'max' => 1440]),
                        new Assert\Callback(function ($value, ExecutionContextInterface $context) use ($request) {
                            static $allowedEvents = [NotificationEventEnum::SESSION_STARTED];
                            if (null !== $value && !in_array($request->request->get('event'), $allowedEvents)) {
                                $context->buildViolation(sprintf('Reminder is allowed only for %s.',
                                    implode(', ', $allowedEvents)))->addViolation();
                            }
                        }),
                    ]),
                    'channels' => new AppAssert\Chain([
                        new Assert\Type('array'),
                        new Assert\NotNull(),
                        new Assert\All(new AppAssert\Chain([
                            new Assert\Type('string'),
                            new Assert\NotBlank(),
                            new Assert\Choice([
                                'choices' => NotificationChannelEnum::values(),
                                'strict' => true,
                            ]),
                        ])),
                    ]),
                ]),
            ])),
        ]);

        $user = $this->getUser();
        $em = $this->getDoctrine()->getManager();
        $repository = $em->getRepository('App:Notification');

        foreach ($request->request as $payload) {
            $channels = $payload['channels'];
            $event = $payload['event'];

            $notification = $repository->findOneBy([
                'user' => $user,
                'event' => $event,
            ]);
            if (null === $notification) {
                $notification = new Notification();
                $notification->setUser($user);
                $notification->setEvent($event);
            }

            if (!$channels) {
                $em->remove($notification);
                continue;
            }

            $notification->setReminder($payload['reminder']);
            $notification->setChannels($channels);
            $em->persist($notification);
        }
        $em->flush();

        return self::STATUS_OK;
    }
}
