<?php

namespace App\Controller;

use App\Entity\Company;
use App\Entity\CompanyCredit;
use App\Utility\RequestValidator;
use App\Validator\Constraints as AppAssert;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;

class CompanyController extends AbstractController
{
    use Traits\CompanyTrait;

    /**
     * @Security("is_granted('ROLE_SUPER') or is_granted('ROLE_ADMIN') or is_granted('ROLE_ENTERPRISE_ADMIN')")
     * @Route("/companies")
     * @Method("GET")
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        if ($request->query->getBoolean('preview')) {
            return $em->createQueryBuilder()
                ->select('c.id', 'c.name')
                ->from('App:Company', 'c')
                ->where('c.isEnabled = TRUE')
                ->orderBy('c.name')
                ->getQuery()
                ->getArrayResult();
        }

        $creditTypeRepository = $em->getRepository('App:CreditType');
        $creditTypes = $creditTypeRepository->getAssociateTypeId();
        $types = array_keys($creditTypes);

        $sortFields = ['name'];
        foreach ($creditTypes as $k => $type) {
            $sortFields[] = 'credit_'.strtolower($k);
        }

        $validator = $this->get(RequestValidator::class);
        $validator->validate($request->query, new Assert\Collection([
            'allowMissingFields' => true,
            'fields' => [
                'is_enabled' => [
                    new Assert\NotBlank(),
                    new AppAssert\Boolean(),
                ],
                'search' => [
                    new Assert\NotBlank(),
                    new Assert\Length(['min' => 3]),
                ],
                'offset' => [
                    new Assert\NotBlank(),
                    new Assert\Range(['min' => 0]),
                ],
                'limit' => [
                    new Assert\NotBlank(),
                    new Assert\Range(['min' => 1, 'max' => 500]),
                ],
                'sort' => new AppAssert\Chain([
                    new Assert\Type('array'),
                    new Assert\Collection([
                        'allowMissingFields' => true,
                        'fields' => array_fill_keys($sortFields, [
                            new Assert\NotBlank(),
                            new Assert\Choice([
                                'choices' => ['ASC', 'DESC'],
                                'strict' => true,
                            ]),
                        ]),
                    ]),
                ]),
            ],
        ]));

        $isEnabled = $request->query->getBoolean('is_enabled', true);

        $qb = $em->createQueryBuilder()
            ->select(
                'c.id',
                'c.name name',
                'MIN(ccl.id) c1_id',
                'MIN(u.firstName) c1_fn',
                'MIN(u.lastName) c1_ln')
            ->from('App:Company', 'c')
            ->leftJoin('App:Client', 'ccl', 'WITH', 'ccl = FIRST(SELECT cl FROM App:Client cl '.
                'WHERE cl.company = c AND cl.isCompanyContact = TRUE ORDER BY cl.id)')
            ->leftJoin('ccl.user', 'u')
            ->leftJoin('c.credits', 'ccr', 'WITH', 'ccr.expiresAt >= CURRENT_DATE()')
            ->where('c.isEnabled = :is_enabled')
            ->setParameter('is_enabled', $isEnabled)
            ->groupBy('c.id');

        $dql = "SUM(CASE WHEN ccr.creditType = '%s' THEN ccr.value ELSE 0 END) credit_%s";
        foreach ($types as $type) {
            $qb->addSelect(sprintf($dql, $type, strtolower($type)));
        }

        if ($search = $request->query->get('search')) {
            $qb->andWhere('LOWER(c.name) LIKE LOWER(:search)')
                ->setParameter('search', $search.'%');
        }

        $qbId = clone $qb;
        $qbId->select('c.id');

        $total = $em->createQueryBuilder()
            ->select('COUNT(c0)')
            ->from('App:Company', 'c0')
            ->where('c0 IN ('.$qbId->getDql().')')
            ->setParameters($qbId->getParameters())
            ->getQuery()
            ->getSingleScalarResult();

        $sort = $request->query->get('sort', ['name' => 'ASC']);
        foreach ($sort as $field => $order) {
            $qb->addOrderBy($field, $order);
        }

        $offset = $request->query->getInt('offset');
        $limit = $request->query->getInt('limit', 20);
        $qb->setFirstResult($offset)->setMaxResults($limit);

        $result = $qb->getQuery()->getArrayResult();
        foreach ($result as &$company) {
            if (null !== $company['c1_id']) {
                $company['contact'] = [
                    'id' => $company['c1_id'],
                    'first_name' => $company['c1_fn'],
                    'last_name' => $company['c1_ln'],
                ];

            }



            unset($company['c1_id'], $company['c1_fn'], $company['c1_ln']);
            foreach ($creditTypes as $k => $type) {
                $key = 'credit_'.strtolower($k);
                $company['credit'][$type->getType()] = $company[$key];
                unset($company[$key]);
            }

            if (!empty($company['credit'])) {
                $company['credit'] = array_sum($company['credit']);
            }

            if (isset($company['id'])) {
                $company['company_credit_consumption'] = $this->getCompanyConsumptionById($company['id']);
            }
        }

        return [
            'result' => $result,
            'total' => $total,
        ];
    }

    /**
     * @Security("is_granted('view', company) or is_granted('ROLE_ADMIN')")
     * @Route("/companies/{id}", requirements={"id": "\d+"})
     * @Method("GET")
     */
    public function viewAction(Company $company)
    {
        return $this->viewCompany($company);
    }

    /**
     * @Security("is_granted('update', company) or is_granted('ROLE_ADMIN')")
     * @Route("/companies/{id}", requirements={"id": "\d+"})
     * @Method("PATCH")
     */
    public function updateAction(Request $request, Company $company)
    {
        $this->saveCompany($request, $company);

        return self::STATUS_OK;
    }

    /**
     * @Security("is_granted('ROLE_ADMIN')")
     * @Route("/companies/{id}", requirements={"id": "\d+"})
     * @Method("DELETE")
     */
    public function deleteAction(Company $company)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($company);
        $em->flush();

        return self::STATUS_OK;
    }

    /**
     * @Security("is_granted('ROLE_ADMIN')")
     * @Route("/companies/{id}/credits", requirements={"id": "\d+"})
     * @Method("GET")
     */
    public function creditsAction(Request $request, Company $company)
    {
        $repository = $this->getDoctrine()->getRepository('App:CompanyCredit');
        $creditTypeRepository = $this->getDoctrine()->getRepository('App:CreditType');
        $companyCreditTypes = $creditTypeRepository->getAllCompanyTypes();

        if ($request->query->getBoolean('preview')) {
            return $repository->getPreviewCreditByCompany($company, $companyCreditTypes);
        }

        return $repository->getCreditByCompany($company, $companyCreditTypes);
    }

    public function getCompanyConsumptionById($id)
    {
        $companyRepository = $this->getDoctrine()->getManager()->getRepository('App:Company');
        $company = $companyRepository->getCompanyById($id);

        $consumption = 0;
        if ($company) {
            $creditConsumption = $company->getConsumptionCredit(true);
            if (!empty($creditConsumption)) {
                $consumption = (is_array($creditConsumption)) ? array_sum($creditConsumption) : 0;
            }
        }
        return $consumption;
    }
}
