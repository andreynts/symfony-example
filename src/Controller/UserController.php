<?php

namespace App\Controller;

use App\Entity\User;
use App\Entity\Company;
use App\Utility\RequestValidator;
use App\Utility\Mailer;
use App\Validator\Constraints as AppAssert;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @Security("is_granted('ROLE_ADMIN')")
 */
class UserController extends AbstractController
{
    use Traits\UserTrait;

    /**
     * @Route("/users")
     * @Method("GET")
     */
    public function indexAction(Request $request)
    {
        $sortFields = ['name'];

        $validator = $this->get(RequestValidator::class);
        $validator->validate($request->query, new Assert\Collection([
            'allowMissingFields' => true,
            'fields' => [
                'is_verified' => [
                    new Assert\NotBlank(),
                    new AppAssert\Boolean(),
                ],
                'is_enabled' => [
                    new Assert\NotBlank(),
                    new AppAssert\Boolean(),
                ],
                'search' => [
                    new Assert\NotBlank(),
                    new Assert\Length(['min' => 3]),
                ],
                'offset' => [
                    new Assert\NotBlank(),
                    new Assert\Range(['min' => 0]),
                ],
                'limit' => [
                    new Assert\NotBlank(),
                    new Assert\Range(['min' => 1, 'max' => 500]),
                ],
                'sort' => new AppAssert\Chain([
                    new Assert\Type('array'),
                    new Assert\Collection([
                        'allowMissingFields' => true,
                        'fields' => array_fill_keys($sortFields, [
                            new Assert\NotBlank(),
                            new Assert\Choice([
                                'choices' => ['ASC', 'DESC'],
                                'strict' => true,
                            ]),
                        ]),
                    ]),
                ]),
            ],
        ]));

        $isEnabled = $request->query->getBoolean('is_enabled', true);

        $em = $this->getDoctrine()->getManager();
        $qb = $em->createQueryBuilder()
            ->select(
                'u.id',
                'u.username email',
                'u.roles',
                'u.firstName first_name',
                'u.lastName last_name',
                "CONCAT(u.firstName, ' ', u.lastName) HIDDEN name",
                'u.photo',
                'u.isVerified is_verified',
                'u.lastAuthAt last_auth_at',
                'u.createdAt created_at')
            ->from('App:User', 'u')
            ->where('u.isEnabled = :is_enabled')
            ->setParameter('is_enabled', $isEnabled);

        if (!$this->isGranted('ROLE_SUPER')) {
            $qb->andWhere("'ROLE_SUPER' <> ANY_OF(STRING_TO_ARRAY(u.roles))");
        }

        if ($request->query->has('is_verified')) {
            $qb->andWhere('u.isVerified = :is_verified')
                ->setParameter('is_verified', $request->query->getBoolean('is_verified'));
        }

        if ($search = $request->query->get('search')) {
            $qb->andWhere('LOWER(u.firstName) LIKE LOWER(:search)')
                ->setParameter('search', $search.'%');
        }

        $qbId = clone $qb;
        $qbId->select('u.id');

        $total = $em->createQueryBuilder()
            ->select('COUNT(u0)')
            ->from('App:User', 'u0')
            ->where('u0 IN ('.$qbId->getDql().')')
            ->setParameters($qbId->getParameters())
            ->getQuery()
            ->getSingleScalarResult();

        $sort = $request->query->get('sort', ['name' => 'ASC']);
        foreach ($sort as $field => $order) {
            $qb->addOrderBy($field, $order);
        }

        $offset = $request->query->getInt('offset');
        $limit = $request->query->getInt('limit', 20);
        $qb->setFirstResult($offset)->setMaxResults($limit);

        $result = $qb->getQuery()->getArrayResult();

        return [
            'result' => $result,
            'total' => $total,
        ];
    }

    /**
     * @Route("/users/{id}", requirements={"id": "\d+"})
     * @Method("GET")
     */
    public function viewAction(User $user)
    {
        return $this->viewUser($user);
    }

    /**
     * @Route("/users/{id}", requirements={"id": "\d+"})
     * @Method("PATCH")
     */
    public function updateAction(Request $request, User $user)
    {
        $this->validateUser($request, $user);
        $this->saveUser($request, $user);

        return self::STATUS_OK;
    }

    /**
     * @Route("/users/{id}", requirements={"id": "\d+"})
     * @Method("DELETE")
     */
    public function deleteAction(User $user)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($user);
        $em->flush();

        return self::STATUS_OK;
    }

    /**
     * @Security("is_granted('ROLE_ENTERPRISE_ADMIN') or is_granted('ROLE_ADMIN')")
     * @Route("/users/invitedList")
     * @Method("GET")
     */
    public function invitedUserAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        
        $company = $this->getClientUser()->getCompany();
        $qb = $em->createQueryBuilder()
            ->select(
                'i.id',
                'i.firstName first_name',
                'i.lastName last_name',
                'i.email',
                'i.createdAt',
                'i.isRegistered is_registered',
                'c.name company_name'
                )
            ->from('App:Invite', 'i')
            ->join('i.company', 'c')
            ->where('i.isArchived = :archived')
            ->andwhere('i.company = :company')
            ->setParameter('archived', false)
            ->setParameter('company', $company)
        ;

        $qbId = clone $qb;
        $qbId->select('i.id');

        $total = $em->createQueryBuilder()
            ->select('COUNT(i0)')
            ->from('App:Invite', 'i0')
            ->where('i0 IN ('.$qbId->getDql().')')
            ->setParameters($qbId->getParameters())
            ->getQuery()
            ->getSingleScalarResult();

        $offset = $request->query->getInt('offset');
        $limit = $request->query->getInt('limit', 20);
        $qb->setFirstResult($offset)->setMaxResults($limit);

        $result = $qb->getQuery()->getArrayResult();

        return [
            'result' => $result,
            'total' => $total,
        ];
    }

    /**
     * @Security("is_granted('ROLE_ENTERPRISE_ADMIN') or is_granted('ROLE_ADMIN')")
     * @Route("/users/resendInvitation")
     * @Method("POST")
     */
    public function resendInvitationAction(Request $request)
    {
        $users = $request->request->get('users');

        if (!empty($users)) {
            foreach ($users as $user) {
                $repository = $this->getDoctrine()->getRepository('App:Invite');
                $invite = $repository->find($user);

                if ($invite) {
                    $this->inviteUser($invite->getEmail(), $invite->getFirstName(), $invite->getLastName(), $invite->getCompany(), $invite->getId());
                }
            }
        }

        return self::STATUS_OK;
    }

    /**
     * @Security("is_granted('ROLE_ENTERPRISE_ADMIN') or is_granted('ROLE_ADMIN')")
     * @Route("/users/archive")
     * @Method("POST")
     */
    public function archiveAction(Request $request)
    {
        $users = $request->request->get('users', []);

        if (!empty($users)) {
            $res = 0;
            foreach ($users as $user) {
                $repository = $this->getDoctrine()->getRepository('App:Invite');
                $invite = $repository->find($user);

                if ($invite) {
                    $invite->setArchived(true);
                    $em = $this->getDoctrine()->getManager();
                    $em->persist($invite);
                    $em->flush();
                    $res++;
                }
            }
        }

        if (count($users) == $res) {
            return self::STATUS_OK;
        }

        return ['status' => 'failed'];
    }

    private function inviteUser(string $email, string $firstName, string $lastName, Company $company = null, string $inviteId = null)
    {
        $payload = [
            'exp' => (new \DateTime('+1 year'))->getTimestamp(),
            '$invite:client' => true,
            '$username' => $email,
            '$first_name' => $firstName,
            '$last_name' => $lastName,
            '$inviteId' => $inviteId,
        ];

        if (null !== $company) {
            $payload['$company'] = [
                'id' => $company->getId(),
                'name' => $company->getName(),
            ];
        }

        $token = $this->get('lexik_jwt_authentication.encoder')->encode($payload);

        $mailer = $this->get(Mailer::class);
        $mailer->sendTo($email, 'invite/client.html.twig', [
            'first_name' => $payload['$first_name'],
            'token' => $token,
        ]);
    }
}
