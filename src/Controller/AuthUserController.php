<?php

namespace App\Controller;

use App\Enum\SessionStatusEnum;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;

class AuthUserController extends AbstractController
{
    use Traits\UserTrait;
    use Traits\ClientTrait;
    use Traits\CoachTrait;
    use Traits\CompanyTrait;

    /**
     * @Route("/auth/user")
     * @Method("GET")
     */
    public function indexAction(Request $request)
    {
        if ($request->query->getBoolean('preview')) {
            goto fallback;
        }

        if ($this->isGranted('ROLE_CLIENT')) {
            return $this->viewClient($this->getClientUser(false));
        } elseif ($this->isGranted('ROLE_COACH')) {
            return $this->viewCoach($this->getCoachUser(false));
        }

        fallback:
            return $this->viewUser($this->getUser());
    }

    /**
     * @Route("/auth/user")
     * @Method("PATCH")
     */
    public function updateAction(Request $request)
    {
        if ($this->isGranted('ROLE_CLIENT')) {
            $this->saveClient($request, $this->getClientUser(false));
        } elseif ($this->isGranted('ROLE_COACH')) {
            $this->saveCoach($request, $this->getCoachUser(false));
        } else {
            $user = $this->getUser();
            $this->validateUser($request, $user);
            $this->saveUser($request, $user);
        }

        return self::STATUS_OK;
    }

    /**
     * @Security("is_granted('ROLE_CLIENT')")
     * @Route("/auth/user/statistics")
     * @Method("GET")
     */
    public function statisticsAction()
    {
        $client = $this->getClientUser();

        $em = $this->getDoctrine()->getManager();
        $sessionsBooked = $em->getRepository('App:Session')
            ->countByStatus(SessionStatusEnum::BOOKED, $client);

        $sessionsCompleted = $em->getRepository('App:Session')
            ->countByStatus(SessionStatusEnum::COMPLETED, $client);

        $questLocRep = $this->getDoctrine()->getManager()->getRepository('App:QuestionLocation');
        $questionLocation = $questLocRep->findLocationByValue('SESSION_FEEDBACK_CLIENT_FORM');

        $feedbacks = [];
        if ($questionLocation) {
            $feedbacks = $em->createQueryBuilder()
                ->select('COUNT(a)')
                ->from('App:Answers', 'a')
                ->innerJoin('a.session', 's')
                ->innerJoin('App:Question', 'q', 'WITH', 'a.question = q')
                ->where('s.client = :client AND s.status = :status AND q.questionLocation = :location')
                ->setParameter('client', $client)
                ->setParameter('status', 'COMPLETED')
                ->setParameter('location', $questionLocation)
                ->groupBy('a.session')
                ->getQuery()
                ->getResult();
        }

        return [
            'sessions' => [
                'booked' => $sessionsBooked,
                'completed' => $sessionsCompleted,
                'feedbacks' => count($feedbacks),
            ],
        ];
    }

    /**
     * @Security("is_granted('ROLE_ENTERPRISE_CLIENT')")
     * @Route("/auth/user/company")
     * @Method("GET")
     */
    public function companyAction()
    {
        $company = $this->getClientUser()->getCompany();

        return [
            'id' => $company->getId(),
            'name' => $company->getName(),
            'logo' => $company->getLogo(),
            'website' => $company->getWebsite(),
        ];
    }
}
