<?php

namespace App\Controller;

use App\Utility\AccessManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @method \App\Entity\User getUser()
 */
abstract class AbstractController extends Controller
{
    const STATUS_OK = ['status' => 'ok'];

    private $client;
    private $coach;

    public function isGranted($attributes, $object = null, UserInterface $user = null): bool
    {
        if (null !== $user) {
            $token = AccessManager::createToken($user);

            return $this->get(AccessManager::class)->isGranted($token, $attributes, $object);
        } elseif (null !== $this->get('security.token_storage')->getToken()) {
            return parent::isGranted($attributes, $object);
        }

        return false;
    }

    /**
     * @return \App\Entity\Client|null
     */
    protected function getClientUser(bool $partial = true)
    {
        if ($this->client) {
            return $this->client;
        }

        $repository = $this->getDoctrine()->getRepository('App:Client');
        $method = $partial ? 'findPartialByUser' : 'findOneByUser';
        $this->client = $repository->$method($this->getUser());

        return $this->client;
    }

    /**
     * @return \App\Entity\Coach|null
     */
    protected function getCoachUser(bool $partial = true)
    {
        if ($this->coach) {
            return $this->coach;
        }

        $repository = $this->getDoctrine()->getRepository('App:Coach');
        $method = $partial ? 'findPartialByUser' : 'findOneByUser';
        $this->coach = $repository->$method($this->getUser());

        return $this->coach;
    }
}
