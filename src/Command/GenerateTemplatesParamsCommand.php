<?php


namespace App\Command;

use App\Entity\EmailTemplateParameter;
use App\Entity\EmailTemplateType;
use App\Utility\DataReader;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class GenerateTemplatesParamsCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this->setName('app:email:templates:generate')
            ->setDescription('Generate parameters of emails templates')
            ->setHelp('This command will parameters for different emails templates');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $em = $this->getContainer()->get('doctrine.orm.entity_manager');
        $tempTypeRepository = $em->getRepository('App:EmailTemplateType');

        $reader = $this->getContainer()->get(DataReader::class);
        $paramsList = $reader->read('tempaltes-parameters.json');
        $addedParams = 0;
        $changedParams = 0;
        foreach ($paramsList as $param) {
            $tempType = null;
            $tempType = $tempTypeRepository->findPartialByTemplateName($param['email_template_name']);

            if ($tempType == null) {
                $tempType = new EmailTemplateType();
                $tempType->setEmailTemplateName($param['email_template_name']);
                $em->persist($tempType);
            }

            $existingParameter = null;
            $existingParameter = $em->getRepository('App:EmailTemplateParameter')
                ->findPartialByParamName($param['name']);

            if ($existingParameter === null) {
                $addedParams++;
                $templateParam = new EmailTemplateParameter();
                $templateParam
                    ->setName($param['name'])
                    ->setValue($param['value'])
                    ->setType($tempType);
                $em->persist($templateParam);
            } else if ($existingParameter->getValue() != $param['value']) {
                $changedParams++;
                $existingParameter->setValue($param['value']);
                $em->persist($existingParameter);
            }
            $em->flush();
        }

        if ($addedParams > 0) {
            $styleText = 'parameter';
            if ($addedParams > 1) {
                $styleText = 'parameters';
            }
            $message = $addedParams . ' ' . $styleText . ' successfully added.';
        } else {
            $message = 'No parameters added';
        }
        $output->writeln(sprintf($message));
        if ($changedParams > 0) {
            $styleText = 'parameter';
            if ($changedParams > 1) {
                $styleText = 'parameters';
            }
            $message = $changedParams . ' ' . $styleText . ' successfully changed.';
            $output->writeln(sprintf($message));
        }
    }
}