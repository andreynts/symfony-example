<?php


namespace App\Command;


use App\Entity\Setting;
use App\Entity\SettingLocation;
use App\Entity\SettingType;
use App\Utility\DataReader;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class GenerateSettingsCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this->setName('app:settings:generate')
            ->setDescription('Generate settings')
            ->setHelp('This command will generate setings');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $em = $this->getContainer()->get('doctrine.orm.entity_manager');
        $settingTypeRepository = $em->getRepository('App:SettingType');
        $settingLocationRepository = $em->getRepository('App:SettingLocation');

        $booleanSettingsType = $settingTypeRepository->findPartialByTypeName('boolean');
        $linkSettingsType = $settingTypeRepository->findPartialByTypeName('link');
        $htmlSettingsType = $settingTypeRepository->findPartialByTypeName('html');

        $footerSettingLocation = $settingLocationRepository->findLocationByValue('FOOTER');
        $privacyPolicyLocation = $settingLocationRepository->findLocationByValue('PRIVACY_POLICY_PAGE');

        if ($booleanSettingsType == null) {
            $booleanSettingsType = new SettingType();
            $booleanSettingsType->setTypeName('boolean');
            $em->persist($booleanSettingsType);
        }

        if ($linkSettingsType == null) {
            $linkSettingsType = new SettingType();
            $linkSettingsType->setTypeName('link');
            $em->persist($linkSettingsType);
        }

        if ($htmlSettingsType == null) {
            $htmlSettingsType = new SettingType();
            $htmlSettingsType->setTypeName('html');
            $em->persist($htmlSettingsType);
        }

        if ($footerSettingLocation == null) {
            $footerSettingLocation = new SettingLocation();
            $footerSettingLocation->setValue('FOOTER');
            $em->persist($footerSettingLocation);
        }

        if ($privacyPolicyLocation == null) {
            $privacyPolicyLocation = new SettingLocation();
            $privacyPolicyLocation->setValue('PRIVACY_POLICY_PAGE');
            $em->persist($privacyPolicyLocation);
        }

        $reader = $this->getContainer()->get(DataReader::class);
        $stylesList = $reader->read('settings.json');
        $addedSettings = 0;
        $changedSettings = 0;
        foreach ($stylesList as $selector => $rule) {
            $existingSetting = null;
            $existingSetting = $em->getRepository('App:Setting')
                ->findPartialByName($rule['name']);

            $settingType = null;
            if ($rule['type'] === 'boolean') {
                $settingType = $booleanSettingsType;
            } else if ($rule['type'] === 'link') {
                $settingType = $linkSettingsType;
            } else if ($rule['type'] === 'html') {
                $settingType = $htmlSettingsType;
            }

            $settingLocation = null;
            if ($rule['location'] === 'FOOTER') {
                $settingLocation = $footerSettingLocation;
            } else if ($rule['location'] === 'PRIVACY_POLICY_PAGE') {
                $settingLocation = $privacyPolicyLocation;
            }

            if ($settingType !== null) {
                if ($existingSetting === null) {
                    $addedSettings++;
                    $setting = new Setting();
                    $setting
                        ->setName($rule['name'])
                        ->setValue($rule['value'])
                        ->setLocation($settingLocation)
                        ->setType($settingType);
                    $em->persist($setting);
                } else if ($existingSetting->getValue() != $rule['value'] ||
                    $rule['type'] !== $existingSetting->getType()->getTypeName()) {
                    $changedSettings++;
                    $existingSetting->setValue($rule['value']);
                    $existingSetting->setType($settingType);
                    $existingSetting->setLocation($settingLocation);
                    $em->persist($existingSetting);
                }
            }
        }

        $em->flush();

        if ($addedSettings > 0) {
            $settingsText = 'setting';
            if ($addedSettings > 1) {
                $settingsText = 'settings';
            }
            $message = $addedSettings.' '.$settingsText.' successfully added.';
        } else {
            $message = 'No settings added';
        }
        $output->writeln(sprintf($message));
        if ($changedSettings > 0) {
            $settingsText = 'setting';
            if ($changedSettings > 1) {
                $settingsText = 'settings';
            }
            $message = $changedSettings.' '.$settingsText.' successfully changed.';
            $output->writeln(sprintf($message));
        }
    }
}