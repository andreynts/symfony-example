<?php

namespace App\Command;

use App\Entity\Client;
use App\Entity\Coach;
use App\Utility\DataReader;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class FixUserLanguageCommand extends ContainerAwareCommand
{
    protected $em;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->em = $entityManager;
        parent::__construct(null);
    }

    protected function configure()
    {
        $this->setName('app:clients:languages:fix')
            ->setDescription('Fix Audit User Languages');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $clients = $this->em->createQuery('Select c FROM ' . Client::class . ' c')->iterate();

        $this->em->beginTransaction();
        try {
            $reader = $this->getContainer()->get(DataReader::class);
            $languagesList = $reader->read('languages.json');
            $langCodes = (!empty($languagesList)) ? array_keys($languagesList) : [];

            $iteration_count = 0;

            foreach($clients as $client) {
                $client = $client[0];
                /**
                 * @var Client $client
                 */
                $languages = $client->getLanguages();

                $newLanguages = [];
                if (!empty($languages)) {
                    foreach ($languages as $language) {
                        if (!in_array($language, $langCodes)) {
                            $newLanguages[] = 'en';
                        } else {
                            $newLanguages[] = $language;
                        }
                    }
                }

                if (!empty($newLanguages)) {
                    $client->setLanguages($newLanguages);
                }

                $this->em->persist($client);
                if(++$iteration_count >= 10) {
                    $this->em->flush();
                    $this->em->clear();
                    $iteration_count = 0;
                }
            }
            unset($clients);

            $coaches = $this->em->createQuery('Select c FROM ' . Coach::class . ' c')->iterate();
            foreach ($coaches as $coach) {
                $coach = $coach[0];
                /**
                 * @var Coach $coach
                 */
                $languages = $coach->getLanguages();

                $newLanguages = [];
                if (!empty($languages)) {
                    foreach ($languages as $language) {
                        if (!in_array($language, $langCodes)) {
                            $newLanguages[] = 'en';
                        } else {
                            $newLanguages[] = $language;
                        }
                    }
                }

                if (!empty($newLanguages)) {
                    $coach->setLanguages($newLanguages);
                }

                $this->em->persist($coach);

                $this->em->persist($coach);
                if(++$iteration_count >= 10) {
                    $this->em->flush();
                    $this->em->clear();
                    $iteration_count = 0;
                }
            }

            $this->em->flush();
        } catch (\Exception $exception) {
            $output->writeln($exception->getMessage());
            $this->em->rollback();
            return;
        }

        $this->em->commit();
    }
}