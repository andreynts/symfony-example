<?php

namespace App\Command;

use App\Entity\ClientCredit;
use App\Entity\CompanyCredit;
use App\Entity\CompanyCreditAllocation;
use App\Entity\CreditType;
use App\Entity\Session;
use App\Enum\CreditTypeEnum;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class CreditTypeMigrationCommand extends ContainerAwareCommand
{
    const DEFAULT_CREDIT_TYPES = [
        CreditTypeEnum::PRIVATE_,
        CreditTypeEnum::COMPLIMENTARY,
        CreditTypeEnum::ENTERPRISE,
        CreditTypeEnum::EXECUTIVE,
    ];

    private $em;

    public function __construct(?string $name = null, EntityManagerInterface $em) {
        parent::__construct($name);

        $this->em = $em;
    }

    protected function configure()
    {
        $this->setName('app:migration:creditTypes')
            ->setDescription('Transfer of old types of credits to the new database structure')
            ->setHelp('This command creates old credit types in the \'credit_type\' table and establishes a link to this table through foreign keys for the backward validity of old data');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->createDefaultCreditTypes($input, $output);
        $this->updateClientCreditEntities($input, $output);
        $this->updateCompanyCreditEntities($input, $output);
        $this->updateSessionEntities($input, $output);
        $this->updateCompanyCreditAllocationEntities($input, $output);
    }

    private function createDefaultCreditTypes(InputInterface $input, OutputInterface $output)
    {
        $output->writeln("--- Create default credit types ---");
        $creditTypeRepository = $this->em->getRepository('App:CreditType');
        $em = $this->em->create($this->em->getConnection(), $this->em->getConfiguration());

        foreach (self::DEFAULT_CREDIT_TYPES as $type) {
            if (!$creditTypeRepository->checkExistType($type)) {
                $creditType = new CreditType();
                $creditType->setType($type);

                $em->persist($creditType);
                $output->writeln("Added new type: $type");
            }
        }
        $em->flush();
        $output->writeln("--- DONE ---");
    }

    private function updateClientCreditEntities(InputInterface $input, OutputInterface $output)
    {
        $output->writeln("--- Updating ClientCreditEntities ---");

        $em = $this->em->create($this->em->getConnection(), $this->em->getConfiguration());
        $creditTypeRepository = $em->getRepository('App:CreditType');

        $em->beginTransaction();
        $affectedRows = 0;
        try {
            $availableTypes = $creditTypeRepository->getAssociateTypes();

            if (!empty($availableTypes)) {
                $clientCredits = $em->createQuery('Select cc FROM ' . ClientCredit::class . ' cc')->iterate();
                $iteration_count = 0;
                foreach ($clientCredits as $credits) {
                    $cc = (isset($credits[0])) ? $credits[0] : null;
                    if (!$cc) { continue; }

                    $currentType = $cc->getType(true);
                    if (isset($availableTypes[$currentType])) {
                        $cc->setCreditType($availableTypes[$currentType]);
                        $affectedRows++;
                        $em->persist($cc);
                        if(++$iteration_count >= 10) {
                            $em->flush();
                            $iteration_count = 0;
                        }
                    }
                }
                unset($clientCredits);
            }

            $em->flush();
        } catch (\Exception $exception) {
            $output->writeln($exception->getMessage());
            $em->rollback();
            return;
        }
        $em->commit();

        $output->writeln("--- DONE. Affected rows: $affectedRows ---");
    }

    private function updateCompanyCreditEntities(InputInterface $input, OutputInterface $output)
    {
        $output->writeln("--- Updating CompanyCreditEntities ---");

        $em = $this->em->create($this->em->getConnection(), $this->em->getConfiguration());
        $creditTypeRepository = $em->getRepository('App:CreditType');

        $em->beginTransaction();
        $affectedRows = 0;
        try {
            $availableTypes = $creditTypeRepository->getAssociateTypes();

            if (!empty($availableTypes)) {
                $companyCredits = $em->createQuery('Select cc FROM ' . CompanyCredit::class . ' cc')->iterate();
                $iteration_count = 0;
                foreach ($companyCredits as $credits) {
                    $cc = (isset($credits[0])) ? $credits[0] : null;
                    if (!$cc) { continue; }

                    $currentType = $cc->getType(true);
                    if (isset($availableTypes[$currentType])) {
                        $cc->setCreditType($availableTypes[$currentType]);
                        $affectedRows++;
                        $em->persist($cc);
                        if(++$iteration_count >= 10) {
                            $em->flush();
                            $iteration_count = 0;
                        }
                    }
                }
                unset($clientCredits);
            }

            $em->flush();
        } catch (\Exception $exception) {
            $output->writeln($exception->getMessage());
            $em->rollback();
            return;
        }
        $em->commit();

        $output->writeln("--- DONE. Affected rows: $affectedRows ---");
    }

    private function updateSessionEntities(InputInterface $input, OutputInterface $output)
    {
        $output->writeln("--- Updating SessionCreditEntities ---");

        $em = $this->em->create($this->em->getConnection(), $this->em->getConfiguration());
        $creditTypeRepository = $em->getRepository('App:CreditType');

        $em->beginTransaction();
        $affectedRows = 0;
        try {
            $availableTypes = $creditTypeRepository->getAssociateTypes();

            if (!empty($availableTypes)) {
                $sessions = $em->createQuery('Select cc FROM ' . Session::class . ' cc')->iterate();
                $iteration_count = 0;
                foreach ($sessions as $session) {
                    $sess = (isset($session[0])) ? $session[0] : null;
                    if (!$sess) { continue; }

                    $currentType = $sess->getCreditType(true);
                    if (isset($availableTypes[$currentType])) {
                        $sess->setCType($availableTypes[$currentType]);
                        $affectedRows++;
                        $em->persist($sess);
                        if(++$iteration_count >= 10) {
                            $em->flush();
                            $iteration_count = 0;
                        }
                    }
                }
                unset($clientCredits);
            }

            $em->flush();
        } catch (\Exception $exception) {
            $output->writeln($exception->getMessage());
            $em->rollback();
            return;
        }
        $em->commit();

        $output->writeln("--- DONE. Affected rows: $affectedRows ---");
    }

    private function updateCompanyCreditAllocationEntities(InputInterface $input, OutputInterface $output)
    {
        $output->writeln("--- Updating CompanyCreditAllocationEntities ---");

        $em = $this->em->create($this->em->getConnection(), $this->em->getConfiguration());
        $creditTypeRepository = $em->getRepository('App:CreditType');

        $em->beginTransaction();
        $affectedRows = 0;
        try {
            $availableTypes = $creditTypeRepository->getAssociateTypes();

            if (!empty($availableTypes)) {
                $sessions = $em->createQuery('Select cc FROM ' . CompanyCreditAllocation::class . ' cc')->iterate();
                $iteration_count = 0;
                foreach ($sessions as $session) {
                    $sess = (isset($session[0])) ? $session[0] : null;
                    if (!$sess) { continue; }

                    $currentType = $sess->getType();
                    if (isset($availableTypes[$currentType])) {
                        $sess->setCreditType($availableTypes[$currentType]);
                        $affectedRows++;
                        $em->persist($sess);
                        if(++$iteration_count >= 10) {
                            $em->flush();
                            $iteration_count = 0;
                        }
                    }
                }
                unset($clientCredits);
            }

            $em->flush();
        } catch (\Exception $exception) {
            $output->writeln($exception->getMessage());
            $em->rollback();
            return;
        }
        $em->commit();

        $output->writeln("--- DONE. Affected rows: $affectedRows ---");
    }
}