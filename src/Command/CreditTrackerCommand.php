<?php

namespace App\Command;

use App\Utility\Mailer;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class CreditTrackerCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this->setName('app:credit:tracker')
            ->setDescription('Manage company and client credits')
            ->setHelp('This command will manage all company and client credits');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln(sprintf(
            'Warning: %d clients, %d companies / Deleted: %d clients, %d companies',
            $this->clientExpiryWarning(), $this->companyExpiryWarning(),
            $this->clientDeleteExpired(), $this->companyDeleteExpired()));
    }

    private function clientExpiryWarning(): int
    {
        $em = $this->getContainer()->get('doctrine.orm.entity_manager');
        $mailer = $this->getContainer()->get(Mailer::class);

        $qb = $em->createQueryBuilder()
            ->select('u.username email', 'u.firstName first_name')
            ->from('App:ClientCredit', 'cc')
            ->join('cc.client', 'c')
            ->join('c.user', 'u', 'WITH', 'u.isEnabled = TRUE')
            ->where("DATE_SUB(cc.expiresAt, 1, 'MONTH') = CURRENT_DATE()")
            ->distinct();

        $count = 0;
        foreach ($qb->getQuery()->iterate() as $row) {
            $data = current($row);
            $mailer->sendTo($data['email'], 'credit/warning/client.html.twig', $data);
            ++$count;
        }

        return $count;
    }

    private function companyExpiryWarning(): int
    {
        $em = $this->getContainer()->get('doctrine.orm.entity_manager');
        $mailer = $this->getContainer()->get(Mailer::class);

        $qb = $em->createQueryBuilder()
            ->select('IDENTITY(cc.company) id')
            ->from('App:CompanyCredit', 'cc')
            ->where("DATE_SUB(cc.expiresAt, 1, 'MONTH') = CURRENT_DATE()")
            ->distinct();

        $count = 0;
        foreach ($qb->getQuery()->getResult('HYDRATE_COLUMN') as $id) {
            $mailer->sendToCompanyAdmin($id, 'credit/warning/company.html.twig');
            ++$count;
        }

        return $count;
    }

    private function clientDeleteExpired(): int
    {
        $em = $this->getContainer()->get('doctrine.orm.entity_manager');
        $mailer = $this->getContainer()->get(Mailer::class);

        $qb = $em->createQueryBuilder()
            ->select('u.username email', 'u.firstName first_name')
            ->from('App:ClientCredit', 'cc')
            ->join('cc.client', 'c')
            ->join('c.user', 'u', 'WITH', 'u.isEnabled = TRUE')
            ->where('cc.expiresAt < CURRENT_DATE()')
            ->distinct();

        foreach ($qb->getQuery()->iterate() as $row) {
            $data = current($row);
            $mailer->sendTo($data['email'], 'credit/expired/client.html.twig', $data);
        }

        return $em->createQueryBuilder()
            ->delete('App:ClientCredit', 'c')
            ->where('c.expiresAt < CURRENT_DATE()')
            ->getQuery()
            ->execute();
    }

    private function companyDeleteExpired(): int
    {
        $em = $this->getContainer()->get('doctrine.orm.entity_manager');
        $mailer = $this->getContainer()->get(Mailer::class);

        $qb = $em->createQueryBuilder()
            ->select('IDENTITY(cc.company) id')
            ->from('App:CompanyCredit', 'cc')
            ->where('cc.expiresAt < CURRENT_DATE()')
            ->distinct();

        foreach ($qb->getQuery()->getResult('HYDRATE_COLUMN') as $id) {
            $mailer->sendToCompanyAdmin($id, 'credit/expired/company.html.twig');
        }

        return $em->createQueryBuilder()
            ->delete('App:CompanyCredit', 'c')
            ->where('c.expiresAt < CURRENT_DATE()')
            ->getQuery()
            ->execute();
    }
}
