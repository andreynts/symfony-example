<?php

namespace App\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class LogClearCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this->setName('app:log:clear')
            ->setDescription('Clear outdated logs')
            ->setHelp('This command will clear outdated logs')
            ->addOption('interval', 'i', InputOption::VALUE_OPTIONAL, 'Rotation interval', '6 months');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->clearCronReports($input, $output);
        $this->clearRequestLog($input, $output);
        $this->clearMailLog($input, $output);
    }

    private function clearCronReports(InputInterface $input, OutputInterface $output)
    {
        $interval = $input->getOption('interval');

        $count = $this->getContainer()->get('doctrine.orm.entity_manager')
            ->createQueryBuilder()
            ->delete('CronCronBundle:CronReport', 'c')
            ->where('c.runAt <= :date')
            ->setParameter('date', new \DateTime('-'.$interval))
            ->getQuery()
            ->execute();

        $output->writeln(sprintf('%d cron report entries deleted', $count));
    }

    private function clearRequestLog(InputInterface $input, OutputInterface $output)
    {
        $interval = $input->getOption('interval');

        $count = $this->getContainer()->get('doctrine.orm.entity_manager')
            ->createQueryBuilder()
            ->delete('App:RequestLog', 'l')
            ->where('l.createdAt <= :date')
            ->setParameter('date', new \DateTime('-'.$interval))
            ->getQuery()
            ->execute();

        $output->writeln(sprintf('%d request log entries deleted', $count));
    }

    private function clearMailLog(InputInterface $input, OutputInterface $output)
    {
        $interval = $input->getOption('interval');

        $count = $this->getContainer()->get('doctrine.orm.entity_manager')
            ->createQueryBuilder()
            ->delete('App:MailLog', 'l')
            ->where('l.createdAt <= :date')
            ->setParameter('date', new \DateTime('-'.$interval))
            ->getQuery()
            ->execute();

        $output->writeln(sprintf('%d email log entries deleted', $count));
    }
}
