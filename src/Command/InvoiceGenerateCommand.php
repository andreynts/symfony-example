<?php

namespace App\Command;

use App\Utility\InvoiceGenerator;
use App\Utility\StripeClient;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class InvoiceGenerateCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this->setName('app:invoice:generate')
            ->setDescription('Generate missing invoices')
            ->setHelp('This command will generate all missing invoices');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $em = $this->getContainer()->get('doctrine.orm.entity_manager');
        $stripe = $this->getContainer()->get(StripeClient::class);
        $invoiceGenerator = $this->getContainer()->get(InvoiceGenerator::class);

        $qb = $em->createQueryBuilder()
            ->select('p')
            ->from('App:Payment', 'p')
            ->where('p.invoice IS NULL');

        $count = 0;
        foreach ($qb->getQuery()->iterate() as $row) {
            $payment = $row[0];

            $charge = $stripe->getCharge($payment->getStripeToken());
            $invoice = $invoiceGenerator->generate($payment, $charge);
            $payment->setInvoice($invoice);

            if (0 === ++$count % 20) {
                $em->flush();
                $em->clear();
            }
        }
        $em->flush();

        $output->writeln(sprintf('%d invoices generated', $count));
    }
}
