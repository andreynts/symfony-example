<?php


namespace App\Command;


use App\Entity\Style;
use App\Entity\StyleType;
use App\Utility\DataReader;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class GenerateStyleCommand extends ContainerAwareCommand
{

    protected function configure()
    {
        $this->setName('app:style:generate')
            ->setDescription('Generate style')
            ->setHelp('This command will generate style');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $colorStyleType = null;
        $imageStyleType = null;
        $textStyleType = null;
        $em = $this->getContainer()->get('doctrine.orm.entity_manager');
        $styleTypeRepository = $em->getRepository('App:StyleType');

        $colorStyleType = $styleTypeRepository->findPartialByTypeName('color');
        $imageStyleType = $styleTypeRepository->findPartialByTypeName('image');
        $textStyleType = $styleTypeRepository->findPartialByTypeName('text');

        if ($colorStyleType == null) {
            $colorStyleType = new StyleType();
            $colorStyleType->setTypeName('color');
            $em->persist($colorStyleType);
        }

        if ($imageStyleType == null) {
            $imageStyleType = new StyleType();
            $imageStyleType->setTypeName('image');
            $em->persist($imageStyleType);
        }

        if ($textStyleType == null) {
            $textStyleType = new StyleType();
            $textStyleType->setTypeName('text');
            $em->persist($textStyleType);
        }

        $reader = $this->getContainer()->get(DataReader::class);
        $stylesList = $reader->read('styles.json');
        $addedStyles = 0;
        $changedStyles = 0;
        foreach ($stylesList as $selector => $rule) {
            $existingRule = null;
            $existingRule = $em->getRepository('App:Style')
                ->findPartialByName($rule['name']);

            if (!array_key_exists('type', $rule)) {
                $rule['type'] = 'color';
            }

            $styleType = null;
            if ($rule['type'] === 'color') {
                $styleType = $colorStyleType;
            } else if ($rule['type'] === 'image') {
                $styleType = $imageStyleType;
            } else if ($rule['type'] === 'text') {
                $styleType = $textStyleType;
            }

            if ($styleType !== null) {
                if ($existingRule === null) {
                    $addedStyles++;
                    $style = new Style();
                    $style
                        ->setName($rule['name'])
                        ->setValue($rule['value'])
                        ->setType($styleType);
                    $em->persist($style);
                } else if ($existingRule->getValue() != $rule['value'] ||
                    $rule['type'] !== $existingRule->getType()->getTypeName()) {
                    $changedStyles++;
                    $styleType = null;
                    if ($rule['type'] === 'color') {
                        $styleType = $colorStyleType;
                    } else if ($rule['type'] === 'image') {
                        $styleType = $imageStyleType;
                    } else if ($rule['type'] === 'text') {
                        $styleType = $textStyleType;
                    }
                    $existingRule->setValue($rule['value']);
                    $existingRule->setType($styleType);
                    $em->persist($existingRule);
                }
            }
        }

        $em->flush();

        if ($addedStyles > 0) {
            $styleText = 'style';
            if ($addedStyles > 1) {
                $styleText = 'styles';
            }
            $message = $addedStyles.' '.$styleText.' successfully added.';
        } else {
            $message = 'No styles added';
        }
        $output->writeln(sprintf($message));
        if ($changedStyles > 0) {
            $styleText = 'style';
            if ($changedStyles > 1) {
                $styleText = 'styles';
            }
            $message = $changedStyles.' '.$styleText.' successfully changed.';
            $output->writeln(sprintf($message));
        }
    }

}