<?php

namespace App\Command;

use App\Entity\Client;
use App\Entity\Company;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class SyncClientProfile extends ContainerAwareCommand
{
    const RATHER_NOT_SAY = 'Rather Not Say';

    protected $em;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->em = $entityManager;
        parent::__construct(null);
    }

    protected function configure()
    {
        $this->setName('app:sync:clientProfile')
            ->setDescription('Sync client profile');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $companies = $this->em->createQuery('Select c FROM ' . Company::class . ' c')->iterate();

        $this->em->beginTransaction();
        try {
            $affectedRows = 0;
            $companyInterationCount = 0;
            foreach ($companies as $company) {
                /**
                 * @var Company $company
                 */
                $company = $company[0];

                $departments = $this->em->getRepository('App:CompanyDepartment')->getNamesByCompany($company);
                $regions = $this->em->getRepository('App:CompanyRegion')->getNamesByCompany($company);

                $defaultDepartment = (!empty($departments)) ? $departments[0] : '';
                $defaultRegion = (!empty($regions)) ? $regions[0] : '';

                $clients = $this->em->createQuery('Select c FROM ' . Client::class . ' c' . ' WHERE c.company = :company')
                    ->setParameter('company', $company)->iterate();

                $clientIterationCount = 0;
                foreach ($clients as $client) {
                    /**
                     * @var Client $client
                     */
                    $client = $client[0];

                    $changed = false;

                    $clientDepartment = $client->getDepartment();
                    if ($clientDepartment != self::RATHER_NOT_SAY && !empty($departments)
                        && !in_array($clientDepartment, $departments) && $departments !== $clientDepartment) {
                        $client->setDepartment($defaultDepartment);
                        $changed = true;
                    }

                    $clientRegion = $client->getRegion();
                    if ($clientRegion != self::RATHER_NOT_SAY && !empty($regions)
                        && !in_array($clientRegion, $regions) &&$regions !== $clientRegion) {
                        $client->setRegion($defaultRegion);
                        $changed = true;
                    }

                    if ($changed) {
                        $this->em->persist($client);
                        $affectedRows++;
                    }

                    if(++$clientIterationCount >= 10) {
                        $this->em->flush();
                        $this->em->clear();
                        $clientIterationCount = 0;
                    }
                }
            }
            $this->em->flush();
            $output->writeln("--- DONE. Affected rows: $affectedRows ---");
        }  catch (\Exception $exception) {
            $output->writeln($exception->getMessage());
            $this->em->rollback();
            return;
        }
        $this->em->commit();
    }
}