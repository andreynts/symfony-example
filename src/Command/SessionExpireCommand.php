<?php

namespace App\Command;

use App\Enum\SessionStatusEnum;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class SessionExpireCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this->setName('app:session:expire')
            ->setDescription('Expire outdated sessions')
            ->setHelp('This command will expire all outdated sessions');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $em = $this->getContainer()->get('doctrine.orm.entity_manager');
        $count = $em->createQueryBuilder()
            ->update('App:Session', 's')
            ->set('s.status', ':new_status')
            ->where('s.status = :old_status')
            ->andWhere("s.endedAt <= DATE_SUB(CURRENT_TIMESTAMP(), 2, 'HOUR')")
            ->setParameter('old_status', SessionStatusEnum::BOOKED)
            ->setParameter('new_status', SessionStatusEnum::EXPIRED)
            ->getQuery()
            ->execute();

        $output->writeln(sprintf('%d sessions expired', $count));
    }
}
