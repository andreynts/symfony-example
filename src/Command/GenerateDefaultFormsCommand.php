<?php

namespace App\Command;

use App\Entity\Answers;
use App\Entity\Client;
use App\Entity\Coach;
use App\Entity\Question;
use App\Entity\QuestionLocation;
use App\Entity\Session;
use App\Entity\SessionClientFeedback;
use App\Entity\SessionClientFeedbackChange;
use App\Entity\SessionCoachFeedback;
use App\Entity\SessionSurvey;
use App\Enum\AnswerTypeEnum;
use App\Utility\DataReader;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class GenerateDefaultFormsCommand extends ContainerAwareCommand
{
    const SESSION_FEEDBACK_COACH_FORM = 'SESSION_FEEDBACK_COACH_FORM';
    const SESSION_FEEDBACK_CLIENT_FORM = 'SESSION_FEEDBACK_CLIENT_FORM';
    const SESSION_CREATE_CLIENT_FORM = 'SESSION_CREATE_CLIENT_FORM';

    protected $em;

    static private $migrationClientFields = [
        'satisfied_session' => 'satisfiedSession',
        'satisfied_coach' => 'satisfiedCoach',
        'recommend_coaching' => 'recommendCoaching',
        'put_into_practice' => 'putIntoPractice',
        'changed_view' => 'changedView',
    ];
    static private $migrationCoachFields = [
        'reason' => 'reason',
        'client_relationship' => 'clientRelationship',
        'what_right' => 'whatRight',
        'key_people' => 'keyPeople',
        'key_processes' => 'keyProcesses',
        'key_communication' => 'keyCommunication',
        'advice' => 'advice',
        'fix_first' => 'fixFirst',
        'topics' => 'topics',
        'shared_tools' => 'sharedTools',
        'next_steps' => 'nextSteps',
        'feel_about_company' => 'feelAboutCompany',
    ];

    static private $migrationSessionFields = [
        'help_with' => 'details',
        'context' => 'background',
        'other_helpful' => 'extraDetails',
        'make_changes' => 'hasSuccess',
        'recommended' => 'didRecommend',
    ];

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->em = $entityManager;
        parent::__construct(null);
    }

    protected function configure()
    {
        $this->setName('app:generate:default:forms')
            ->setDescription('Generate Default Forms');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->em->beginTransaction();
        try {
            $this->createCoachForm();
            $output->writeln('Added feedback form for coach');
            $this->createClientForm();
            $output->writeln('Added feedback form for client');
            $this->createSessionClientForm();
            $output->writeln('Added session form for client');
            $this->migration($output);
        } catch (\Exception $exception) {
            $output->writeln($exception->getMessage());
            $this->em->rollback();
            return;
        }

        $this->em->commit();
    }

    private function createCoachForm()
    {
        $location = $this->createLocation(self::SESSION_FEEDBACK_COACH_FORM);

        $fields = $this->prepareCoachFields($location);

        foreach ($fields as $field) {
            $this->createField($field);
        }

        $this->em->flush();
    }

    private function createClientForm()
    {
        $location = $this->createLocation(self::SESSION_FEEDBACK_CLIENT_FORM);

        $fields = $this->prepareClientFields($location);

        foreach ($fields as $field) {
            $this->createField($field);
        }

        $this->em->flush();
    }

    private function createSessionClientForm()
    {
        $location = $this->createLocation(self::SESSION_CREATE_CLIENT_FORM);

        $fields = $this->prepareClientSessionFields($location);

        foreach ($fields as $field) {
            $this->createField($field);
        }

        $this->em->flush();
    }

    private function createLocation(string $label)
    {
        $repository = $this->em->getRepository('App:QuestionLocation');

        if ($repository->checkExistByValue($label)) {
            throw new \Exception('The form already exists, first delete the old form to create a new one. Warning! All form data will be deleted.', 409);
        }

        $location = new QuestionLocation();
        $location->setValue($label);

        $this->em->persist($location);
        $this->em->flush();

        return $location;
    }

    private function createField(array $field)
    {
        $question = new Question();
        if (isset($field['label'])) {
            $question->setLabel($field['label']);
        }
        if (isset($field['role'])) {
            $question->setRole($field['role']);
        }
        if (isset($field['position'])) {
            $question->setPosition($field['position']);
        }
        if (isset($field['answerType'])) {
            $question->setAnswerType($field['answerType']);
        }
        if (isset($field['values'])) {
            $question->setValues($field['values']);
        }
        if (isset($field['location'])) {
            $question->setQuestionLocation($field['location']);
        }

        $this->em->persist($question);
    }

    private function prepareCoachFields(QuestionLocation $location)
    {
        $role = 'ROLE_COACH';

        $reader = $this->getContainer()->get(DataReader::class);
        $reasonList = $reader->read('session/reasons.json');
        $clientRelationship = $reader->read('session/client-relationship.json');

        $form = [
            [
                'label' => 'What was the reason for the session?',
                'role' => $role,
                'position' => 1,
                'answerType' => AnswerTypeEnum::TYPE_DROP_DOWN,
                'location' => $location,
                'values' => [
                    'tips' => '',
                    'name' => 'reason',
                    'values' => (!empty($reasonList)) ? $reasonList : [],
                    'required' => true,
                ],
            ],
            [
                'label' => 'What best describes the clients relationship with the company?',
                'role' => $role,
                'position' => 2,
                'answerType' => AnswerTypeEnum::TYPE_DROP_DOWN,
                'location' => $location,
                'values' => [
                    'tips' => '',
                    'name' => 'client_relationship',
                    'values' => (!empty($clientRelationship)) ? $clientRelationship : [],
                    'required' => true,
                ],
            ],
            [
                'label' => 'What is clear that the company is getting right?',
                'role' => $role,
                'position' => 3,
                'answerType' => AnswerTypeEnum::TYPE_FREE_TEXT,
                'location' => $location,
                'values' => [
                    'tips' => 'Based on this coaching session, what have you learnt about the following? (Answer all that are appropriate):',
                    'name' => 'what_right',
                    'values' => '',
                    'required' => false,
                ],
            ],
            [
                'label' => 'People',
                'role' => $role,
                'position' => 4,
                'answerType' => AnswerTypeEnum::TYPE_FREE_TEXT,
                'location' => $location,
                'values' => [
                    'tips' => 'What do you think are the key issues (answer all that apply)?',
                    'name' => 'key_people',
                    'values' => '',
                    'required' => false,
                ],
            ],
            [
                'label' => 'Processes',
                'role' => $role,
                'position' => 5,
                'answerType' => AnswerTypeEnum::TYPE_FREE_TEXT,
                'location' => $location,
                'values' => [
                    'tips' => '',
                    'name' => 'key_processes',
                    'values' => '',
                    'required' => false,
                ],
            ],
            [
                'label' => 'Communication needs',
                'role' => $role,
                'position' => 6,
                'answerType' => AnswerTypeEnum::TYPE_FREE_TEXT,
                'location' => $location,
                'values' => [
                    'tips' => '',
                    'name' => 'key_communication',
                    'values' => '',
                    'required' => false,
                ],
            ],
            [
                'label' => 'What advice would you give to the company?',
                'role' => $role,
                'position' => 7,
                'answerType' => AnswerTypeEnum::TYPE_FREE_TEXT,
                'location' => $location,
                'values' => [
                    'tips' => '',
                    'name' => 'advice',
                    'values' => '',
                    'required' => false,
                ],
            ],
            [
                'label' => 'What would they fix first if they were CEO for the day?',
                'role' => $role,
                'position' => 8,
                'answerType' => AnswerTypeEnum::TYPE_FREE_TEXT,
                'location' => $location,
                'values' => [
                    'tips' => '',
                    'name' => 'fix_first',
                    'values' => '',
                    'required' => true,
                ],
            ],
            [
                'label' => 'What did you cover in the session?',
                'role' => $role,
                'position' => 9,
                'answerType' => AnswerTypeEnum::TYPE_FREE_TEXT,
                'location' => $location,
                'values' => [
                    'tips' => 'Notes for the session:',
                    'name' => 'topics',
                    'values' => '',
                    'required' => true,
                ],
            ],
            [
                'label' => 'What tools, techniques or methods were shared?',
                'role' => $role,
                'position' => 10,
                'answerType' => AnswerTypeEnum::TYPE_FREE_TEXT,
                'location' => $location,
                'values' => [
                    'tips' => '',
                    'name' => 'shared_tools',
                    'values' => '',
                    'required' => true,
                ],
            ],
            [
                'label' => 'What are they going to do next?',
                'role' => $role,
                'position' => 11,
                'answerType' => AnswerTypeEnum::TYPE_FREE_TEXT,
                'location' => $location,
                'values' => [
                    'tips' => '',
                    'name' => 'next_steps',
                    'values' => '',
                    'required' => true,
                ],
            ],
            [
                'label' => 'Which 3 words did the client use to describe how they feel about the company today?',
                'role' => $role,
                'position' => 12,
                'answerType' => AnswerTypeEnum::TYPE_TEXT_FIELD,
                'location' => $location,
                'values' => [
                    'tips' => '',
                    'name' => 'feel_about_company',
                    'values' => '',
                    'required' => true,
                ],
            ],
            [
                'answerType' => AnswerTypeEnum::TYPE_SUBMIT_BUTTON,
                'role' => $role,
                'location' => $location,
                'label' => 'Save',
                'values' => [
                    'value' => 'Save'
                ],
                'position' => 13,
            ],
        ];

        return $form;
    }

    private function prepareClientFields(QuestionLocation $location)
    {
        $role = 'ROLE_CLIENT';

        $reader = $this->getContainer()->get(DataReader::class);
        $putIntoPractice = $reader->read('session/put-into-practice.json');
        $changedView = $reader->read('session/changed-view.json');

        $form = [
            [
                'label' => 'How satisfied were you with the coaching session?',
                'role' => $role,
                'position' => 1,
                'answerType' => AnswerTypeEnum::TYPE_RADIO_BUTTON_RATING,
                'location' => $location,
                'values' => [
                    'tips' => '',
                    'name' => 'satisfied_session',
                    'range' => [1,2,3,4,5,6,7,8,9,10],
                    'minLabel' => 'Not at all',
                    'maxLabel' => 'Extremely satisfied',
                    'value' => '',
                    'required' => true,
                ],
            ],
            [
                'label' => 'How satisfied were you with the coach?',
                'role' => $role,
                'position' => 2,
                'answerType' => AnswerTypeEnum::TYPE_RADIO_BUTTON_RATING,
                'location' => $location,
                'values' => [
                    'tips' => '',
                    'name' => 'satisfied_coach',
                    'range' => [1,2,3,4,5,6,7,8,9,10],
                    'minLabel' => 'Not at all',
                    'maxLabel' => 'Extremely satisfied',
                    'value' => '',
                    'required' => true,
                ],
            ],
            [
                'label' => 'How likely are you to recommend MyThrive coaching to your friends / colleagues?',
                'role' => $role,
                'position' => 3,
                'answerType' => AnswerTypeEnum::TYPE_RADIO_BUTTON_RATING,
                'location' => $location,
                'values' => [
                    'tips' => '',
                    'name' => 'recommend_coaching',
                    'range' => [1,2,3,4,5,6,7,8,9,10],
                    'minLabel' => 'Not at all',
                    'maxLabel' => 'Likely',
                    'value' => '',
                    'required' => true,
                ],
            ],
            [
                'label' => 'How quickly can you put what you learnt in your session into practice?',
                'role' => $role,
                'position' => 4,
                'answerType' => AnswerTypeEnum::TYPE_DROP_DOWN,
                'location' => $location,
                'values' => [
                    'tips' => '',
                    'name' => 'put_into_practice',
                    'values' => (!empty($putIntoPractice)) ? $putIntoPractice : [],
                    'required' => true,
                ],
            ],
            [
                'label' => 'Which of the following did the session change your view of (tick all that apply)?',
                'role' => $role,
                'position' => 5,
                'answerType' => AnswerTypeEnum::TYPE_TICK_BOX,
                'location' => $location,
                'values' => [
                    'tips' => '',
                    'name' => 'changed_view',
                    'values' => (!empty($changedView)) ? $changedView : [],
                    'required' => true,
                ],
            ],
            [
                'answerType' => AnswerTypeEnum::TYPE_SUBMIT_BUTTON,
                'role' => $role,
                'location' => $location,
                'label' => 'Submit to Coach',
                'values' => [
                    'value' => 'Submit to Coach'
                ],
                'position' => 6,
            ],
        ];

        return $form;
    }

    private function prepareClientSessionFields(QuestionLocation $location)
    {
        $role = 'ROLE_CLIENT';

        $form = [
            [
                'label' => 'What specifically do you want this coaching session to help with?',
                'role' => $role,
                'position' => 1,
                'answerType' => AnswerTypeEnum::TYPE_FREE_TEXT,
                'location' => $location,
                'values' => [
                    'tips' => '',
                    'name' => 'help_with',
                    'values' => '',
                    'required' => true,
                ],
            ],
            [
                'label' => 'What background or context is relevant for this conversation?',
                'role' => $role,
                'position' => 2,
                'answerType' => AnswerTypeEnum::TYPE_FREE_TEXT,
                'location' => $location,
                'values' => [
                    'tips' => '',
                    'name' => 'context',
                    'values' => '',
                    'required' => true,
                ],
            ],
            [
                'label' => 'Is there anything else that would be helpful to share with your coach?',
                'role' => $role,
                'position' => 3,
                'answerType' => AnswerTypeEnum::TYPE_FREE_TEXT,
                'location' => $location,
                'values' => [
                    'tips' => '',
                    'name' => 'other_helpful',
                    'values' => '',
                    'required' => true,
                ],
            ],
            [
                'label' => 'Were you successful in making changes since your last session?',
                'role' => $role,
                'position' => 4,
                'answerType' => AnswerTypeEnum::TYPE_RADIO_BUTTON,
                'location' => $location,
                'values' => [
                    'tips' => '',
                    'name' => 'make_changes',
                    'value' => [
                        'no' => 'No',
                        'yes' => 'Yes',
                    ],
                    'required' => true,
                ],
            ],
            [
                'label' => 'Did you recommend MyThrive to someone else?',
                'role' => $role,
                'position' => 5,
                'answerType' => AnswerTypeEnum::TYPE_RADIO_BUTTON,
                'location' => $location,
                'values' => [
                    'tips' => '',
                    'name' => 'recommended',
                    'value' => [
                        'no' => 'No',
                        'yes' => 'Yes',
                    ],
                    'required' => true,
                ],
            ],
        ];

        return $form;
    }

    private function migration(OutputInterface $output)
    {
        $this->applyCoachMigration($output);
        $this->applyClientMigration($output);
        $this->applySessionMigration($output);
    }

    private function applyCoachMigration(OutputInterface $output)
    {
        $feedbacks = $this->em->createQuery('Select scf FROM ' . SessionCoachFeedback::class . ' scf')->iterate();

        $location = $this->em->getRepository('App:QuestionLocation')->findLocationByValue(self::SESSION_FEEDBACK_COACH_FORM);

        if (!$location) {
            throw new \Exception('Location not found', 404);
        }

        $iterationCount = $totalRows = 0;
        foreach ($feedbacks as $feedback) {
            /**
             * @var SessionCoachFeedback $feedback
             * @var Session $session
             * @var Client $client
             * @var Coach $coach
             */
            $feedback = $feedback[0];
            $session = $feedback->getSession();
            $client = $session->getClient();
            $coach = $session->getCoach();

            if (!$session || !$client || !$coach) { continue; }

            $preparedData = $this->prepareAnswers($feedback, 'coach');

            foreach ($preparedData as $k => &$data) {

                if (empty($data['question_entity']) || empty($data['value'])) { continue; }

                $answer = new Answers();
                $answer
                    ->setSession($session)
                    ->setQuestion($data['question_entity'])
                    ->setCoach($coach)
                    ->setClient($client)
                    ->setResponse($data['value']);

                $this->em->persist($answer);

                if(++$iterationCount >= 10) {
                    $this->em->flush();
                    $iterationCount = 0;
                }

                $totalRows++;
            }
        }
        $this->em->flush();
        $output->writeln('Coach feedback migration applied. Affected rows: ' . $totalRows);
    }

    private function applyClientMigration(OutputInterface $output)
    {
        $feedbacks = $this->em->createQuery('Select scf FROM ' . SessionClientFeedback::class . ' scf')->iterate();

        $location = $this->em->getRepository('App:QuestionLocation')->findLocationByValue(self::SESSION_FEEDBACK_CLIENT_FORM);

        if (!$location) {
            throw new \Exception('Location not found', 404);
        }

        $iterationCount = $totalRows = 0;
        foreach ($feedbacks as $feedback) {
            /**
             * @var SessionClientFeedback $feedback
             * @var Session $session
             * @var Client $client
             * @var Coach $coach
             */
            $feedback = $feedback[0];
            $session = $feedback->getSession();
            $client = $session->getClient();
            $coach = $session->getCoach();

            if (!$session || !$client || !$coach) { continue; }

            $preparedData = $this->prepareAnswers($feedback, 'client');

            foreach ($preparedData as $k => &$data) {
                if ($k == 'changed_view' && $changedView = $feedback->getChangedView()) {
                    $changedViewData = [];
                    foreach ($changedView as $value) {
                        /**
                         * @var SessionClientFeedbackChange $value
                         */
                        $changedViewData[] = $value->getName();
                    }
                    $data['value'] = ['changed_view' => $changedViewData];
                }

                if (empty($data['question_entity']) || empty($data['value'])) { continue; }

                $answer = new Answers();
                $answer
                    ->setSession($session)
                    ->setQuestion($data['question_entity'])
                    ->setCoach($coach)
                    ->setClient($client)
                    ->setResponse($data['value']);

                $this->em->persist($answer);

                if(++$iterationCount >= 10) {
                    $this->em->flush();
                    $iterationCount = 0;
                }

                $totalRows++;
            }
        }
        $this->em->flush();

        $output->writeln('Client feedback migration applied. Affected rows: ' . $totalRows);
    }

    private function applySessionMigration(OutputInterface $output)
    {

        $sessions = $this->em->createQuery('Select s FROM ' . Session::class . ' s')->iterate();

        $location = $this->em->getRepository('App:QuestionLocation')->findLocationByValue(self::SESSION_CREATE_CLIENT_FORM);

        if (!$location) {
            throw new \Exception('Location not found', 404);
        }

        $iterationCount = $totalRows = 0;
        foreach ($sessions as $session) {
            /**
             * @var Session $session
             * @var Session $session
             * @var Client $client
             * @var Coach $coach
             */
            $session = $session[0];
            $client = $session->getClient();
            $coach = $session->getCoach();

            if (!$session || !$client || !$coach) { continue; }

            $preparedData = $this->prepareAnswers($session, 'session');

            foreach ($preparedData as $k => &$data) {

                if (empty($data['question_entity']) || empty($data['value'])) { continue; }

                $answer = new Answers();
                $answer
                    ->setSession($session)
                    ->setQuestion($data['question_entity'])
                    ->setCoach($coach)
                    ->setClient($client)
                    ->setResponse($data['value']);

                $this->em->persist($answer);

                if(++$iterationCount >= 10) {
                    $this->em->flush();
                    $iterationCount = 0;
                }

                $totalRows++;
            }
        }
        $this->em->flush();

        $output->writeln('Session feedback migration applied. Affected rows: ' . $totalRows);
    }

    private function prepareAnswers($feedback, string $role = 'coach'): array
    {
        $data = [];
        $surveyRepo = null;
        if ($role == 'coach') {
            $migrationFields = self::$migrationCoachFields;
        } elseif ($role == 'client') {
            $migrationFields = self::$migrationClientFields;
        } else {
            $migrationFields = self::$migrationSessionFields;
            $surveyRepo = $this->em->getRepository('App:SessionSurvey');
        }

        $questionRepository = $this->em->getRepository('App:Question');
        foreach ($migrationFields as $k => $field) {
            if ($role == 'session' && ($k == 'make_changes' || $k == 'recommended')) {
                /**
                 * @var SessionSurvey $survey
                 */
                $survey = $surveyRepo->getSurveyBySession($feedback);

                if (!$survey) {continue; }

                if ($k == 'make_changes') {
                    $surveyValue = ($survey->getHasSuccess()) ? 'Yes' : 'No';
                } else {
                    $surveyValue = ($survey->getDidRecommend()) ? 'Yes' : 'No';
                }

                $data[$k]['value'] = [$k => $surveyValue];

            } elseif ($value = $feedback->getProperty($field)) {

                if (!$value) {continue; }

                $data[$k]['value'] = [$k => $value];
            }

            if ($question = $questionRepository->getQuestionByFieldName($k)) {
                $data[$k]['question_entity'] = $question;
            }
        }
        return $data;
    }
}