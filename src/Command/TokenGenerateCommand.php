<?php

namespace App\Command;

use App\Utility\OpenTokClient;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class TokenGenerateCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this->setName('app:token:generate')
            ->setDescription('Generate missing session tokens')
            ->setHelp('This command will generate all missing session tokens');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $env = getenv('APP_PLATFORM');

        $em = $this->getContainer()->get('doctrine.orm.entity_manager');
        $openTok = $this->getContainer()->get(OpenTokClient::class);

        $qb = $em->createQueryBuilder()
            ->select(
                's', 'e',
                'PARTIAL cl.{id}',
                'PARTIAL co.{id}',
                'PARTIAL clu.{id,firstName,lastName}',
                'PARTIAL cou.{id,firstName,lastName}')
            ->from('App:Session', 's')
            ->join('s.expertise', 'e')
            ->join('s.client', 'cl')
            ->join('s.coach', 'co')
            ->join('cl.user', 'clu')
            ->join('co.user', 'cou')
            ->where('s.tokClientToken IS NULL OR s.tokCoachToken IS NULL')
            ->andWhere('s.startedAt > CURRENT_TIMESTAMP() AND s.endedAt < :date')
            ->setParameter('date', new \DateTime('+24 hours'));

        $count = 0;
        foreach ($qb->getQuery()->iterate() as $row) {
            $session = $row[0];
            $data = [
                'env' => $env,
                'session' => [
                    'id' => $session->getId(),
                    'name' => $session->getExpertise()->getName(),
                ],
            ];

            $client = $session->getClient();
            $coach = $session->getCoach();

            $tokSession = $session->getTokSession();
            if (null === $tokSession) {
                continue;
            }
            $expireTime = $session->getEndedAt()->getTimestamp();
            if ('dev' === $env) { // extend by 10 days
                $expireTime += 864000;
            } else { // 2 hours until EXPIRED status
                $expireTime += 7200;
            }

            $session->setTokClientToken($openTok->generateToken($tokSession, [
                'expireTime' => $expireTime,
                'data' => json_encode($data + [
                    'client' => [
                        'id' => $client->getId(),
                        'first_name' => $client->getUser()->getFirstName(),
                        'last_name' => $client->getUser()->getLastName(),
                    ],
                ]),
            ]));

            $session->setTokCoachToken($openTok->generateToken($tokSession, [
                'expireTime' => $expireTime,
                'data' => json_encode($data + [
                    'coach' => [
                        'id' => $coach->getId(),
                        'first_name' => $coach->getUser()->getFirstName(),
                        'last_name' => $coach->getUser()->getLastName(),
                    ],
                ]),
            ]));

            if (0 === ++$count % 20) {
                $em->flush();
                $em->clear();
            }
        }
        $em->flush();

        $output->writeln(sprintf('%d sessions updated', $count));
    }
}
