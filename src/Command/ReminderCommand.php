<?php

namespace App\Command;

use App\Entity\Session;
use App\Enum\NotificationEventEnum;
use App\Enum\SessionStatusEnum;
use App\Utility\Mailer;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ReminderCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this->setName('app:reminder')
            ->setDescription('Send reminder notifications')
            ->setHelp('This command will send all reminder notifications');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->sessionStartedReminder($input, $output);
    }

    private function sessionStartedReminder(InputInterface $input, OutputInterface $output)
    {
        $em = $this->getContainer()->get('doctrine.orm.entity_manager');
        $mailer = $this->getContainer()->get(Mailer::class);

        $qb = $em->createQueryBuilder();
        $qb->select(
                's.id',
                's.type type',
                's.reminder reminder',
                's.startedAt started_at',
                's.endedAt ended_at',
                'e.name expertise_name',
                'cl.position client_position',
                'cl.companyName client_company_name',
                'clu.username client_email',
                'clu.firstName client_first_name',
                'clu.lastName client_last_name',
                'clu.timezone client_timezone',
                'cln.id client_notification',
                'cou.username coach_email',
                'cou.firstName coach_first_name',
                'cou.lastName coach_last_name',
                'cou.timezone coach_timezone',
                'con.id coach_notification')
            ->from('App:Session', 's')
            ->join('s.expertise', 'e')
            ->join('s.client', 'cl')
            ->join('cl.user', 'clu')
            ->join('s.coach', 'co')
            ->join('co.user', 'cou')
            ->leftJoin('App:Notification', 'cln', 'WITH', 'cln.user = clu AND cln.event = :event AND cln.reminder IS NOT NULL')
            ->leftJoin('App:Notification', 'con', 'WITH', 'con.user = cou AND con.event = :event AND con.reminder IS NOT NULL')
            ->where('s.status = :status AND BIT_OR(s.reminder, :mask) <> s.reminder')
            ->andWhere($qb->expr()->orX(
                "s.startedAt <= DATE_ADD(CURRENT_TIMESTAMP(), (cln.reminder * 60), 'SECOND')",
                "s.startedAt <= DATE_ADD(CURRENT_TIMESTAMP(), (con.reminder * 60), 'SECOND')"))
            ->setParameter('event', NotificationEventEnum::SESSION_STARTED)
            ->setParameter('status', SessionStatusEnum::BOOKED)
            ->setParameter('mask', Session::REMINDER_ALL);

        $clientCounter = 0;
        $coachCounter = 0;

        $bitList = [];
        foreach ($qb->getQuery()->iterate() as $row) {
            $data = current($row);

            $bitmask = $data['reminder'];
            if ($data['client_notification'] && ($bitmask | Session::REMINDER_CLIENT) !== $bitmask) {
                $mailer->sendTo($data['client_email'], 'session/reminder/client.html.twig', $data);
                $bitmask |= Session::REMINDER_CLIENT;
                ++$clientCounter;
            }

            if ($data['coach_notification'] && ($bitmask | Session::REMINDER_COACH) !== $bitmask) {
                $mailer->sendTo($data['coach_email'], 'session/reminder/coach.html.twig', $data);
                $bitmask |= Session::REMINDER_COACH;
                ++$coachCounter;
            }

            $bitList[$bitmask][] = $data['id'];
        }

        foreach ($bitList as $bitmask => $idList) {
            $em->createQueryBuilder()
                ->update('App:Session', 's')
                ->set('s.reminder', ':bitmask')
                ->where('s IN (:id_list)')
                ->setParameter('bitmask', $bitmask)
                ->setParameter('id_list', $idList)
                ->getQuery()
                ->execute();
        }

        $output->writeln(sprintf('Client: %d sent / Coach: %d sent', $clientCounter, $coachCounter));
    }
}
