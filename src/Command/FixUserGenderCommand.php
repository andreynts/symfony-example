<?php

namespace App\Command;

use App\Entity\Client;
use App\Entity\Coach;
use App\Utility\DataReader;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class FixUserGenderCommand extends ContainerAwareCommand
{
    protected $em;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->em = $entityManager;
        parent::__construct(null);
    }

    protected function configure()
    {
        $this->setName('app:audit:gender:fix')
            ->setDescription('Fix Audit Gender');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $clients = $this->em->createQuery('Select c FROM ' . Client::class . ' c')->iterate();

        $this->em->beginTransaction();
        try {
            $reader = $this->getContainer()->get(DataReader::class);
            $genderList = $reader->read('gender.json');

            $iteration_count = 0;
            foreach($clients as $client) {
                $client = $client[0];
                /**
                 * @var Client $client
                 */
                $gender = $client->getGender();

                if (!in_array($gender, $genderList)) {
                    $client->setGender('Male'); //set default gender
                }

                $this->em->persist($client);
                if(++$iteration_count >= 10) {
                    $this->em->flush();
                    $this->em->clear();
                    $iteration_count = 0;
                }
            }
            unset($clients);

            $coaches = $this->em->createQuery('Select c FROM ' . Coach::class . ' c')->iterate();
            foreach ($coaches as $coach) {
                $coach = $coach[0];
                /**
                 * @var Coach $coach
                 */
                $gender = $coach->getGender();

                if (!in_array($gender, $genderList)) {
                    $coach->setGender('Male'); //set default gender
                }

                $this->em->persist($coach);

                $this->em->persist($coach);
                if(++$iteration_count >= 10) {
                    $this->em->flush();
                    $this->em->clear();
                    $iteration_count = 0;
                }
            }
            $this->em->flush();
        } catch (\Exception $exception) {
            $output->writeln($exception->getMessage());
            $this->em->rollback();
            return;
        }
        $this->em->commit();
    }
}