<?php

namespace App\Enum;

class NotificationChannelEnum extends AbstractEnum
{
    const EMAIL_PRIMARY = 'EMAIL_PRIMARY';
    const EMAIL_SECONDARY = 'EMAIL_SECONDARY';
    const PUSH = 'PUSH';
    const SMS = 'SMS';
}
