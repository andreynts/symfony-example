<?php

namespace App\Enum;

class ExpertiseLevelEnum extends AbstractEnum
{
    const CAN_COACH = 'CAN_COACH';
    const EXPERT = 'EXPERT';
}
