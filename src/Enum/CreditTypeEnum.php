<?php

namespace App\Enum;

class CreditTypeEnum extends AbstractEnum
{
    const PRIVATE_ = 'PRIVATE';
    const ENTERPRISE = 'ENTERPRISE';
    const EXECUTIVE = 'EXECUTIVE';
    const COMPLIMENTARY = 'COMPLIMENTARY';
}
