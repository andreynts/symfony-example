<?php

namespace App\Enum;

class NotificationEventEnum extends AbstractEnum
{
    const DEFAULT_ = 'DEFAULT';
    const SESSION_STARTED = 'SESSION_STARTED';
}
