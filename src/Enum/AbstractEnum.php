<?php

namespace App\Enum;

abstract class AbstractEnum
{
    protected static $cache = [];

    public static function items(): array
    {
        if (!array_key_exists(static::class, static::$cache)) {
            $reflection = new \ReflectionClass(static::class);
            $refConstants = $reflection->getConstants();
            $constants = array_filter($refConstants, function ($name) {
                return 0 !== strpos($name, '__'); // private
            }, ARRAY_FILTER_USE_KEY);
            static::$cache[static::class] = $constants;
        }

        return static::$cache[static::class];
    }

    public static function keys(): array
    {
        return array_keys(static::items());
    }

    public static function values(): array
    {
        return array_values(static::items());
    }

    public static function exists($value, bool $strict = true): bool
    {
        return in_array($value, static::items(), $strict);
    }
}
