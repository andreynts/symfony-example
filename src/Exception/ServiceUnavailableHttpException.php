<?php

namespace App\Exception;

use Symfony\Component\HttpKernel\Exception\ServiceUnavailableHttpException as BaseServiceUnavailableHttpException;

class ServiceUnavailableHttpException extends BaseServiceUnavailableHttpException
{
    const RETRY_AFTER = 60;

    /**
     * {@inheritdoc}
     */
    public function __construct($message = null, \Exception $previous = null, $code = 0)
    {
        parent::__construct(self::RETRY_AFTER, $message, $previous, $code);
    }
}
