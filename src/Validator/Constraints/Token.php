<?php

namespace App\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\Exception\ConstraintDefinitionException;

class Token extends Constraint
{
    public $requiredFields = [];

    public function __construct($options = null)
    {
        if (is_array($options) && !array_intersect(array_keys($options), ['groups', 'constraints'])) {
            $options = ['requiredFields' => $options];
        }

        parent::__construct($options);

        if (!is_array($this->requiredFields)) {
            throw new ConstraintDefinitionException('The option "requiredFields" is expected to be an array in constraint '.__CLASS__.'.');
        }
    }

    public function getDefaultOption()
    {
        return 'requiredFields';
    }
}
