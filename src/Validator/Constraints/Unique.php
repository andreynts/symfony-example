<?php

namespace App\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

class Unique extends Constraint
{
    const NOT_UNIQUE_ERROR = '7879ab46-a867-41e7-abc4-cec278b6b50a';

    public $message = 'This collection contains duplicates.';

    public $properties = null;

    protected static $errorNames = [
        self::NOT_UNIQUE_ERROR => 'NOT_UNIQUE_ERROR',
    ];

    public function getDefaultOption()
    {
        return 'properties';
    }
}
