<?php

namespace App\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

class Exists extends Constraint
{
    const VALUE_REQUIRED_ERROR = '9f9aff25-a829-4cc2-8abd-084d4e7afb89';

    public $value;

    public $message = 'Value {{ value }} is required.';

    protected static $errorNames = [
        self::VALUE_REQUIRED_ERROR => 'VALUE_REQUIRED_ERROR',
    ];

    public function getDefaultOption()
    {
        return 'value';
    }
}
