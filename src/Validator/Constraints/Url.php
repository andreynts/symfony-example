<?php

namespace App\Validator\Constraints;

use Symfony\Component\Validator\Constraints\Url as BaseUrl;

class Url extends BaseUrl
{
    const HOSTNAME_INVALID_ERROR = '09021cfc-d5d8-4588-b6bc-1f1f07bf6b01';
    const CONTENT_TYPE_INVALID_ERROR = 'ffa7b1ea-7ee3-4db1-b52e-8f289c6d227f';

    public $allowedHostNames = [];
    public $allowedContentTypes = [];

    public $hostNameMessage = 'The host name is not allowed.';
    public $contentTypeMessage = 'The content type is not allowed.';

    protected static $errorNames = [
        self::HOSTNAME_INVALID_ERROR => 'HOSTNAME_INVALID_ERROR',
        self::CONTENT_TYPE_INVALID_ERROR => 'CONTENT_TYPE_INVALID_ERROR',
    ];
}
