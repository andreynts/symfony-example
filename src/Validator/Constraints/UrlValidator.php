<?php

namespace App\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\Constraints\UrlValidator as BaseUrlValidator;

class UrlValidator extends BaseUrlValidator
{
    public function validate($value, Constraint $constraint)
    {
        parent::validate($value, $constraint);

        if (null === $value || '' === $value) {
            return;
        }

        if ($constraint->allowedHostNames) {
            $hostName = parse_url($value, PHP_URL_HOST);
            if ($port = parse_url($value, PHP_URL_PORT)) {
                $hostName .= ':'.$port;
            }

            if (!in_array($hostName, $constraint->allowedHostNames)) {
                $this->context->buildViolation($constraint->hostNameMessage)
                    ->setParameter('{{ value }}', $this->formatValue($hostName))
                    ->setCode(Url::HOSTNAME_INVALID_ERROR)
                    ->addViolation();
            }
        }

        if ($constraint->allowedContentTypes) {
            if (function_exists('curl_init')) {
                $ch = curl_init($value);
                curl_setopt($ch, CURLOPT_NOBODY, true);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_exec($ch);
                $contentType = curl_getinfo($ch, CURLINFO_CONTENT_TYPE);
            } else {
                $contentType = '';
                foreach (@get_headers($value) ?: [] as $header) {
                    if ('Content-Type' === strtok($header, ':')) {
                        $contentType = trim(explode(':', $header)[1]);
                        break;
                    }
                }
            }

            if (false !== $pos = mb_strpos($contentType, ';')) {
                $contentType = mb_substr($contentType, 0, $pos);
            }

            if (!in_array($contentType, $constraint->allowedContentTypes)) {
                $this->context->buildViolation($constraint->contentTypeMessage)
                    ->setParameter('{{ value }}', $this->formatValue($hostName))
                    ->setCode(Url::CONTENT_TYPE_INVALID_ERROR)
                    ->addViolation();
            }
        }
    }
}
