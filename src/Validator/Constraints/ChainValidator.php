<?php

namespace App\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;

class ChainValidator extends ConstraintValidator
{
    public function validate($value, Constraint $constraint)
    {
        if (!$constraint instanceof Chain) {
            throw new UnexpectedTypeException($constraint, __NAMESPACE__.'\Chain');
        }

        $context = $this->context;
        $validator = $context->getValidator()->inContext($context);

        foreach ($constraint->constraints as $constraint) {
            $validator->validate($value, $constraint);
            $violations = $validator->getViolations();
            if ($violations->count() > 0) {
                return;
            }
        }
    }
}
