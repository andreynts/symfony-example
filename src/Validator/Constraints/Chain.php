<?php

namespace App\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

class Chain extends Constraint
{
    public $constraints = [];

    public function getDefaultOption()
    {
        return 'constraints';
    }

    public function getRequiredOptions()
    {
        return ['constraints'];
    }

    protected function getCompositeOption()
    {
        return 'constraints';
    }
}
