<?php

namespace App\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

class Boolean extends Constraint
{
    const NOT_BOOLEAN_ERROR = '7ae47deb-8c24-4998-8ef1-f760de9e8055';

    public $message = 'This value should be boolean.';

    protected static $errorNames = [
        self::NOT_BOOLEAN_ERROR => 'NOT_BOOLEAN_ERROR',
    ];
}
