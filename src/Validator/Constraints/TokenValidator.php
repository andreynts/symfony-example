<?php

namespace App\Validator\Constraints;

use App\Enum\ErrorEnum;
use Lexik\Bundle\JWTAuthenticationBundle\Encoder\JWTEncoderInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Exception\JWTDecodeFailureException;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;

class TokenValidator extends ConstraintValidator
{
    private $encoder;

    public function __construct(JWTEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    public function validate($value, Constraint $constraint)
    {
        if (!$constraint instanceof Token) {
            throw new UnexpectedTypeException($constraint, __NAMESPACE__.'\Token');
        }

        try {
            $payload = $this->encoder->decode($value);
        } catch (JWTDecodeFailureException $ex) {
            switch ($ex->getReason()) {
                case JWTDecodeFailureException::INVALID_TOKEN:
                case JWTDecodeFailureException::UNVERIFIED_TOKEN:
                    $this->context->buildViolation('E_TOKEN_INVALID')
                        ->setCode(ErrorEnum::E_TOKEN_INVALID)
                        ->addViolation();

                    return;
                case JWTDecodeFailureException::EXPIRED_TOKEN:
                    $this->context->buildViolation('E_TOKEN_EXPIRED')
                        ->setCode(ErrorEnum::E_TOKEN_EXPIRED)
                        ->addViolation();

                    return;
                default:
                    throw $ex;
            }
        }

        foreach ($constraint->requiredFields as $field) {
            if (!array_key_exists($field, $payload)) {
                $this->context->buildViolation('E_TOKEN_INVALID')
                    ->setCode(ErrorEnum::E_TOKEN_INVALID)
                    ->addViolation();

                return;
            }
        }

        $constraint->payload = $payload;
    }
}
