<?php

namespace App\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;

class UniqueValidator extends ConstraintValidator
{
    public function validate($value, Constraint $constraint)
    {
        if (!$constraint instanceof Unique) {
            throw new UnexpectedTypeException($constraint, __NAMESPACE__.'\Unique');
        }

        if (null === $value) {
            return;
        }

        if (!is_array($value) && !$value instanceof \Traversable) {
            throw new UnexpectedTypeException($value, 'array or Traversable');
        }

        if ($properties = (array) $constraint->properties) {
            $value = array_map(function ($v) use ($properties) {
                return array_intersect_key($v, array_flip($properties));
            }, $value);
        }

        if ($value !== array_unique($value, SORT_REGULAR)) {
            $this->context->buildViolation($constraint->message)
                ->setCode(Unique::NOT_UNIQUE_ERROR)
                ->addViolation();
        }
    }
}
