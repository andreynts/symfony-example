<?php

namespace App\Entity;

use App\Enum\CreditTypeEnum;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="credit_types", indexes={
 *     @ORM\Index(columns={"type", "is_visible"})
 * })
 * @ORM\Entity(repositoryClass="App\Repository\CreditTypeRepository")
 */
class CreditType implements Interfaces\CreditInterface
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer", options={"unsigned": true})
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $title;
    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $type;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isVisible = true;


    public function getId()
    {
        return $this->id;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getType()
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getIsVisible()
    {
        return $this->isVisible;
    }

    public function setIsVisible(bool $isVisible): self
    {
        $this->isVisible = $isVisible;

        return $this;
    }
}