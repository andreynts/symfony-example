<?php

namespace App\Entity;

use App\Enum\NotificationChannelEnum;
use App\Enum\NotificationEventEnum;
use App\Enum\SessionTypeEnum;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="coaches", indexes={
 *     @ORM\Index(columns={"languages"}),
 *     @ORM\Index(columns={"country"}),
 *     @ORM\Index(columns={"latitude", "longitude"}),
 *     @ORM\Index(columns={"session_types"})
 * })
 * @ORM\Entity(repositoryClass="App\Repository\CoachRepository")
 * @ORM\HasLifecycleCallbacks
 */
class Coach
{
    use Traits\UpdatedAtTrait;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer", options={"unsigned": true})
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var User
     *
     * @ORM\OneToOne(targetEntity="User")
     * @ORM\JoinColumn(referencedColumnName="id", nullable=false, onDelete="CASCADE")
     */
    private $user;

    /**
     * @ORM\Column(type="string", length=128, nullable=true)
     */
    private $secondaryEmail;

    /**
     * @ORM\Column(type="string", length=32)
     */
    private $mobileNumber;

    /**
     * @ORM\Column(type="simple_array")
     */
    private $languages = [];

    /**
     * @ORM\Column(type="string", length=32)
     */
    private $ageRange;

    /**
     * @ORM\Column(type="string", length=32)
     */
    private $gender;

    /**
     * @ORM\Column(type="string", length=2, options={"fixed": true})
     */
    private $country;

    /**
     * @ORM\Column(type="string", length=32)
     */
    private $nationality;

    /**
     * @ORM\Column(type="string", length=48)
     */
    private $address1;

    /**
     * @ORM\Column(type="string", length=48, nullable=true)
     */
    private $address2;

    /**
     * @ORM\Column(type="string", length=32)
     */
    private $city;

    /**
     * @ORM\Column(type="string", length=32, nullable=true)
     */
    private $county;

    /**
     * @ORM\Column(type="string", length=16)
     */
    private $postcode;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=6, nullable=true)
     */
    private $latitude;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=6, nullable=true)
     */
    private $longitude;

    /**
     * @ORM\Column(type="string", length=32)
     */
    private $employmentStatus;

    /**
     * @ORM\Column(type="string", length=64, nullable=true)
     */
    private $companyName;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $companyWebsite;

    /**
     * @ORM\Column(type="string", length=32, nullable=true)
     */
    private $companyType;

    /**
     * @ORM\Column(type="string", length=64, nullable=true)
     */
    private $companyRegNumber;

    /**
     * @ORM\Column(type="string", length=64, nullable=true)
     */
    private $companyVatRegNumber;

    /**
     * @ORM\Column(type="string", length=64, nullable=true)
     */
    private $companyIndemnityProvider;

    /**
     * @ORM\Column(type="string", length=32, nullable=true)
     */
    private $companyIndemnityCover;

    /**
     * @ORM\Column(type="string", length=512, nullable=true)
     */
    private $otherCompanies;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="CoachQualification", mappedBy="coach", cascade={"persist", "remove"}, orphanRemoval=true)
     */
    private $qualifications;

    /**
     * @ORM\Column(type="string", length=128, nullable=true)
     */
    private $otherRelevantQualifications;

    /**
     * @ORM\Column(type="string", length=128, nullable=true)
     */
    private $otherQualifications;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="CoachTechExpertise", mappedBy="coach", cascade={"persist", "remove"}, orphanRemoval=true)
     */
    private $techExpertise;

    /**
     * @ORM\Column(type="string", length=64)
     */
    private $otherSeniorPosition;

    /**
     * @ORM\Column(type="smallint", options={"unsigned": true})
     */
    private $corporateWeekTime = 0;

    /**
     * @ORM\Column(type="smallint", options={"unsigned": true})
     */
    private $careerSmeTime = 0;

    /**
     * @ORM\Column(type="string", length=128, nullable=true)
     */
    private $careerSmeRoles;

    /**
     * @ORM\Column(type="smallint", options={"unsigned": true})
     */
    private $careerCorporateTime = 0;

    /**
     * @ORM\Column(type="string", length=128, nullable=true)
     */
    private $careerCorporateRoles;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="CoachCareerCompany", mappedBy="coach", cascade={"persist", "remove"}, orphanRemoval=true)
     */
    private $careerCompanies;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="CoachCareerIndustry", mappedBy="coach", cascade={"persist", "remove"}, orphanRemoval=true)
     */
    private $careerIndustries;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="CoachCoachedPeople", mappedBy="coach", cascade={"persist", "remove"}, orphanRemoval=true)
     */
    private $coachedPeople;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="CoachCoachedCompany", mappedBy="coach", cascade={"persist", "remove"}, orphanRemoval=true)
     */
    private $coachedCompanies;

    /**
     * @ORM\Column(type="string", length=128)
     */
    private $coachHoursInYear;

    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="Industry")
     * @ORM\JoinTable(name="coach_industries")
     */
    private $industries;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="CoachExpertise", mappedBy="coach", cascade={"persist", "remove"}, orphanRemoval=true)
     */
    private $expertise;

    /**
     * @ORM\Column(type="text")
     */
    private $about;

    /**
     * @ORM\Column(type="text")
     */
    private $testimonial;

    /**
     * @ORM\Column(type="simple_array")
     */
    private $sessionTypes = [SessionTypeEnum::DEFAULT_];

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="CoachAvailability", mappedBy="coach")
     */
    private $availability;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="Session", mappedBy="coach")
     */
    private $sessions;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="Answers", mappedBy="coach")
     */
    private $answers;

    public function __construct()
    {
        $this->qualifications = new ArrayCollection();
        $this->techExpertise = new ArrayCollection();
        $this->careerCompanies = new ArrayCollection();
        $this->careerIndustries = new ArrayCollection();
        $this->coachedPeople = new ArrayCollection();
        $this->coachedCompanies = new ArrayCollection();
        $this->industries = new ArrayCollection();
        $this->expertise = new ArrayCollection();
        $this->availability = new ArrayCollection();
        $this->sessions = new ArrayCollection();
        $this->answers = new ArrayCollection();
    }

    /**
     * @ORM\PrePersist
     */
    public function onPrePersist(LifecycleEventArgs $event)
    {
        $defaultNotification = new Notification();
        $defaultNotification->setUser($this->getUser());
        $defaultNotification->setEvent(NotificationEventEnum::DEFAULT_);
        $defaultNotification->setChannels([NotificationChannelEnum::EMAIL_PRIMARY]);
        $event->getEntityManager()->persist($defaultNotification);

        $sessionStartedNotification = new Notification();
        $sessionStartedNotification->setUser($this->getUser());
        $sessionStartedNotification->setEvent(NotificationEventEnum::SESSION_STARTED);
        $sessionStartedNotification->setReminder(60);
        $sessionStartedNotification->setChannels([NotificationChannelEnum::EMAIL_PRIMARY]);
        $event->getEntityManager()->persist($sessionStartedNotification);
    }

    public function getId()
    {
        return $this->id;
    }

    public function getUser()
    {
        return $this->user;
    }

    public function setUser(User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getSecondaryEmail()
    {
        return $this->secondaryEmail;
    }

    public function setSecondaryEmail($secondaryEmail): self
    {
        $this->secondaryEmail = $secondaryEmail;

        return $this;
    }

    public function getMobileNumber()
    {
        return $this->mobileNumber;
    }

    public function setMobileNumber(string $mobileNumber): self
    {
        $this->mobileNumber = $mobileNumber;

        return $this;
    }

    public function getLanguages(): array
    {
        return $this->languages;
    }

    public function setLanguages(array $languages): self
    {
        $this->languages = array_unique($languages);

        return $this;
    }

    public function getAgeRange()
    {
        return $this->ageRange;
    }

    public function setAgeRange(string $ageRange): self
    {
        $this->ageRange = $ageRange;

        return $this;
    }

    public function getGender()
    {
        return $this->gender;
    }

    public function setGender(string $gender): self
    {
        $this->gender = $gender;

        return $this;
    }

    public function getCountry()
    {
        return $this->country;
    }

    public function setCountry(string $country): self
    {
        $this->country = $country;

        return $this;
    }

    public function getNationality()
    {
        return $this->nationality;
    }

    public function setNationality(string $nationality): self
    {
        $this->nationality = $nationality;

        return $this;
    }

    public function getAddress1()
    {
        return $this->address1;
    }

    public function setAddress1(string $address1): self
    {
        $this->address1 = $address1;

        return $this;
    }

    public function getAddress2()
    {
        return $this->address2;
    }

    public function setAddress2($address2): self
    {
        $this->address2 = $address2;

        return $this;
    }

    public function getCity()
    {
        return $this->city;
    }

    public function setCity(string $city): self
    {
        $this->city = $city;

        return $this;
    }

    public function getCounty()
    {
        return $this->county;
    }

    public function setCounty($county): self
    {
        $this->county = $county;

        return $this;
    }

    public function getPostcode()
    {
        return $this->postcode;
    }

    public function setPostcode(string $postcode): self
    {
        $this->postcode = $postcode;

        return $this;
    }

    public function getLatitude()
    {
        return $this->latitude;
    }

    public function setLatutude($latitude): self
    {
        $this->latitude = $latitude;

        return $this;
    }

    public function getLongitude()
    {
        return $this->longitude;
    }

    public function setLongitude($longitude): self
    {
        $this->longitude = $longitude;

        return $this;
    }

    public function getEmploymentStatus()
    {
        return $this->employmentStatus;
    }

    public function setEmploymentStatus(string $employmentStatus): self
    {
        $this->employmentStatus = $employmentStatus;

        return $this;
    }

    public function getCompanyName()
    {
        return $this->companyName;
    }

    public function setCompanyName($companyName): self
    {
        $this->companyName = $companyName;

        return $this;
    }

    public function getCompanyWebsite()
    {
        return $this->companyWebsite;
    }

    public function setCompanyWebsite($companyWebsite): self
    {
        $this->companyWebsite = $companyWebsite;

        return $this;
    }

    public function getCompanyType()
    {
        return $this->companyType;
    }

    public function setCompanyType($companyType): self
    {
        $this->companyType = $companyType;

        return $this;
    }

    public function getCompanyRegNumber()
    {
        return $this->companyRegNumber;
    }

    public function setCompanyRegNumber($companyRegNumber): self
    {
        $this->companyRegNumber = $companyRegNumber;

        return $this;
    }

    public function getCompanyVatRegNumber()
    {
        return $this->companyVatRegNumber;
    }

    public function setCompanyVatRegNumber($companyVatRegNumber): self
    {
        $this->companyVatRegNumber = $companyVatRegNumber;

        return $this;
    }

    public function getCompanyIndemnityProvider()
    {
        return $this->companyIndemnityProvider;
    }

    public function setCompanyIndemnityProvider($companyIndemnityProvider): self
    {
        $this->companyIndemnityProvider = $companyIndemnityProvider;

        return $this;
    }

    public function getCompanyIndemnityCover()
    {
        return $this->companyIndemnityCover;
    }

    public function setCompanyIndemnityCover($companyIndemnityCover): self
    {
        $this->companyIndemnityCover = $companyIndemnityCover;

        return $this;
    }

    public function getOtherCompanies()
    {
        return $this->otherCompanies;
    }

    public function setOtherCompanies($otherCompanies): self
    {
        $this->otherCompanies = $otherCompanies;

        return $this;
    }

    public function getQualifications(bool $serialized = false)
    {
        if (!$serialized) {
            return $this->qualifications;
        }

        return array_map(function (CoachQualification $qualification) {
            return [
                'name' => $qualification->getName(),
                'awarded_at' => $qualification->getAwardedAt(),
            ];
        }, $this->qualifications->getValues());
    }

    public function getOtherRelevantQualifications()
    {
        return $this->otherRelevantQualifications;
    }

    public function setOtherRelevantQualifications($otherRelevantQualifications): self
    {
        $this->otherRelevantQualifications = $otherRelevantQualifications;

        return $this;
    }

    public function getOtherQualifications()
    {
        return $this->otherQualifications;
    }

    public function setOtherQualifications($otherQualifications): self
    {
        $this->otherQualifications = $otherQualifications;

        return $this;
    }

    public function getTechExpertise(bool $serialized = false)
    {
        if (!$serialized) {
            return $this->techExpertise;
        }

        return array_map(function (CoachTechExpertise $techExpertise) {
            return $techExpertise->getName();
        }, $this->techExpertise->getValues());
    }

    public function getOtherSeniorPosition()
    {
        return $this->otherSeniorPosition;
    }

    public function setOtherSeniorPosition(string $otherSeniorPosition): self
    {
        $this->otherSeniorPosition = $otherSeniorPosition;

        return $this;
    }

    public function getCorporateWeekTime()
    {
        return $this->corporateWeekTime;
    }

    public function setCorporateWeekTime(int $corporateWeekTime): self
    {
        $this->corporateWeekTime = $corporateWeekTime;

        return $this;
    }

    public function getCareerSmeTime()
    {
        return $this->careerSmeTime;
    }

    public function setCareerSmeTime(int $careerSmeTime): self
    {
        $this->careerSmeTime = $careerSmeTime;

        return $this;
    }

    public function getCareerSmeRoles()
    {
        return $this->careerSmeRoles;
    }

    public function setCareerSmeRoles($careerSmeRoles): self
    {
        $this->careerSmeRoles = $careerSmeRoles;

        return $this;
    }

    public function getCareerCorporateTime()
    {
        return $this->careerCorporateTime;
    }

    public function setCareerCorporateTime(int $careerCorporateTime): self
    {
        $this->careerCorporateTime = $careerCorporateTime;

        return $this;
    }

    public function getCareerCorporateRoles()
    {
        return $this->careerCorporateRoles;
    }

    public function setCareerCorporateRoles($careerCorporateRoles): self
    {
        $this->careerCorporateRoles = $careerCorporateRoles;

        return $this;
    }

    public function getCareerCompanies(bool $serialized = false)
    {
        if (!$serialized) {
            return $this->careerCompanies;
        }

        return array_map(function (CoachCareerCompany $careerCompany) {
            return $careerCompany->getName();
        }, $this->careerCompanies->getValues());
    }

    public function getCareerIndustries(bool $serialized = false)
    {
        if (!$serialized) {
            return $this->careerIndustries;
        }

        return array_map(function (CoachCareerIndustry $careerIndustry) {
            return $careerIndustry->getName();
        }, $this->careerIndustries->getValues());
    }

    public function getCoachedPeople(bool $serialized = false)
    {
        if (!$serialized) {
            return $this->coachedPeople;
        }

        return array_map(function (CoachCoachedPeople $coachedPeople) {
            return $coachedPeople->getName();
        }, $this->coachedPeople->getValues());
    }

    public function getCoachedCompanies(bool $serialized = false)
    {
        if (!$serialized) {
            return $this->coachedCompanies;
        }

        return array_map(function (CoachCoachedCompany $coachedCompany) {
            return $coachedCompany->getName();
        }, $this->coachedCompanies->getValues());
    }

    public function getCoachHoursInYear()
    {
        return $this->coachHoursInYear;
    }

    public function setCoachHoursInYear(string $coachHoursInYear): self
    {
        $this->coachHoursInYear = $coachHoursInYear;

        return $this;
    }

    public function getIndustries(bool $serialized = false)
    {
        if (!$serialized) {
            return $this->industries;
        }

        return array_map(function (Industry $industry) {
            return $industry->getId();
        }, $this->industries->getValues());
    }

    public function getExpertise(bool $serialized = false)
    {
        if (!$serialized) {
            return $this->expertise;
        }

        return array_map(function (CoachExpertise $expertise) {
            return [
                'id' => $expertise->getExpertise()->getId(),
                'level' => $expertise->getLevel(),
            ];
        }, $this->expertise->getValues());
    }

    public function getAbout()
    {
        return $this->about;
    }

    public function setAbout(string $about): self
    {
        $this->about = $about;

        return $this;
    }

    public function getTestimonial()
    {
        return $this->testimonial;
    }

    public function setTestimonial(string $testimonial): self
    {
        $this->testimonial = $testimonial;

        return $this;
    }

    public function getSessionTypes(): array
    {
        return $this->sessionTypes;
    }

    public function setSessionTypes(array $sessionTypes): self
    {
        $this->sessionTypes = array_unique($sessionTypes);

        return $this;
    }

    public function getAvailability()
    {
        return $this->availability;
    }

    public function getSessions()
    {
        return $this->sessions;
    }
}
