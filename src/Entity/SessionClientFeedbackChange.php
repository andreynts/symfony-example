<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="session_client_feedback_changes")
 * @ORM\Entity(repositoryClass="App\Repository\SessionClientFeedbackChangeRepository")
 */
class SessionClientFeedbackChange
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer", options={"unsigned": true})
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var SessionClientFeedback
     *
     * @ORM\ManyToOne(targetEntity="SessionClientFeedback", inversedBy="changedView")
     * @ORM\JoinColumn(referencedColumnName="id", nullable=false, onDelete="CASCADE")
     */
    private $sessionClientFeedback;

    /**
     * @ORM\Column(type="string", length=64)
     */
    private $name;

    public function getId()
    {
        return $this->id;
    }

    public function getSessionClientFeedback()
    {
        return $this->sessionClientFeedback;
    }

    public function setSessionClientFeedback(SessionClientFeedback $sessionClientFeedback): self
    {
        $this->sessionClientFeedback = $sessionClientFeedback;

        return $this;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }
}
