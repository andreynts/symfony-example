<?php


namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="styles", indexes={
 *     @ORM\Index(columns={"style_type_id"})
 * })
 * @ORM\Entity(repositoryClass="App\Repository\StyleRepository")
 */
class Style
{

    /**
     * @ORM\Id
     * @ORM\Column(type="integer", options={"unsigned": true})
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     */
    private $name;

    /**
     * @ORM\Column(type="string")
     */
    private $value;

    /**
     * @var StyleType
     *
     * @ORM\ManyToOne(targetEntity="StyleType")
     * @ORM\JoinColumn(referencedColumnName="id", onDelete="SET NULL")
     */
    private $styleType;

    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getValue()
    {
        return $this->value;
    }

    public function getType()
    {
        return $this->styleType;
    }

    public function setName(string $styleName): self
    {
        $this->name = $styleName;

        return $this;
    }

    public function setValue(string $styleValue): self
    {
        $this->value = $styleValue;

        return $this;
    }

    public function setType($styleType): self
    {
        $this->styleType = $styleType;

        return $this;
    }

}