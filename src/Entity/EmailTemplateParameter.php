<?php


namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="email_template_parameters", indexes={
 *     @ORM\Index(columns={"email_template_type_id"})
 * })
 * @ORM\Entity(repositoryClass="App\Repository\EmailTemplateParameterRepository")
 */
class EmailTemplateParameter
{

    /**
     * @ORM\Id
     * @ORM\Column(type="integer", options={"unsigned": true})
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var EmailTemplateType
     *
     * @ORM\ManyToOne(targetEntity="EmailTemplateType")
     * @ORM\JoinColumn(referencedColumnName="id", onDelete="SET NULL")
     */
    private $emailTemplateType;

    /**
     * @ORM\Column(type="string")
     */
    private $name;

    /**
     * @ORM\Column(type="string")
     */
    private $value;

    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getValue()
    {
        return $this->value;
    }

    public function getEmailTemplateType()
    {
        return $this->emailTemplateType;
    }

    public function setName(string $styleName): self
    {
        $this->name = $styleName;

        return $this;
    }

    public function setValue(string $styleValue): self
    {
        $this->value = $styleValue;

        return $this;
    }

    public function setType($emailTemplateType): self
    {
        $this->emailTemplateType = $emailTemplateType;

        return $this;
    }
}