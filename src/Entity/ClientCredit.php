<?php

namespace App\Entity;

use App\Enum\CreditTypeEnum;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Event\LifecycleEventArgs;

/**
 * @ORM\Table(name="client_credits", indexes={
 *     @ORM\Index(columns={"client_id", "type", "expires_at"})
 * })
 * @ORM\Entity(repositoryClass="App\Repository\ClientCreditRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class ClientCredit implements Interfaces\CreditInterface, \JsonSerializable
{
    use Traits\CreatedAtTrait;

    const DEFAULT_EXPIRY = '+1 year';
    const REFUND_EXPIRY = self::DEFAULT_EXPIRY;

    const TYPES = [
        CreditTypeEnum::PRIVATE_,
        CreditTypeEnum::ENTERPRISE,
        CreditTypeEnum::EXECUTIVE,
        CreditTypeEnum::COMPLIMENTARY,
    ];

    private $em;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer", options={"unsigned": true})
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var Client
     *
     * @ORM\ManyToOne(targetEntity="Client", inversedBy="credits")
     * @ORM\JoinColumn(referencedColumnName="id", nullable=false, onDelete="CASCADE")
     */
    private $client;

    /**
     * @ORM\Column(type="string", length=16, nullable=true)
     */
    private $type;

    /**
     * @var CreditType
     *
     * @ORM\ManyToOne(targetEntity="CreditType", cascade={"persist"},)
     * @ORM\JoinColumn(referencedColumnName="id", nullable=true, onDelete="CASCADE")
     */
    private $creditType;

    /**
     * @ORM\Column(type="smallint", options={"unsigned": true})
     */
    private $value;

    /**
     * @ORM\Column(type="date")
     */
    private $expiresAt;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isRefund = false;

    /**
     *  @ORM\PrePersist
     */
    public function setDynamicCreditType(LifecycleEventArgs $event)
    {
        if (is_string($this->type)) {
            $em = $event->getEntityManager();
            $repository = $em->getRepository('App:CreditType');
            $type = $repository->getCreditTypeByType($this->type);
            if ($type) {
                $this->setCreditType($type);
                $this->setType(null);
            }
        }
    }

    public function getId()
    {
        return $this->id;
    }

    public function getClient()
    {
        return $this->client;
    }

    public function setClient(Client $client): self
    {
        $this->client = $client;

        return $this;
    }

    public function getType($migration = false)
    {
        if ($migration) {
            return $this->type;
        }
        return ($this->creditType) ? $this->creditType->getType() : null;
    }

    public function setType($type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getValue()
    {
        return $this->value;
    }

    public function setValue(int $value): self
    {
        $this->value = $value;

        return $this;
    }

    public function getExpiresAt()
    {
        return $this->expiresAt;
    }

    public function setExpiresAt(\DateTime $expiresAt): self
    {
        $this->expiresAt = $expiresAt;

        return $this;
    }

    public function isRefund(): bool
    {
        return $this->isRefund;
    }

    public function setRefund(bool $isRefund): self
    {
        $this->isRefund = $isRefund;

        return $this;
    }

    public function getCreditType()
    {
        return $this->creditType;
    }

    public function setCreditType(CreditType $creditType): self
    {
        $this->creditType = $creditType;

        return $this;
    }

    public function jsonSerialize(): array
    {
        return [
            'type' => $this->getType(),
            'value' => $this->getValue(),
            'expires_at' => $this->getExpiresAt(),
            'is_refund' => $this->isRefund(),
            'created_at' => $this->getCreatedAt(),
        ];
    }
}
