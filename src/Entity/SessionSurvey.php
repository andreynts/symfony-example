<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="session_survey")
 * @ORM\Entity(repositoryClass="App\Repository\SessionSurveyRepository")
 */
class SessionSurvey
{
    use Traits\CreatedAtTrait;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer", options={"unsigned": true})
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var Session
     *
     * @ORM\ManyToOne(targetEntity="Session")
     * @ORM\JoinColumn(referencedColumnName="id", nullable=false, onDelete="CASCADE")
     */
    private $session;

    /**
     * @ORM\Column(type="boolean")
     */
    private $hasSuccess;

    /**
     * @ORM\Column(type="boolean")
     */
    private $didRecommend;

    public function getId()
    {
        return $this->id;
    }

    public function getSession()
    {
        return $this->session;
    }

    public function setSession(Session $session): self
    {
        $this->session = $session;

        return $this;
    }

    public function getHasSuccess()
    {
        return $this->hasSuccess;
    }

    public function setHasSuccess(bool $hasSuccess): self
    {
        $this->hasSuccess = $hasSuccess;

        return $this;
    }

    public function getDidRecommend()
    {
        return $this->didRecommend;
    }

    public function setDidRecommend(bool $didRecommend): self
    {
        $this->didRecommend = $didRecommend;

        return $this;
    }
}
