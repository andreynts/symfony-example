<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="coach_qualifications")
 * @ORM\Entity(repositoryClass="App\Repository\CoachQualificationRepository")
 */
class CoachQualification
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer", options={"unsigned": true})
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var Coach
     *
     * @ORM\ManyToOne(targetEntity="Coach", inversedBy="qualifications")
     * @ORM\JoinColumn(referencedColumnName="id", nullable=false, onDelete="CASCADE")
     */
    private $coach;

    /**
     * @ORM\Column(type="string", length=128)
     */
    private $name;

    /**
     * @ORM\Column(type="date")
     */
    private $awardedAt;

    public function getId()
    {
        return $this->id;
    }

    public function getCoach()
    {
        return $this->coach;
    }

    public function setCoach(Coach $coach): self
    {
        $this->coach = $coach;

        return $this;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getAwardedAt()
    {
        return $this->awardedAt;
    }

    public function setAwardedAt(\DateTime $awardedAt): self
    {
        $this->awardedAt = $awardedAt;

        return $this;
    }
}
