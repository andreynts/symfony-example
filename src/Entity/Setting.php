<?php


namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="settings", indexes={
 *     @ORM\Index(columns={"setting_type_id"}),
 *     @ORM\Index(columns={"setting_location_id"})
 * })
 * @ORM\Entity(repositoryClass="App\Repository\SettingRepository")
 */
class Setting
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer", options={"unsigned": true})
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     */
    private $name;

    /**
     * @ORM\Column(type="text")
     */
    private $value;

    /**
     * @var SettingType
     *
     * @ORM\ManyToOne(targetEntity="SettingType")
     * @ORM\JoinColumn(referencedColumnName="id", onDelete="SET NULL")
     */
    private $settingType;

    /**
     * @var SettingLocation
     *
     * @ORM\ManyToOne(targetEntity="SettingLocation")
     * @ORM\JoinColumn(referencedColumnName="id", onDelete="SET NULL")
     */
    private $settingLocation;

    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getValue()
    {
        return $this->value;
    }

    public function getType()
    {
        return $this->settingType;
    }

    public function getLocation()
    {
        return $this->settingLocation;
    }

    public function setName(string $settingName): self
    {
        $this->name = $settingName;

        return $this;
    }

    public function setValue(string $settingValue): self
    {
        $this->value = $settingValue;

        return $this;
    }

    public function setType($settingType): self
    {
        $this->settingType = $settingType;

        return $this;
    }

    public function setLocation($settingLocation): self
    {
        $this->settingLocation = $settingLocation;

        return $this;
    }

}