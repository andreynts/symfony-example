<?php

namespace App\Entity;

use App\Enum\NotificationChannelEnum;
use App\Enum\NotificationEventEnum;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="clients", indexes={
 *     @ORM\Index(columns={"company_id", "is_company_contact"})
 * })
 * @ORM\Entity(repositoryClass="App\Repository\ClientRepository")
 * @ORM\HasLifecycleCallbacks
 */
class Client
{
    use Traits\UpdatedAtTrait;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer", options={"unsigned": true})
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var User
     *
     * @ORM\OneToOne(targetEntity="User")
     * @ORM\JoinColumn(referencedColumnName="id", nullable=false, onDelete="CASCADE")
     */
    private $user;

    /**
     * @var Company
     *
     * @ORM\ManyToOne(targetEntity="Company", inversedBy="clients")
     * @ORM\JoinColumn(referencedColumnName="id", onDelete="SET NULL")
     */
    private $company;

    /**
     * @ORM\Column(type="string", length=128, nullable=true)
     */
    private $secondaryEmail;

    /**
     * @ORM\Column(type="string", length=32)
     */
    private $mobileNumber;

    /**
     * @ORM\Column(type="simple_array", nullable=true)
     */
    private $languages = [];

    /**
     * @ORM\Column(type="string", length=32, nullable=true)
     */
    private $ageRange;

    /**
     * @ORM\Column(type="string", length=32, nullable=true)
     */
    private $gender;

    /**
     * @ORM\Column(type="string", length=2, options={"fixed": true}, nullable=true)
     */
    private $country;

    /**
     * @ORM\Column(type="string", length=32, nullable=true)
     */
    private $nationality;

    /**
     * @ORM\Column(type="string", length=64, nullable=true)
     */
    private $companyName;

    /**
     * @ORM\Column(type="string", length=72, nullable=true)
     */
    private $department;

    /**
     * @ORM\Column(type="string", length=72, nullable=true)
     */
    private $position;

    /**
     * @ORM\Column(type="string", length=72, nullable=true)
     */
    private $region;

    /**
     * @ORM\Column(type="string", length=32, nullable=true)
     */
    private $workDuration;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $attitude;

    /**
     * @ORM\Column(type="smallint", options={"unsigned": true}, nullable=true)
     */
    private $satisfiedCompany;

    /**
     * @ORM\Column(type="smallint", options={"unsigned": true}, nullable=true)
     */
    private $recommendCompany;

    /**
     * @ORM\Column(type="smallint", options={"unsigned": true}, nullable=true)
     */
    private $futureAtCompany;

    /**
     * @ORM\Column(type="smallint", options={"unsigned": true}, nullable=true)
     */
    private $futureTeam;

    /**
     * @ORM\Column(type="smallint", options={"unsigned": true}, nullable=true)
     */
    private $futureCompany;

    /**
     * @ORM\Column(type="smallint", options={"unsigned": true}, nullable=true)
     */
    private $futureIndustry;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isCompanyContact = false;

    /**
     * @ORM\Column(type="string", length=20, nullable=true)
     */
    private $referenceNumber;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="ClientCredit", mappedBy="client")
     */
    private $credits;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="Session", mappedBy="client")
     */
    private $sessions;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="Answers", mappedBy="client")
     */
    private $answers;

    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="Coach")
     * @ORM\JoinTable(name="favourite_coaches")
     */
    private $favouriteCoaches;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="ClientCreditConsumption", mappedBy="client", cascade={"persist"}, orphanRemoval=true)
     */
    private $consumptionCredit;

    public function __construct()
    {
        $this->credits = new ArrayCollection();
        $this->sessions = new ArrayCollection();
        $this->favouriteCoaches = new ArrayCollection();
        $this->consumptionCredit = new ArrayCollection();
        $this->answers = new ArrayCollection();
    }

    /**
     * @ORM\PrePersist
     */
    public function onPrePersist(LifecycleEventArgs $event)
    {
        $defaultNotification = new Notification();
        $defaultNotification->setUser($this->getUser());
        $defaultNotification->setEvent(NotificationEventEnum::DEFAULT_);
        $defaultNotification->setChannels([NotificationChannelEnum::EMAIL_PRIMARY]);
        $event->getEntityManager()->persist($defaultNotification);

        $sessionStartedNotification = new Notification();
        $sessionStartedNotification->setUser($this->getUser());
        $sessionStartedNotification->setEvent(NotificationEventEnum::SESSION_STARTED);
        $sessionStartedNotification->setReminder(60);
        $sessionStartedNotification->setChannels([NotificationChannelEnum::EMAIL_PRIMARY]);
        $event->getEntityManager()->persist($sessionStartedNotification);
    }

    public function getId()
    {
        return $this->id;
    }

    public function getUser()
    {
        return $this->user;
    }

    public function setUser(User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getCompany()
    {
        return $this->company;
    }

    public function setCompany($company): self
    {
        $this->company = $company;

        return $this;
    }

    public function getSecondaryEmail()
    {
        return $this->secondaryEmail;
    }

    public function setSecondaryEmail($secondaryEmail): self
    {
        $this->secondaryEmail = $secondaryEmail;

        return $this;
    }

    public function getMobileNumber()
    {
        return $this->mobileNumber;
    }

    public function setMobileNumber(string $mobileNumber): self
    {
        $this->mobileNumber = $mobileNumber;

        return $this;
    }

    public function getLanguages(): array
    {
        return $this->languages;
    }

    public function setLanguages(array $languages): self
    {
        $this->languages = array_unique($languages);

        return $this;
    }

    public function getAgeRange()
    {
        return $this->ageRange;
    }

    public function setAgeRange(string $ageRange): self
    {
        $this->ageRange = $ageRange;

        return $this;
    }

    public function getGender()
    {
        return $this->gender;
    }

    public function setGender(string $gender): self
    {
        $this->gender = $gender;

        return $this;
    }

    public function getCountry()
    {
        return $this->country;
    }

    public function setCountry(string $country): self
    {
        $this->country = $country;

        return $this;
    }

    public function getNationality()
    {
        return $this->nationality;
    }

    public function setNationality(string $nationality): self
    {
        $this->nationality = $nationality;

        return $this;
    }

    public function getCompanyName()
    {
        return $this->companyName;
    }

    public function setCompanyName(string $companyName): self
    {
        $this->companyName = $companyName;

        return $this;
    }

    public function getDepartment()
    {
        return $this->department;
    }

    public function setDepartment(string $department): self
    {
        $this->department = $department;

        return $this;
    }

    public function getPosition()
    {
        return $this->position;
    }

    public function setPosition(string $position): self
    {
        $this->position = $position;

        return $this;
    }

    public function getRegion()
    {
        return $this->region;
    }

    public function setRegion(string $region): self
    {
        $this->region = $region;

        return $this;
    }

    public function getWorkDuration()
    {
        return $this->workDuration;
    }

    public function setWorkDuration(string $workDuration): self
    {
        $this->workDuration = $workDuration;

        return $this;
    }

    public function getAttitude()
    {
        return $this->attitude;
    }

    public function setAttitude(string $attitude): self
    {
        $this->attitude = $attitude;

        return $this;
    }

    public function getSatisfiedCompany()
    {
        return $this->satisfiedCompany;
    }

    public function setSatisfiedCompany(int $satisfiedCompany): self
    {
        $this->satisfiedCompany = $satisfiedCompany;

        return $this;
    }

    public function getRecommendCompany()
    {
        return $this->recommendCompany;
    }

    public function setRecommendCompany(int $recommendCompany): self
    {
        $this->recommendCompany = $recommendCompany;

        return $this;
    }

    public function getFutureAtCompany()
    {
        return $this->futureAtCompany;
    }

    public function setFutureAtCompany(int $futureAtCompany): self
    {
        $this->futureAtCompany = $futureAtCompany;

        return $this;
    }

    public function getFutureTeam()
    {
        return $this->futureTeam;
    }

    public function setFutureTeam(int $futureTeam): self
    {
        $this->futureTeam = $futureTeam;

        return $this;
    }

    public function getFutureCompany()
    {
        return $this->futureCompany;
    }

    public function setFutureCompany(int $futureCompany): self
    {
        $this->futureCompany = $futureCompany;

        return $this;
    }

    public function getFutureIndustry()
    {
        return $this->futureIndustry;
    }

    public function setFutureIndustry(int $futureIndustry): self
    {
        $this->futureIndustry = $futureIndustry;

        return $this;
    }

    public function isCompanyContact(): bool
    {
        return $this->isCompanyContact;
    }

    public function setCompanyContact(bool $isCompanyContact): self
    {
        $this->isCompanyContact = $isCompanyContact;

        return $this;
    }

    public function getCredits()
    {
        return $this->credits;
    }

    public function getSessions()
    {
        return $this->sessions;
    }

    public function getFavouriteCoaches()
    {
        return $this->favouriteCoaches;
    }

    public function getReferenceNumber()
    {
        return $this->referenceNumber;
    }

    public function setReferenceNumber(string $referenceNumber): self
    {
        $this->referenceNumber = $referenceNumber;

        return $this;
    }

    public function getConsumptionCredit(bool $serialized = false)
    {
        if (!$serialized) {
            return $this->consumptionCredit;
        }

        return array_map(function (ClientCreditConsumption $consumption) {
            return $consumption->getCreditCost();
        }, $this->consumptionCredit->getValues());
    }
}
