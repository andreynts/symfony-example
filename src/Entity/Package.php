<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="packages")
 * @ORM\Entity(repositoryClass="App\Repository\PackageRepository")
 */
class Package implements \JsonSerializable
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer", options={"unsigned": true})
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @ORM\Column(type="decimal", precision=9, scale=2)
     */
    private $price;

    /**
     * @ORM\Column(type="string", length=3, options={"fixed": true})
     */
    private $currency;

    /**
     * @ORM\Column(type="smallint", options={"unsigned": true})
     */
    private $credit;

    /**
     * @ORM\Column(type="string", length=16)
     */
    private $creditType;

    /**
     * @ORM\Column(type="decimal", precision=4, scale=1)
     */
    private $vat;

    public function getId()
    {
        return $this->id;
    }

    public function getPrice()
    {
        return $this->price;
    }

    public function setPrice(string $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getCurrency()
    {
        return $this->currency;
    }

    public function setCurrency(string $currency): self
    {
        $this->currency = $currency;

        return $this;
    }

    public function getCredit()
    {
        return $this->credit;
    }

    public function setCredit(int $credit): self
    {
        $this->credit = $credit;

        return $this;
    }

    public function getCreditType()
    {
        return $this->creditType;
    }

    public function setCreditType(string $creditType): self
    {
        $this->creditType = $creditType;

        return $this;
    }

    public function getVat()
    {
        return $this->vat;
    }

    public function setVat(string $vat): self
    {
        $this->vat = $vat;

        return $this;
    }

    public function jsonSerialize(): array
    {
        return [
            'price' => $this->getPrice(),
            'currency' => $this->getCurrency(),
            'credit' => $this->getCredit(),
            'credit_type' => $this->getCreditType(),
        ];
    }
}
