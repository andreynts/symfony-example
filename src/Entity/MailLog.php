<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="mail_log")
 * @ORM\Entity(repositoryClass="App\Repository\MailLogRepository")
 */
class MailLog
{
    use Traits\CreatedAtTrait;

    /**
     * @ORM\Id
     * @ORM\Column(type="bigint", options={"unsigned": true})
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @ORM\Column(type="simple_array", name="to_")
     */
    private $to;

    /**
     * @ORM\Column(type="simple_array", name="from_")
     */
    private $from;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $subject;

    /**
     * @ORM\Column(type="text")
     */
    private $body;

    /**
     * @ORM\Column(type="integer", options={"unsigned": true})
     */
    private $result;

    public function getId()
    {
        return $this->id;
    }

    public function getTo()
    {
        return $this->to;
    }

    public function setTo(array $to): self
    {
        $this->to = $to;

        return $this;
    }

    public function getFrom()
    {
        return $this->from;
    }

    public function setFrom(array $from): self
    {
        $this->from = $from;

        return $this;
    }

    public function getSubject()
    {
        return $this->subject;
    }

    public function setSubject(string $subject): self
    {
        $this->subject = substr($subject, 0, 255);

        return $this;
    }

    public function getBody()
    {
        return $this->body;
    }

    public function setBody(string $body): self
    {
        $this->body = $body;

        return $this;
    }

    public function getResult()
    {
        return $this->result;
    }

    public function setResult(int $result): self
    {
        $this->result = $result;

        return $this;
    }
}
