<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="error_log")
 * @ORM\Entity(repositoryClass="App\Repository\ErrorLogRepository")
 */
class ErrorLog
{
    use Traits\CreatedAtTrait;

    /**
     * @ORM\Id
     * @ORM\Column(type="bigint", options={"unsigned": true})
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
    * @ORM\Column(type="string", length=100, nullable=true)
    */
    private $sessionId;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    public $action;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private  $errorCode;

    /**
     * @ORM\Column(type="string", length=256, nullable=true)
     */
    private $errorMessage;

    public function getId()
    {
        return $this->id;
    }

    public function getSessionId()
    {
        return $this->sessionId;
    }

    public function setSessionId(string $sessionId): self
    {
        $this->sessionId = $sessionId;

        return $this;
    }

    public function getAction()
    {
        return $this->action;
    }

    public function setAction(string $action): self
    {
        $this->action = $action;

        return $this;
    }

    public function getErrorCode()
    {
        return $this->errorCode;
    }

    public function setErrorCode(string $errorCode): self
    {
        $this->errorCode = $errorCode;

        return $this;
    }

    public function getErrorMessage()
    {
        return $this->errorMessage;
    }

    public function setErrorMessage(string $errorMessage): self
    {
        $this->errorMessage = $errorMessage;

        return $this;
    }
}