<?php

namespace App\Entity;

use App\Enum\CreditTypeEnum;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="client_credit_consumptions", indexes={
 *     @ORM\Index(columns={"client_id", "session_id", "created_at"})
 * })
 * @ORM\Entity(repositoryClass="App\Repository\ClientCreditConsumptionRepository")
 */
class ClientCreditConsumption implements Interfaces\CreditInterface
{
    use Traits\CreatedAtTrait;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer", options={"unsigned": true})
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var Client
     *
     * @ORM\ManyToOne(targetEntity="Client", inversedBy="credits")
     * @ORM\JoinColumn(referencedColumnName="id", nullable=false, onDelete="CASCADE")
     */
    private $client;

    /**
     * @var Session
     *
     * @ORM\OneToOne(targetEntity="Session")
     * @ORM\JoinColumn(referencedColumnName="id", nullable=false, onDelete="CASCADE")
     */
    private $session;

    /**
     * @ORM\Column(type="smallint", options={"unsigned": true})
     */
    private $creditCost;

    public function getId()
    {
        return $this->id;
    }

    public function getClient()
    {
        return $this->client;
    }

    public function setClient(Client $client): self
    {
        $this->client = $client;

        return $this;
    }

    public function getSession()
    {
        return $this->session;
    }

    public function setSession(Session $session): self
    {
        $this->session = $session;

        return $this;
    }

    public function getCreditCost()
    {
        return $this->creditCost;
    }

    public function setCreditCost(int $creditCost): self
    {
        $this->creditCost = $creditCost;

        return $this;
    }
}