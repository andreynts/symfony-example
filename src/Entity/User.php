<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\AdvancedUserInterface;

/**
 * @ORM\Table(name="users", indexes={
 *     @ORM\Index(columns={"first_name"})
 * })
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 */
class User implements AdvancedUserInterface
{
    use Traits\CreatedAtTrait;
    use Traits\UpdatedAtTrait;

    const MAX_AUTH_FAILS = 5;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer", options={"unsigned": true})
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=128, unique=true)
     */
    private $username;

    /**
     * @ORM\Column(type="string", length=60, options={"fixed": true})
     */
    private $password;

    /**
     * @ORM\Column(type="simple_array")
     */
    private $roles = [];

    /**
     * @ORM\Column(type="string", length=32)
     */
    private $firstName;

    /**
     * @ORM\Column(type="string", length=32)
     */
    private $lastName;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $photo;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isEnabled = true;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isVerified = false;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $lastAuthAt;

    /**
     * @ORM\Column(type="smallint", options={"unsigned": true, "default": 0})
     */
    private $authFails = 0;

    /**
     * @ORM\Column(type="string", length=48, options={"default": "UTC"})
     */
    private $timezone = 'UTC';

	/**
	 * @ORM\Column(type="boolean", nullable=true)
	 */
	private $emailUpdates = true;


    public function getId()
    {
        return $this->id;
    }

    public function getUsername()
    {
        return $this->username;
    }

    public function setUsername(string $username): self
    {
        $this->username = strtolower($username);

        return $this;
    }

    public function getPassword()
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    public function getRoles(): array
    {
        return $this->roles;
    }

    public function setRoles(array $roles): self
    {
        // allow only one row (or refactoring is required!)
        $this->roles = array_slice(array_unique($roles), 0, 1);

        return $this;
    }

    public function getFirstName()
    {
        return $this->firstName;
    }

    public function setFirstName(string $firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    public function getLastName()
    {
        return $this->lastName;
    }

    public function setLastName(string $lastName): self
    {
        $this->lastName = $lastName;

        return $this;
    }

    public function getPhoto()
    {
        return $this->photo;
    }

    public function setPhoto($photo): self
    {
        $this->photo = $photo;

        return $this;
    }

    public function isEnabled(): bool
    {
        return $this->isEnabled;
    }

    public function setEnabled(bool $isEnabled): self
    {
        $this->isEnabled = $isEnabled;

        return $this;
    }

    public function isVerified(): bool
    {
        return $this->isVerified;
    }

    public function setVerified(bool $isVerified): self
    {
        $this->isVerified = $isVerified;

        return $this;
    }

    public function getLastAuthAt()
    {
        return $this->lastAuthAt;
    }

    public function setLastAuthAt(\DateTime $lastAuthAt): self
    {
        $this->lastAuthAt = $lastAuthAt;

        return $this;
    }

    public function getAuthFails()
    {
        return $this->authFails;
    }

    public function setAuthFails(int $authFails): self
    {
        $this->authFails = $authFails;

        return $this;
    }

    public function getTimezone(): string
    {
        return $this->timezone;
    }

    public function setTimezone(string $timezone): self
    {
        $this->timezone = $timezone;

        return $this;
    }

    public function withTimezone(\DateTime $date)
    {
        $clonedDate = clone $date;

        return $clonedDate->setTimezone(new \DateTimeZone($this->timezone));
    }

    public function getEmail()
    {
        return filter_var($this->username, FILTER_VALIDATE_EMAIL) ?: null;
    }

    public function getSalt()
    {
        return null;
    }

    public function eraseCredentials()
    {
    }

    public function isAccountNonExpired(): bool
    {
        return true;
    }

    public function isAccountNonLocked(): bool
    {
        return $this->isEnabled && $this->isVerified && $this->authFails < self::MAX_AUTH_FAILS;
    }

    public function isCredentialsNonExpired(): bool
    {
        return true;
    }

	public function getEmailUpdates()
	{
		return $this->emailUpdates;
	}

	public function setEmailUpdates(bool $emailUpdates): self
	{
		$this->emailUpdates = $emailUpdates;

		return $this;
	}

    public function getTimezoneFromDate(\DateTime $date)
    {
        $cloneDate = clone $date;
        return 'UTC ' . $cloneDate->format(' P');
	}
}
