<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="request_log")
 * @ORM\Entity(repositoryClass="App\Repository\RequestLogRepository")
 */
class RequestLog
{
    use Traits\CreatedAtTrait;

    /**
     * @ORM\Id
     * @ORM\Column(type="bigint", options={"unsigned": true})
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @ORM\Column(type="smallint", options={"unsigned": true})
     */
    private $status;

    /**
     * @ORM\Column(type="string", length=16)
     */
    private $method;

    /**
     * @ORM\Column(type="string", length=2048)
     */
    private $uri;

    /**
     * @ORM\Column(type="json", options={"jsonb": true})
     */
    private $requestHeaders;

    /**
     * @ORM\Column(type="json", nullable=true, options={"jsonb": true})
     */
    private $requestBody;

    /**
     * @ORM\Column(type="json", options={"jsonb": true})
     */
    private $responseHeaders;

    /**
     * @ORM\Column(type="json", nullable=true, options={"jsonb": true})
     */
    private $responseBody;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(referencedColumnName="id", onDelete="SET NULL")
     */
    private $user;

    /**
     * @ORM\Column(type="string", length=15)
     */
    private $ip;

    /**
     * @ORM\Column(type="integer", options={"unsigned": true})
     */
    private $duration;

    /**
     * @ORM\Column(type="bigint", options={"unsigned": true})
     */
    private $memory;

    public function getId()
    {
        return $this->id;
    }

    public function getStatus()
    {
        return $this->status;
    }

    public function setStatus(int $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getMethod()
    {
        return $this->method;
    }

    public function setMethod(string $method): self
    {
        $this->method = $method;

        return $this;
    }

    public function getUri()
    {
        return $this->uri;
    }

    public function setUri(string $uri): self
    {
        $this->uri = substr($uri, 0, 2048);

        return $this;
    }

    public function getRequestHeaders()
    {
        return $this->requestHeaders;
    }

    public function setRequestHeaders(array $requestHeaders): self
    {
        $this->requestHeaders = $requestHeaders;

        return $this;
    }

    public function getRequestBody()
    {
        return $this->requestBody;
    }

    public function setRequestBody($requestBody): self
    {
        $this->requestBody = $requestBody;

        return $this;
    }

    public function getResponseHeaders()
    {
        return $this->responseHeaders;
    }

    public function setResponseHeaders(array $responseHeaders): self
    {
        $this->responseHeaders = $responseHeaders;

        return $this;
    }

    public function getResponseBody()
    {
        return $this->responseBody;
    }

    public function setResponseBody($responseBody): self
    {
        $this->responseBody = $responseBody;

        return $this;
    }

    public function getUser()
    {
        return $this->user;
    }

    public function setUser($user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getIp()
    {
        return $this->ip;
    }

    public function setIp(string $ip): self
    {
        $this->ip = $ip;

        return $this;
    }

    public function getDuration()
    {
        return $this->duration;
    }

    public function setDuration(int $duration): self
    {
        $this->duration = $duration;

        return $this;
    }

    public function getMemory()
    {
        return $this->memory;
    }

    public function setMemory(int $memory): self
    {
        $this->memory = $memory;

        return $this;
    }
}
