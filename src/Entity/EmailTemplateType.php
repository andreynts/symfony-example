<?php


namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="email_template_type")
 * @ORM\Entity(repositoryClass="App\Repository\EmailTemplateTypesRepository")
 */
class EmailTemplateType
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer", options={"unsigned": true})
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     */
    private $email_template_name;


    public function getId()
    {
        return $this->id;
    }

    public function getEmailTemplateName()
    {
        return $this->email_template_name;
    }

    public function setEmailTemplateName(string $emailTemplateName): self
    {
        $this->email_template_name = $emailTemplateName;

        return $this;
    }
}