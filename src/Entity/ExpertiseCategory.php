<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="expertise_categories", uniqueConstraints={
 *     @ORM\UniqueConstraint(columns={"name", "group_name"})
 * })
 * @ORM\Entity(repositoryClass="App\Repository\ExpertiseCategoryRepository")
 */
class ExpertiseCategory implements \JsonSerializable
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer", options={"unsigned": true})
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=48)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=32, nullable=true)
     */
    private $groupName;

    /**
     * @ORM\Column(type="integer", options={"default": 0})
     */
    private $position;

    /**
     * @ORM\Column(type="boolean", options={"default": true})
     */
    private $isPublic;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="Expertise", mappedBy="category")
     */
    private $expertise;

    public function __construct()
    {
        $this->expertise = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getGroupName()
    {
        return $this->groupName;
    }

    public function setGroupName($groupName): self
    {
        $this->groupName = $groupName;

        return $this;
    }

    public function getPosition()
    {
        return $this->position;
    }

    public function setPosition(int $position): self
    {
        $this->position = $position;

        return $this;
    }

    public function isPublic()
    {
        return $this->isPublic;
    }

    public function setPublic(bool $isPublic): self
    {
        $this->isPublic = $isPublic;

        return $this;
    }

    public function getExpertise(bool $serialized = false)
    {
        if (!$serialized) {
            return $this->expertise;
        }

        return array_map(function (Expertise $expertise) {
            return [
                'id' => $expertise->getId(),
                'name' => $expertise->getName(),
            ];
        }, $this->expertise->getValues());
    }

    public function jsonSerialize(): array
    {
        return [
            'id' => $this->getId(),
            'name' => $this->getName(),
            'group' => $this->getGroupName(),
            'is_public' => $this->isPublic(),
            'expertise' => $this->getExpertise(true),
        ];
    }
}
