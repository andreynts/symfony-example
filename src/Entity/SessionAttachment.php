<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="session_attachments")
 * @ORM\Entity(repositoryClass="App\Repository\SessionAttachmentRepository")
 */
class SessionAttachment
{
    use Traits\CreatedAtTrait;

    const CONTENT_TYPES = [
        'application/msword',                                                           // doc
        'application/vnd.openxmlformats-officedocument.wordprocessingml.document',      // docx
        'application/vnd.ms-excel',                                                     // xls
        'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',            // xlsx
        'application/vnd.ms-powerpoint',                                                // ppt
        'application/vnd.openxmlformats-officedocument.presentationml.presentation',    // pptx
        'application/pdf',                                                              // pdf
        'image/jpeg',                                                                   // jpg
        'image/png',                                                                    // png
    ];

    /**
     * @ORM\Id
     * @ORM\Column(type="integer", options={"unsigned": true})
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var Session
     *
     * @ORM\ManyToOne(targetEntity="Session", inversedBy="attachments")
     * @ORM\JoinColumn(referencedColumnName="id", nullable=false, onDelete="CASCADE")
     */
    private $session;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $path;

    public function getId()
    {
        return $this->id;
    }

    public function getSession()
    {
        return $this->session;
    }

    public function setSession(Session $session): self
    {
        $this->session = $session;

        return $this;
    }

    public function getPath()
    {
        return $this->path;
    }

    public function setPath(string $path): self
    {
        $this->path = $path;

        return $this;
    }
}
