<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="coach_availability", indexes={
 *     @ORM\Index(columns={"coach_id", "started_at"}),
 *     @ORM\Index(columns={"coach_id", "ended_at"})
 * })
 * @ORM\Entity(repositoryClass="App\Repository\CoachAvailabilityRepository")
 */
class CoachAvailability implements \JsonSerializable
{
    use Traits\CreatedAtTrait;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer", options={"unsigned": true})
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var Coach
     *
     * @ORM\ManyToOne(targetEntity="Coach", inversedBy="availability")
     * @ORM\JoinColumn(referencedColumnName="id", nullable=false, onDelete="CASCADE")
     */
    private $coach;

    /**
     * @ORM\Column(type="datetime")
     */
    private $startedAt;

    /**
     * @ORM\Column(type="datetime")
     */
    private $endedAt;

    public function getId()
    {
        return $this->id;
    }

    public function getCoach()
    {
        return $this->coach;
    }

    public function setCoach(Coach $coach): self
    {
        $this->coach = $coach;

        return $this;
    }

    public function getStartedAt()
    {
        return $this->startedAt;
    }

    public function setStartedAt(\DateTime $startedAt): self
    {
        $this->startedAt = $startedAt;

        return $this;
    }

    public function getEndedAt()
    {
        return $this->endedAt;
    }

    public function setEndedAt(\DateTime $endedAt): self
    {
        $this->endedAt = $endedAt;

        return $this;
    }

    public function jsonSerialize()
    {
        return [
            'started_at' => $this->getStartedAt(),
            'ended_at' => $this->getEndedAt(),
        ];
    }
}
