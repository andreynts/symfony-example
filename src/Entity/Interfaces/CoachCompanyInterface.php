<?php

namespace App\Entity\Interfaces;


interface CoachCompanyInterface
{
    const STATUS_ENABLE = 'enable';
    const STATUS_DISABLE = 'disable';
}