<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="session_client_feedback")
 * @ORM\Entity(repositoryClass="App\Repository\SessionClientFeedbackRepository")
 */
class SessionClientFeedback implements \JsonSerializable
{
    use Traits\CreatedAtTrait;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer", options={"unsigned": true})
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var Session
     *
     * @ORM\OneToOne(targetEntity="Session")
     * @ORM\JoinColumn(referencedColumnName="id", nullable=false, onDelete="CASCADE")
     */
    private $session;

    /**
     * @ORM\Column(type="smallint", options={"unsigned": true})
     */
    private $satisfiedSession;

    /**
     * @ORM\Column(type="smallint", options={"unsigned": true})
     */
    private $satisfiedCoach;

    /**
     * @ORM\Column(type="smallint", options={"unsigned": true})
     */
    private $recommendCoaching;

    /**
     * @ORM\Column(type="string", length=32)
     */
    private $putIntoPractice;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="SessionClientFeedbackChange", mappedBy="sessionClientFeedback", cascade={"persist"})
     */
    private $changedView;

    public function __construct()
    {
        $this->changedView = new ArrayCollection();
    }

    public function getProperty($property) {
        if (property_exists($this, $property)) {
            return $this->$property;
        }
    }

    public function getId()
    {
        return $this->id;
    }

    public function getSession()
    {
        return $this->session;
    }

    public function setSession(Session $session): self
    {
        $this->session = $session;

        return $this;
    }

    public function getSatisfiedSession()
    {
        return $this->satisfiedSession;
    }

    public function setSatisfiedSession(int $satisfiedSession): self
    {
        $this->satisfiedSession = $satisfiedSession;

        return $this;
    }

    public function getSatisfiedCoach()
    {
        return $this->satisfiedCoach;
    }

    public function setSatisfiedCoach(int $satisfiedCoach): self
    {
        $this->satisfiedCoach = $satisfiedCoach;

        return $this;
    }

    public function getRecommendCoaching()
    {
        return $this->recommendCoaching;
    }

    public function setRecommendCoaching(int $recommendCoaching): self
    {
        $this->recommendCoaching = $recommendCoaching;

        return $this;
    }

    public function getPutIntoPractice()
    {
        return $this->putIntoPractice;
    }

    public function setPutIntoPractice(string $putIntoPractice): self
    {
        $this->putIntoPractice = $putIntoPractice;

        return $this;
    }

    public function getChangedView()
    {
        return $this->changedView;
    }

    public function jsonSerialize(): array
    {
        return [
            'satisfied_session' => $this->getSatisfiedSession(),
            'satisfied_coach' => $this->getSatisfiedCoach(),
            'recommend_coaching' => $this->getRecommendCoaching(),
            'put_into_practice' => $this->getPutIntoPractice(),
            'changed_view' => array_map(function (SessionClientFeedbackChange $changedView) {
                return $changedView->getName();
            }, $this->getChangedView()->getValues()),
        ];
    }
}
