<?php

namespace App\Entity;

use App\Enum\SessionTypeEnum;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Event\LifecycleEventArgs;

/**
 * @ORM\Table(name="sessions", indexes={
 *     @ORM\Index(columns={"type"}),
 *     @ORM\Index(columns={"status"})
 * })
 * @ORM\Entity(repositoryClass="App\Repository\SessionRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Session
{
    use Traits\CreatedAtTrait;
    use Traits\UpdatedAtTrait;

    const INTERVAL = 15;
    const TIME_OFFSET = 30;

    const DURATION = [
        SessionTypeEnum::DEFAULT_ => 30,
        SessionTypeEnum::EXECUTIVE => 60,
    ];
    const MIN_DURATION = self::DURATION[SessionTypeEnum::DEFAULT_];

    const REMINDER_NONE = 0b00;
    const REMINDER_CLIENT = 0b01;
    const REMINDER_COACH = 0b10;
    const REMINDER_ALL = 0b11;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer", options={"unsigned": true})
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var Client
     *
     * @ORM\ManyToOne(targetEntity="Client", inversedBy="sessions")
     * @ORM\JoinColumn(referencedColumnName="id", nullable=false, onDelete="CASCADE")
     */
    private $client;

    /**
     * @var Coach
     *
     * @ORM\ManyToOne(targetEntity="Coach", inversedBy="sessions")
     * @ORM\JoinColumn(referencedColumnName="id", nullable=false, onDelete="CASCADE")
     */
    private $coach;

    /**
     * @var CoachAvailability
     *
     * @ORM\OneToOne(targetEntity="CoachAvailability")
     * @ORM\JoinColumn(referencedColumnName="id", onDelete="CASCADE")
     */
    private $availability;

    /**
     * @var Expertise
     *
     * @ORM\ManyToOne(targetEntity="Expertise")
     * @ORM\JoinColumn(referencedColumnName="id", nullable=false, onDelete="RESTRICT")
     */
    private $expertise;

    /**
     * @ORM\Column(type="string", length=16)
     */
    private $type;

    /**
     * @ORM\Column(type="datetime")
     */
    private $startedAt;

    /**
     * @ORM\Column(type="datetime")
     */
    private $endedAt;

    /**
     * @ORM\Column(type="string", length=16, nullable=true)
     */
    private $creditType;

    /**
     * @var CreditType
     *
     * @ORM\ManyToOne(targetEntity="CreditType", cascade={"persist"},)
     * @ORM\JoinColumn(referencedColumnName="id", nullable=true, onDelete="CASCADE")
     */
    private $cType;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $details;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $background;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $extraDetails;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="SessionAttachment", mappedBy="session", cascade={"persist", "remove"}, orphanRemoval=true)
     */
    private $attachments;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $clientNotes;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $coachNotes;

    /**
     * @ORM\Column(type="string", length=24)
     */
    private $status;

    /**
     * @ORM\Column(type="smallint", options={"unsigned": true})
     */
    private $rescheduled = 0;

    /**
     * @ORM\Column(type="string", length=32, options={"fixed": true})
     */
    private $roomKey;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $tokSession;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $tokClientToken;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $tokCoachToken;

    /**
     * @ORM\Column(type="smallint", options={"unsigned": true, "default": Session::REMINDER_NONE})
     */
    private $reminder = self::REMINDER_NONE;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="Answers", mappedBy="session", cascade={"persist"}, orphanRemoval=true)
     */
    private $answers;

    public function __construct()
    {
        $this->attachments = new ArrayCollection();
        $this->answers = new ArrayCollection();
    }

    public function getProperty($property) {
        if (property_exists($this, $property)) {
            return $this->$property;
        }
    }

    /**
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
    public function setDynamicCreditType(LifecycleEventArgs $event)
    {
        if (is_string($this->creditType)) {
            $em = $event->getEntityManager();
            $repository = $em->getRepository('App:CreditType');
            $type = $repository->getCreditTypeByType($this->creditType);
            if ($type) {
                $this->setCType($type);
                $this->setCreditType(null);
            }
        }
    }

    public function getId()
    {
        return $this->id;
    }

    public function getClient()
    {
        return $this->client;
    }

    public function setClient(Client $client): self
    {
        $this->client = $client;

        return $this;
    }

    public function getCoach()
    {
        return $this->coach;
    }

    public function setCoach(Coach $coach): self
    {
        $this->coach = $coach;

        return $this;
    }

    public function getAvailability()
    {
        return $this->availability;
    }

    public function setAvailability($availability): self
    {
        $this->availability = $availability;

        return $this;
    }

    public function getExpertise()
    {
        return $this->expertise;
    }

    public function setExpertise(Expertise $expertise): self
    {
        $this->expertise = $expertise;

        return $this;
    }

    public function getType()
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getStartedAt()
    {
        return $this->startedAt;
    }

    public function setStartedAt(\DateTime $startedAt): self
    {
        $this->startedAt = $startedAt;

        return $this;
    }

    public function getEndedAt()
    {
        return $this->endedAt;
    }

    public function setEndedAt(\DateTime $endedAt): self
    {
        $this->endedAt = $endedAt;

        return $this;
    }

    public function getCreditType($migration = false)
    {
        if ($migration) {
            return $this->creditType;
        }
        return ($this->cType) ? $this->cType->getType() : null;
    }

    public function setCreditType($creditType): self
    {
        $this->creditType = $creditType;

        return $this;
    }

    public function getDetails()
    {
        return $this->details;
    }

    public function setDetails(string $details): self
    {
        $this->details = $details;

        return $this;
    }

    public function getBackground()
    {
        return $this->background;
    }

    public function setBackground(string $background): self
    {
        $this->background = $background;

        return $this;
    }

    public function getExtraDetails()
    {
        return $this->extraDetails;
    }

    public function setExtraDetails(string $extraDetails): self
    {
        $this->extraDetails = $extraDetails;

        return $this;
    }

    public function getAttachments()
    {
        return $this->attachments;
    }

    public function getClientNotes()
    {
        return $this->clientNotes;
    }

    public function setClientNotes($clientNotes): self
    {
        $this->clientNotes = $clientNotes;

        return $this;
    }

    public function getCoachNotes()
    {
        return $this->coachNotes;
    }

    public function setCoachNotes($coachNotes): self
    {
        $this->coachNotes = $coachNotes;

        return $this;
    }

    public function getStatus()
    {
        return $this->status;
    }

    public function setStatus(string $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getRescheduled(): int
    {
        return $this->rescheduled;
    }

    public function setRescheduled(string $rescheduled): self
    {
        $this->rescheduled = $rescheduled;

        return $this;
    }

    public function getRoomKey()
    {
        return $this->roomKey;
    }

    public function setRoomKey(string $roomKey): self
    {
        $this->roomKey = $roomKey;

        return $this;
    }

    public function getTokSession()
    {
        return $this->tokSession;
    }

    public function setTokSession(string $tokSession): self
    {
        $this->tokSession = $tokSession;

        return $this;
    }

    public function getTokClientToken()
    {
        return $this->tokClientToken;
    }

    public function setTokClientToken(string $tokClientToken): self
    {
        $this->tokClientToken = $tokClientToken;

        return $this;
    }

    public function getTokCoachToken()
    {
        return $this->tokCoachToken;
    }

    public function setTokCoachToken(string $tokCoachToken): self
    {
        $this->tokCoachToken = $tokCoachToken;

        return $this;
    }

    public function getReminder(): int
    {
        return $this->reminder;
    }

    public function setReminder(int $reminder): self
    {
        $this->reminder = $reminder;

        return $this;
    }

    public function getCType()
    {
        return $this->cType;
    }

    public function setCType(CreditType $creditType): self
    {
        $this->cType = $creditType;

        return $this;
    }
}
