<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="session_coach_feedback")
 * @ORM\Entity(repositoryClass="App\Repository\SessionCoachFeedbackRepository")
 */
class SessionCoachFeedback implements \JsonSerializable
{
    use Traits\CreatedAtTrait;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer", options={"unsigned": true})
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var Session
     *
     * @ORM\OneToOne(targetEntity="Session")
     * @ORM\JoinColumn(referencedColumnName="id", nullable=false, onDelete="CASCADE")
     */
    private $session;

    /**
     * @ORM\Column(type="text")
     */
    private $reason;

    /**
     * @ORM\Column(type="text")
     */
    private $clientRelationship;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $whatRight;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $keyPeople;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $keyProcesses;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $keyCommunication;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $advice;

    /**
     * @ORM\Column(type="simple_array")
     */
    private $feelAboutCompany = [];

    /**
     * @ORM\Column(type="text")
     */
    private $fixFirst;

    /**
     * @ORM\Column(type="text")
     */
    private $topics;

    /**
     * @ORM\Column(type="text")
     */
    private $sharedTools;

    /**
     * @ORM\Column(type="text")
     */
    private $nextSteps;

    public function getProperty($property) {
        if (property_exists($this, $property)) {
            return $this->$property;
        }
    }

    public function getId()
    {
        return $this->id;
    }

    public function getSession()
    {
        return $this->session;
    }

    public function setSession(Session $session): self
    {
        $this->session = $session;

        return $this;
    }

    public function getReason()
    {
        return $this->reason;
    }

    public function setReason(string $reason): self
    {
        $this->reason = $reason;

        return $this;
    }

    public function getClientRelationship()
    {
        return $this->clientRelationship;
    }

    public function setClientRelationship(string $clientRelationship): self
    {
        $this->clientRelationship = $clientRelationship;

        return $this;
    }

    public function getWhatRight()
    {
        return $this->whatRight;
    }

    public function setWhatRight($whatRight): self
    {
        $this->whatRight = $whatRight;

        return $this;
    }

    public function getKeyPeople()
    {
        return $this->keyPeople;
    }

    public function setKeyPeople($keyPeople): self
    {
        $this->keyPeople = $keyPeople;

        return $this;
    }

    public function getKeyProcesses()
    {
        return $this->keyProcesses;
    }

    public function setKeyProcesses($keyProcesses): self
    {
        $this->keyProcesses = $keyProcesses;

        return $this;
    }

    public function getKeyCommunication()
    {
        return $this->keyCommunication;
    }

    public function setKeyCommunication($keyCommunication): self
    {
        $this->keyCommunication = $keyCommunication;

        return $this;
    }

    public function getAdvice()
    {
        return $this->advice;
    }

    public function setAdvice($advice): self
    {
        $this->advice = $advice;

        return $this;
    }

    public function getFeelAboutCompany(): array
    {
        return $this->feelAboutCompany;
    }

    public function setFeelAboutCompany(array $feelAboutCompany): self
    {
        $this->feelAboutCompany = $feelAboutCompany;

        return $this;
    }

    public function getFixFirst()
    {
        return $this->fixFirst;
    }

    public function setFixFirst(string $fixFirst): self
    {
        $this->fixFirst = $fixFirst;

        return $this;
    }

    public function getTopics()
    {
        return $this->topics;
    }

    public function setTopics(string $topics): self
    {
        $this->topics = $topics;

        return $this;
    }

    public function getSharedTools()
    {
        return $this->sharedTools;
    }

    public function setSharedTools(string $sharedTools): self
    {
        $this->sharedTools = $sharedTools;

        return $this;
    }

    public function getNextSteps()
    {
        return $this->nextSteps;
    }

    public function setNextSteps(string $nextSteps): self
    {
        $this->nextSteps = $nextSteps;

        return $this;
    }

    public function jsonSerialize(): array
    {
        return [
            'reason' => $this->getReason(),
            'client_relationship' => $this->getClientRelationship(),
            'what_right' => $this->getWhatRight(),
            'key_people' => $this->getKeyPeople(),
            'key_processes' => $this->getKeyProcesses(),
            'key_communication' => $this->getKeyCommunication(),
            'advice' => $this->getAdvice(),
            'feel_about_company' => $this->getFeelAboutCompany(),
            'fix_first' => $this->getFixFirst(),
            'topics' => $this->getTopics(),
            'shared_tools' => $this->getSharedTools(),
            'next_steps' => $this->getNextSteps(),
        ];
    }
}
