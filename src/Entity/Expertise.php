<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="expertise")
 * @ORM\Entity(repositoryClass="App\Repository\ExpertiseRepository")
 */
class Expertise
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer", options={"unsigned": true})
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var ExpertiseCategory
     *
     * @ORM\ManyToOne(targetEntity="ExpertiseCategory", inversedBy="expertise")
     * @ORM\JoinColumn(referencedColumnName="id", nullable=false, onDelete="CASCADE")
     */
    private $category;

    /**
     * @ORM\Column(type="string", length=128)
     */
    private $name;

    public function getId()
    {
        return $this->id;
    }

    public function getCategory()
    {
        return $this->category;
    }

    public function setCategory(ExpertiseCategory $category): self
    {
        $this->category = $category;

        return $this;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }
}
