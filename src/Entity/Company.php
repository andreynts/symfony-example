<?php

namespace App\Entity;

use App\Enum\CreditTypeEnum;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="companies", indexes={
 *     @ORM\Index(columns={"name"})
 * })
 * @ORM\Entity(repositoryClass="App\Repository\CompanyRepository")
 * @ORM\HasLifecycleCallbacks
 */
class Company
{
    use Traits\CreatedAtTrait;
    use Traits\UpdatedAtTrait;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer", options={"unsigned": true})
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=64)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $logo;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $website;

    /**
     * @ORM\Column(type="string", length=48)
     */
    private $address1;

    /**
     * @ORM\Column(type="string", length=48, nullable=true)
     */
    private $address2;

    /**
     * @ORM\Column(type="string", length=32)
     */
    private $city;

    /**
     * @ORM\Column(type="string", length=32, nullable=true)
     */
    private $county;

    /**
     * @ORM\Column(type="string", length=16)
     */
    private $postcode;

    /**
     * @ORM\Column(type="string", length=2, options={"fixed": true})
     */
    private $country;

    /**
     * @ORM\Column(type="string", length=64, nullable=true)
     */
    private $regNumber;

    /**
     * @ORM\Column(type="string", length=64, nullable=true)
     */
    private $vatRegNumber;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="CompanyRegion", mappedBy="company", cascade={"persist", "remove"}, orphanRemoval=true)
     */
    private $regions;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="CompanyDepartment", mappedBy="company", cascade={"persist", "remove"}, orphanRemoval=true)
     */
    private $departments;

  /**
   * @var ArrayCollection
   *
   * @ORM\OneToMany(targetEntity="CoachCompany", mappedBy="company", cascade={"persist", "remove"}, orphanRemoval=true)
   */
  private $coaches;

    /**
     * @ORM\Column(type="string", length=32, nullable=true)
     */
    private $poNumber;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $brief;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isEnabled = true;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="Client", mappedBy="company")
     */
    private $clients;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="CompanyCredit", mappedBy="company")
     */
    private $credits;

	/**
	 * @ORM\Column(type="boolean")
	 */
    private $tok_record;


    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="Invite", mappedBy="company", cascade={"persist", "remove"}, orphanRemoval=true)
     */
    private $invites;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="CompanyCreditConsumption", mappedBy="company", cascade={"persist"}, orphanRemoval=true)
     */
    private $consumptionCredit;

    /**
     * @ORM\Column(type="json", options={"jsonb": true}, nullable=true)
     */
    private $sessionLength;

    public function __construct()
    {
        $this->regions = new ArrayCollection();
        $this->departments = new ArrayCollection();
        $this->clients = new ArrayCollection();
        $this->credits = new ArrayCollection();
        $this->coaches = new ArrayCollection();
        $this->invites = new ArrayCollection();
        $this->consumptionCredit = new ArrayCollection();
    }

    /**
     * @ORM\PreUpdate
     */
    public function onPreUpdate(PreUpdateEventArgs $event)
    {
        if ($event->hasChangedField('name')) {
            $event->getEntityManager()
                ->createQueryBuilder()
                ->update('App:Client', 'c')
                ->set('c.companyName', ':name')
                ->where('c.company = :company')
                ->setParameter('name', $event->getNewValue('name'))
                ->setParameter('company', $this)
                ->getQuery()
                ->execute();
        }
        if ($event->hasChangedField('isEnabled')) {
            $isEnabled = $event->getNewValue('isEnabled');
            $event->getEntityManager()
                ->createQueryBuilder()
                ->update('App:User', 'u')
                ->set('u.roles', ':roles')
                ->where('u IN (SELECT IDENTITY(c.user) FROM App:Client c WHERE c.company = :company)')
                ->setParameter('roles', [$isEnabled ? 'ROLE_ENTERPRISE_CLIENT' : 'ROLE_CLIENT'])
                ->setParameter('company', $this)
                ->getQuery()
                ->execute();
        }
    }

    /**
     * @ORM\PreRemove
     */
    public function onPreRemove(LifecycleEventArgs $event)
    {
        $repository = $event->getEntityManager()->getRepository('App:CreditType');
        $types = $repository->getAllCompanyTypes();

        $event->getEntityManager()
            ->createQueryBuilder()
            ->delete('App:ClientCredit', 'cc')
            ->where('cc.client IN (SELECT c.id FROM App:Client c WHERE c.company = :company)')
            ->andWhere('cc.type IN (:types)')
            ->setParameter('company', $this)
            ->setParameter('types', $types)
            ->getQuery()
            ->execute();

        $event->getEntityManager()
            ->createQueryBuilder()
            ->update('App:User', 'u')
            ->set('u.roles', ':roles')
            ->where('u IN (SELECT IDENTITY(c.user) FROM App:Client c WHERE c.company = :company)')
            ->setParameter('roles', ['ROLE_CLIENT'])
            ->setParameter('company', $this)
            ->getQuery()
            ->execute();

        $event->getEntityManager()
            ->createQueryBuilder()
            ->update('App:Client', 'c')
            ->set('c.isCompanyContact', 'FALSE')
            ->where('c.company = :company')
            ->setParameter('company', $this)
            ->getQuery()
            ->execute();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getLogo()
    {
        return $this->logo;
    }

    public function setLogo($logo): self
    {
        $this->logo = $logo;

        return $this;
    }

    public function getWebsite()
    {
        return $this->website;
    }

    public function setWebsite($website): self
    {
        $this->website = $website;

        return $this;
    }

    public function getAddress1()
    {
        return $this->address1;
    }

    public function setAddress1(string $address1): self
    {
        $this->address1 = $address1;

        return $this;
    }

    public function getAddress2()
    {
        return $this->address2;
    }

    public function setAddress2($address2): self
    {
        $this->address2 = $address2;

        return $this;
    }

    public function getCity()
    {
        return $this->city;
    }

    public function setCity(string $city): self
    {
        $this->city = $city;

        return $this;
    }

    public function getCounty()
    {
        return $this->county;
    }

    public function setCounty($county): self
    {
        $this->county = $county;

        return $this;
    }

    public function getPostcode()
    {
        return $this->postcode;
    }

    public function setPostcode(string $postcode): self
    {
        $this->postcode = $postcode;

        return $this;
    }

    public function getCountry()
    {
        return $this->country;
    }

    public function setCountry(string $country): self
    {
        $this->country = $country;

        return $this;
    }

    public function getRegNumber()
    {
        return $this->regNumber;
    }

    public function setRegNumber($regNumber): self
    {
        $this->regNumber = $regNumber;

        return $this;
    }

    public function getVatRegNumber()
    {
        return $this->vatRegNumber;
    }

    public function setVatRegNumber($vatRegNumber): self
    {
        $this->vatRegNumber = $vatRegNumber;

        return $this;
    }

    public function getRegions(bool $serialized = false)
    {
        if (!$serialized) {
            return $this->regions;
        }

        return array_map(function (CompanyRegion $region) {
            return $region->getName();
        }, $this->regions->getValues());
    }

    public function getDepartments(bool $serialized = false)
    {
        if (!$serialized) {
            return $this->departments;
        }

        return array_map(function (CompanyDepartment $department) {
            return $department->getName();
        }, $this->departments->getValues());
    }

    public function getCoaches(bool $serialized = false)
    {
      if (!$serialized) {
        return $this->coaches;
      }

      return array_map(function (CoachCompany $coach) {
        return $coach->getCoach()->getId();
      }, $this->coaches->getValues());
    }

    public function getInvites(bool $serialized = false)
    {
        if (!$serialized) {
            return $this->invites;
        }

        return array_map(function (Invite $invite) {
            return $invite->getCompany()->getId();
        }, $this->invites->getValues());
    }

    public function getConsumptionCredit(bool $serialized = false)
    {
        if (!$serialized) {
            return $this->consumptionCredit;
        }

        return array_map(function (CompanyCreditConsumption $consumption) {
            return $consumption->getCreditCost();
        }, $this->consumptionCredit->getValues());
    }

    public function getPoNumber()
    {
        return $this->poNumber;
    }

    public function setPoNumber($poNumber): self
    {
        $this->poNumber = $poNumber;

        return $this;
    }

    public function getBrief()
    {
        return $this->brief;
    }

    public function setBrief($brief): self
    {
        $this->brief = $brief;

        return $this;
    }

    public function isEnabled(): bool
    {
        return $this->isEnabled;
    }

    public function setEnabled(bool $isEnabled): self
    {
        $this->isEnabled = $isEnabled;

        return $this;
    }

    public function getClients()
    {
        return $this->clients;
    }

    public function getCredits()
    {
        return $this->credits;
    }
    public function getTokRecord()
    {
    	return $this->tok_record;
    }
	public function setTokRecord(bool $tok_record): self
	{
		$this->tok_record = $tok_record;

		return $this;
	}

    public function getSessionLength()
    {
        return $this->sessionLength;
	}

    public function setSessionLength(array $sessionLength): self
    {
        $this->sessionLength = $sessionLength;

        return $this;
	}

    public function getAvailableSessionLength(): array
    {
        $sessionLength = [];
        $priceMapping = [
            '05' => 1, '1'  => 2,
            '15' => 3, '2'  => 4,
            '25' => 5, '3'  => 6,
            '35' => 7, '4'  => 8,
            '45' => 9, '5'  => 10,
            '55' => 11, '6'  => 12,
        ];

        if (!empty($this->sessionLength)) {
            foreach ($this->sessionLength as $key => $value) {
                if ($value && isset($priceMapping[substr($key, 7)])) {
                    $k = substr($key, 7);
                    $sessionLength[$k] = $priceMapping[$k];
                }
            }
        }

        return $sessionLength;
	}

}
