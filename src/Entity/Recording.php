<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="recordings")
 * @ORM\Entity(repositoryClass="App\Repository\RecordingRepository")
 */
class Recording
{
    use Traits\CreatedAtTrait;
    use Traits\UpdatedAtTrait;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer", options={"unsigned": true})
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var Session
     *
     * @ORM\ManyToOne(targetEntity="Session")
     * @ORM\JoinColumn(referencedColumnName="id", nullable=false, onDelete="CASCADE")
     */
    private $session;

    /**
     * @ORM\Column(type="guid")
     */
    private $archiveId;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $url;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $transcript;

    public function getId()
    {
        return $this->id;
    }

    public function getSession()
    {
        return $this->session;
    }

    public function setSession(Session $session): self
    {
        $this->session = $session;

        return $this;
    }

    public function getArchiveId()
    {
        return $this->archiveId;
    }

    public function setArchiveId(string $archiveId): self
    {
        $this->archiveId = $archiveId;

        return $this;
    }

    public function getUrl()
    {
        return $this->url;
    }

    public function setUrl($url): self
    {
        $this->url = $url;

        return $this;
    }

    public function getTranscript()
    {
        return $this->transcript;
    }

    public function setTranscript($transcript): self
    {
        $this->transcript = $transcript;

        return $this;
    }
}
