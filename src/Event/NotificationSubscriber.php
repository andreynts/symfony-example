<?php

namespace App\Event;

use App\Enum\NotificationChannelEnum;
use App\Utility\Mailer;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class NotificationSubscriber implements EventSubscriberInterface
{
    private $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function onNotification(NotificationEvent $event)
    {
        $em = $this->container->get('doctrine.orm.entity_manager');
        $notification = $em->getRepository('App:Notification')->findOneBy([
            'user' => $event->getUser(),
            'event' => $event->getEvent(),
        ]);
        if (null === $notification) {
            return;
        }

        $channels = $notification->getChannels();
        if (in_array(NotificationChannelEnum::EMAIL_PRIMARY, $channels)) {
            $this->sendEmail($event->getMessage(), $event->getPrimaryEmail());
        }
        if (in_array(NotificationChannelEnum::EMAIL_SECONDARY, $channels)) {
            $this->sendEmail($event->getMessage(), $event->getSecondaryEmail());
        }
        if (in_array(NotificationChannelEnum::PUSH, $channels)) {
            $this->sendPush($event->getShortText(), $event->getUser());
        }
        if (in_array(NotificationChannelEnum::SMS, $channels)) {
            $this->sendSms($event->getShortText(), $event->getMobileNumber());
        }
    }

    public static function getSubscribedEvents(): array
    {
        return [NotificationEvent::NAME => 'onNotification'];
    }

    private function sendEmail($message, $address)
    {
        if (null === $message || null === $address) {
            return;
        }

        $mailer = $this->container->get(Mailer::class);
        $mailer->send($message, $address);
    }

    private function sendPush($shortText, UserInterface $user)
    {
    }

    private function sendSms($shortText, $mobileNumber)
    {
    }
}
