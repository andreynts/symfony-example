<?php

namespace App\Event;

use App\Entity\User;
use App\Enum\NotificationEventEnum;
use Symfony\Component\EventDispatcher\Event;
use Symfony\Component\Security\Core\User\UserInterface;

class NotificationEvent extends Event
{
    const NAME = 'app.notification';

    private $user;
    private $event;
    private $message;
    private $primaryEmail;
    private $secondaryEmail;
    private $shortText;
    private $mobileNumber;

    public function __construct(UserInterface $user, string $event = NotificationEventEnum::DEFAULT_)
    {
        $this->user = $user;
        $this->event = $event;

        if ($user instanceof User) {
            $this->primaryEmail = $user->getEmail();
        }
    }

    public function getUser(): UserInterface
    {
        return $this->user;
    }

    public function getEvent(): string
    {
        return $this->event;
    }

    public function setMessage($message): self
    {
        $this->message = $message;

        return $this;
    }

    public function getMessage()
    {
        return $this->message;
    }

    public function setPrimaryEmail($primaryEmail): self
    {
        $this->primaryEmail = $primaryEmail;

        return $this;
    }

    public function getPrimaryEmail()
    {
        return $this->primaryEmail;
    }

    public function setSecondaryEmail($secondaryEmail): self
    {
        $this->secondaryEmail = $secondaryEmail;

        return $this;
    }

    public function getSecondaryEmail()
    {
        return $this->secondaryEmail;
    }

    public function setShortText(string $shortText): self
    {
        $this->shortText = $shortText;

        return $this;
    }

    public function getShortText()
    {
        return $this->shortText;
    }

    public function setMobileNumber($mobileNumber): self
    {
        $this->mobileNumber = $mobileNumber;

        return $this;
    }

    public function getMobileNumber()
    {
        return $this->mobileNumber;
    }
}
