<?php

namespace App\Event;

use App\Entity\RequestLog;
use App\Entity\User;
use App\Enum\ErrorEnum;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\Event\GetResponseForControllerResultEvent;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Serializer\Normalizer\DateTimeNormalizer;
use Symfony\Component\Stopwatch\Stopwatch;

class KernelSubscriber implements EventSubscriberInterface
{
    private $container;
    private $exception;
    private $stopwatch;
    private $log;

    public function __construct(ContainerInterface $container)
    {
        $stopwatch = new Stopwatch();
        $this->stopwatch = $stopwatch->start('default');

        $this->container = $container;
        $this->log = new RequestLog();
        $this->log->setCreatedAt(new \DateTime());
    }

    public function onEarlyKernelRequest(GetResponseEvent $event)
    {
        if (!$event->isMasterRequest()) {
            return;
        }

        $kernel = $this->container->get('kernel');
        if ('dev' === $kernel->getEnvironment()) {
            $request = $event->getRequest();
            if ($request->isMethod('OPTIONS')) {
                $response = new JsonResponse();
                $this->addCorsHeaders($request, $response);
                $event->setResponse($response);
            }
        }
    }

    public function onKernelRequest(GetResponseEvent $event)
    {
        if (!$event->isMasterRequest()) {
            return;
        }

        $request = $event->getRequest();
        if ('json' === $request->getContentType()) {
            $this->jsonToRequest($request);

            $data = $request->request->all();
            $this->secureDataKey($data, 'password');
            $this->log->setRequestBody($data);
        }

        if ($request->isMethod('GET')) {
            if ($callback = $request->query->get('callback')) {
                $request->attributes->set('callback', $callback);
                $request->query->remove('callback');
            }
        }
    }

    public function onKernelException(GetResponseForExceptionEvent $event)
    {
        $this->exception = $event->getException();
    }

    public function onKernelView(GetResponseForControllerResultEvent $event)
    {
        $request = $event->getRequest();
        $format = $request->getRequestFormat();

        $serializer = $this->container->get('serializer');
        $data = $event->getControllerResult();

        switch ($format) {
            case 'json':
                $normalized = $serializer->normalize($data, 'json');
                $response = new JsonResponse($normalized);
                if ($callback = $request->attributes->get('callback')) {
                    $response->setCallback($callback);
                }
                $this->log->setResponseBody($normalized);
                $event->setResponse($response);
                break;
            case 'csv':
                $reflectionObject = new \ReflectionObject($serializer);
                $reflectionProperty = $reflectionObject->getProperty('normalizers');
                $reflectionProperty->setAccessible(true);
                $reflectionProperty->setValue($serializer, array_map(function ($normalizer) {
                    if ($normalizer instanceof DateTimeNormalizer) {
                        $normalizer = new DateTimeNormalizer(\DateTime::RFC850);
                    }

                    return $normalizer;
                }, $reflectionProperty->getValue($serializer)));

                $data = $serializer->normalize($data, 'csv');
                $response = new Response($serializer->encode($data, 'csv'));
                $response->headers->set('Content-Type', 'text/csv');
                $event->setResponse($response);
        }
    }

    public function onKernelResponse(FilterResponseEvent $event)
    {
        $request = $event->getRequest();
        $response = $event->getResponse();

        $version = $this->container->getParameter('version');
        $response->headers->set('API-Version', $version);

        $kernel = $this->container->get('kernel');
        if ('dev' === $kernel->getEnvironment()) { // CORS
            $this->addCorsHeaders($event->getRequest(), $response);
        }

        if ($this->exception instanceof \Exception) {
            $code = $this->exception->getCode();
            if (!ErrorEnum::exists($code)) {
                $code = ErrorEnum::E_UNDEFINED;
            }
            $response->headers->set('API-Error', $code);

            $message = $this->exception->getMessage();
            if ($message && $kernel->isDebug()) {
                $message = preg_split('/\r\n|\n|\r/', $message, 2)[0];
                $response->headers->set('API-Debug', $message);
            }
        }

        $this->stopwatch->stop();

        $dispatcher = $this->container->get('event_dispatcher');
        $dispatcher->addListener(KernelEvents::TERMINATE, function () use ($request, $response) {
            try {
                $this->log->setMethod($request->getMethod());
                $this->log->setUri($request->getRequestUri());
                $this->log->setStatus($response->getStatusCode());
                $this->log->setRequestHeaders($request->headers->all());
                $this->log->setResponseHeaders($response->headers->all());

                if (null !== $token = $this->container->get('security.token_storage')->getToken()) {
                    if (($user = $token->getUser()) instanceof User) {
                        $this->log->setUser($user);
                    }
                }

                $this->log->setIp($request->getClientIp());
                $this->log->setDuration($this->stopwatch->getDuration());
                $this->log->setMemory($this->stopwatch->getMemory());

                $em = $this->container->get('doctrine.orm.entity_manager');
                $em->persist($this->log);
                $em->flush();
            } catch (\Exception $ex) {
                // we don't care
            }
        }, 1024);
    }

    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::REQUEST => [['onEarlyKernelRequest', 250], ['onKernelRequest']],
            KernelEvents::EXCEPTION => 'onKernelException',
            KernelEvents::VIEW => 'onKernelView',
            KernelEvents::RESPONSE => 'onKernelResponse',
        ];
    }

    private function jsonToRequest(Request $request): void
    {
        $content = $request->getContent();
        if (!$content) {
            return;
        }

        $data = json_decode($content, true);
        if (($error = JSON_ERROR_NONE !== json_last_error()) || !is_array($data)) {
            $message = $error ? json_last_error_msg() : 'Array or object is expected.';
            throw new BadRequestHttpException($message, null, ErrorEnum::E_BAD_REQUEST);
        }

        array_walk_recursive($data, function (&$value) {
            if (is_string($value)) {
                $value = trim($value);
            }
        });

        if (null !== $data) {
            $request->request->replace($data);
        }
    }

    private function secureDataKey(&$data, string $key)
    {
        if (is_array($data) && isset($data[$key])) {
            $data[$key] = '<secure>';
            array_walk($data, __METHOD__);
        }
    }

    private function addCorsHeaders(Request $request, Response $response): void
    {
        $response->headers->set('Access-Control-Allow-Origin', $request->headers->get('Origin', '*'));
        $response->headers->set('Access-Control-Allow-Headers', 'Origin, Content-Type, Accept, Authorization, Name');
        $response->headers->set('Access-Control-Allow-Methods', 'GET, PUT, PATCH, POST, DELETE, HEAD, OPTIONS');
        $response->headers->set('Access-Control-Expose-Headers', 'API-Version, API-Error');
        $response->headers->set('Access-Control-Allow-Credentials', 'true');
    }
}
