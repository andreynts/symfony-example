<?php

namespace App\Utility;

use App\Exception\ServiceUnavailableHttpException;
use Stripe\Charge;
use Stripe\Error;
use Stripe\Stripe;

class StripeClient extends Stripe implements UtilityInterface
{
    public function __construct()
    {
        self::setApiKey(getenv('STRIPE_SECRET_KEY'));
    }

    public function getCharge(string $id): Charge
    {
        try {
            return Charge::retrieve($id);
        } catch (Error\Base $ex) {
            throw new ServiceUnavailableHttpException($ex->getMessage(), $ex, $ex->getCode());
        }
    }

    public function charge(string $token, $amount, string $currency, string $description = '', array $metadata = []): Charge
    {
        if (is_string($amount)) {
            $amount = $amount * 100;
        }

        try {
            $charge = Charge::create([
                'source' => $token,
                'amount' => $amount,
                'currency' => $currency,
                'description' => $description,
                'metadata' => $metadata,
            ]);
        } catch (Error\Base $ex) {
            throw new ServiceUnavailableHttpException($ex->getMessage(), $ex, $ex->getCode());
        }

        return $charge;
    }
}
