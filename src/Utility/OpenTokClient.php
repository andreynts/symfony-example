<?php

namespace App\Utility;

use App\Exception\ServiceUnavailableHttpException;
use OpenTok\ArchiveMode;
use OpenTok\Exception\ArchiveDomainException;
use OpenTok\MediaMode;
use OpenTok\OpenTok;
use OpenTok\OutputMode;
use OpenTok\Role;

class OpenTokClient extends OpenTok implements UtilityInterface
{
    public function __construct()
    {
        parent::__construct(getenv('OPENTOK_API_KEY'), getenv('OPENTOK_API_SECRET'));
    }

    public function createSession($options = [])
    {
        try {
            return parent::createSession($options + [
                'mediaMode' => MediaMode::ROUTED,
                'archiveMode' => ArchiveMode::MANUAL,
            ]);
        } catch (\Exception $ex) {
            throw new ServiceUnavailableHttpException($ex->getMessage(), $ex, $ex->getCode());
        }
    }

    public function generateToken($sessionId, $options = [])
    {
        try {
            return parent::generateToken($sessionId, $options + [
                'role' => Role::PUBLISHER,
            ]);
        } catch (\Exception $ex) {
            throw new ServiceUnavailableHttpException($ex->getMessage(), $ex, $ex->getCode());
        }
    }

    public function startArchive($sessionId, $options = [])
    {
        try {
            return parent::startArchive($sessionId, $options + [
                'hasAudio' => true,
                'hasVideo' => false,
                'outputMode' => OutputMode::INDIVIDUAL,
            ]);
        } catch (ArchiveDomainException $ex) {
            return null;
        } catch (\Exception $ex) {
            throw new ServiceUnavailableHttpException($ex->getMessage(), $ex, $ex->getCode());
        }
    }

    public function stopArchive($archiveId)
    {
        try {
            return parent::stopArchive($archiveId);
        } catch (\Exception $ex) {
            throw new ServiceUnavailableHttpException($ex->getMessage(), $ex, $ex->getCode());
        }
    }
}
