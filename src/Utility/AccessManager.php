<?php

namespace App\Utility;

use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Core\Authorization\AccessDecisionManagerInterface;
use Symfony\Component\Security\Core\User\UserInterface;

class AccessManager implements UtilityInterface
{
    private $decisionManager;

    public function __construct(AccessDecisionManagerInterface $decisionManager)
    {
        $this->decisionManager = $decisionManager;
    }

    public static function createToken(UserInterface $user): TokenInterface
    {
        return new UsernamePasswordToken($user, 'none', 'none', $user->getRoles());
    }

    public function isGranted(TokenInterface $token, $attributes, $object = null)
    {
        if (!is_array($attributes)) {
            $attributes = [$attributes];
        }

        return $this->decisionManager->decide($token, $attributes, $object);
    }
}
