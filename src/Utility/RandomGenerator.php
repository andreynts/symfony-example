<?php

namespace App\Utility;

class RandomGenerator
{
    const ALPHABET = '0123456789ABCDEFGHIJKLMNOPQRSTUVWZYZabcdefghijklmnopqrstuvwxyz';

    public static function randomString(int $length, string $alphabet = self::ALPHABET): string
    {
        if ($length < 1) {
            throw new \InvalidArgumentException('Length must be a positive integer.');
        }

        $max = strlen($alphabet) - 1;
        if ($max < 1) {
            throw new \InvalidArgumentException('Invalid alphabet.');
        }

        $str = '';
        for ($i = 0; $i < $length; ++$i) {
            $str .= $alphabet[random_int(0, $max)];
        }

        return $str;
    }
}
