<?php

namespace App\Utility;

use App\Enum\ErrorEnum;
use App\Validator\Constraints as AppAssert;
use Symfony\Component\HttpFoundation\ParameterBag;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class RequestValidator implements UtilityInterface
{
    const CODEMAP = [
        Assert\Choice::NO_SUCH_CHOICE_ERROR => ErrorEnum::E_CHOICE_INVALID,
        Assert\Collection::MISSING_FIELD_ERROR => ErrorEnum::E_COLLECTION_MISSING_FIELD,
        Assert\Collection::NO_SUCH_FIELD_ERROR => ErrorEnum::E_COLLECTION_NO_SUCH_FIELD,
        Assert\Count::TOO_FEW_ERROR => ErrorEnum::E_COUNT_TOO_FEW,
        Assert\Count::TOO_MANY_ERROR => ErrorEnum::E_COUNT_TOO_MANY,
        Assert\Country::NO_SUCH_COUNTRY_ERROR => ErrorEnum::E_COUNTRY_INVALID,
        Assert\Date::INVALID_DATE_ERROR => ErrorEnum::E_DATE_INVALID,
        Assert\Date::INVALID_FORMAT_ERROR => ErrorEnum::E_DATE_INVALID_FORMAT,
        Assert\DateTime::INVALID_FORMAT_ERROR => ErrorEnum::E_DATETIME_INVALID_FORMAT,
        Assert\DateTime::INVALID_DATE_ERROR => ErrorEnum::E_DATETIME_INVALID_DATE,
        Assert\DateTime::INVALID_TIME_ERROR => ErrorEnum::E_DATETIME_INVALID_TIME,
        Assert\Email::INVALID_FORMAT_ERROR => ErrorEnum::E_EMAIL_INVALID_FORMAT,
        Assert\Email::MX_CHECK_FAILED_ERROR => ErrorEnum::E_EMAIL_MX_CHECK_FAILED,
        Assert\Email::HOST_CHECK_FAILED_ERROR => ErrorEnum::E_EMAIL_HOST_CHECK_FAILED,
        Assert\EqualTo::NOT_EQUAL_ERROR => ErrorEnum::E_EQUALTO_NOT_EQUAL,
        Assert\Expression::EXPRESSION_FAILED_ERROR => ErrorEnum::E_EXPRESSION_FAILED,
        Assert\File::EMPTY_ERROR => ErrorEnum::E_FILE_EMPTY,
        Assert\File::TOO_LARGE_ERROR => ErrorEnum::E_FILE_TOO_LARGE,
        Assert\File::INVALID_MIME_TYPE_ERROR => ErrorEnum::E_FILE_MIME_TYPE_INVALID,
        Assert\GreaterThan::TOO_LOW_ERROR => ErrorEnum::E_GREATERTHAN_TOO_LOW,
        Assert\GreaterThanOrEqual::TOO_LOW_ERROR => ErrorEnum::E_GREATERTHANOREQUAL_TOO_LOW,
        Assert\IdenticalTo::NOT_IDENTICAL_ERROR => ErrorEnum::E_IDENTICALTO_NOT_IDENTICAL,
        Assert\Image::SIZE_NOT_DETECTED_ERROR => ErrorEnum::E_IMAGE_SIZE_NOT_DETECTED,
        Assert\Image::TOO_WIDE_ERROR => ErrorEnum::E_IMAGE_TOO_WIDE,
        Assert\Image::TOO_NARROW_ERROR => ErrorEnum::E_IMAGE_TOO_NARROW,
        Assert\Image::TOO_HIGH_ERROR => ErrorEnum::E_IMAGE_TOO_HIGH,
        Assert\Image::TOO_LOW_ERROR => ErrorEnum::E_IMAGE_TOO_LOW,
        Assert\Image::CORRUPTED_IMAGE_ERROR => ErrorEnum::E_IMAGE_CORRUPTED,
        Assert\IsFalse::NOT_FALSE_ERROR => ErrorEnum::E_ISFALSE_NOT_FALSE,
        Assert\IsNull::NOT_NULL_ERROR => ErrorEnum::E_ISNULL_NOT_NULL,
        Assert\IsTrue::NOT_TRUE_ERROR => ErrorEnum::E_ISTRUE_NOT_TRUE,
        Assert\Language::NO_SUCH_LANGUAGE_ERROR => ErrorEnum::E_LANGUAGE_INVALID,
        Assert\Length::TOO_SHORT_ERROR => ErrorEnum::E_LENGTH_TOO_SHORT,
        Assert\Length::TOO_LONG_ERROR => ErrorEnum::E_LENGTH_TOO_LONG,
        Assert\Length::INVALID_CHARACTERS_ERROR => ErrorEnum::E_LENGTH_INVALID_CHARACTERS,
        Assert\LessThan::TOO_HIGH_ERROR => ErrorEnum::E_LESSTHAN_TOO_HIGH,
        Assert\LessThanOrEqual::TOO_HIGH_ERROR => ErrorEnum::E_LESSTHANOREQUAL_TOO_HIGH,
        Assert\Locale::NO_SUCH_LOCALE_ERROR => ErrorEnum::E_LOCALE_INVALID,
        Assert\NotBlank::IS_BLANK_ERROR => ErrorEnum::E_NOTBLANK_IS_BLANK,
        Assert\NotEqualTo::IS_EQUAL_ERROR => ErrorEnum::E_NOTEQUALTO_IS_EQUAL,
        Assert\NotIdenticalTo::IS_IDENTICAL_ERROR => ErrorEnum::E_NOTIDENTICALTO_IS_IDENTICAL,
        Assert\NotNull::IS_NULL_ERROR => ErrorEnum::E_NOTNULL_IS_NULL,
        Assert\Range::INVALID_CHARACTERS_ERROR => ErrorEnum::E_RANGE_INVALID_CHARACTERS,
        Assert\Range::TOO_HIGH_ERROR => ErrorEnum::E_RANGE_TOO_HIGH,
        Assert\Range::TOO_LOW_ERROR => ErrorEnum::E_RANGE_TOO_LOW,
        Assert\Regex::REGEX_FAILED_ERROR => ErrorEnum::E_REGEX_FAILED,
        Assert\Time::INVALID_FORMAT_ERROR => ErrorEnum::E_TIME_INVALID_FORMAT,
        Assert\Time::INVALID_TIME_ERROR => ErrorEnum::E_TIME_INVALID,
        Assert\Type::INVALID_TYPE_ERROR => ErrorEnum::E_TYPE_INVALID,
        Assert\Url::INVALID_URL_ERROR => ErrorEnum::E_URL_INVALID,
        AppAssert\Boolean::NOT_BOOLEAN_ERROR => ErrorEnum::E_BOOLEAN_NOT_BOOLEAN,
        AppAssert\Exists::VALUE_REQUIRED_ERROR => ErrorEnum::E_EXISTS_VALUE_REQUIRED,
        AppAssert\Unique::NOT_UNIQUE_ERROR => ErrorEnum::E_UNIQUE_DUPLICATES,
        AppAssert\Url::HOSTNAME_INVALID_ERROR => ErrorEnum::E_URL_HOSTNAME_INVALID,
        AppAssert\Url::CONTENT_TYPE_INVALID_ERROR => ErrorEnum::E_URL_CONTENT_TYPE_INVALID,
    ];

    private $validator;

    public function __construct(ValidatorInterface $validator)
    {
        $this->validator = $validator;
    }

    public function validate($input, $constraints = null, $groups = null)
    {
        if ($input instanceof ParameterBag) {
            $input = $input->all();
        }

        $errors = $this->validator->validate($input, $constraints, $groups);
        if (0 !== count($errors)) {
            $error = $errors[0];

            $message = $error->getMessage();
            if ($path = $error->getPropertyPath()) {
                $message = $path.' '.$message;
            }

            $code = $error->getCode();
            if (!is_numeric($code)) {
                $code = self::CODEMAP[$error->getCode()] ?? 0;
            }
            throw new BadRequestHttpException($message, null, $code ?: ErrorEnum::E_BAD_REQUEST);
        }
    }
}
