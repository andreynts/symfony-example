<?php

namespace App\Utility;

use App\Entity\MailLog;
use App\Entity\Session;
use App\Entity\StyleType;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\DomCrawler\Crawler;
use Symfony\Component\Routing\Generator\UrlGenerator;

class Mailer implements UtilityInterface
{
    private $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function create(string $view, array $parameters = []): \Swift_Message
    {
        array_walk_recursive($parameters, function (&$value) {
            if ($value instanceof \DateTimeInterface) {
                $value = $value->format(\DateTime::RFC850);
            }
        });

        $encoder = $this->container->get('lexik_jwt_authentication.encoder');
        $parameters['_vb_token'] = $encoder->encode([
            'exp' => (new \DateTime('+1 year'))->getTimestamp(),
            '$view' => $view,
            '$params' => $parameters,
        ]);

        $body = $this->twigRenderMessage($view, $parameters);

        $crawler = new Crawler($body);
        $subject = $crawler->filterXPath('descendant-or-self::head/title')->first()->text();

        return (new \Swift_Message($subject, $body, 'text/html'))
            ->setFrom(getenv('MAILER_FROM'));
    }

    public function twigRenderMessage(string $view, array $parameters = [])
    {
        $templatesType = ['GENERAL_TEMPLATE'];

        array_walk_recursive($parameters, function (&$value) {
            if ($value instanceof \DateTimeInterface) {
                $value = $value->format(\DateTime::RFC850);
            }
        });

        $encoder = $this->container->get('lexik_jwt_authentication.encoder');
        $em = $this->container->get('doctrine.orm.entity_manager');

        $parameters['_vb_token'] = $encoder->encode([
            'exp' => (new \DateTime('+1 year'))->getTimestamp(),
            '$view' => $view,
            '$params' => $parameters,
        ]);

        foreach ($templatesType as $templateType) {
            if ($templateType !== null) {
                $em = $this->container->get('doctrine.orm.entity_manager');
                $templateParameteres = $em->createQueryBuilder()
                    ->select(
                        'p.id',
                        'p.name',
                        'p.value',
                        't.email_template_name'
                    )
                    ->from('App:EmailTemplateParameter', 'p')
                    ->join('p.emailTemplateType', 't')
                    ->where('t.email_template_name = :param_name')
                    ->setParameter('param_name', $templateType)
                    ->getQuery()
                    ->getResult();
                foreach ($templateParameteres as $param) {
                    $parameters[$param['name']] = $param['value'];
                }
            }
        }
        $emailsStyles = $em->createQueryBuilder()
            ->select('
                s.name,
                st.type_name,
                s.value')
            ->from('App:Style', 's')
            ->leftJoin('App:StyleType', 'st', 'WITH', 's.styleType=st')
            ->where('s.name = \'primary_color\'')
            ->orWhere('s.name = \'primary_color_light\'')
            ->orWhere('s.name = \'email_logo\'')
            ->orWhere('s.name = \'email_header_image\'')
            ->getQuery()
            ->getResult();

        foreach ($emailsStyles as $style) {
            if ($style['name'] === 'email_header_image' && $style['type_name'] === 'color') {
                $parameters['email_header_color'] = $style['value'];
            } else {
                $parameters[$style['name']] = $style['value'];
            }
        }
        var_dump($parameters);
        $twig = $this->container->get('twig');
        $body = $twig->render('mail/' . $view, $parameters);
        return $body;
    }

    public function send(\Swift_Message $message, $addresses): int
    {
        $message->setTo($addresses);

        $mailer = $this->container->get('mailer');
        $result = $mailer->send($message);

        try {
            $appEnv = getenv('APP_ENV');
            if ($appEnv !== 'dev') {
                $log = new MailLog();
                $log->setTo(array_keys($message->getTo()));
                $log->setFrom(array_keys($message->getFrom()));
                $log->setSubject($message->getSubject());
                $log->setBody($message->getBody());
                $log->setResult($result);

                $this->container->get('doctrine')->resetEntityManager('doctrine.orm.entity_manager');
                $em = $this->container->get('doctrine.orm.entity_manager');
                $em->persist($log);
                $em->flush();
            }
        } catch (\Exception $ex) {
            // we don't care
        }

        return $result;
    }

    public function sendTo($addresses, string $view, array $parameters = []): int
    {
        return $this->send($this->create($view, $parameters), $addresses);
    }

    public function sendToAdmin(string $view, array $parameters = []): int
    {
        $em = $this->container->get('doctrine.orm.entity_manager');
        $addresses = $em->createQueryBuilder()
            ->select('u.username', "CONCAT(u.firstName, ' ', u.lastName)")
            ->from('App:User', 'u')
            ->where('u.isEnabled = TRUE')
            ->andWhere("'ROLE_ADMIN' = ANY_OF(STRING_TO_ARRAY(u.roles))")
            ->getQuery()
            ->getResult('HYDRATE_KEY_PAIR');

        return $this->send($this->create($view, $parameters), $addresses);
    }

    public function sendToCompanyAdmin($company, string $view, array $parameters = []): int
    {
        $em = $this->container->get('doctrine.orm.entity_manager');
        $addresses = $em->createQueryBuilder()
            ->select('u.username', "CONCAT(u.firstName, ' ', u.lastName)")
            ->from('App:Client', 'c')
            ->join('c.user', 'u', 'WITH', 'u.isEnabled = TRUE')
            ->where('c.company = :company')
            ->andWhere("'ROLE_ENTERPRISE_ADMIN' = ANY_OF(STRING_TO_ARRAY(u.roles))")
            ->setParameter('company', $company)
            ->getQuery()
            ->getResult('HYDRATE_KEY_PAIR');

        return $this->send($this->create($view, $parameters), $addresses);
    }

    public function getSessionIcsUrl(Session $session)
    {
        return $this->container->get('router')->generate('mail', [
            'token' => $this->container->get('lexik_jwt_authentication.encoder')->encode([
                'exp' => (new \DateTime('+1 year'))->getTimestamp(),
                '$view' => '@ics',
                '$params' => [
                    IcsGenerator::SUMMARY => 'MyThrive Session',
                    IcsGenerator::DESCRIPTION => $session->getExpertise()->getName(),
                    IcsGenerator::DTSTART => '@' . $session->getStartedAt()->getTimestamp(),
                    IcsGenerator::DTEND => '@' . $session->getEndedAt()->getTimestamp(),
                    IcsGenerator::ORGANIZER => [getenv('MAILER_FROM'), 'MyThrive'],
                    IcsGenerator::URL => 'https://' . getenv('APP_HOST'),
                    IcsGenerator::LOCATION => 'Online',
                    IcsGenerator::ALARM => '15M',
                ],
            ]),
        ], UrlGenerator::ABSOLUTE_URL);
    }
}
