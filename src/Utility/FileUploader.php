<?php

namespace App\Utility;

use App\Entity\Upload;
use App\Entity\User;
use App\Exception\ServiceUnavailableHttpException;
use Aws\S3\Exception\S3Exception;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\File\MimeType\ExtensionGuesser;
use Symfony\Component\HttpFoundation\File\MimeType\MimeTypeGuesser;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;

class FileUploader implements UtilityInterface
{
    private $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function createFileFromRequest(Request $request, string $extension = null, bool $forceSecureName = false)
    {
        $tmpPath = tempnam(sys_get_temp_dir(), '');
        file_put_contents($tmpPath, $request->getContent(true));
        register_shutdown_function(function () use ($tmpPath) {
            unlink($tmpPath);
        });

        $mimeType = MimeTypeGuesser::getInstance()->guess($tmpPath);
        if (null === $mimeType) {
            $mimeType = $request->headers->get('Content-Type', '');
            if (false !== $pos = mb_strpos($mimeType, ';')) {
                $mimeType = mb_substr($mimeType, 0, $pos);
            }
        }

        $headerName = $request->headers->get('Name', '');
        $name = preg_replace("/[^\w!.*'()-]/", '', $headerName);
        if (null === $extension) {
            $extension = strtolower(pathinfo($name, PATHINFO_EXTENSION))
                ?: ExtensionGuesser::getInstance()->guess($mimeType) ?: 'dat';
        }

        $filename = pathinfo($name, PATHINFO_FILENAME);
        if (!$filename || $forceSecureName) {
            $filename = RandomGenerator::randomString(8);
        }

        $basename = $filename.'.'.$extension;
        $fileSize = filesize($tmpPath);

        return new UploadedFile($tmpPath, $basename, $mimeType, $fileSize, UPLOAD_ERR_OK, true);
    }

    public function storeContents(string $contents, string $filename, string $contentType)
    {
        $url = $this->store($this->createKey($filename), $contents, $contentType);

        $em = $this->container->get('doctrine.orm.entity_manager');
        $em->persist($this->createUpload($url, $contentType, strlen($contents)));
        $em->flush();

        return $url;
    }

    public function storeFile(UploadedFile $file): string
    {
        $handle = fopen($file->getPathname(), 'rb');
        $key = $this->createKey($file->getClientOriginalName());
        $url = $this->store($key, $handle, $file->getClientMimeType());
        fclose($handle);

        $em = $this->container->get('doctrine.orm.entity_manager');
        $em->persist($this->createUpload($url, $file->getClientMimeType(), $file->getClientSize()));
        $em->flush();

        return $url;
    }

    public function storeImage(UploadedFile &$file, int $width, int $height, string $format = 'jpeg'): string
    {
        $path = $file->getPathname();

        $im = new \Imagick($path);
        $im->setInterlaceScheme(\Imagick::INTERLACE_PLANE);
        $im->setCompressionQuality(90);
        $im->setImageFormat($format);
        $im->stripImage();

        $em = $this->container->get('doctrine.orm.entity_manager');
        $mimeType = $im->getImageMimeType();
        $filename = $file->getClientOriginalName();

        for ($i = 3; $i >= 1; --$i) {
            $imX = clone $im;
            $imX->cropThumbnailImage($width * $i, $height * $i);
            $key = $this->createKey($filename, 1 !== $i ? '@'.$i.'x' : null);
            $url = $this->store($key, $imX->getImageBlob(), $mimeType);
            $fileSize = $imX->getImageLength();
            $imX->clear();

            $em->persist($this->createUpload($url, $mimeType, $fileSize));
        }

        $file = new UploadedFile($path, $filename, $mimeType, $fileSize, UPLOAD_ERR_OK, true);
        $em->flush();

        return $url;
    }

    private function store(string $key, $body, string $contentType = null): string
    {
        try {
            return $this->container->get('aws.s3')->putObject([
                'Bucket' => getenv('AWS_S3_BUCKET'),
                'Key' => $key,
                'Body' => $body,
                'ACL' => 'public-read',
                'ContentType' => $contentType,
            ])['ObjectURL'];
        } catch (S3Exception $ex) {
            throw new ServiceUnavailableHttpException($ex->getAwsErrorMessage(), $ex, $ex->getAwsErrorCode());
        }
    }

    private function createKey(string $filename, string $suffix = null): string
    {
        static $yzB = null;
        if (null === $yzB) {
            $yzB = dechex(date('yzB')).'/';
            $yzB .= hash('adler32', random_bytes(16));
        }

        if ($suffix) {
            $extension = pathinfo($filename, PATHINFO_EXTENSION);
            $filename = pathinfo($filename, PATHINFO_FILENAME).$suffix.'.'.$extension;
        }

        return sprintf('%s/static/%s/%s', getenv('APP_PLATFORM'), $yzB, $filename);
    }

    private function createUpload(string $path, string $mimeType, int $size): Upload
    {
        $upload = new Upload();
        $upload->setPath($path);
        $upload->setMimeType($mimeType);
        $upload->setSize($size);

        if (null !== $token = $this->container->get('security.token_storage')->getToken()) {
            if (($user = $token->getUser()) instanceof User) {
                $upload->setUser($user);
            }
        }

        $request = $this->container->get('request_stack')->getCurrentRequest();
        if (null !== $request) {
            $upload->setIp($request->getClientIp());
        }

        return $upload;
    }
}
