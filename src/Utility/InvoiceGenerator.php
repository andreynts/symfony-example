<?php

namespace App\Utility;

use App\Entity\Payment;
use Stripe\Charge;
use Symfony\Component\DependencyInjection\ContainerInterface;

class InvoiceGenerator implements UtilityInterface
{
    const CONTENT_TYPE = 'application/pdf';

    private $container;

    public function __construct(ContainerInterface $container)
    {
        define('K_PATH_IMAGES', $container->get('kernel')->getProjectDir());

        $this->container = $container;
    }

    public function generate(Payment $payment, Charge $charge, callable $callback = null)
    {
        $invoiceNumber = str_pad($payment->getId(), Payment::NUMBER_PADDING, '0', STR_PAD_LEFT);
        $date = (new \DateTime())->format('d/m/Y');
        $title = sprintf('Order #%s', $invoiceNumber);
        $subject = 'MyThrive Invoice';

        $pdf = new \TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
        $pdf->SetCreator('XZ1X0SN1KCPH');
        $pdf->SetAuthor('MyThrive');
        $pdf->SetTitle($title);
        $pdf->SetSubject($subject);

        $pdf->setHeaderData('/templates/invoice/logo.png', 40, $subject.' - '.$date, $title);
        $pdf->setHeaderFont([PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN]);
        $pdf->setFooterFont([PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA]);

        $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER * 3);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER * 1.5);

        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
        $pdf->SetAutoPageBreak(true, PDF_MARGIN_BOTTOM);
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
        $pdf->setFontSubsetting(true);

        $pdf->AddPage();

        $mailer = $this->container->get(Mailer::class);
        $view = $mailer->twigRenderMessage('invoice/invoice.html.twig', [
            'payment' => $payment,
            'charge' => $charge,
        ]);
        $pdf->writeHTMLCell(0, 0, '', '', $view, 0, 1);

        $buffer = $pdf->Output(null, 'S');
        $filename = sprintf('INV%s.pdf', $invoiceNumber);

        if (null !== $callback) {
            $callback($buffer, $filename, self::CONTENT_TYPE);
        }

        $uploader = $this->container->get(FileUploader::class);

        return $uploader->storeContents($buffer, $filename, self::CONTENT_TYPE);
    }
}
