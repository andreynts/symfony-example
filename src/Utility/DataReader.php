<?php

namespace App\Utility;

use Symfony\Component\HttpKernel\KernelInterface;

class DataReader implements UtilityInterface
{
    private $kernel;
    private $root;

    public function __construct(KernelInterface $kernel)
    {
        $this->kernel = $kernel;
        $this->root = $kernel->getRootDir().'/..';
    }

    public function read(string $path)
    {
        $filename = $this->root.'/data/'.$path;
        if (!is_file($filename)) {
            throw new \RuntimeException(sprintf(
                'The data resource "%s" cannot be found.',
                $filename
            ));
        }

        $data = json_decode(file_get_contents($filename), true);
        if (null === $data) {
            throw new \RuntimeException(sprintf(
                'The data response "%s" contains invalid JSON: %s',
                $filename,
                json_last_error_msg()
            ));
        }

        return $data;
    }
}
