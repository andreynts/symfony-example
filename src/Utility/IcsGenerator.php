<?php

namespace App\Utility;

/**
 * @author Alexander Lokhman <alex.lokhman@gmail.com>
 */
class IcsGenerator implements \ArrayAccess
{
    const ALARM = 'ALARM';
    const DESCRIPTION = 'DESCRIPTION';
    const DTEND = 'DTEND';
    const DTSTART = 'DTSTART';
    const LOCATION = 'LOCATION';
    const ORGANIZER = 'ORGANIZER';
    const SUMMARY = 'SUMMARY';
    const URL = 'URL';

    const ICS_PROPERTIES = [
        self::ALARM,
        self::DESCRIPTION,
        self::DTEND,
        self::DTSTART,
        self::LOCATION,
        self::ORGANIZER,
        self::SUMMARY,
        self::URL,
    ];

    private $properties = [];
    private $timezone;

    public function __construct(array $parameters = [], string $timezone = 'UTC')
    {
        foreach ($parameters as $key => $value) {
            $this->offsetSet($key, $value);
        }
        $this->timezone = $timezone;
    }

    public function __toString(): string
    {
        $output = [
            'BEGIN:VCALENDAR',
            'VERSION:2.0',
            'PRODID:-//hacksw/handcal//NONSGML v1.0//EN',
            'CALSCALE:GREGORIAN',
            'BEGIN:VEVENT',
        ];

        $output[] = 'UID:'.uniqid();
        $output[] = 'DTSTAMP:'.$this->formatDate('now', true);

        if (isset($this->properties[self::ALARM])) {
            $output[] = 'BEGIN:VALARM';
            $output[] = 'TRIGGER:-PT'.$this->properties[self::ALARM];
            $output[] = 'ACTION:DISPLAY';
            $output[] = 'END:VALARM';
            unset($this->properties[self::ALARM]);
        }

        foreach ($this->properties as $key => $value) {
            switch ($key) {
                case self::DTSTART:
                case self::DTEND:
                    $key .= ';TZID='.$this->timezone;
                    break;
                case self::ORGANIZER:
                    if (isset($value[1])) {
                        $key .= ';CN='.$value[1];
                    }
                    $value = 'MAILTO:'.$value[0];
                    break;
                case self::URL:
                    $key .= ';VALUE=URI';
            }
            $output[] = $key.':'.$value;
        }

        $output[] = 'END:VEVENT';
        $output[] = 'END:VCALENDAR';

        return implode("\r\n", $output);
    }

    public function offsetExists($offset): bool
    {
        return isset($this->properties[$offset]);
    }

    public function offsetGet($offset)
    {
        return $this->properties[$offset];
    }

    public function offsetSet($offset, $value): void
    {
        if (null === $value || !in_array($offset, self::ICS_PROPERTIES)) {
            return;
        }

        switch ($offset) {
            case self::DTSTART:
            case self::DTEND:
                $value = $this->formatDate($value);
                break;
            case self::ORGANIZER:
                $value = (array) $value;
                break;
            default:
                $value = preg_replace('/([\,;])/', '\\\$1', $value);
        }
        $this->properties[$offset] = $value;
    }

    public function offsetUnset($offset): void
    {
        unset($this->properties[$offset]);
    }

    public function setTimezone($timezone): void
    {
        $this->timezone = $timezone;
    }

    private function formatDate($date, bool $utc = false): string
    {
        $dt = new \DateTime($date, new \DateTimeZone($this->timezone));

        return $dt->format('Ymd\THis'.($utc ? '\Z' : ''));
    }
}
