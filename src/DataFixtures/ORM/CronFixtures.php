<?php

namespace App\DataFixtures\ORM;

use Cron\CronBundle\Entity\CronJob;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class CronFixtures extends Fixture
{
    const DATA = [[
        'name' => 'reminder',
        'command' => 'app:reminder',
        'schedule' => '* * * * *',
        'description' => 'Send reminder notifications',
    ], [
        'name' => 'session-expire',
        'command' => 'app:session:expire',
        'schedule' => '* * * * *',
        'description' => 'Expire outdated sessions',
    ], [
        'name' => 'credit-tracker',
        'command' => 'app:credit:tracker',
        'schedule' => '0 0 * * *',
        'description' => 'Manage company and client credits',
    ], [
        'name' => 'invoice-generator',
        'command' => 'app:invoice:generate',
        'schedule' => '0 * * * *',
        'description' => 'Generate missing invoices',
    ], [
        'name' => 'token-generator',
        'command' => 'app:token:generate',
        'schedule' => '*/5 * * * *',
        'description' => 'Generate missing session tokens',
    ], [
        'name' => 'log-clear',
        'command' => 'app:log:clear --interval="1 day"',
        'schedule' => '0 0 * * *',
        'description' => 'Clear outdated logs',
    ], [
        'name' => 'sync-client-profile',
        'command' => 'app:sync:clientProfile',
        'schedule' => '0 0 * * *',
        'description' => 'Update region and department for client schm'
    ]];

    public function load(ObjectManager $manager)
    {
        foreach (self::DATA as $data) {
            $job = new CronJob();
            $job->setName($data['name']);
            $job->setCommand($data['command']);
            $job->setSchedule($data['schedule']);
            $job->setDescription($data['description']);
            $job->setEnabled(true);
            $manager->persist($job);
        }
        $manager->flush();
    }
}
