<?php

namespace App\DataFixtures\ORM;

use App\Entity\Coach;
use App\Entity\CoachCareerCompany;
use App\Entity\CoachCareerIndustry;
use App\Entity\CoachCoachedCompany;
use App\Entity\CoachCoachedPeople;
use App\Entity\CoachExpertise;
use App\Entity\CoachQualification;
use App\Entity\CoachTechExpertise;
use App\Entity\User;
use App\Enum\ExpertiseLevelEnum;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class CoachFixtures extends Fixture
{
    const DATA = [
        'coached_people' => [
            'Board Director',
            'Senior Manager',
            'Manager',
        ],
        'coached_companies' => [
            'Local businesses near my home town/city',
            'Regional businesses',
            'Businesses with a presence across the UK',
            'FTSE100 businesses',
            'Public sector',
            'Private sector',
        ],
        'career_companies' => [
            'Local businesses near my home town/city',
            'Businesses with a presence across the UK',
            'Public sector',
        ],
        'career_industries' => [
            'Defence & Security',
            'Professional Services',
            'Accountancy',
        ],
        'industries' => [
            'Automotive',
            'Defence & Security',
            'Engineering & Construction',
            'Financial Services - Private',
        ],
        'expertise' => [
            'Feeling stuck, stressed or unsure' => ExpertiseLevelEnum::CAN_COACH,
            'Being an engaging leader' => ExpertiseLevelEnum::CAN_COACH,
            'Building or fixing relationships' => ExpertiseLevelEnum::CAN_COACH,
            'Attracting and hiring the right people' => ExpertiseLevelEnum::CAN_COACH,
            'General coaching' => ExpertiseLevelEnum::EXPERT,
        ],
    ];

    public function load(ObjectManager $manager)
    {
        $encoder = $this->container->get('security.password_encoder');

        $industryList = $manager->getRepository('App:Industry')->findByName(self::DATA['industries']);
        $expertiseList = $manager->getRepository('App:Expertise')->findByName(array_keys(self::DATA['expertise']));

        foreach (range(1, 50) as $i) {
            $user = new User();
            $user->setUsername(sprintf('coach%d@thrivepartners.co.uk', $i));
            $user->setPassword($encoder->encodePassword($user, 'Thriving!23'));
            $user->setRoles(['ROLE_COACH']);
            $user->setFirstName('Training');
            $user->setLastName('Coach '.$i);
            $user->setVerified(true);
            $manager->persist($user);

            $coach = new Coach();
            $coach->setUser($user);
            $coach->setMobileNumber('+447700000000');
            $coach->setLanguages(['en_GB', 'de', 'de_AT']);
            $coach->setAgeRange('26-30');
            $coach->setGender('Female');
            $coach->setCountry('GB');
            $coach->setNationality('British');
            $coach->setAddress1('136 High Holborn');
            $coach->setCity('London');
            $coach->setCounty('Greater London');
            $coach->setPostcode('WC1V 6PX');
            $coach->setEmploymentStatus('Self-employed');
            $coach->setCompanyType('Thrive Partners Ltd');
            $coach->setCompanyIndemnityProvider('Hiscox');
            $coach->setCompanyIndemnityCover('£1 Million');

            $qualification = new CoachQualification();
            $qualification->setCoach($coach);
            $qualification->setName('Level 7 Masters Certificate in Coaching');
            $qualification->setAwardedAt(new \DateTime('2004-04-01'));
            $coach->getQualifications()->add($qualification);

            $coach->setOtherRelevantQualifications('Level 5 Diploma in Management & Leadership');
            $coach->setOtherQualifications('FCIPD');

            $techExpertise = new CoachTechExpertise();
            $techExpertise->setCoach($coach);
            $techExpertise->setName('Microsoft Office 2010+');
            $coach->getTechExpertise()->add($techExpertise);

            $coach->setOtherSeniorPosition('Senior Manager');
            $coach->setCorporateWeekTime(20);
            $coach->setCareerSmeTime(0);
            $coach->setCareerCorporateTime(100);
            $coach->setCareerCorporateRoles('Coach');

            foreach (self::DATA['career_companies'] as $name) {
                $careerCompany = new CoachCareerCompany();
                $careerCompany->setCoach($coach);
                $careerCompany->setName($name);
                $coach->getCareerCompanies()->add($careerCompany);
            }

            foreach (self::DATA['career_industries'] as $name) {
                $careerIndustry = new CoachCareerIndustry();
                $careerIndustry->setCoach($coach);
                $careerIndustry->setName($name);
                $coach->getCareerIndustries()->add($careerIndustry);
            }

            foreach (self::DATA['coached_people'] as $name) {
                $coachedPeople = new CoachCoachedPeople();
                $coachedPeople->setCoach($coach);
                $coachedPeople->setName($name);
                $coach->getCoachedPeople()->add($coachedPeople);
            }

            foreach (self::DATA['coached_companies'] as $name) {
                $coachedCompany = new CoachCoachedCompany();
                $coachedCompany->setCoach($coach);
                $coachedCompany->setName($name);
                $coach->getCoachedCompanies()->add($coachedCompany);
            }

            $coach->setCoachHoursInYear(40);

            foreach ($industryList as $industry) {
                $coach->getIndustries()->add($industry);
            }

            foreach ($expertiseList as $expertise) {
                $coachExpertise = new CoachExpertise();
                $coachExpertise->setCoach($coach);
                $coachExpertise->setExpertise($expertise);
                $coachExpertise->setLevel(self::DATA['expertise'][$expertise->getName()]);
                $coach->getExpertise()->add($coachExpertise);
            }

            $coach->setAbout('N/A');
            $coach->setTestimonial('N/A');

            $manager->persist($coach);
        }
        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            UserFixtures::class,
            IndustryFixtures::class,
            ExpertiseFixtures::class,
        ];
    }
}
