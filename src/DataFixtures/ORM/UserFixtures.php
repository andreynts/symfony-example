<?php

namespace App\DataFixtures\ORM;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class UserFixtures extends Fixture
{
    const DATA = [[
        'username' => 'support@brandwidth.com',
        'password' => 'Whatever1',
        'roles' => ['ROLE_SUPER'],
        'first_name' => 'Alex',
        'last_name' => 'Lokhman II',
    ], [
        'username' => 'system@thrivepartners.co.uk',
        'password' => 'Whatever1',
        'roles' => ['ROLE_ADMIN'],
        'first_name' => 'Wendy',
        'last_name' => 'Robertson',
    ]];

    public function load(ObjectManager $manager)
    {
        $encoder = $this->container->get('security.password_encoder');

        foreach (self::DATA as $data) {
            $user = new User();
            $user->setUsername($data['username']);
            $user->setPassword($encoder->encodePassword($user, $data['password']));
            $user->setRoles($data['roles']);
            $user->setFirstName($data['first_name']);
            $user->setLastName($data['last_name']);
            $user->setVerified(true);
            $manager->persist($user);
        }
        $manager->flush();
    }
}
