<?php

namespace App\DataFixtures\ORM;

use App\Entity\Client;
use App\Entity\Company;
use App\Entity\CompanyDepartment;
use App\Entity\CompanyRegion;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class ClientFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $encoder = $this->container->get('security.password_encoder');

        $company = $this->getDefaultCompany();
        $manager->persist($company);

        foreach (range(1, 50) as $i) {
            $user = new User();
            $user->setUsername(sprintf('client%d@thrivepartners.co.uk', $i));
            $user->setPassword($encoder->encodePassword($user, 'Thriving!23'));
            $user->setRoles(['ROLE_ENTERPRISE_CLIENT']);
            $user->setFirstName('Training');
            $user->setLastName('Client '.$i);
            $user->setVerified(true);
            $manager->persist($user);

            $client = new Client();
            $client->setUser($user);
            $client->setCompany($company);
            $client->setMobileNumber('+447700000000');
            $client->setLanguages(['en_GB']);
            $client->setAgeRange('26-30');
            $client->setGender('Female');
            $client->setCountry('GB');
            $client->setNationality('British');
            $client->setCompanyName($company->getName());
            $client->setDepartment($company->getDepartments()->get(0)->getName());
            $client->setPosition('Client');
            $client->setRegion($company->getRegions()->get(0)->getName());
            $client->setWorkDuration('2 years');
            $client->setAttitude('I am on my way to the top and I am on the lookout for opportunities to get me there.');
            $client->setSatisfiedCompany(5);
            $client->setRecommendCompany(5);
            $client->setFutureAtCompany(5);
            $client->setFutureTeam(5);
            $client->setFutureCompany(5);
            $client->setFutureIndustry(5);
            $manager->persist($client);
        }
        $manager->flush();
    }

    public function getDependencies()
    {
        return [UserFixtures::class];
    }

    private function getDefaultCompany(): Company
    {
        $company = new Company();
        $company->setName('Thrive Training');
        $company->setAddress1('136 High Holborn');
        $company->setCity('London');
        $company->setCounty('Greater London');
        $company->setPostcode('WC1V 6PX');
        $company->setCountry('GB');
        $company->setRegNumber('09831900');
        $company->setTokRecord(true);

        $region = new CompanyRegion();
        $region->setCompany($company);
        $region->setName('Europe');
        $company->getRegions()->add($region);

        $department = new CompanyDepartment();
        $department->setCompany($company);
        $department->setName('Development');
        $company->getDepartments()->add($department);

        return $company;
    }
}
